/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of Qt Simulator
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include "exampleapp.h"

#include <QApplication>
#include <QDebug>
#include <QtGui/QLineEdit>
#include <QtGui/QHBoxLayout>
#include <QtNetwork/QHostAddress>
#include <QtSimulator/connection.h>
#include <QtSimulator/version.h>
#include <QtSimulator/connectionworker.h>

using namespace Simulator;

//! [2]
ExampleApp::ExampleApp(QWidget *parent)
    : QWidget(parent)
    , mConnection(new Connection(Connection::Client, "example", Version(1,0,0,0)))
{
    mWorker = mConnection->connectToServer();
    mWorker->setParent(this);
    mWorker->addReceiver(this);

    QHBoxLayout *layout = new QHBoxLayout(this);
    mLineEdit = new QLineEdit(this);
    layout->addWidget(mLineEdit);
    setLayout(layout);
    connect(mLineEdit, SIGNAL(textEdited ( const QString &)), this, SLOT(textEdited(const QString &)));
}
//! [2]

//! [0]
void ExampleApp::showText(const QString &text)
{
  mLineEdit->setText(text);
  qWarning() << "showText:" << text;
}
//! [0]

//! [1]
void ExampleApp::textEdited(const QString &text)
{
      mWorker->call("showText", text);
}
//! [1]

int main(int argc, char **argv)
{
  QApplication app(argc, argv);
  ExampleApp ex;
  ex.showFullScreen();
  return app.exec();
}

