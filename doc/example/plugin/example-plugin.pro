isEmpty(SIMULATOR_BUILD_PATH): SIMULATOR_BUILD_PATH = $$(SIMULATOR_BUILD_PATH)
isEmpty(SIMULATOR_DEPENDENCY_PATH): SIMULATOR_DEPENDENCY_PATH = $$(SIMULATOR_DEPENDENCY_PATH)

isEmpty(SIMULATOR_BUILD_PATH): error(Please call qmake with SIMULATOR_BUILD_PATH=<path to Qt Simulator build directory>)
isEmpty(SIMULATOR_DEPENDENCY_PATH): error(Please call qmake with SIMULATOR_DEPENDENCY_PATH=<simulator-dependency-path>)

SOURCES     += example-plugin.cpp examplepage.cpp
HEADERS     += example-plugin.h examplepage.h
QT          += network

TEMPLATE     = lib
CONFIG      += plugin
INCLUDEPATH += $${SIMULATOR_DEPENDENCY_PATH}/include/
TARGET       = $$qtLibraryTarget(example_plugin)
DESTDIR      = $${SIMULATOR_BUILD_PATH}/plugins/simulator
LIBS        += -L$${SIMULATOR_DEPENDENCY_PATH}/lib -lsimulatorplugin -lremotecontrolwidget
