/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of Qt Simulator
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include "examplepage.h"
#include <remotecontrolwidget/optionsitem.h>
#include <QtGui/QLineEdit>
#include <QtGui/QIcon>
#include <QtCore/QSettings>
#include "simulatorplugin/connection.h"
#include "simulatorplugin/version.h"
#include "simulatorplugin/connectionworker.h"

using namespace Simulator;

//! [0]
ExamplePage::ExamplePage(QWidget *parent)
    : ToolBoxPage(parent)
    , mConnection(0)
{
    QList<OptionsItem *> optionsList;
    target = new QLineEdit(this);
    optionsList << new OptionsItem(tr("Example control"), target);
    setOptions(optionsList);

    setTitle(tr("Example Page"));

    mConnection = new Connection(Connection::Server, "example", Version(1,0,0,0), this);

    connect(target, SIGNAL(textEdited ( const QString &)), this, SLOT(textEdited(const QString &)));
    connect(mConnection, SIGNAL(newConnection(Simulator::ConnectionWorker *)), this, SLOT(newConnection(Simulator::ConnectionWorker *)));
}
//! [0]

ExamplePage::~ExamplePage()
{
}

//! [1]
void ExamplePage::textEdited(const QString &text)
{
    foreach (ConnectionWorker *worker, mWorkers)
        worker->call("showText", text);
}
//! [1]

void ExamplePage::showText(const QString &text)
{
    target->setText(text);
}

// These functions can be used to read/write persistent settings. If you overwrite these as shown here
// you have to make sure the base class function if called first.
// If you do not need to save anything just remove these functions.
//! [2]
void ExamplePage::writeSettings(QSettings &settings) const
{
    ToolBoxPage::writeSettings(settings);

    settings.beginGroup(title());
    settings.setValue("myValue", 5);
    settings.endGroup();
}

void ExamplePage::readSettings(QSettings &settings)
{
    ToolBoxPage::readSettings(settings);

    settings.beginGroup(title());
    if (settings.contains("myValue"))
        int myValue = settings.value("myValue").toInt();
    settings.endGroup();
}
//! [2]

//! [3]
void ExamplePage::newConnection(ConnectionWorker *worker)
{
    connect(worker, SIGNAL(disconnected()), this, SLOT(disconnected()));
    mWorkers.append(worker);
    worker->addReceiver(this);
}

void ExamplePage::disconnected()
{
    ConnectionWorker *worker = qobject_cast<ConnectionWorker *>(sender());
    worker->deleteLater();
    mWorkers.removeAll(worker);
}
//! [3]

QIcon ExamplePage::icon() const
{
    return QIcon();
}
