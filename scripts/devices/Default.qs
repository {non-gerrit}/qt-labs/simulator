sysinfo.generic.inputMethod = sysinfo.generic.SingleTouch | sysinfo.generic.Keyboard | sysinfo.generic.Keys
sysinfo.generic.setFeature(sysinfo.generic.BluetoothFeature, true)
sysinfo.generic.setFeature(sysinfo.generic.CameraFeature, true)
sysinfo.generic.setFeature(sysinfo.generic.FmradioFeature, false)
sysinfo.generic.setFeature(sysinfo.generic.IrFeature, false)
sysinfo.generic.setFeature(sysinfo.generic.LedFeature, false)
sysinfo.generic.setFeature(sysinfo.generic.MemcardFeature, true)
sysinfo.generic.setFeature(sysinfo.generic.UsbFeature, false)
sysinfo.generic.setFeature(sysinfo.generic.VibFeature, true)
sysinfo.generic.setFeature(sysinfo.generic.WlanFeature, true)
sysinfo.generic.setFeature(sysinfo.generic.SimFeature, true)
sysinfo.generic.setFeature(sysinfo.generic.LocationnFeature, true)
sysinfo.generic.setFeature(sysinfo.generic.VideoOutFeature, true)
sysinfo.generic.setFeature(sysinfo.generic.HapticsFeature, true)
sysinfo.generic.manufacturer = "Simulator Manufacturer"
sysinfo.generic.model = "Simulator Model"
sysinfo.generic.productName = "Simulator Product Name"

