// show some functionality of the 'camera' object

var cams = camera.availableCameras()
for (var i = 0; i < cams.length; ++i) {
    print("Camera: " + i + " has name " + cams[i] + " and image path " + camera.imagePath(cams[i]))
}

camera.addCamera("My Cam", "This is a test camera", "no image")
