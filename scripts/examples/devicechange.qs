// This script shows how you can change the currently
// displayed skin
// Interesting hooks:
//    simulator.deviceCount()
//    simulator.setDevice(int)
//    simulator.setDevice(string)
//    simulator.currentDeviceName()
//    simulator.currentDeviceIndex()

var devCount = simulator.deviceCount();
var originalName = simulator.currentDeviceName();
messageBox("The device has " + devCount + " different skins");

var current = 0;
for (current = 0; current < devCount; ++current) {
    simulator.setDevice(current);
    messageBox("Switched to device: " + 
                simulator.currentDeviceName() + 
               " Index: " + simulator.currentDeviceIndex());
}

simulator.setDevice(originalName);
