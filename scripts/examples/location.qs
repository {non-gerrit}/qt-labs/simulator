// This script changes the current location every few seconds
// Interesting parts:
// location.latitude
// location.longitude
// location.altitude
// Check the documentation for a full list.

var i = 0;
while (i < 10)
{
    location.latitude += 0.0001;
    location.longitude -= 0.0001;
    location.altitude += 0.000001;
    location.useCurrentTimestamp = true;

    print ("Updated position:" + i);
    i = i + 1;

    yield(2000);
}
