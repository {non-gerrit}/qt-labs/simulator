var supported = simulator.supportedScreenOrientations()
print("supported screen orientations: " + supported)

for (var i in supported) {
    var orientation = supported[i]
    if (orientation != simulator.deviceOrientation()) {
        simulator.setDeviceOrientation(orientation, true)
        break
    }
}
