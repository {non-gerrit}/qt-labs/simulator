// use all functionality of the sensors object

sensors.ambientLightLevel = sensors.Dark
print(sensors.ambientLightLevel)

sensors.accelerometerX = 1
sensors.accelerometerY = 2
sensors.accelerometerZ = 3
print(sensors.accelerometerX + ' ' + sensors.accelerometerY + ' ' + sensors.accelerometerZ)

sensors.magnetometerX = 1
sensors.magnetometerY = 2
sensors.magnetometerZ = 3
sensors.magnetometerCalibrationLevel = 4
print(sensors.magnetometerX + ' ' + sensors.magnetometerY + ' ' + sensors.magnetometerZ + ' ' + sensors.magnetometerCalibrationLevel)

sensors.compassAzimuth = 42
sensors.compassCalibrationLevel = 43
print(sensors.compassAzimuth + ' ' + sensors.compassCalibrationLevel)

sensors.proximitySensorClose = true
print(sensors.proximitySensorClose)

sensors.useCurrentTimestamp = true
sensors.useCurrentTimestamp = false
sensors.timestamp = new Date
