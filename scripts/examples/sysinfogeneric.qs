// this script aims to use all of sysinfo.generic

sysinfo.generic.currentLanguage = "bork"
sysinfo.generic.currentCountryCode = "Borkland"
sysinfo.generic.addAvailableLanguage("talk")
sysinfo.generic.setFeature(sysinfo.generic.BluetoothFeature, true)
sysinfo.generic.setVersion(sysinfo.generic.QtCore, "4.7")
sysinfo.generic.displayBrightness = 12
sysinfo.generic.colorDepth = 24
sysinfo.generic.currentProfile = sysinfo.generic.OfflineProfile
sysinfo.generic.currentPowerState = sysinfo.generic.BatteryPower
sysinfo.generic.simStatus = sysinfo.generic.SimNotAvailable
sysinfo.generic.inputMethod = sysinfo.generic.Keys | sysinfo.generic.Keyboard
sysinfo.generic.imsi = "543210987654321"
sysinfo.generic.imei = "54-321098-765432-1"
sysinfo.generic.manufacturer = "Custom"
sysinfo.generic.model = "MyModel"
sysinfo.generic.productName = "NewProduct"
sysinfo.generic.batteryLevel = 79
sysinfo.generic.deviceLocked = true
