// set some network data

sysinfo.network.currentMobileCountryCode = "fi";
sysinfo.network.currentMobileNetworkCode = "abc"
sysinfo.network.homeMobileCountryCode = "de";
sysinfo.network.homeMobileNetworkCode = "def"
sysinfo.network.locationAreaCode = 1234
sysinfo.network.currentMode = sysinfo.network.GsmMode

sysinfo.network.cellId = 1;
sysinfo.network.setNetworkSignalStrength(sysinfo.network.GsmMode, 10);
yield(2000);
sysinfo.network.cellId = 2;
sysinfo.network.setNetworkSignalStrength(sysinfo.network.GsmMode, 50);
yield(2000);
sysinfo.network.cellId = 3;
sysinfo.network.setNetworkSignalStrength(sysinfo.network.GsmMode, 20);
yield(2000);
sysinfo.network.cellId = 4;
sysinfo.network.setNetworkSignalStrength(sysinfo.network.GsmMode, 100);
yield(2000)
