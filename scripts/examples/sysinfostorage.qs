// show some functionality of sysinfo.storage

var drives = sysinfo.storage.logicalDrives()
for (var i = 0; i < drives.length; ++i) {
    print ("Drive: " + i + " is called " + drives[i] + " and has " +
           sysinfo.storage.availableDiskSpace(drives[i]) + " bytes available")
}

sysinfo.storage.addDrive("My drive", sysinfo.storage.CdromDrive, 1000, 250)
sysinfo.storage.setAvailableSpace("My drive", 500)
