var inX = input.mouseX();
var midX = simulator.availableGeometryX() + simulator.availableGeometryWidth() / 2;
var startLeft
if (inX < midX)
    startLeft = true
else if (inX > midX)
    startLeft = false
else return

touch.beginTouch(0, inX, 100);
touch.beginTouch(1, inX, 110);
while (1) {
    if (startLeft) {
        if (inX > midX)
            break;
        inX += 10;
    } else {
        if (inX < midX)
            break
        inX -=10;
    }

    touch.updateTouch(0, inX, 100);
    touch.updateTouch(1, inX, 110);
    yield(1);
}
touch.endTouch(0, inX, 100);
touch.endTouch(1, inX, 110);
