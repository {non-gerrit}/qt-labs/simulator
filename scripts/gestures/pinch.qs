var minX = simulator.availableGeometryX();
var maxX = simulator.availableGeometryX() + simulator.availableGeometryWidth();
var initial1 = Math.max(minX, input.mouseX() - 100);
var initial2 = Math.min(maxX, input.mouseX() + 100);
var diff = initial2 - initial1

touch.beginTouch(0, initial1, 100);
touch.beginTouch(1, initial2, 100);
for (var i = 1; i < diff/4; i++) {
    if (initial1 != maxX)
        ++initial1
    if (initial2 != minX)
        --initial2
    touch.updateTouch(0, initial1, 100);
    touch.updateTouch(1, initial2, 100);
    yield(1);
}

touch.endTouch(0, Math.min(maxX, initial1), 100);
touch.endTouch(1, Math.min(minX, initial2), 100);
