var inX = input.mouseX();
var midX = simulator.availableGeometryX() + simulator.availableGeometryWidth() / 2;
var startLeft
if (inX < midX)
    startLeft = true
else if (inX > midX)
    startLeft = false
else return

touch.beginTouch(0, inX, 100);
touch.beginTouch(1, inX, 110);
touch.beginTouch(2, inX, 120);
while (1) {
    if (startLeft) {
        inX += 10;
        if (inX > midX)
            break;
    } else {
        inX -=10;
        if (inX <midX)
            break
    }

    touch.updateTouch(0, inX, 100);
    touch.updateTouch(1, inX, 110);
    touch.updateTouch(2, inX, 120);
    yield(1);
}
touch.endTouch(0, inX, 100);
touch.endTouch(1, inX, 110);
touch.endTouch(2, inX, 120);
