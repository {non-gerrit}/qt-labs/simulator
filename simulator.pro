TEMPLATE = app

# Grab paths from environment variables if unset
isEmpty(QT_NOKIA_SDK_PATH): QT_NOKIA_SDK_PATH = $$(QT_NOKIA_SDK_PATH)
isEmpty(QT_MOBILITY_SOURCE_PATH): QT_MOBILITY_SOURCE_PATH = $$(QT_MOBILITY_SOURCE_PATH)
isEmpty(QMF_INCLUDEDIR): QMF_INCLUDEDIR = $$(QMF_INCLUDEDIR)
isEmpty(SIMULATOR_DEPENDENCY_PATH): SIMULATOR_DEPENDENCY_PATH = $$(SIMULATOR_DEPENDENCY_PATH)

isEmpty(QT_NOKIA_SDK_PATH): error(Please call qmake with QT_NOKIA_SDK_PATH=<path to nokia-sdk source directory>)
isEmpty(QT_MOBILITY_SOURCE_PATH): error(Please call qmake with QT_MOBILITY_SOURCE_PATH=<path to mobility source directory>)
isEmpty(QMF_INCLUDEDIR): error(Please call qmake with QMF_INCLUDEDIR=<qmf-source/src/libraries/qmfclient>)
isEmpty(SIMULATOR_DEPENDENCY_PATH): error(Please call qmake with SIMULATOR_DEPENDENCY_PATH=<simulator-dependency-path>)

TARGET = simulator
DESTDIR= ./
QT += network opengl
CONFIG += mobility
MOBILITY += contacts versit organizer
contains(QT_CONFIG, phonon) {
    QT += phonon
}

macx {
    ICON = $$PWD/src/ui/icons/appicon.icns
}

include(src/src.pri)
include(doc/doc.pri)

# install rules
!isEmpty(PREFIX) {
    target.path = $$PREFIX

    macx: INSTALLSUBDIR=$${TARGET}.app/Contents/MacOS/

    scripts.files = scripts
    scripts.path = $$PREFIX/$$INSTALLSUBDIR

    stubdata.files = stubdata
    stubdata.path = $$PREFIX/$$INSTALLSUBDIR

    fonts.files = fonts/*.ttf
    fonts.path = $$PREFIX/$$INSTALLSUBDIR/fonts

    devices.files = models
    devices.path = $$PREFIX/$$INSTALLSUBDIR

    INSTALLS += target scripts stubdata fonts devices
}

RC_FILE = simulator.rc
