/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "contacts.h"

#include <contacts/qcontact.h>
#include <contacts/qcontactmanager.h>
#include <contacts/details/qcontactname.h>
#include <contacts/details/qcontactdisplaylabel.h>
#include <contacts/details/qcontactphonenumber.h>
#include <contacts/engines/qcontactmemorybackend_p.h>
#include <versit/qversitreader.h>
#include <versit/qversitwriter.h>
#include <versit/qversitcontactexporter.h>
#include <versit/qversitcontactimporter.h>

#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QFile>
#include <QtCore/QTextStream>

using namespace QtMobility;

Contacts::Contacts(QObject *parent)
    : QObject(parent)
{
    QMap<QString, QString> engineParams;
    engineParams.insert("id", "simulator");
    mManager = new QContactManager("memory", engineParams, this);

    connect(mManager, SIGNAL(contactsAdded(const QList<QContactLocalId> &)), SIGNAL(contactsAdded(const QList<QContactLocalId> &)));
    connect(mManager, SIGNAL(contactsChanged(const QList<QContactLocalId> &)), SIGNAL(contactsChanged(const QList<QContactLocalId> &)));
    connect(mManager, SIGNAL(contactsRemoved(const QList<QContactLocalId> &)), SIGNAL(contactsRemoved(const QList<QContactLocalId> &)));
    connect(mManager, SIGNAL(relationshipsAdded(QList<QContactLocalId>)), SIGNAL(relationshipsAdded(QList<QContactLocalId>)));
    connect(mManager, SIGNAL(relationshipsRemoved(QList<QContactLocalId>)), SIGNAL(relationshipsRemoved(QList<QContactLocalId>)));
}

QContactManager *Contacts::manager()
{
    return mManager;
}

void Contacts::setInitialData()
{
    importFromVCardFile("stubdata/standardselfcontact.vcf");
    if (mManager->contactIds().count() == 1) {
        mManager->setSelfContactId(mManager->contactIds().at(0));
    } else {
        qWarning("The file stubdata/standardselfcontact.vcf contains more than one contact. Not setting a self contact.");
    }

    importFromVCardFile("stubdata/standardcontacts.vcf");
}

void Contacts::publish() const
{
    emit contactsDataChanged(*mManager);
}

bool Contacts::importFromVCardFile(const QString &fileName)
{
    QFile contactsFile(fileName);
    contactsFile.open(QIODevice::ReadOnly);
    if (!contactsFile.isReadable())
        return false;

    QVersitReader reader;
    reader.setDevice(&contactsFile);
    if (!reader.startReading())
        return false;
    reader.waitForFinished();

    QVersitContactImporter importer;
    if (!importer.importDocuments(reader.results())) {
        qWarning() << "Could not import contacts from " << fileName;
        return false;
    }

    QList<QContact> contacts = importer.contacts();
    QMap<int, QContactManager::Error> errors;
    mManager->saveContacts(&contacts, &errors);

    return true;
}

bool Contacts::exportToVCardFile(const QString &fileName)
{
    QFile contactsFile(fileName);
    contactsFile.open(QIODevice::WriteOnly);
    if (!contactsFile.isWritable())
        return false;

    QVersitContactExporter exporter;
    if (!exporter.exportContacts(mManager->contacts())) {
        qWarning() << "Could not export contacts";
        return false;
    }

    QVersitWriter writer(&contactsFile);
    if (!writer.startWriting(exporter.documents()))
        return false;
    writer.waitForFinished();

    return true;
}
