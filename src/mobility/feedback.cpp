/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "feedback.h"

#include <QtCore/QDebug>
#include <QtCore/QTimer>

QTM_USE_NAMESPACE

Feedback::Feedback(QObject *parent)
    : QObject(parent)
    , mNextActuatorId(0)
    , mNextEffectId(0)
{
}

QList<ActuatorData> Feedback::actuators() const
{
    return mActuators.values();
}

int Feedback::defaultActuatorId() const
{
    return mDefaultActuator;
}

ActuatorData Feedback::actuator(int actuatorId) const
{
    if (!mActuators.contains(actuatorId)) {
        ActuatorData data;
        data.id = -1;
        return data;
    }
    return mActuators.value(actuatorId);
}

Feedback::EffectData Feedback::effectForActuator(int actuatorId) const
{
    foreach (const EffectData &data, mEffects) {
        if (data.actuatorId == actuatorId)
            return data;
    }

    EffectData data;
    data.actuatorId = -1;
    return data;
}

void Feedback::setInitialData()
{
    ActuatorData data;
    data.id = mNextActuatorId++;
    data.name = tr("Vibration Actuator");
    data.enabled = true;
    data.state = QFeedbackActuator::Ready;
    mActuators.insert(data.id, data);
    mDefaultActuator = data.id;
    emit actuatorAdded(data);

    data.id = mNextActuatorId++;
    data.name = tr("Audio Actuator");
    data.enabled = false;
    data.state = QFeedbackActuator::Ready;
    mActuators.insert(data.id, data);
    emit actuatorAdded(data);
}

int Feedback::startEffect(int actuatorId, const QString &info, int duration)
{
    if (!mActuators.contains(actuatorId))
        return -1;

    ActuatorData &actuator = mActuators[actuatorId];
    if (actuator.state != QFeedbackActuator::Ready)
        return -1;

    actuator.state = QFeedbackActuator::Busy;

    int effectId = mNextEffectId++;
    EffectData effect;
    effect.actuatorId = actuatorId;
    effect.info = info;
    effect.duration = duration;
    effect.state = QFeedbackEffect::Running;
    effect.runTime = 0;

    QTimer *timer = new QTimer(this);
    effect.timer = timer;
    mTimerToEffectId.insert(timer, effectId);
    connect(timer, SIGNAL(timeout()), this, SLOT(doneWithEffect()));
    timer->setSingleShot(true);
    if (duration != QFeedbackEffect::Infinite)
        timer->start(duration);

    effect.lastStartTime.start();
    mEffects.insert(effectId, effect);

    emit actuatorChanged(actuator);
    emit effectStateChanged(effectId, effect.state);

    return effectId;
}

bool Feedback::resumeEffect(int effectId)
{
    if (!mEffects.contains(effectId))
        return false;
    EffectData &effect = mEffects[effectId];

    effect.state = QFeedbackEffect::Running;
    effect.lastStartTime.start();
    if (effect.duration != QFeedbackEffect::Infinite && effect.timer) {
        const int remaining = qMax((qint64)0, effect.duration - effect.runTime);
        effect.timer->start(remaining);
    }

    emit effectStateChanged(effectId, effect.state);

    return true;
}

bool Feedback::pauseEffect(int effectId)
{
    if (!mEffects.contains(effectId))
        return false;
    EffectData &effect = mEffects[effectId];

    if (effect.state == QFeedbackEffect::Running) {
        effect.runTime += effect.lastStartTime.elapsed();
        effect.timer->stop();
    }
    effect.state = QFeedbackEffect::Paused;

    emit effectStateChanged(effectId, effect.state);

    return true;
}

bool Feedback::stopEffect(int effectId)
{
    if (!mEffects.contains(effectId))
        return false;
    EffectData &effect = mEffects[effectId];

    ActuatorData &actuator = mActuators[effect.actuatorId];
    actuator.state = QFeedbackActuator::Ready;

    if (effect.timer) {
        effect.timer->stop();
        mTimerToEffectId.remove(effect.timer);
        delete effect.timer;
    }
    mEffects.remove(effectId);

    emit effectStateChanged(effectId, QFeedbackEffect::Stopped);
    emit actuatorChanged(actuator);

    return true;
}

void Feedback::setEffectDuration(int effectId, int duration)
{
    if (!mEffects.contains(effectId))
        return;
    EffectData &effect = mEffects[effectId];

    effect.duration = duration;

    if (effect.timer) {
        effect.timer->stop();

        if (effect.state == QFeedbackEffect::Running
                && effect.duration != QFeedbackEffect::Infinite) {
            const int remaining = qMax((qint64)0, effect.duration - effect.runTime - effect.lastStartTime.elapsed());
            effect.timer->start(remaining);
        }
    }

    // ### emit!
}

void Feedback::setActuatorEnabled(int actuatorId, bool enabled)
{
    if (!mActuators.contains(actuatorId))
        return;
    ActuatorData &actuator = mActuators[actuatorId];
    actuator.enabled = enabled;

    emit actuatorChanged(actuator);
}

void Feedback::setDefaultActuatorId(int actuatorId)
{
    if (actuatorId == mDefaultActuator)
        return;
    mDefaultActuator = actuatorId;
    emit defaultActuatorIdChanged(actuatorId);
}

void Feedback::setActuatorData(const ActuatorData &data)
{
    if (!mActuators.contains(data.id))
        return;
    const ActuatorData &old = mActuators.value(data.id);
    if (old.name == data.name
            && old.enabled == data.enabled
            && old.state == data.state) {
        return;
    }

    // setting the state to ready kills a running effect
    const EffectData &runningEffect = effectForActuator(data.id);
    if (data.state == QFeedbackActuator::Ready && runningEffect.actuatorId == data.id) {
        stopEffect(mTimerToEffectId.value(runningEffect.timer));
        return;
    }

    // else just set it
    mActuators.insert(data.id, data);
    emit actuatorChanged(data);
}

void Feedback::setEffectState(int actuatorId, QFeedbackEffect::State state)
{
    const EffectData &activeEffect = effectForActuator(actuatorId);
    if (activeEffect.actuatorId != actuatorId)
        return;

    if (state == activeEffect.state)
        return;

    const int effectId = mTimerToEffectId.value(activeEffect.timer);
    if (state == QFeedbackEffect::Stopped) {
        stopEffect(effectId);
    } else if (state == QFeedbackEffect::Running) {
        resumeEffect(effectId);
    } else if (state == QFeedbackEffect::Paused) {
        pauseEffect(effectId);
    } else if (state == QFeedbackEffect::Loading) {
        EffectData &mutableEffect = mEffects[effectId];
        if (mutableEffect.state == QFeedbackEffect::Running) {
            mutableEffect.runTime += mutableEffect.lastStartTime.elapsed();
            mutableEffect.timer->stop();
        }
        mutableEffect.state = state;
        emit effectStateChanged(effectId, state);
    }
}

void Feedback::addActuator(const QtMobility::ActuatorData &data)
{
    QtMobility::ActuatorData actuator = data;
    actuator.id = mNextActuatorId++;
    mActuators.insert(actuator.id, actuator);

    emit actuatorAdded(actuator);

    if (mDefaultActuator == -1 && mActuators.size() == 1) {
        mDefaultActuator = actuator.id;
        emit defaultActuatorIdChanged(mDefaultActuator);
    }
}

void Feedback::removeActuator(int actuatorId)
{
    if (!mActuators.contains(actuatorId))
        return;

    foreach (const EffectData &effect, mEffects) {
        if (effect.actuatorId == actuatorId)
            stopEffect(mTimerToEffectId.value(effect.timer));
    }

    if (mDefaultActuator == actuatorId) {
        mDefaultActuator = -1;
        emit defaultActuatorIdChanged(mDefaultActuator);
    }

    mActuators.remove(actuatorId);
    emit actuatorRemoved(actuatorId);
}

void Feedback::doneWithEffect()
{
    QTimer *timer = qobject_cast<QTimer *>(sender());
    Q_ASSERT(timer && mTimerToEffectId.contains(timer));
    if (timer)
        timer->deleteLater();

    const int effectId = mTimerToEffectId.value(timer, -1);
    if (effectId == -1)
        return;

    mTimerToEffectId.remove(timer);

    if (!mEffects.contains(effectId))
        return;

    const int actuatorId = mEffects[effectId].actuatorId;
    ActuatorData &actuator = mActuators[actuatorId];

    mEffects.remove(effectId);
    actuator.state = QFeedbackActuator::Ready;

    emit effectStateChanged(effectId, QFeedbackEffect::Stopped);
    emit actuatorChanged(actuator);
}
