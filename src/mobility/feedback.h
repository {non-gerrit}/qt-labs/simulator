/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef FEEDBACK_H
#define FEEDBACK_H

#include "qfeedbackdata_simulator_p.h"
#include "qfeedbackeffect.h"
#include <QObject>
#include <QHash>
#include <QElapsedTimer>

QT_FORWARD_DECLARE_CLASS(QTimer)

class Feedback : public QObject
{
    Q_OBJECT
public:
    class EffectData {
    public:
        int actuatorId;
        QString info;
        int duration;
        QtMobility::QFeedbackEffect::State state;
        QTimer *timer;
        QElapsedTimer lastStartTime;
        qint64 runTime;
    };

    explicit Feedback(QObject *parent = 0);

    QList<QtMobility::ActuatorData> actuators() const;
    int defaultActuatorId() const;
    QtMobility::ActuatorData actuator(int actuatorId) const;
    EffectData effectForActuator(int actuatorId) const;

    int startEffect(int actuatorId, const QString &info, int duration);
    bool resumeEffect(int effectId);
    bool pauseEffect(int effectId);
    bool stopEffect(int effectId);
    void setEffectDuration(int effectId, int duration);
    void setActuatorEnabled(int actuatorId, bool enabled);

    void setDefaultActuatorId(int actuatorId);
    void setActuatorData(const QtMobility::ActuatorData &data);
    void setEffectState(int actuatorId, QtMobility::QFeedbackEffect::State state);

    void addActuator(const QtMobility::ActuatorData &data);
    void removeActuator(int actuatorId);

public slots:
    void setInitialData();

signals:
    void actuatorAdded(QtMobility::ActuatorData data);
    void actuatorChanged(QtMobility::ActuatorData data);
    void actuatorRemoved(int actuatorId);
    void defaultActuatorIdChanged(int actuatorId);
    void effectStateChanged(int effectId, QtMobility::QFeedbackEffect::State state);

private slots:
    void doneWithEffect();

private:
    int mDefaultActuator;
    QHash<int, QtMobility::ActuatorData> mActuators;
    QHash<int, EffectData> mEffects;
    QHash<QTimer *, int> mTimerToEffectId;
    int mNextActuatorId;
    int mNextEffectId;
};

#endif // FEEDBACK_H
