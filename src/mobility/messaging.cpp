/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "messaging.h"

#include <qmailstore.h>
#include <qmailfolder.h>
#include <qcopserver.h>
#include <qsimulatordata_p.h>

#include <QtCore/QDir>
#include <QtCore/QThread>

class MaildirImporter : public QThread
{
public:
    MaildirImporter(const QString &path, QObject *parent = 0)
        : QThread(parent), mPath(path)
    {
    }

protected:
    virtual void run()
    {
        QDir dir(mPath);
        if (!dir.exists())
            return;

        QMailStore *store = QMailStore::instance();
        QMailAccountIdList accountIdList = store->queryAccounts(QMailAccountKey::messageType(QMailMessageMetaDataFwd::Email, QMailDataComparator::Includes));
        if (accountIdList.isEmpty())
            return;
        QMailFolderId folderId = store->account(accountIdList.first()).standardFolder(QMailFolder::InboxFolder);
        if (!folderId.isValid())
            return;

        foreach (const QFileInfo &fileInfo, dir.entryInfoList(QDir::Files)) {
            QFile file(fileInfo.absoluteFilePath());
            file.open(QIODevice::ReadOnly);
            QByteArray data = file.readAll();
            file.close();

            // RFC 2822 requires CRLF line endings
            for (int i = 0; i < data.size(); ++i) {
                if (data.at(i) == '\n' && i > 0 && data.at(i-1) != '\r') {
                    data.insert(i, '\r');
                    ++i;
                }
            }

            QMailMessage message(QMailMessage::fromRfc2822(data));
            if (message.headerFields().isEmpty())
                continue;

            QMailMessage *storeMessage = new QMailMessage(message);
            storeMessage->setMessageType(QMailMessage::Email);
            storeMessage->setParentAccountId(accountIdList.first());
            storeMessage->setParentFolderId(folderId);
            storeMessage->setStatus(QMailMessage::Incoming | QMailMessage::New);
            QMailStore::instance()->addMessage(storeMessage);
        }
    }

private:
    QString mPath;
};

Messaging::Messaging(QObject *parent) :
    QObject(parent)
{
    new QCopServer(this);
}

void Messaging::setInitialData()
{
    // have to use the same on the client
    QtSimulatorPrivate::qt_setQmfPaths();

    QMailStore *mailstore = QMailStore::instance();

    // remove existing data
    mailstore->removeAccounts(QMailAccountKey());
    mailstore->removeFolders(QMailFolderKey());
    mailstore->removeMessages(QMailMessageKey());

    // add SMS account
    QMailAccount *smsAccount = new QMailAccount();
    smsAccount->setName("SMS");
    smsAccount->setMessageType(QMailMessageMetaDataFwd::Sms);
    smsAccount->setStatus(QMailAccount::UserEditable
                          | QMailAccount::UserRemovable
                          | QMailAccount::CanRetrieve
                          | QMailAccount::CanTransmit
                          | QMailAccount::Enabled
                          | QMailAccount::CanCreateFolders);
    mailstore->addAccount(smsAccount, 0);

    // add standard folders
    addDefaultFolders(smsAccount);

    // add MMS account
    QMailAccount *mmsAccount = new QMailAccount();
    mmsAccount->setName("MMS");
    mmsAccount->setMessageType(static_cast<QMailMessageMetaDataFwd::MessageType>(
            QMailMessageMetaDataFwd::Mms | QMailMessageMetaDataFwd::Email));
    mmsAccount->setStatus(QMailAccount::UserEditable
                          | QMailAccount::UserRemovable
                          | QMailAccount::CanRetrieve
                          | QMailAccount::CanTransmit
                          | QMailAccount::Enabled
                          | QMailAccount::CanCreateFolders);
    mailstore->addAccount(mmsAccount, 0);

    // add standard folders
    addDefaultFolders(mmsAccount);

    importMaildir("stubdata/standardmessages");
}

void Messaging::addDefaultFolders(QMailAccount *account)
{
    QMailStore *mailstore = QMailStore::instance();
    QMailFolder *folder = 0;

    folder = new QMailFolder("Inbox", QMailFolderId(), account->id());
    mailstore->addFolder(folder);
    account->setStandardFolder(QMailFolder::InboxFolder, folder->id());

    folder = new QMailFolder("Drafts", QMailFolderId(), account->id());
    mailstore->addFolder(folder);
    account->setStandardFolder(QMailFolder::DraftsFolder, folder->id());

    folder = new QMailFolder("Outbox", QMailFolderId(), account->id());
    mailstore->addFolder(folder);
    // QMF does not allow setting this
    //account->setStandardFolder(QMailFolder::OutboxFolder, folder->id());

    folder = new QMailFolder("Sent", QMailFolderId(), account->id());
    mailstore->addFolder(folder);
    account->setStandardFolder(QMailFolder::SentFolder, folder->id());

    folder = new QMailFolder("Trash", QMailFolderId(), account->id());
    mailstore->addFolder(folder);
    account->setStandardFolder(QMailFolder::TrashFolder, folder->id());

    mailstore->updateAccount(account);
}

QMailFolderId Messaging::inboxFolderId(QMailMessageMetaDataFwd::MessageType type)
{
    QMailStore *store = QMailStore::instance();
    QMailAccountIdList accountIdList = store->queryAccounts(QMailAccountKey::messageType(type, QMailDataComparator::Includes));
    if (accountIdList.isEmpty())
        return QMailFolderId();
    QMailAccount account(accountIdList.first());
    return account.standardFolder(QMailFolder::InboxFolder);
}

void Messaging::addMessage(QMailFolderId folderId, QMailMessage *message)
{
    QMailStore *store = QMailStore::instance();
    message->setParentAccountId(QMailFolder(folderId).parentAccountId());
    message->setParentFolderId(folderId);
    store->addMessage(message);
}

void Messaging::importMaildir(const QString &path)
{
    // remove done threads
    QMutableListIterator<MaildirImporter *> it(mImporterThreads);
    while (it.hasNext()) {
        MaildirImporter *importer = it.next();
        if (importer->isFinished()) {
            delete importer;
            it.remove();
        }
    }

    // start new importer thread
    MaildirImporter *maildirImporter = new MaildirImporter(path, this);
    maildirImporter->start();
    mImporterThreads += maildirImporter;
}
