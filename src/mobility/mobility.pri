INCLUDEPATH += src/mobility
DEPENDPATH += src/mobility
SOURCES += \
    mobilitymanager.cpp \
    contacts.cpp \
    mobilitydata.cpp \
    messaging.cpp \
    organizer.cpp \
    organizertreemodel.cpp \
    feedback.cpp
HEADERS += \
    mobilitymanager.h \
    contacts.h \
    mobilitydata.h \
    messaging.h \
    organizer.h \
    organizertreemodel.h \
    feedback.h
