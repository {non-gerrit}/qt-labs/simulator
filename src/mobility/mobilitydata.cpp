/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "mobilitydata.h"

#include <contacts/qcontactmanager.h>
#include <contacts/qcontact.h>
#include <contacts/details/qcontactemailaddress.h>
#include <contacts/details/qcontactphonenumber.h>
#include <qmailmessage.h>

#include <qmath.h>

using namespace QtMobility;

MobilityData::MobilityData(QObject *parent)
    : QObject(parent)
    , mContacts(new Contacts(this))
    , mOrganizer(new Organizer(this))
    , mFeedback(new Feedback(this))
    , mLocationUi(0)
    , mSystemInfoGenericUi(0)
    , mSystemInfoNetworkUi(0)
    , mSystemInfoStorageUi(0)
    , mMessaging(new Messaging(this))
    , mSensorsUi(0)
    , mCameraUi(0)
    , mNfcUi(0)
    , mInitialized(false)
{
}

void MobilityData::setInitialData()
{
    setInitialLocationData();
    mContacts->setInitialData();
    mOrganizer->setInitialData();
    mFeedback->setInitialData();
    setInitialSystemInfoGenericData();
    setInitialSystemInfoNetworkData();
    setInitialSystemInfoStorageData();
    mMessaging->setInitialData();
    setInitialSensorsData();
    setInitialCameraData();
}

void MobilityData::setInitialSensorsData()
{
    SensorsUi::SensorsData sensors;
    sensors.ambientLightLevel = SensorsScriptInterface::Light;
    sensors.lux = 100.0;

    sensors.accelerometerX = 0;
    sensors.accelerometerY = 9.8;
    sensors.accelerometerZ = 0;

    sensors.magnetometerX = 0;
    sensors.magnetometerY = 0;
    sensors.magnetometerZ = 30;
    sensors.magnetometerCalibrationLevel = 0.85;

    sensors.compassCalibrationLevel = 0.85;
    sensors.compassAzimuth = 0;

    sensors.proximitySensorClose = true;

    sensors.useCurrentTime = true;

    mSensorsUi->setSensorsData(sensors);
    mInitialized = true;
}

void MobilityData::setInitialCameraData()
{
    CameraScriptInterface *si = mCameraUi->scriptInterface();
    si->addCamera(tr("Front"), tr("Front Camera"), tr("Front"));
    si->addCamera(tr("Back"), tr("Back Camera"), tr("Back"));
}

void MobilityData::setInitialLocationData()
{
    LocationUi::LocationData data;
    data.latitude = 52.5056819;
    data.longitude = 13.3232027;
    data.altitude = 4.897392;
    data.direction = 0;
    data.groundSpeed = 0;
    data.verticalSpeed = 0;
    data.magneticVariation = 0;
    data.horizontalAccuracy = 0;
    data.verticalAccuracy = 0;
    data.useCurrentTime = true;
    mLocationUi->setLocation(data);

    LocationUi::SatelliteData sData;
    LocationUi::SatelliteData::SatelliteInfo si;
    si.prn = 2;
    si.elevation = 22;
    si.azimuth = 239;
    si.signalStrength = 6;
    si.inUse = true;
    sData.satellites.append(si);
    si.prn = 5;
    si.elevation = 47;
    si.azimuth = 297;
    si.signalStrength = 6;
    si.inUse = false;
    sData.satellites.append(si);
    si.prn = 7;
    si.elevation = 72;
    si.azimuth = 87;
    si.signalStrength = 6;
    si.inUse = false;
    sData.satellites.append(si);
    mLocationUi->setSatelliteData(sData);
}

void MobilityData::setInitialSystemInfoGenericData()
{
    GenericSystemInfoUi::GenericData data;
    data.currentLanguage = "en";
    data.currentCountryCode = "EN";
    data.availableLanguages.append("en");
    data.availableLanguages.append("de");
    data.features[GenericSystemInfoScriptInterface::LocationFeature] = true;
    data.features[GenericSystemInfoScriptInterface::UsbFeature] =  true;
    data.versions[GenericSystemInfoScriptInterface::QtCore] = "4.6 probably";
    data.versions[GenericSystemInfoScriptInterface::Firmware] = "1.9-alpha-rc7";
    data.displayBrightness = 100;
    data.colorDepth = 32;
    data.profile = GenericSystemInfoScriptInterface::NormalProfile;
    data.currentPowerState =  GenericSystemInfoScriptInterface::WallPower;
    data.simStatus = GenericSystemInfoScriptInterface::SimNotAvailable;
    data.inputFlags = static_cast<GenericSystemInfoScriptInterface::InputMethod>
        (GenericSystemInfoScriptInterface::Keyboard | GenericSystemInfoScriptInterface::Mouse);
    data.imei = "12-345678-901234-5";
    data.imsi = "12345679012345";
    data.manufacturer = "simulator manufacturer";
    data.model = "simulator model";
    data.productName = "simulator product name";
    data.batteryLevel = 84;
    data.deviceLocked = false;
    mSystemInfoGenericUi->setGenericData(data);
}

void MobilityData::setInitialSystemInfoNetworkData()
{
    NetworkSystemInfoUi::NetworkData data;
    data.cellId = 12345;
    data.locationAreaCode = 54321;
    data.currentMobileCountryCode = QLatin1String("242");
    data.currentMobileNetworkCode = QLatin1String("123456789");
    data.homeMobileCountryCode = QLatin1String("+47");
    data.homeMobileNetworkCode = QLatin1String("987654321");
    data.currentMode = NetworkSystemInfoScriptInterface::EthernetMode;

    for (int i = 0; i < data.networkInfo.size(); ++i) {
        NetworkSystemInfoUi::NetworkData::ModeInfo &mode = data.networkInfo[i];

        mode.name = QLatin1String("name");
        mode.macAddress = QLatin1String("ff:ff:ff:ff:ff:ff");
        mode.signalStrength = 75;
        mode.status = NetworkSystemInfoScriptInterface::UndefinedStatus;
    }
    mSystemInfoNetworkUi->setNetworkData(data);
}

void MobilityData::setInitialSystemInfoStorageData()
{
    StorageSystemInfoScriptInterface *si = mSystemInfoStorageUi->scriptInterface();
    si->addDrive("Internal Drive", StorageSystemInfoScriptInterface::InternalDrive,
                 256*1024*qint64(1024), 32*1024*qint64(1024));
    si->addDrive("Removable Drive", StorageSystemInfoScriptInterface::RemovableDrive,
                 4*1024*1024*qint64(1024), 3*1024*1024*qint64(1024));
}

static int randomInt(int begin, int end)
{
    return begin + floor((qreal)qrand() / RAND_MAX * (end - begin));
}

void MobilityData::addNewEmail()
{
    // ### A script should do this...
    QContactManager *manager = mContacts->manager();

    // find sender
    QList<QContact> contacts = manager->contacts();
    QContact sender = contacts.at(randomInt(0, contacts.size()));

    // find target
    QContact receiver = manager->contact(manager->selfContactId());

    // make message
    QMailMessage *message = new QMailMessage();
    message->setMessageType(QMailMessage::Email);
    message->setSubject(tr("Generated message"));
    message->setBody(
            QMailMessageBody::fromData(tr("This is a simulator-generated email message."),
                                       QMailMessageContentType("text/plain"),
                                       QMailMessageBodyFwd::NoEncoding));
    message->setFrom(QMailAddress(sender.displayLabel(),
                                  sender.detail<QContactEmailAddress>().emailAddress()));
    message->setTo(QMailAddress(receiver.displayLabel(),
                                receiver.detail<QContactEmailAddress>().emailAddress()));
    message->setStatus(QMailMessage::Incoming | QMailMessage::New);

    QMailFolderId inboxId = mMessaging->inboxFolderId(QMailMessageMetaDataFwd::Email);
    if (!inboxId.isValid())
        return;

    mMessaging->addMessage(inboxId, message);
}

void MobilityData::addNewSms()
{
    // ### A script should do this...
    QContactManager *manager = mContacts->manager();

    // find sender
    QList<QContact> contacts = manager->contacts();
    QContact sender = contacts.at(randomInt(0, contacts.size()));

    // find target
    QContact receiver = manager->contact(manager->selfContactId());

    // make message
    QMailMessage *message = new QMailMessage();
    message->setMessageType(QMailMessage::Sms);
    message->setSubject(tr("Generated message"));
    message->setBody(
            QMailMessageBody::fromData(tr("This is a simulator-generated SMS message."),
                                       QMailMessageContentType("text/plain"),
                                       QMailMessageBodyFwd::NoEncoding));
    message->setFrom(QMailAddress(sender.displayLabel(),
                                  sender.detail<QContactPhoneNumber>().number()));
    message->setTo(QMailAddress(receiver.displayLabel(),
                                receiver.detail<QContactPhoneNumber>().number()));
    message->setStatus(QMailMessage::Incoming | QMailMessage::New);

    QMailFolderId inboxId = mMessaging->inboxFolderId(QMailMessageMetaDataFwd::Sms);
    if (!inboxId.isValid())
        return;

    mMessaging->addMessage(inboxId, message);
}

void MobilityData::rotateDevice(Orientation to)
{
    SensorsUi::SensorsData sensors = mSensorsUi->sensorsData();

    switch (to) {
    case topUp:
        sensors.accelerometerX = 0;
        sensors.accelerometerY = 9.8;
        sensors.accelerometerZ = 0;
        break;
    case topDown:
        sensors.accelerometerX = 0;
        sensors.accelerometerY = -9.8;
        sensors.accelerometerZ = 0;
        break;
    case leftUp:
        sensors.accelerometerX = -9.8;
        sensors.accelerometerY = 0;
        sensors.accelerometerZ = 0;
        break;
    case rightUp:
        sensors.accelerometerX = 9.8;
        sensors.accelerometerY = 0;
        sensors.accelerometerZ = 0;
        break;
//    case faceUp:
//        sensors.accelerometerX = 0;
//        sensors.accelerometerY = 0;
//        sensors.accelerometerZ = 9.8;
//        break;
//    case faceDown:
//        sensors.accelerometerX = 0;
//        sensors.accelerometerY = 0;
//        sensors.accelerometerZ = -9.8;
//        break;
    }

    mSensorsUi->setSensorsData(sensors);
}
