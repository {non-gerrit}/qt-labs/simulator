/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef MOBILITYDATA_H
#define MOBILITYDATA_H

#include <QObject>

// while a forward declaration would suffice, we include these here
// as everything using MobilityData will usually want to include them too
#include "contacts.h"
#include "organizer.h"
#include "feedback.h"
#include "systeminfogenericui.h"
#include "systeminfonetworkui.h"
#include "systeminfostorageui.h"
#include "docgalleryui.h"
#include "messaging.h"
#include "sensorsui.h"
#include "cameraui.h"
#include "deviceitem.h"
#include "nfcui.h"

#include <remotecontrolwidget/locationui.h>

class MobilityData : public QObject
{
Q_OBJECT
public:
    explicit MobilityData(QObject *parent = 0);

    Contacts *mContacts;
    Organizer *mOrganizer;
    Feedback *mFeedback;
    LocationUi *mLocationUi;
    GenericSystemInfoUi *mSystemInfoGenericUi;
    NetworkSystemInfoUi *mSystemInfoNetworkUi;
    StorageSystemInfoUi *mSystemInfoStorageUi;
    DocGalleryUi *mDocGalleryUi;
    Messaging *mMessaging;
    SensorsUi *mSensorsUi;
    CameraUi *mCameraUi;
    NfcUi *mNfcUi;

    void setInitialData();

public slots:
    void addNewEmail();
    void addNewSms();

    // for adjusting data based on orientation of device
    void rotateDevice(Orientation to);

private:
    void setInitialSensorsData();
    void setInitialLocationData();
    void setInitialSystemInfoNetworkData();
    void setInitialSystemInfoStorageData();
    void setInitialSystemInfoGenericData();
    void setInitialCameraData();

    bool mInitialized;
};

#endif // MOBILITYDATA_H
