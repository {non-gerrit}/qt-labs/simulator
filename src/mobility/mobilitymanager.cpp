/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "mobilitymanager.h"
#include "mobilitydata.h"

#include <qsimulatordata_p.h>
#include <contacts/engines/qcontactmemorybackend_p.h>
#include <contacts/engines/qcontactmemorybackenddata_simulator_p.h>
#include <contacts/qcontactmanager.h>
#include <contacts/qcontactmanager_p.h>
#include <contacts/qcontactmanagerenginev2wrapper_p.h>
#include <contacts/qcontactrelationship.h>
#include <organizer/qorganizermanager.h>
#include <organizer/qorganizeritem.h>

#include <QtCore/QList>
#include <QtCore/QDebug>

#include <QtNetwork/QLocalSocket>
#include <QtNetwork/QLocalServer>

#include <limits>

using namespace QtMobility;

int MobilityClient::contactsClientsCount = 0;

MobilityServer::MobilityServer(const VersionStruct &version, MobilityData *mobility, QObject *parent)
    : QObject(parent)
    , mVersion(version)
    , mMobility(mobility)
{
    mServer = new QLocalServer();
    QLocalServer::removeServer(SIMULATOR_MOBILITY_SERVERNAME + mVersion.toString());
    if (!mServer->listen(SIMULATOR_MOBILITY_SERVERNAME + mVersion.toString())) {
        qFatal("Could not open mobility server");
    }
    connect(mServer, SIGNAL(newConnection()), this, SLOT(newConnection()));

    qt_registerSystemInfoTypes();
    qt_registerContactsTypes();
    qt_registerLocationTypes();
    qt_registerSensorTypes();
    qt_registerOrganizerTypes();
    qt_registerFeedbackTypes();
    qt_registerDocGalleryTypes();
    qt_registerCameraTypes();
}

MobilityServer::~MobilityServer()
{
    if (mServer) {
        mServer->close();
        QLocalServer::removeServer(SIMULATOR_MOBILITY_SERVERNAME + mVersion.toString());
        delete mServer;
        mServer = 0;
    }
}

void MobilityServer::newConnection()
{
    // get the incoming connection
    QLocalSocket* socket = mServer->nextPendingConnection();
    if (!socket->isValid()) {
        qWarning() << "Invalid socket!";
        return;
    }

    // expect an initial application connect packet to to verify that this is
    // indeed a simulator client application and to get its pid
    using namespace ::QtSimulatorPrivate;

    // read the command id
    qint32 requestCommand = 0;
    qint64 bytesToRead = sizeof(requestCommand);
    qint64 bytesRead = qt_blockingRead(
            socket, reinterpret_cast<char *>(&requestCommand), bytesToRead, 500);
    if (bytesRead < bytesToRead)
    {
        qWarning("Dropped incoming connection, couldn't read command id.");
        return;
    }
    if (requestCommand != QtSimulatorPrivate::Command::ApplicationConnect)
    {
        qWarning("Dropped incoming connection, it sent an invalid command.");
        return;
    }

    // read the command
    QtSimulatorPrivate::ApplicationConnectCommand cmd;
    bytesToRead = sizeof(cmd.request);
    bytesRead = qt_blockingRead(
            socket, reinterpret_cast<char *>(&cmd.request), bytesToRead, 500);
    if (bytesRead < bytesToRead)
    {
        qWarning("Dropped incoming connection, couldn't read command data.");
        return;
    }

    // send a reply
    cmd.reply.version = mVersion;
    qint64 bytesToWrite = sizeof(cmd.reply);
    qint64 bytesWritten = qt_blockingWrite(
            socket, reinterpret_cast<const char *>(&cmd.reply), bytesToWrite, 500);
    if (bytesWritten < bytesToWrite)
    {
        qWarning("Dropped incoming connection, couldn't send connect reply.");
        return;
    }

    // success: register client
    emit applicationConnected(cmd.reply.appId, cmd.request.version);
    mClients.append(new MobilityClient(mVersion.toString(), cmd.request.applicationPid, socket, this));
}

void MobilityServer::removeClient(MobilityClient *c)
{
    mClients.removeAll(c);
}

MobilityClient::MobilityClient(const QString &simVersion, qint64 pid, QLocalSocket *receiveSocket, MobilityServer *server)
    : QObject(server)
    , mPid(pid)
    , mReceiveSocket(receiveSocket)
    , mSendSocket(0)
    , mMobilityServer(server)
    , mNotifyClientOnContactsChange(true)
    , mNotifyClientOnOrganizerChange(true)
    , mNotifyClientOnFeedbackChange(true)
{
    // initiate sendSocket as a backchannel to the client
    mSendSocket = new QLocalSocket(this);
    mSendSocket->connectToServer(qt_mobilityServerName(simVersion, pid));
    if (!mSendSocket->waitForConnected(1000))
        qFatal("Couldn't set up back channel to mobility interface of client");

    // there might be data available already
    readyRead();
    connect(mReceiveSocket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(mReceiveSocket, SIGNAL(disconnected()), this, SLOT(disconnect()));
}

MobilityClient::~MobilityClient()
{
    mSendSocket->disconnectFromServer();
    mSendSocket->deleteLater();
}

void MobilityClient::disconnect()
{
    mMobilityServer->removeClient(this);
    delete this;
}

void MobilityClient::readyRead()
{
    mReadBuffer += mReceiveSocket->readAll();
    forever {
        QtSimulatorPrivate::IncomingRemoteMetacall rpc;
        if (rpc.read(&mReadBuffer)) {
            if (rpc.call(mReceiveSocket, this)) {
                continue;
            }
            qWarning("Ignoring a call to %s, No such slot in MobilityClient.", rpc.signature().data());
        }
        break;
    }
}

void MobilityClient::sendSystemGenericInfo(const GenericSystemInfoUi::GenericData &data)
{
    QSystemInfoData sysInfoData;
    sysInfoData.availableLanguages = data.availableLanguages;
    sysInfoData.currentCountryCode = data.currentCountryCode;
    sysInfoData.currentLanguage = data.currentLanguage;
    sysInfoData.features = data.features;
    sysInfoData.versions = data.versions;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setSystemInfoData", sysInfoData);
    QSystemDeviceInfoData deviceInfoData;
    deviceInfoData.batteryLevel = data.batteryLevel;
    deviceInfoData.currentPowerState = static_cast<QtMobility::QSystemDeviceInfo::PowerState>(data.currentPowerState);
    deviceInfoData.currentProfile = static_cast<QtMobility::QSystemDeviceInfo::Profile>(data.profile);
    deviceInfoData.deviceLocked = data.deviceLocked;
    deviceInfoData.imei = data.imei;
    deviceInfoData.imsi = data.imsi;
    QtMobility::QSystemDeviceInfo::InputMethodFlags flags;
    if (data.inputFlags & GenericSystemInfoScriptInterface::Keys)
        flags |= QSystemDeviceInfo::Keys;
    if (data.inputFlags & GenericSystemInfoScriptInterface::Keypad)
        flags |= QSystemDeviceInfo::Keypad;
    if (data.inputFlags & GenericSystemInfoScriptInterface::Keyboard)
        flags |= QSystemDeviceInfo::Keyboard;
    if (data.inputFlags & GenericSystemInfoScriptInterface::MultiTouch)
        flags |= QSystemDeviceInfo::MultiTouch;
    if (data.inputFlags & GenericSystemInfoScriptInterface::SingleTouch)
        flags |= QSystemDeviceInfo::SingleTouch;
    if (data.inputFlags & GenericSystemInfoScriptInterface::Mouse)
        flags |= QSystemDeviceInfo::Mouse;
    deviceInfoData.inputMethodType = flags;
    deviceInfoData.manufacturer = data.manufacturer;
    deviceInfoData.model = data.model;
    deviceInfoData.productName = data.productName;
    deviceInfoData.simStatus = static_cast<QtMobility::QSystemDeviceInfo::SimStatus>(data.simStatus);
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setSystemDeviceInfoData", deviceInfoData);

    QSystemDisplayInfoData displayInfoData;
    displayInfoData.colorDepth = data.colorDepth;
    displayInfoData.displayBrightness = data.displayBrightness;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setSystemDisplayInfoData", displayInfoData);

}

void MobilityClient::sendSystemStorageInfo(const StorageSystemInfoUi::StorageData &data)
{
    QSystemStorageInfoData storage;
    QHashIterator<QString, StorageSystemInfoUi::StorageData::DriveInfo> iter(data.drives);
    while (iter.hasNext()) {
        iter.next();
        QSystemStorageInfoData::DriveInfo info;
        info.type = static_cast<QSystemStorageInfo::DriveType>(iter.value().type);
        info.availableSpace = iter.value().availableSpace;
        info.totalSpace = iter.value().totalSpace;
        storage.drives.insert(iter.key(), info);
    }

    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setSystemStorageInfoData", storage);
}

void MobilityClient::sendSystemNetworkInfo(const NetworkSystemInfoUi::NetworkData &data)
{
    QSystemNetworkInfoData network;
    network.cellId = data.cellId;
    network.locationAreaCode = data.locationAreaCode;
    network.currentMobileCountryCode = data.currentMobileCountryCode;
    network.currentMobileNetworkCode = data.currentMobileNetworkCode;
    network.homeMobileCountryCode = data.homeMobileCountryCode;
    network.homeMobileNetworkCode = data.homeMobileNetworkCode;
    network.currentMode = static_cast<QSystemNetworkInfo::NetworkMode>(data.currentMode);
    foreach (const NetworkSystemInfoUi::NetworkData::ModeInfo &modeInfo, data.networkInfo) {
        QSystemNetworkInfoData::NetworkInfo info;
        info.name = modeInfo.name;
        info.macAddress = modeInfo.macAddress;
        info.signalStrength = modeInfo.signalStrength;
        info.status = static_cast<QSystemNetworkInfo::NetworkStatus>(modeInfo.status);
        network.networkInfo.append(info);
    }

    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setSystemNetworkInfoData", network);
}

void MobilityClient::sendLocationData(const LocationUi::LocationData &data)
{
    QGeoPositionInfoData geoPositionData;
    geoPositionData.latitude = data.latitude;
    geoPositionData.longitude = data.longitude;
    geoPositionData.altitude = data.altitude;

    geoPositionData.direction = data.direction;
    geoPositionData.groundSpeed = data.groundSpeed;
    geoPositionData.verticalSpeed = data.verticalSpeed;
    geoPositionData.magneticVariation = data.magneticVariation;
    geoPositionData.horizontalAccuracy = data.horizontalAccuracy;
    geoPositionData.verticalAccuracy = data.verticalAccuracy;

    if (data.useCurrentTime)
        geoPositionData.dateTime = QDateTime();
    else
        geoPositionData.dateTime = data.timestamp;
    geoPositionData.minimumInterval = 1000;
    geoPositionData.enabled = true;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setLocationData", geoPositionData);
}

void MobilityClient::sendSatelliteData(const LocationUi::SatelliteData &data)
{
    QGeoSatelliteInfoData satelliteData;
    for (int i =0; i < data.satellites.count(); i++) {
        QGeoSatelliteInfoData::SatelliteInfo info;
        info.prn = data.satellites.at(i).prn;
        info.azimuth = data.satellites.at(i).azimuth;
        info.elevation = data.satellites.at(i).elevation;
        info.signalStrength = data.satellites.at(i).signalStrength;
        info.inUse = data.satellites.at(i).inUse;
        satelliteData.satellites.append(info);
    }
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setSatelliteData", satelliteData);
}

void MobilityClient::sendDocGallery(const QtMobility::DocGallerySimulatorData &data)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setDocGalleryData", data);
}

void MobilityClient::sendSensorsData(const SensorsUi::SensorsData &data)
{
    QDateTime timestampToSend;
    if (!data.useCurrentTime)
        timestampToSend = data.timestamp;
    QAmbientLightReadingData ambientData;
    ambientData.lightLevel = static_cast<QtMobility::QAmbientLightReading::LightLevel>(data.ambientLightLevel);
    ambientData.timestamp = timestampToSend;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setAmbientLightData", ambientData);

    QLightReadingData lightData;
    lightData.lux = data.lux;
    lightData.timestamp = timestampToSend;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setLightData", lightData);

    QAccelerometerReadingData accelermometerData;
    accelermometerData.x = data.accelerometerX;
    accelermometerData.y = data.accelerometerY;
    accelermometerData.z = data.accelerometerZ;
    accelermometerData.timestamp = timestampToSend;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setAccelerometerData", accelermometerData);

    QMagnetometerReadingData magnetometerData;
    magnetometerData.x = data.magnetometerX * 1e-6; // scale to Teslas
    magnetometerData.y = data.magnetometerY * 1e-6;
    magnetometerData.z = data.magnetometerZ * 1e-6;
    magnetometerData.calibrationLevel = data.magnetometerCalibrationLevel;
    magnetometerData.timestamp = timestampToSend;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setMagnetometerData", magnetometerData);

    QCompassReadingData compassData;
    compassData.azimuth = data.compassAzimuth;
    compassData.calibrationLevel = data.compassCalibrationLevel;
    compassData.timestamp = timestampToSend;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setCompassData", compassData);

    QProximityReadingData proximityData;
    proximityData.close = data.proximitySensorClose;
    proximityData.timestamp = timestampToSend;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setProximityData", proximityData);
}

void MobilityClient::sendCameraData(const CameraUi::CameraData &data)
{
    QCameraData camData;
    QHashIterator<QString, CameraUi::CameraData::CameraDetails> iter(data.cameras);
    while (iter.hasNext()) {
        iter.next();
        QCameraData::QCameraDetails details;
        details.description = iter.value().description;
        details.imagePath = iter.value().imagePath;
        camData.cameras.insert(iter.key(), details);
    }

    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setCameraData", camData);
}

void MobilityClient::sendCameraAdded(const QString &name, const CameraUi::CameraData::CameraDetails &newDetails)
{
    QCameraData::QCameraDetails details;
    details.description = newDetails.description;
    details.imagePath = newDetails.imagePath;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "addCamera", name, details);
}

void MobilityClient::sendCameraRemoved(const QString &name)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "removeCamera", name);
}

void MobilityClient::sendCameraChanged(const QString &name, const CameraUi::CameraData::CameraDetails &newDetails)
{
    QCameraData::QCameraDetails details;
    details.description = newDetails.description;
    details.imagePath = newDetails.imagePath;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "changeCamera", name, details);
}

// Re-implement non-exported function from src/contacts/qcontactmanager_p.h to
// access the engine for a manager through its private d ptr.
QContactManagerEngineV2* QContactManagerData::engine(const QContactManager* manager)
{
    if (manager)
        return manager->d->m_engine;
    return 0;
}

// Re-implement non-exported function from src/contacts/engines/qcontactmemoryengine_p.h to
// access the d ptr of a QContactMemoryEngine.
QContactMemoryEngineData* QContactMemoryEngineData::data(QContactMemoryEngine* engine)
{
    if (engine)
        return engine->d;
    return 0;
}

// Sends simulator contact data to client, overwriting its local data.
// Can invalidate ids the client stored locally.
// Ideally it's used only for the initial data transmission.
void MobilityClient::sendContactData(const QContactManager &manager)
{
    QContactSimulatorData data;
    QContactMemoryEngineData *engineData = QContactMemoryEngineData::data(static_cast<QContactMemoryEngine *>(static_cast<QContactManagerEngineV2Wrapper *>(QContactManagerData::engine(&manager))->wrappee()));
    data.m_selfContactId = engineData->m_selfContactId;
    data.m_contacts = engineData->m_contacts;
    data.m_contactIds = engineData->m_contactIds;
    data.m_relationships = engineData->m_relationships;
    data.m_orderedRelationships = engineData->m_orderedRelationships;
    data.m_definitionIds = engineData->m_definitionIds;
    data.m_definitions = engineData->m_definitions;
    data.m_nextContactId = engineData->m_nextContactId;

    // to avoid collisions, give each client a large area for unique detail ids
    // ### TODO: Ugly hack.
    contactsClientsCount++;
    data.m_lastDetailId = std::numeric_limits<int>::max() / 1000 * contactsClientsCount;

    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setContactData", data);
}

void MobilityClient::setRequestsSystemInfo()
{
    connect(mMobilityServer->mMobility->mSystemInfoStorageUi, SIGNAL(storageDataChanged(StorageSystemInfoUi::StorageData)),
            SLOT(sendSystemStorageInfo(StorageSystemInfoUi::StorageData)));
    connect(mMobilityServer->mMobility->mSystemInfoGenericUi, SIGNAL(genericDataChanged(GenericSystemInfoUi::GenericData)),
            SLOT(sendSystemGenericInfo(GenericSystemInfoUi::GenericData)));
    connect(mMobilityServer->mMobility->mSystemInfoNetworkUi, SIGNAL(networkDataChanged(NetworkSystemInfoUi::NetworkData)),
            SLOT(sendSystemNetworkInfo(NetworkSystemInfoUi::NetworkData)));
    sendSystemGenericInfo(mMobilityServer->mMobility->mSystemInfoGenericUi->genericData());
    sendSystemNetworkInfo(mMobilityServer->mMobility->mSystemInfoNetworkUi->networkData());
    sendSystemStorageInfo(mMobilityServer->mMobility->mSystemInfoStorageUi->storageData());
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "initialSystemInfoDataSent");
}

void MobilityClient::setRequestsLocationInfo()
{
    connect(mMobilityServer->mMobility->mLocationUi, SIGNAL(locationChanged(LocationUi::LocationData)),
            SLOT(sendLocationData(LocationUi::LocationData)));
    connect(mMobilityServer->mMobility->mLocationUi, SIGNAL(satelliteDataChanged(LocationUi::SatelliteData)),
            SLOT(sendSatelliteData(LocationUi::SatelliteData)));
    sendLocationData(mMobilityServer->mMobility->mLocationUi->locationData());
    sendSatelliteData(mMobilityServer->mMobility->mLocationUi->satelliteData());
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "initialLocationDataSent");
}

void MobilityClient::setRequestsContactInfo()
{
    connect(mMobilityServer->mMobility->mContacts, SIGNAL(contactsDataChanged(QContactManager)), this, SLOT(sendContactData(QContactManager)));
    connect(mMobilityServer->mMobility->mContacts, SIGNAL(contactsAdded(QList<QContactLocalId>)), this, SLOT(sendAddedContacts(QList<QContactLocalId>)));
    connect(mMobilityServer->mMobility->mContacts, SIGNAL(contactsChanged(QList<QContactLocalId>)), this, SLOT(sendChangedContacts(QList<QContactLocalId>)));
    connect(mMobilityServer->mMobility->mContacts, SIGNAL(contactsRemoved(QList<QContactLocalId>)), this, SLOT(sendRemovedContacts(QList<QContactLocalId>)));
    connect(mMobilityServer->mMobility->mContacts, SIGNAL(relationshipsAdded(QList<QContactLocalId>)), this, SLOT(sendRelationshipsFor(QList<QContactLocalId>)));
    connect(mMobilityServer->mMobility->mContacts, SIGNAL(relationshipsRemoved(QList<QContactLocalId>)), this, SLOT(sendRelationshipsFor(QList<QContactLocalId>)));
}

void MobilityClient::setRequestsSensors()
{
    connect(mMobilityServer->mMobility->mSensorsUi, SIGNAL(sensorsDataChanged(SensorsUi::SensorsData)),
            SLOT(sendSensorsData(SensorsUi::SensorsData)));
    sendSensorsData(mMobilityServer->mMobility->mSensorsUi->sensorsData());
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "initialSensorsDataSent");
}

void MobilityClient::setRequestsNfc()
{
    connect(mMobilityServer->mMobility->mNfcUi, SIGNAL(targetEnteringProximity(QByteArray)),
            SLOT(sendNfcTargetEnteringProximity(QByteArray)));
    connect(mMobilityServer->mMobility->mNfcUi, SIGNAL(targetLeavingProximity(QByteArray)),
            SLOT(sendNfcTargetLeavingProximity(QByteArray)));
}

void MobilityClient::setRequestsOrganizer()
{
    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();
    connect(manager, SIGNAL(itemsAdded(QList<QOrganizerItemId>)),
            this, SLOT(sendSaveOrganizerItems(QList<QOrganizerItemId>)));
    connect(manager, SIGNAL(itemsChanged(QList<QOrganizerItemId>)),
            this, SLOT(sendSaveOrganizerItems(QList<QOrganizerItemId>)));
    connect(manager, SIGNAL(itemsRemoved(QList<QOrganizerItemId>)),
            this, SLOT(sendRemoveOrganizerItems(QList<QOrganizerItemId>)));
    connect(manager, SIGNAL(collectionsAdded(QList<QOrganizerCollectionId>)),
            this, SLOT(sendSaveOrganizerCollections(QList<QOrganizerCollectionId>)));
    connect(manager, SIGNAL(collectionsChanged(QList<QOrganizerCollectionId>)),
            this, SLOT(sendSaveOrganizerCollections(QList<QOrganizerCollectionId>)));
    connect(manager, SIGNAL(collectionsRemoved(QList<QOrganizerCollectionId>)),
            this, SLOT(sendRemoveOrganizerCollections(QList<QOrganizerCollectionId>)));

    // ### This reorders the remote local ids! Better than ignoring it though.
    // luckily, the signal seems to be unused by the memory engine
    connect(manager, SIGNAL(dataChanged()),
            this, SLOT(triggerOrganizerDataResend()));

    const QString managerUri = mMobilityServer->mMobility->mOrganizer->manager()->managerUri();
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setOrganizerManagerUri", managerUri);
}

void MobilityClient::setRequestsFeedback()
{
    const Feedback *feedback = mMobilityServer->mMobility->mFeedback;

    connect(feedback, SIGNAL(actuatorChanged(QtMobility::ActuatorData)),
            this, SLOT(sendActuatorChange(QtMobility::ActuatorData)));
    connect(feedback, SIGNAL(actuatorAdded(QtMobility::ActuatorData)),
            this, SLOT(sendActuatorChange(QtMobility::ActuatorData)));
    // We do not listen to actuatorRemoved on purpose, the api does not allow for
    // removing actuators at runtime.
    connect(feedback, SIGNAL(effectStateChanged(int,QtMobility::QFeedbackEffect::State)),
            this, SLOT(sendEffectStateChange(int,QtMobility::QFeedbackEffect::State)));
    connect(feedback, SIGNAL(defaultActuatorIdChanged(int)),
            this, SLOT(sendDefaultActuatorIdChange(int)));

    foreach (const QtMobility::ActuatorData &data, feedback->actuators()) {
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "setActuator", data);
    }
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setDefaultActuator", feedback->defaultActuatorId());
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "initialFeedbackDataSent");
}

void MobilityClient::setRequestsDocGallery()
{
    connect(mMobilityServer->mMobility->mDocGalleryUi,
            SIGNAL(docGalleryDataChanged(QtMobility::DocGallerySimulatorData)),
            this, SLOT(sendDocGallery(QtMobility::DocGallerySimulatorData)));
    sendDocGallery(mMobilityServer->mMobility->mDocGalleryUi->data());
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "initialDocGalleryDataSent");
}

void MobilityClient::setRequestsCameras()
{
    connect(mMobilityServer->mMobility->mCameraUi, SIGNAL(cameraDataChanged(CameraUi::CameraData)),
        SLOT(sendCameraData(CameraUi::CameraData)));
    connect(mMobilityServer->mMobility->mCameraUi, SIGNAL(cameraAdded(QString, CameraUi::CameraData::CameraDetails)),
        SLOT(sendCameraAdded(QString, CameraUi::CameraData::CameraDetails)));
    connect(mMobilityServer->mMobility->mCameraUi, SIGNAL(cameraRemoved(QString)),
        SLOT(sendCameraRemoved(QString)));
    connect(mMobilityServer->mMobility->mCameraUi, SIGNAL(cameraChanged(QString, CameraUi::CameraData::CameraDetails)),
        SLOT(sendCameraChanged(QString, CameraUi::CameraData::CameraDetails)));
    sendCameraData(mMobilityServer->mMobility->mCameraUi->cameraData());
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "initialCameraDataSent");
}

void MobilityClient::triggerContactDataResend()
{
    mMobilityServer->mMobility->mContacts->publish();
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "initialContactDataSent");
}

void MobilityClient::triggerOrganizerDataResend()
{
    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    Simulator::OrganizerCollectionId defaultCollectionId;
    defaultCollectionId.id = manager->defaultCollection().id();
    QtSimulatorPrivate::RemoteMetacall<void>::call(
                mSendSocket, QtSimulatorPrivate::NoSync,
                "clearOrganizerData", defaultCollectionId);

    mKnownDetailDefinitions.clear();
    syncOrganizerDetailDefinitions();

    foreach (const QOrganizerCollection &collection, manager->collections()) {
        if (collection == manager->defaultCollection())
            continue;
        QtSimulatorPrivate::RemoteMetacall<void>::call(
                    mSendSocket, QtSimulatorPrivate::NoSync,
                    "saveOrganizerCollection", collection);
    }

    foreach (const QOrganizerItem &item, manager->items()) {
        QtSimulatorPrivate::RemoteMetacall<void>::call(
                    mSendSocket, QtSimulatorPrivate::NoSync,
                    "saveOrganizerItem", item);
    }


    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "initialOrganizerDataSent");
}

Simulator::SaveContactReply MobilityClient::requestSaveContact(const QContact &contact)
{
    Simulator::SaveContactReply reply;
    reply.savedContact = contact;

    // execute change and notify other clients in the process
    mNotifyClientOnContactsChange = false;
    mMobilityServer->mMobility->mContacts->manager()->saveContact(&reply.savedContact);
    mNotifyClientOnContactsChange = true;

    reply.error = mMobilityServer->mMobility->mContacts->manager()->error();
    return reply;
}

int MobilityClient::requestRemoveContact(uint id)
{
    // execute change and notify other clients in the process
    mNotifyClientOnContactsChange = false;
    mMobilityServer->mMobility->mContacts->manager()->removeContact(id);
    mNotifyClientOnContactsChange = true;

    return static_cast<int>(mMobilityServer->mMobility->mContacts->manager()->error());
}

Simulator::SaveRelationshipReply MobilityClient::requestSaveRelationship(const QContactRelationship &relationship)
{
    Simulator::SaveRelationshipReply reply;
    reply.savedRelationship = relationship;

    // execute change and notify other clients in the process
    mNotifyClientOnContactsChange = false;
    mMobilityServer->mMobility->mContacts->manager()->saveRelationship(&reply.savedRelationship);
    mNotifyClientOnContactsChange = true;

    reply.error = mMobilityServer->mMobility->mContacts->manager()->error();
    return reply;
}

QByteArray MobilityClient::nfcSendCommand(const QByteArray &command)
{
    return mMobilityServer->mMobility->mNfcUi->processCommand(command);
}

int MobilityClient::requestRemoveRelationship(const QContactRelationship &relationship)
{
    // execute change and notify other clients in the process
    mNotifyClientOnContactsChange = false;
    mMobilityServer->mMobility->mContacts->manager()->removeRelationship(relationship);
    mNotifyClientOnContactsChange = true;

    return static_cast<int>(mMobilityServer->mMobility->mContacts->manager()->error());
}

void MobilityClient::sendAddedContacts(const QList<QContactLocalId> &contactIds)
{
    if (!mNotifyClientOnContactsChange)
        return;

    foreach (QContactLocalId id, contactIds) {
        const QContact &contact = mMobilityServer->mMobility->mContacts->manager()->contact(id);
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "addContact", contact);
    }
}

void MobilityClient::sendChangedContacts(const QList<QContactLocalId> &contactIds)
{
    if (!mNotifyClientOnContactsChange)
        return;

    foreach (QContactLocalId id, contactIds) {
        const QContact &contact = mMobilityServer->mMobility->mContacts->manager()->contact(id);
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "changeContact", contact);
    }
}

void MobilityClient::sendRemovedContacts(const QList<QContactLocalId> &contactIds)
{
    if (!mNotifyClientOnContactsChange)
        return;

    foreach (QContactLocalId id, contactIds) {
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "removeContact", id);
    }
}

void MobilityClient::sendRelationshipsFor(const QList<QContactLocalId> &contactIds)
{
    if (!mNotifyClientOnContactsChange)
        return;

    QContactManager *manager = mMobilityServer->mMobility->mContacts->manager();

    foreach (QContactLocalId localId, contactIds) {
        QContactId id(manager->contact(localId).id());
        QList<QContactRelationship> relationships(manager->relationships(id));
        QVariantList list;
        foreach (const QContactRelationship &r, relationships)
            list.append(QVariant::fromValue(r));
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "setRelationships", localId, list);
    }
}

Simulator::SaveOrganizerItemReply MobilityClient::requestSaveOrganizerItem(
    const QtMobility::QOrganizerItem &item)
{
    Simulator::SaveOrganizerItemReply reply;
    reply.savedItem = item;

    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    // execute change and notify other clients in the process
    mNotifyClientOnOrganizerChange = false;
    manager->saveItem(&reply.savedItem);
    mNotifyClientOnOrganizerChange = true;

    reply.error = manager->error();
    return reply;
}

int MobilityClient::requestRemoveOrganizerItem(Simulator::OrganizerItemId id)
{
    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    mNotifyClientOnOrganizerChange = false;
    manager->removeItem(id.id);
    mNotifyClientOnOrganizerChange = true;

    return static_cast<int>(manager->error());
}

Simulator::SaveOrganizerCollectionReply MobilityClient::requestSaveOrganizerCollection(
    const QtMobility::QOrganizerCollection &collection)
{
    Simulator::SaveOrganizerCollectionReply reply;
    reply.savedCollection = collection;

    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    // execute change and notify other clients in the process
    mNotifyClientOnOrganizerChange = false;
    manager->saveCollection(&reply.savedCollection);
    mNotifyClientOnOrganizerChange = true;

    reply.error = manager->error();
    return reply;
}

int MobilityClient::requestRemoveOrganizerCollection(Simulator::OrganizerCollectionId id)
{
    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    mNotifyClientOnOrganizerChange = false;
    manager->removeCollection(id.id);
    mNotifyClientOnOrganizerChange = true;

    return static_cast<int>(manager->error());
}

int MobilityClient::requestSaveOrganizerDetailDefinition(
    QtMobility::QOrganizerItemDetailDefinition definition, QString itemType)
{
    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    mNotifyClientOnOrganizerChange = false;
    //qDebug() << "Simu: Saving detail" << definition.name() << itemType;
    manager->saveDetailDefinition(definition, itemType);
    mNotifyClientOnOrganizerChange = true;

    QOrganizerManager::Error error = manager->error();
    if (error == QOrganizerManager::NoError) {
        mKnownDetailDefinitions[itemType][definition.name()] = definition;
    }

    return static_cast<int>(error);
}

int MobilityClient::requestRemoveOrganizerDetailDefinition(QString definitionId, QString itemType)
{
    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    mNotifyClientOnOrganizerChange = false;
    //qDebug() << "Simu: Removing detail" << definitionId << itemType;
    manager->removeDetailDefinition(definitionId, itemType);
    mNotifyClientOnOrganizerChange = true;

    QOrganizerManager::Error error = manager->error();
    if (error == QOrganizerManager::NoError) {
        mKnownDetailDefinitions[itemType].remove(definitionId);
    }

    return static_cast<int>(error);
}

void MobilityClient::syncOrganizerDetailDefinitions()
{
    // changes of detail definitions don't trigger a signal in the manager,
    // so we sync them up lazily

    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    // send any new or changed detail definitions
    foreach (const QString &itemType, manager->supportedItemTypes()) {
        foreach (const QOrganizerItemDetailDefinition &def, manager->detailDefinitions(itemType)) {
            if (mKnownDetailDefinitions[itemType][def.name()] == def)
                continue;
            QtSimulatorPrivate::RemoteMetacall<void>::call(
                        mSendSocket, QtSimulatorPrivate::NoSync,
                        "saveOrganizerDetailDefinition", def, itemType);
            mKnownDetailDefinitions[itemType].insert(def.name(), def);
            //qDebug() << "Simu: Saved detail " << def.name();
        }
    }

    // remove any removed
    QMutableHashIterator<QString, QHash<QString, QOrganizerItemDetailDefinition> > it(mKnownDetailDefinitions);
    while (it.hasNext()) {
        it.next();
        const QString &itemType = it.key();

        QMutableHashIterator<QString, QOrganizerItemDetailDefinition> inner(it.value());
        while (inner.hasNext()) {
            inner.next();
            const QString &definitionId = inner.key();

            if (!manager->detailDefinition(definitionId, itemType).isEmpty())
                continue;
            QtSimulatorPrivate::RemoteMetacall<void>::call(
                        mSendSocket, QtSimulatorPrivate::NoSync,
                        "removeOrganizerDetailDefinition", definitionId, itemType);
            inner.remove();
            //qDebug() << "Simu: Removed detail " << definitionId;
        }
    }
}

void MobilityClient::sendSaveOrganizerItems(const QList<QOrganizerItemId> &itemIds)
{
    if (!mNotifyClientOnOrganizerChange)
        return;

    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    syncOrganizerDetailDefinitions();

    foreach (QOrganizerItemId id, itemIds) {
        const QOrganizerItem &item = manager->item(id);
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "saveOrganizerItem", item);
    }
}

void MobilityClient::sendRemoveOrganizerItems(const QList<QOrganizerItemId> &itemIds)
{
    if (!mNotifyClientOnOrganizerChange)
        return;

    syncOrganizerDetailDefinitions();

    foreach (QOrganizerItemId id, itemIds) {
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "removeOrganizerItem", id);
        //qDebug() << "Remove item" << id;
    }
}

void MobilityClient::sendSaveOrganizerCollections(const QList<QtMobility::QOrganizerCollectionId> &collectionIds)
{
    if (!mNotifyClientOnOrganizerChange)
        return;

    QOrganizerManager *manager = mMobilityServer->mMobility->mOrganizer->manager();

    syncOrganizerDetailDefinitions();

    foreach (QOrganizerCollectionId collectionId, collectionIds) {
        const QOrganizerCollection &collection = manager->collection(collectionId);
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "saveOrganizerCollection", collection);
    }
}

void MobilityClient::sendRemoveOrganizerCollections(const QList<QtMobility::QOrganizerCollectionId> &collectionIds)
{
    if (!mNotifyClientOnOrganizerChange)
        return;

    syncOrganizerDetailDefinitions();

    foreach (QOrganizerCollectionId id, collectionIds) {
        QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                       "removeOrganizerCollection", id);
        //qDebug() << "Remove collection" << id;
    }
}

int MobilityClient::startFeedbackEffect(int actuatorId, QString info, int duration)
{
    mNotifyClientOnFeedbackChange = false;
    const int effectId = mMobilityServer->mMobility->mFeedback->startEffect(actuatorId, info, duration);
    mNotifyClientOnFeedbackChange = true;
    return effectId;
}

bool MobilityClient::resumeFeedbackEffect(int effectId)
{
    mNotifyClientOnFeedbackChange = false;
    bool ok = mMobilityServer->mMobility->mFeedback->resumeEffect(effectId);
    mNotifyClientOnFeedbackChange = true;
    return ok;
}

bool MobilityClient::pauseFeedbackEffect(int effectId)
{
    mNotifyClientOnFeedbackChange = false;
    bool ok = mMobilityServer->mMobility->mFeedback->pauseEffect(effectId);
    mNotifyClientOnFeedbackChange = true;
    return ok;
}

bool MobilityClient::stopFeedbackEffect(int effectId)
{
    mNotifyClientOnFeedbackChange = false;
    bool ok = mMobilityServer->mMobility->mFeedback->stopEffect(effectId);
    mNotifyClientOnFeedbackChange = true;
    return ok;
}

void MobilityClient::setFeedbackEffectDuration(int effectId, int duration)
{
    mNotifyClientOnFeedbackChange = false;
    mMobilityServer->mMobility->mFeedback->setEffectDuration(effectId, duration);
    mNotifyClientOnFeedbackChange = true;
}

void MobilityClient::setFeedbackActuatorEnabled(int actuatorId, bool enabled)
{
    mNotifyClientOnFeedbackChange = false;
    mMobilityServer->mMobility->mFeedback->setActuatorEnabled(actuatorId, enabled);
    mNotifyClientOnFeedbackChange = true;
}

void MobilityClient::sendActuatorChange(const QtMobility::ActuatorData &actuator)
{
    if (!mNotifyClientOnFeedbackChange)
        return;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setActuator", actuator);
}

void MobilityClient::sendDefaultActuatorIdChange(int actuatorId)
{
    if (!mNotifyClientOnFeedbackChange)
        return;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setDefaultActuator", actuatorId);
}

void MobilityClient::sendEffectStateChange(int effectId, QtMobility::QFeedbackEffect::State state)
{
    if (!mNotifyClientOnFeedbackChange)
        return;
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "setFeedbackEffectState", effectId, static_cast<int>(state));
}

void MobilityClient::sendNfcTargetEnteringProximity(const QByteArray &uid)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "targetEnteringProximity", uid);
}

void MobilityClient::sendNfcTargetLeavingProximity(const QByteArray &uid)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mSendSocket, QtSimulatorPrivate::NoSync,
                                                   "targetLeavingProximity", uid);
}
