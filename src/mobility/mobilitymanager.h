/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef MOBILITY_H
#define MOBILITY_H

#include "qsimulatordata_p.h"
#include "sensorsui.h"
#include "systeminfonetworkui.h"
#include "systeminfostorageui.h"
#include "systeminfogenericui.h"
#include "docgalleryui.h"
#include "cameraui.h"

#include <remotecontrolwidget/locationui.h>

#include <systeminfo/qsysteminfodata_simulator_p.h>
#include <location/qlocationdata_simulator_p.h>
#include <mobilitysimulator/mobilityconnection_p.h>
#include <contacts/engines/qcontactmemorybackenddata_simulator_p.h>
#include <contacts/engines/qcontactmemorybackenddata_simulator_p.h>
#include <organizer/qorganizeritemid.h>
#include <organizer/qorganizeritemdetaildefinition.h>
#include <../plugins/sensors/simulator/qsensordata_simulator_p.h>
#include <../plugins/multimedia/simulator/qsimulatormultimediadata_p.h>
#include <qorganizerdata_simulator_p.h>
#include <qfeedbackeffect.h>

#include <QtCore/QHash>

class QLocalSocket;
class QLocalServer;
class MobilityData;
class MobilityClient;
namespace QtMobility {
    class QContactManager;
    class QContact;
    class QOrganizerItem;
    class ActuatorData;
}
// Required to give some slots below a connectable signature.
using namespace QtMobility;

class MobilityServer : public QObject
{
    Q_OBJECT
public:
    explicit MobilityServer(const VersionStruct &version, MobilityData *mobility, QObject *parent = 0);
    ~MobilityServer();

private slots:
    void newConnection();

signals:
    void applicationConnected(int id, VersionStruct version);

private:
    void removeClient(MobilityClient *c);

    QLocalServer *mServer;
    QList<MobilityClient *> mClients;

    MobilityData *mMobility;
    VersionStruct mVersion;

    friend class MobilityClient;
};

class MobilityClient : public QObject
{
    Q_OBJECT
public:
    MobilityClient(const QString &simVersion, qint64 pid, QLocalSocket *receiveSocket, MobilityServer *server);
    ~MobilityClient();

public slots:
    void sendSystemStorageInfo(const StorageSystemInfoUi::StorageData &data);
    void sendSystemGenericInfo(const GenericSystemInfoUi::GenericData &data);
    void sendSystemNetworkInfo(const NetworkSystemInfoUi::NetworkData &data);

    void sendLocationData(const LocationUi::LocationData &data);
    void sendSatelliteData(const LocationUi::SatelliteData &data);

    void sendContactData(const QContactManager &manager);

    void sendSensorsData(const SensorsUi::SensorsData &data);

    void sendCameraData(const CameraUi::CameraData &data);
    void sendCameraAdded(const QString &name, const CameraUi::CameraData::CameraDetails &newDetails);
    void sendCameraRemoved(const QString &name);
    void sendCameraChanged(const QString &name, const CameraUi::CameraData::CameraDetails &newDetails);

    void sendNfcTargetEnteringProximity(const QByteArray &uid);
    void sendNfcTargetLeavingProximity(const QByteArray &uid);

private slots:
    void disconnect();
    void readyRead();

    void sendAddedContacts(const QList<QContactLocalId> &);
    void sendChangedContacts(const QList<QContactLocalId> &);
    void sendRemovedContacts(const QList<QContactLocalId> &);
    void sendRelationshipsFor(const QList<QContactLocalId> &);

    void sendSaveOrganizerItems(const QList<QOrganizerItemId> &);
    void sendRemoveOrganizerItems(const QList<QOrganizerItemId> &);
    void sendSaveOrganizerCollections(const QList<QOrganizerCollectionId> &);
    void sendRemoveOrganizerCollections(const QList<QOrganizerCollectionId> &);

    void sendDocGallery(const QtMobility::DocGallerySimulatorData &);

    void sendActuatorChange(const QtMobility::ActuatorData &);
    void sendDefaultActuatorIdChange(int actuatorId);
    void sendEffectStateChange(int effectId, QtMobility::QFeedbackEffect::State state);

    // called remotely
    void setRequestsSystemInfo();
    void setRequestsLocationInfo();
    void setRequestsContactInfo();
    void setRequestsSensors();
    void setRequestsNfc();
    void setRequestsOrganizer();
    void setRequestsFeedback();
    void setRequestsDocGallery();
    void triggerContactDataResend();
    void triggerOrganizerDataResend();
    void setRequestsCameras();

    QtMobility::Simulator::SaveContactReply requestSaveContact(const QtMobility::QContact &contact);
    int requestRemoveContact(uint id);
    QtMobility::Simulator::SaveRelationshipReply requestSaveRelationship(const QtMobility::QContactRelationship &relationship);
    int requestRemoveRelationship(const QtMobility::QContactRelationship &relationship);

    QtMobility::Simulator::SaveOrganizerItemReply requestSaveOrganizerItem(
        const QtMobility::QOrganizerItem &item);
    int requestRemoveOrganizerItem(QtMobility::Simulator::OrganizerItemId id);
    QtMobility::Simulator::SaveOrganizerCollectionReply requestSaveOrganizerCollection(
        const QtMobility::QOrganizerCollection &collection);
    int requestRemoveOrganizerCollection(QtMobility::Simulator::OrganizerCollectionId id);
    int requestSaveOrganizerDetailDefinition(
        QtMobility::QOrganizerItemDetailDefinition definition, QString itemType);
    int requestRemoveOrganizerDetailDefinition(QString definitionId, QString itemType);

    int startFeedbackEffect(int actuatorId, QString info, int duration);
    bool resumeFeedbackEffect(int effectId);
    bool pauseFeedbackEffect(int effectId);
    bool stopFeedbackEffect(int effectId);
    void setFeedbackEffectDuration(int effectId, int duration);
    void setFeedbackActuatorEnabled(int actuatorId, bool enabled);

    QByteArray nfcSendCommand(const QByteArray &command);

private:
    void syncOrganizerDetailDefinitions();

private:
    qint64 mPid;
    QLocalSocket *mReceiveSocket;
    QLocalSocket *mSendSocket;
    QByteArray mReadBuffer;
    MobilityServer *mMobilityServer;

    bool mNotifyClientOnContactsChange;
    bool mNotifyClientOnOrganizerChange;
    bool mNotifyClientOnFeedbackChange;

    QHash<QString, QHash<QString, QOrganizerItemDetailDefinition> > mKnownDetailDefinitions;

    static int contactsClientsCount;
};

#endif // MOBILITY_H
