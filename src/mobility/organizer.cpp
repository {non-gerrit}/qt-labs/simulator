/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "organizer.h"
#include "organizertreemodel.h"

#include <QtOrganizer/QOrganizerManager>
#include <QtOrganizer/QOrganizerItem>
#include <QtOrganizer/QOrganizerCollection>
#include <QtOrganizer/QOrganizerEvent>

#include <QtVersit/QVersitReader>
#include <QtVersit/QVersitWriter>
#include <QtVersit/QVersitDocument>
#include <QtVersitOrganizer/QVersitOrganizerImporter>
#include <QtVersitOrganizer/QVersitOrganizerExporter>

#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QFile>
#include <QtCore/QTextStream>

#include <QDebug>

using namespace QtMobility;

Organizer::Organizer(QObject *parent)
    : QObject(parent),
      mManager(new QOrganizerManager("memory")),
      mOrganizerTreeModel(0)
{
}

bool Organizer::importFromVCardFile(const QString &fileName)
{
    QFile organizerFile(fileName);
    organizerFile.open(QIODevice::ReadOnly);
    if (!organizerFile.isReadable()) {
        return false;
    }

    QVersitReader reader;
    reader.setDevice(&organizerFile);
    if (!reader.startReading() || !reader.waitForFinished() || reader.error() != QVersitReader::NoError) {
        qWarning() << "Read failed, " << reader.error();
    }

    QVersitOrganizerImporter importer;
    if (!importer.importDocument(reader.results().at(0))) {
        qWarning() << "Could not import organizer data from " << fileName;
        return false;
    }

    QList<QOrganizerItem> items = importer.items();
    if (! mManager->saveItems(&items) ) {
        qWarning() << QString("Could not import organizer data to current QOrganizerItemManager( %1 )").arg(mManager->managerName());
        return false;
    }
    return true;
}

bool Organizer::exportToVCardFile(const QString &fileName)
{
    QFile organizerFile(fileName);
    organizerFile.open(QIODevice::WriteOnly);
    if (!organizerFile.isWritable()) {
        return false;
    }

    QVersitOrganizerExporter exporter;
    exporter.exportItems(mManager->items());

    QVersitWriter writer(&organizerFile);
    QList<QVersitDocument> documents;
    documents.append(exporter.document());
    if (writer.startWriting(documents)) {
        return writer.waitForFinished();
    }
    return false;
}

QOrganizerManager *Organizer::manager()
{
    return mManager;
}

OrganizerTreeModel *Organizer::organizerModel()
{
    if (!mOrganizerTreeModel)
        mOrganizerTreeModel = new OrganizerTreeModel(mManager);
    return mOrganizerTreeModel;
}

void Organizer::setInitialData()
{
    QOrganizerCollection collection;
    collection.setMetaData(QOrganizerCollection::KeyName, "Test Collection");
    mManager->saveCollection(&collection);

    QOrganizerEvent event;
    event.setStartDateTime(QDateTime::currentDateTime());
    event.setEndDateTime(QDateTime::currentDateTime().addSecs(60*60));
    event.setDescription("Test event");
    //event.setCollectionId(collection.id());
    mManager->saveItem(&event);
}

void Organizer::publish() const
{

}
