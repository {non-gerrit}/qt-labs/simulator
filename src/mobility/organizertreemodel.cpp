/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "organizertreemodel.h"
#ifdef QT_DEBUG
    #include <modeltest.h>
#endif

#include <QOrganizerManager>
#include <QOrganizerItemDetail>

#include <QStringList>
#include <QDebug>

using namespace QtMobility;

namespace {
    int itemIdFromPath(const QString &path) {
        return path.section("/", 1, 1).toInt();
    }

    int detailIdFromPath(const QString &path) {
        return path.section("/", 3, 3).toInt();
    }

    int valueKeyIdFromPath(const QString &path) {
        return path.section("/", 5, 5).toInt();
    }
}

OrganizerTreeModel::OrganizerTreeModel(QOrganizerManager *manager, QObject *parent)
    : QAbstractItemModel(parent), m_manager(manager), m_pseudoUuid(0), m_itemCache(1000)
{
#ifdef QT_DEBUG
    new ModelTest(this, this);
#endif
    connect(m_manager, SIGNAL(dataChanged()), this, SLOT(resetWholeData()));


    connect(m_manager, SIGNAL(itemsAdded(const QList<QOrganizerItemId>&)),
            this, SLOT(internalAddItems(const QList<QOrganizerItemId>&)));
    connect(m_manager, SIGNAL(itemsChanged(const QList<QOrganizerItemId>&)),
            this, SLOT(internalChangeItems(QList<QOrganizerItemId>)));
    connect(m_manager, SIGNAL(itemsRemoved(const QList<QOrganizerItemId>&)),
            this, SLOT(internalRemoveItems(QList<QOrganizerItemId>)));

//    connect(m_manager, SIGNAL(collectionsAdded(const QList<QOrganizerCollectionId>&)),
//            this, SLOT(slotCollectionsAdded()));
//    connect(m_manager, SIGNAL(collectionsChanged(const QList<QOrganizerCollectionId>&)),
//            this, SLOT(slotCollectionsChanged()));
//    connect(m_manager, SIGNAL(collectionsRemoved(const QList<QOrganizerCollectionId>&)),
//            this, SLOT(slotCollectionsRemoved()));
    initDataFromManager();
}

void OrganizerTreeModel::internalAddItems(const QList<QOrganizerItemId> &organizerItemIds)
{
    int oldCount = this->rowCount();
    beginInsertRows(QModelIndex(), oldCount, oldCount + organizerItemIds.size() - 1);
    m_organizerItemIds += organizerItemIds;
    endInsertRows();
}

void OrganizerTreeModel::internalChangeItems(const QList<QOrganizerItemId> &organizerItemIds)
{
    foreach (QOrganizerItemId changedId, organizerItemIds) {
        int changedIndex = m_organizerItemIds.indexOf(changedId);
        Q_ASSERT(changedIndex != -1);
        m_itemCache.remove(changedIndex);
        QModelIndex changedModelIndex = index(changedIndex, 0);
        emit dataChanged(changedModelIndex, changedModelIndex);
    }
}

void OrganizerTreeModel::internalRemoveItems(const QList<QOrganizerItemId> &organizerItemIds)
{
    foreach (QOrganizerItemId changedId, organizerItemIds) {
        int removedIndex = m_organizerItemIds.indexOf(changedId);
        Q_ASSERT(removedIndex != -1);
        beginRemoveRows(QModelIndex(), removedIndex, removedIndex);
        m_organizerItemIds.removeAt(removedIndex);
        m_itemCache.clear(); // the signal emitted from beginRemoveRows might have accessed the cache...
        endRemoveRows();
    }
}

void OrganizerTreeModel::initDataFromManager()
{
    m_organizerItemIds = m_manager->itemIds();
    if (m_manager->error() != QOrganizerManager::NoError)
    {
        qWarning() << tr("There was an error in the organizer manager. - ErrorCode: ") << m_manager->error();
    }
}

void OrganizerTreeModel::resetWholeData()
{
    beginResetModel();
    initDataFromManager();
    m_itemCache.clear();
    endResetModel();
}

QOrganizerItemDetail OrganizerTreeModel::detailFromPath(const QString &path) const
{
    Q_ASSERT(path.contains("QOrganizerItemDetail"));
    int detailId = detailIdFromPath(path);
    return itemFromPath(path).details().at(detailId);
}

QPair<QString, QVariant> OrganizerTreeModel::keyValuePairFromPath(const QString &path) const
{
    Q_ASSERT(path.contains("value"));

    QOrganizerItemDetail detail = detailFromPath(path);
    int valueKeyId = valueKeyIdFromPath(path);

    //maps the valuemap to ids
    QStringList keyList;
    if (m_valueKeyListHash.contains(path)) {
        keyList = m_valueKeyListHash.value(path);
    } else {
        keyList = detail.variantValues().keys();
        m_valueKeyListHash.insert(path, keyList);
    }
    QString key = keyList.at(valueKeyId);

    return QPair<QString, QVariant>(key, detail.variantValue(key));
}

QOrganizerItem OrganizerTreeModel::itemFromPath(const QString &path) const
{
    Q_ASSERT(path.contains("QOrganizerItemId"));
    return cachedItemFromInternalId(itemIdFromPath(path));
}

QOrganizerItem OrganizerTreeModel::cachedItemFromInternalId(int id) const
{
    if (m_itemCache.contains(id))
        return *m_itemCache.object(id);
    QOrganizerItem* item = new QOrganizerItem(m_manager->item(m_organizerItemIds.at(id)));
    m_itemCache.insert(id, item);
    return *item;
}

uint OrganizerTreeModel::cachedPostion(const QString &path) const
{
    Q_ASSERT(!path.isEmpty());
    if (m_pathUintHash.contains(path)) {
        return m_pathUintHash.value(path);
    }

    m_pathUintHash.insert(path, m_pseudoUuid);
    m_uintPathHash.insert(m_pseudoUuid, path);
    return m_pseudoUuid++;
}

QString OrganizerTreeModel::cachedPath(uint position) const
{
    return m_uintPathHash.value(position);
}


QVariant OrganizerTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        if (parent(index).isValid()) {
            QString path = cachedPath(index.internalId());
            if (path.contains("value")) { //it is a detail value
                QPair<QString, QVariant> keyValuePair = keyValuePairFromPath(path);
                if (detailFromPath(path).definitionName() != keyValuePair.first) {
                    QString value;
                    if (keyValuePairFromPath(path).second.type() == QVariant::Time)
                        value = keyValuePairFromPath(path).second.toTime().toString(Qt::SystemLocaleLongDate);
                    else if (keyValuePairFromPath(path).second.type() == QVariant::Date)
                        value = keyValuePairFromPath(path).second.toDate().toString(Qt::SystemLocaleLongDate);
                    else if (keyValuePairFromPath(path).second.type() == QVariant::DateTime)
                        value = keyValuePairFromPath(path).second.toDateTime().toString(Qt::SystemLocaleLongDate);
                    else
                        value = keyValuePairFromPath(path).second.toString();
                    return QString("%1: %2").arg(keyValuePair.first, value);
                } else {
                    return keyValuePairFromPath(path).second.toString();
                }
            } else if (path.contains("QOrganizerItemDetail")) { //it is a detail
                return detailFromPath(path).definitionName();
            }
        } else { //no parent means it is a root item
            QOrganizerItem organizerItem = cachedItemFromInternalId(index.row());
            return !organizerItem.displayLabel().isEmpty() ?
                            organizerItem.displayLabel() : organizerItem.description();
        }
    }

    return QVariant();
}

QVariant OrganizerTreeModel::headerData(int, Qt::Orientation, int) const
{
    return QVariant();
}

QModelIndex OrganizerTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();
    QString path;
    if (parent.isValid()) {
        QString parentPath = cachedPath(parent.internalId());
        if (parentPath.contains("QOrganizerItemDetail")) { //parent is a detail
            if (detailFromPath(parentPath).variantValues().count() <= row) {
                return QModelIndex();
            }

            path = parentPath + "/value/%1";
            path = path.arg(QString::number(row));
        } else if (parentPath.contains("QOrganizerItemId")) { //parent is an organizer item
            if (itemFromPath(parentPath).details().count() <= row) {
                return QModelIndex();
            }
            path = parentPath + "/QOrganizerItemDetail/%1";
            path = path.arg(QString::number(row));
        }
    } else { //no parent means it is a root item
        path = "QOrganizerItemId/%1";
        path = path.arg(QString::number(row));
    }
    return createIndex(row, column, cachedPostion(path));
}

QModelIndex OrganizerTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return QModelIndex();
    }
    QString path = cachedPath(index.internalId());
    int row = -1;
    if (path.contains("value")) { // QOrganizerItemId/3/QOrganizerItemDetail/4/value/7
        path = path.section("/", 0, 3);
        row = detailIdFromPath(path);
    }
    else if (path.contains("QOrganizerItemDetail")) { // QOrganizerItemId/3/QOrganizerItemDetail/4
        path = path.section("/", 0, 1);
        row = itemIdFromPath(path);
    } else {
        return QModelIndex();
    }
    return createIndex(row, 0, cachedPostion(path));
}

int OrganizerTreeModel::rowCount(const QModelIndex &aParent) const
{
    if (!aParent.isValid()) {
        return m_organizerItemIds.count(); //no parent means it is a root item
    }
    QString parentPath = cachedPath(aParent.internalId());
    if (parentPath.contains("value")) { //values don't have rows for childs
        return 0;
    }
    else if (parentPath.contains("QOrganizerItemDetail")) { //parent is a detail
        return detailFromPath(parentPath).variantValues().count();
    } else if (parentPath.contains("QOrganizerItemId")) { //parent is an organizer item
        return itemFromPath(parentPath).details().count();
    }
    Q_ASSERT(false); //This should not happen
    return 0;
}

int OrganizerTreeModel::columnCount(const QModelIndex &) const
{
    return 1;
}
