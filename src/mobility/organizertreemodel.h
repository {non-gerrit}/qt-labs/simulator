/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef ORGANIZERTREEMODEL_H
#define ORGANIZERTREEMODEL_H

#include <QAbstractItemModel>

#include <qmobilityglobal.h>
#include <QOrganizerItemId>
#include <QOrganizerItem>

#include <QCache>
#include <QDebug>

QTM_BEGIN_NAMESPACE
    class QOrganizerManager;
    class QOrganizerItemDetail;
    class QOrganizerItem;
QTM_END_NAMESPACE

QTM_USE_NAMESPACE

class OrganizerTreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit OrganizerTreeModel(QOrganizerManager *manager, QObject *parent = 0);
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

public slots:
    void showHashes()
    {
        for (QHash<uint, QString>::const_iterator it = m_uintPathHash.constBegin(); it != m_uintPathHash.constEnd(); ++it) {
            qDebug() << it.key() << " : " << it.value();
        }
    }

private slots:
    void resetWholeData();
    void internalAddItems(const QList<QOrganizerItemId> &organizerItemIds);
    void internalChangeItems(const QList<QOrganizerItemId> &organizerItemIds);
    void internalRemoveItems(const QList<QOrganizerItemId> &organizerItemIds);

private:
    void initDataFromManager();
    QOrganizerItemDetail detailFromPath(const QString &path) const;
    QPair<QString, QVariant> keyValuePairFromPath(const QString &path) const;
    QOrganizerItem itemFromPath(const QString &path) const;
    QOrganizerItem cachedItemFromInternalId(int id) const;
    uint cachedPostion(const QString &path) const;
    QString cachedPath(uint position) const;

private:
    QList<QOrganizerItemId> m_organizerItemIds;
    QOrganizerManager *m_manager;

    //these hashes more like caches so we need it in const methods
    mutable uint m_pseudoUuid;
    mutable QHash<uint, QString> m_uintPathHash;
    mutable QHash<QString, uint> m_pathUintHash;
    mutable QHash<QString, QStringList> m_valueKeyListHash;
    mutable QCache<int, QOrganizerItem> m_itemCache;
};

#endif // ORGANIZERTREEMODEL_H
