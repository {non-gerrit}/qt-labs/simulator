/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "ndefeditor.h"
#include "ui_ndefeditor.h"

#include <qndefrecord.h>
#include <qndefnfctextrecord.h>
#include <qndefnfcurirecord.h>
#include <qndefmessage.h>

#include <QtCore/QUrl>
#include <QtCore/QBuffer>

#include <QtGui/QMenu>
#include <QtGui/QListWidgetItem>
#include <QtGui/QFileDialog>
#include <QtGui/QImageReader>

QTM_USE_NAMESPACE

static QString imageFormatToMimeType(const QByteArray &format)
{
    if (format == "bmp")
        return QLatin1String("image/bmp");
    else if (format == "gif")
        return QLatin1String("image/gif");
    else if (format == "jpg" || format == "jpeg")
        return QLatin1String("image/jpeg");
    else if (format == "mng")
        return QLatin1String("video/x-mng");
    else if (format == "png")
        return QLatin1String("image/png");
    else if (format == "pbm")
        return QLatin1String("image/x-portable-bitmap");
    else if (format == "pgm")
        return QLatin1String("image/x-portable-graymap");
    else if (format == "ppm")
        return QLatin1String("image/x-portable-pixmap");
    else if (format == "tiff")
        return QLatin1String("image/tiff");
    else if (format == "xbm")
        return QLatin1String("image/x-xbitmap");
    else if (format == "xpm")
        return QLatin1String("image/x-xpixmap");
    else if (format == "svg")
        return QLatin1String("image/svg+xml");
    else
        return QString();
}

class RecordItem : public QListWidgetItem
{
public:
    RecordItem(const QNdefRecord &record);
    ~RecordItem();

    void setRecord(const QNdefRecord &record);
    QNdefRecord record() const;

    QVariant data(int role) const;

private:
    QNdefRecord m_record;
};

RecordItem::RecordItem(const QNdefRecord &record)
:   m_record(record)
{
}

RecordItem::~RecordItem()
{
}

void RecordItem::setRecord(const QNdefRecord &record)
{
    m_record = record;
}

QNdefRecord RecordItem::record() const
{
    return m_record;
}

QVariant RecordItem::data(int role) const
{
    if (role == Qt::DisplayRole) {
        if (m_record.isRecordType<QNdefNfcTextRecord>())
            return NdefEditor::tr("NFC Text");
        else if (m_record.isRecordType<QNdefNfcUriRecord>())
            return NdefEditor::tr("NFC URI");
        else if (m_record.typeNameFormat() == QNdefRecord::Mime)
            return NdefEditor::tr("MIME: <%1>").arg(QString::fromAscii(m_record.type().constData()));
        else
            return NdefEditor::tr("Unknown Record");
    }

    return QListWidgetItem::data(role);
}

NdefEditor::NdefEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NdefEditor)
{
    ui->setupUi(this);

    addMenu = new QMenu(this);
    addMenu->addAction(tr("NFC Text Record"), this, SLOT(addNfcTextRecord()));
    addMenu->addAction(tr("NFC URI Record"), this, SLOT(addNfcUriRecord()));
    addMenu->addAction(tr("MIME Image Record"), this, SLOT(addMimeRecord()));

    ui->addRecord->setMenu(addMenu);

    on_recordList_currentRowChanged(ui->recordList->currentRow());

    connect(ui->textData, SIGNAL(editingFinished()), this, SIGNAL(editingFinished()));
    connect(ui->textLocale, SIGNAL(editingFinished()), this, SIGNAL(editingFinished()));
    connect(ui->uriData, SIGNAL(editingFinished()), this, SIGNAL(editingFinished()));
}

NdefEditor::~NdefEditor()
{
    delete ui;
}

void NdefEditor::setNdefMessage(const QNdefMessage &message)
{
    while (ui->recordList->count())
        delete ui->recordList->takeItem(0);

    foreach (const QNdefRecord &record, message) {
        RecordItem *item = new RecordItem(record);
        ui->recordList->addItem(item);
    }
}

QNdefMessage NdefEditor::ndefMessage() const
{
    QNdefMessage message;

    for (int i = 0; i < ui->recordList->count(); ++i) {
        RecordItem *item = static_cast<RecordItem *>(ui->recordList->item(i));
        if (!item)
            continue;

        message.append(item->record());
    }

    return message;
}

void NdefEditor::on_recordList_currentRowChanged(int currentRow)
{
    ui->removeRecord->setEnabled(currentRow != -1);

    RecordItem *item = static_cast<RecordItem *>(ui->recordList->item(currentRow));
    if (!item) {
        ui->recordEditor->setCurrentIndex(0);
        return;
    }

    QNdefRecord record = item->record();
    if (record.isRecordType<QNdefNfcTextRecord>()) {
        QNdefNfcTextRecord textRecord(record);

        ui->recordEditor->setCurrentIndex(1);

        ui->textLocale->setText(textRecord.locale());
        ui->textData->setText(textRecord.text());
    } else if (record.isRecordType<QNdefNfcUriRecord>()) {
        QNdefNfcUriRecord uriRecord(record);

        ui->recordEditor->setCurrentIndex(2);

        ui->uriData->setText(uriRecord.uri().toString());
    } else if (record.typeNameFormat() == QNdefRecord::Mime) {
        if (record.type().startsWith("image/")) {
            ui->recordEditor->setCurrentIndex(3);

            QByteArray data = record.payload();
            QBuffer buffer(&data);
            buffer.open(QIODevice::ReadOnly);

            QImageReader reader(&buffer);

            ui->mimeImageType->setText(imageFormatToMimeType(reader.format()));

            ui->mimeImageImage->setPixmap(QPixmap::fromImageReader(&reader));
        }
    } else {
        ui->recordEditor->setCurrentIndex(0);
    }
}

void NdefEditor::addNfcTextRecord()
{
    QNdefNfcTextRecord record;

    RecordItem *item = new RecordItem(record);

    ui->recordList->addItem(item);
}

void NdefEditor::addNfcUriRecord()
{
    QNdefNfcUriRecord record;

    RecordItem *item = new RecordItem(record);

    ui->recordList->addItem(item);
}

void NdefEditor::addMimeRecord()
{
    QNdefRecord record;
    record.setTypeNameFormat(QNdefRecord::Mime);
    record.setType("image/jpeg");

    record.setType("image/png");

    RecordItem *item = new RecordItem(record);

    ui->recordList->addItem(item);
}

void NdefEditor::textRecordEditingFinished()
{
    RecordItem *item = static_cast<RecordItem *>(ui->recordList->currentItem());
    if (!item) {
        ui->recordEditor->setCurrentIndex(0);
        return;
    }

    QNdefNfcTextRecord record = item->record();
    record.setText(ui->textData->text());
    record.setLocale(ui->textLocale->text());

    item->setRecord(record);
}

void NdefEditor::uriRecordEditingFinished()
{
    RecordItem *item = static_cast<RecordItem *>(ui->recordList->currentItem());
    if (!item) {
        ui->recordEditor->setCurrentIndex(0);
        return;
    }

    QNdefNfcUriRecord record = item->record();
    record.setUri(QUrl(ui->uriData->text()));

    item->setRecord(record);
}

void NdefEditor::on_removeRecord_clicked()
{
    delete ui->recordList->takeItem(ui->recordList->currentRow());
}

void NdefEditor::on_mimeImageOpen_clicked()
{
    QString mimeDataFile = QFileDialog::getOpenFileName(this, tr("Select Image File"));
    if (mimeDataFile.isEmpty())
        return;

    QFile imageFile(mimeDataFile);
    if (!imageFile.open(QIODevice::ReadOnly))
        ui->mimeImageImage->clear();

    QByteArray imageData = imageFile.readAll();

    QBuffer buffer(&imageData);
    buffer.open(QIODevice::ReadOnly);

    QImageReader reader(&buffer);
    QString mimeType = imageFormatToMimeType(reader.format());
    ui->mimeImageType->setText(mimeType);

    QImage image = reader.read();

    ui->mimeImageImage->setPixmap(QPixmap::fromImage(image));

    RecordItem *item = static_cast<RecordItem *>(ui->recordList->currentItem());
    if (!item) {
        ui->recordEditor->setCurrentIndex(0);
        return;
    }

    QNdefRecord record = item->record();

    record.setType(mimeType.toAscii());
    record.setPayload(imageData);

    item->setRecord(record);
    emit editingFinished();
}
