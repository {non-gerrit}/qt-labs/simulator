/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef NDEFEDITOR_H
#define NDEFEDITOR_H

#include <qmobilityglobal.h>

#include <QtGui/QWidget>

namespace Ui {
    class NdefEditor;
}

class QMenu;

QTM_BEGIN_NAMESPACE
class QNdefMessage;
QTM_END_NAMESPACE

QTM_USE_NAMESPACE

class NdefEditor : public QWidget
{
    Q_OBJECT

public:
    explicit NdefEditor(QWidget *parent = 0);
    ~NdefEditor();

    QNdefMessage ndefMessage() const;
    void setNdefMessage(const QNdefMessage &message);

signals:
    void editingFinished();

private slots:
    void on_mimeImageOpen_clicked();
    void on_removeRecord_clicked();
    void on_recordList_currentRowChanged(int currentRow);

    void textRecordEditingFinished();
    void uriRecordEditingFinished();

    void addNfcTextRecord();
    void addNfcUriRecord();
    void addMimeRecord();

private:
    Ui::NdefEditor *ui;
    QMenu *addMenu;
};

#endif // NDEFEDITOR_H
