/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "nfctargetgenerator.h"
#include "ui_nfctargetgenerator.h"

#include "tagtype1.h"
#include "tagtype2.h"

#include <QtCore/QSettings>

#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>

#include <QtCore/QDebug>
#include <QtGui/QPushButton>

NfcTargetGenerator::NfcTargetGenerator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NfcTargetGenerator), tagEditor(0)
{
    QPushButton *button = 0;

    ui->setupUi(this);

    button = ui->buttonBox->addButton("New", QDialogButtonBox::ActionRole);
    button->setIcon(QIcon::fromTheme("document-new"));
    connect(button, SIGNAL(clicked()), this, SLOT(action_new()));

    button = ui->buttonBox->addButton("Open", QDialogButtonBox::ActionRole);
    button->setIcon(QIcon::fromTheme("document-open"));
    connect(button, SIGNAL(clicked()), this, SLOT(open_triggered()));

    button = ui->buttonBox->addButton("Save", QDialogButtonBox::ActionRole);
    button->setIcon(QIcon::fromTheme("document-save"));
    connect(button, SIGNAL(clicked()), this, SLOT(save_triggered()));

    button = ui->buttonBox->addButton("Save As", QDialogButtonBox::ActionRole);
    button->setIcon(QIcon::fromTheme("document-save-as"));
    connect(button, SIGNAL(clicked()), this, SLOT(save_As_triggered()));

    button = ui->buttonBox->addButton("Close", QDialogButtonBox::ActionRole);
    button->setIcon(QIcon::fromTheme("document-close"));
    connect(button, SIGNAL(clicked()), this, SLOT(reject()));

    on_targetType_currentIndexChanged(0);
}

NfcTargetGenerator::~NfcTargetGenerator()
{
    delete ui;
}

void NfcTargetGenerator::save(const QString &filename)
{
    if (ui->name->text().isEmpty()) {
      QMessageBox::critical(this, "Name is Empty", "Tag name field is empty.");
      return;
    }

    QSettings target(filename, QSettings::IniFormat);

    target.beginGroup(QLatin1String("Target"));
    target.setValue(QLatin1String("Name"), ui->name->text());

    switch (ui->targetType->currentIndex()) {
    case 0:
        target.setValue(QLatin1String("Type"), QLatin1String("TagType1"));
        break;
    case 1:
        target.setValue(QLatin1String("Type"), QLatin1String("TagType2"));
        break;
    default:
        target.setValue(QLatin1String("Type"), QLatin1String("Unknown"));
    }

    target.endGroup();

    tagEditor->save(&target);
}

void NfcTargetGenerator::on_targetType_currentIndexChanged(int index)
{
    if (tagEditor)
        delete tagEditor;

    switch (index) {
    case 0:
        tagEditor = new TagType1(this);
        break;
    case 1:
        tagEditor = new TagType2(this);
        break;
    default:
        tagEditor = 0;
    }

    if (tagEditor)
        ui->tagEditor->addWidget(tagEditor);
}

void NfcTargetGenerator::load(const QString &filename)
{
    QSettings target(filename, QSettings::IniFormat);

    target.beginGroup(QLatin1String("Target"));
    ui->name->setText(target.value(QLatin1String("Name")).toString());

    const QString tagType = target.value(QLatin1String("Type")).toString();
    if (tagType == QLatin1String("TagType1"))
        ui->targetType->setCurrentIndex(0);
    else if (tagType == QLatin1String("TagType2"))
        ui->targetType->setCurrentIndex(1);

    target.endGroup();

    tagEditor->load(&target);
}

static QString nfcDataPath()
{
    return QCoreApplication::applicationDirPath()+"/stubdata/nfctargets";
}

void NfcTargetGenerator::save_triggered()
{
    if (m_filename.isEmpty())
        save_As_triggered();
    else
        save(m_filename);
}

void NfcTargetGenerator::save_As_triggered()
{
    m_filename = QFileDialog::getSaveFileName(this, tr("Save File As"), nfcDataPath(),
                                              QLatin1String("*.nfc"));

    if (!m_filename.isEmpty())
        save(m_filename);
}

void NfcTargetGenerator::open_triggered()
{
    m_filename = QFileDialog::getOpenFileName(this, tr("Open File"), nfcDataPath(),
                                              QLatin1String("*.nfc"));

    if (!m_filename.isEmpty())
        load(m_filename);
}

void NfcTargetGenerator::action_new()
{
  on_targetType_currentIndexChanged(ui->targetType->currentIndex());
  m_filename.clear();
  ui->name->clear();
}
