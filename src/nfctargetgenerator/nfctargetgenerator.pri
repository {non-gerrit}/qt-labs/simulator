QT       *= core gui
CONFIG *= mobility
MOBILITY *= connectivity
INCLUDEPATH += src/nfctargetgenerator
DEPENDPATH += src/nfctargetgenerator

SOURCES += \
        nfctargetgenerator.cpp \
    tagtype1.cpp \
    tagtype2.cpp \
    ndefeditor.cpp \
    tlveditor.cpp \
    $$QT_MOBILITY_SOURCE_PATH/src/connectivity/nfc/qtlv.cpp \

HEADERS  += nfctargetgenerator.h \
    tagtype1.h \
    tagtype2.h \
    ndefeditor.h \
    tlveditor.h \
    tageditor.h \
    $$QT_MOBILITY_SOURCE_PATH/src/connectivity/nfc/qtlv_p.h \

FORMS    += nfctargetgenerator.ui \
    tagtype1.ui \
    tagtype2.ui \
    ndefeditor.ui \
    tlveditor.ui \

