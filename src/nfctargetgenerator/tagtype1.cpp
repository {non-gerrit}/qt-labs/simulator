/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "tagtype1.h"
#include "ui_tagtype1.h"
#include "../../src/connectivity/nfc/qtlv_p.h"

#include <math.h>

#include <QtCore/QSettings>
#include <QDebug>

QTM_USE_NAMESPACE

static inline quint8 blockByteToAddress(quint8 block, quint8 byte)
{
    return ((block & 0x0f) << 3) | (byte & 0x07);
}

TagType1::TagType1(QWidget *parent)
:   TagEditor(parent), ui(new Ui::TagType1)
{
    ui->setupUi(this);
}

TagType1::~TagType1()
{
    delete ui;
}

void TagType1::save(QSettings *settings) const
{
    settings->beginGroup(QLatin1String("TagType1"));

    quint8 hr0 = ui->hr0->value();

    settings->setValue(QLatin1String("HR0"), hr0);

    settings->setValue(QLatin1String("HR1"), ui->hr1->value());

    quint8 nmn = ui->ndefMessage->isChecked() ? 0xe1 : 0;

    double version = ui->nfcVersion->value();
    quint8 vno = (quint8(floor(version)) << 4) | quint8(10 * (version - floor(version)));

    quint8 tms = (ui->memorySize->value() / 8) - 1;

    quint8 ra;
    switch (ui->readAccess->currentIndex()) {
    case 0:
        ra = 0x00;
        break;
    default:
        ra = 0x00;
    }

    quint8 wa;
    switch (ui->writeAccess->currentIndex()) {
    case 0:
        wa = 0x00;
        break;
    case 1:
        wa = 0x0f;
        break;
    default:
        wa = 0x00;
    }

    quint8 rwa = (ra << 4) | wa;

    quint16 lock = 0;
    if (ui->lock0->isChecked())
        lock |= 0x01 << 0;
    if (ui->lock1->isChecked())
        lock |= 0x01 << 1;
    if (ui->lock2->isChecked())
        lock |= 0x01 << 2;
    if (ui->lock3->isChecked())
        lock |= 0x01 << 3;
    if (ui->lock4->isChecked())
        lock |= 0x01 << 4;
    if (ui->lock5->isChecked())
        lock |= 0x01 << 5;
    if (ui->lock6->isChecked())
        lock |= 0x01 << 6;
    if (ui->lock7->isChecked())
        lock |= 0x01 << 7;
    if (ui->lock8->isChecked())
        lock |= 0x01 << 8;
    if (ui->lock9->isChecked())
        lock |= 0x01 << 9;
    if (ui->lock10->isChecked())
        lock |= 0x01 << 10;
    if (ui->lock11->isChecked())
        lock |= 0x01 << 11;
    if (ui->lock12->isChecked())
        lock |= 0x01 << 12;
    if (ui->lock13->isChecked())
        lock |= 0x01 << 13;
    if (ui->lock14->isChecked())
        lock |= 0x01 << 14;

    QByteArray data;

    // uid
    data.append(QByteArray::fromHex(ui->uid->text().remove(QChar(':')).toAscii()));
    data.append('\0');
    Q_ASSERT(data.length() == 8);

    // capability
    data.append(nmn);
    data.append(vno);
    data.append(tms);
    data.append(rwa);

    Q_ASSERT(data.length() == 12);

    data.reserve(ui->memorySize->value());
    for (int i = data.length(); i < ui->memorySize->value(); ++i)
        data[i] = '\0';

    //qDebug() << "Data length = " << data.length();

    // lock bytes
    data[blockByteToAddress(0x0e, 0)] = lock & 0x0f;
    data[blockByteToAddress(0x0e, 1)] = lock >> 8;

    QTlvWriter writer(&data);
    writer.addReservedMemory(0, 12);    // skip uid, cc
    writer.addReservedMemory(104, 16);  // skip reserved block D, lock block E
    writer.addReservedMemory(120, 8);   // skip reserved block F

    for (int i = 0; i < ui->tlvEditor->tlvItemCount(); ++i) {
        QPair<quint8, QByteArray> tlv = ui->tlvEditor->tlvItemAt(i);
        writer.writeTlv(tlv.first, tlv.second);
    }

    if (data.length() != ui->memorySize->value()) {
        qDebug() << "Data length:" << data.length();
    }

    settings->setValue(QLatin1String("Data"), data);

    //qDebug() << data.toHex();

    settings->endGroup();
}

void TagType1::load(QSettings *settings)
{
    settings->beginGroup(QLatin1String("TagType1"));

    quint8 hr0 = settings->value(QLatin1String("HR0"), 0x11).toUInt();

    ui->hr0->setValue(hr0);
    if (hr0 == 0x11)
        ui->memoryStructure->setCurrentIndex(0);
    else if (hr0 >= 0x10 && hr0 <= 0x1f)
        ui->memoryStructure->setCurrentIndex(1);
    else
        ui->memoryStructure->setCurrentIndex(2);

    quint8 hr1 = settings->value(QLatin1String("HR1"), 0x00).toUInt();
    ui->hr1->setValue(hr1);

    QByteArray data = settings->value(QLatin1String("Data")).toByteArray();

    QByteArray uid = data.mid(0, 7).toHex();
    ui->uid->setText(uid);

    quint8 nmn = data.at(8);
    ui->ndefMessage->setChecked(nmn == 0xe1);

    quint8 vno = data.at(9);
    ui->nfcVersion->setValue((vno >> 4) + 0.1 *(vno & 0x0f));

    quint8 tms = data.at(10);
    ui->memorySize->setValue(8 * (tms + 1));

    quint8 rwa = data.at(11);
    switch (rwa >> 4) {
    case 0:
        ui->readAccess->setCurrentIndex(0);
        break;
    default:
        ui->readAccess->setCurrentIndex(0);
    }

    switch (rwa & 0x0f) {
    case 0:
        ui->writeAccess->setCurrentIndex(0);
        break;
    case 0x0f:
        ui->writeAccess->setCurrentIndex(1);
        break;
    default:
        ui->writeAccess->setCurrentIndex(0);
    }

    quint16 lock = (quint8(data[blockByteToAddress(0x0e, 1)]) << 8) |
                   quint8(data[blockByteToAddress(0x0e, 0)]);

    ui->lock0->setChecked(lock & 0x0001);
    ui->lock1->setChecked(lock & 0x0002);
    ui->lock2->setChecked(lock & 0x0004);
    ui->lock3->setChecked(lock & 0x0008);
    ui->lock4->setChecked(lock & 0x0010);
    ui->lock5->setChecked(lock & 0x0020);
    ui->lock6->setChecked(lock & 0x0040);
    ui->lock7->setChecked(lock & 0x0080);
    ui->lock8->setChecked(lock & 0x0100);
    ui->lock9->setChecked(lock & 0x0200);
    ui->lock10->setChecked(lock & 0x0400);
    ui->lock11->setChecked(lock & 0x0800);
    ui->lock12->setChecked(lock & 0x1000);
    ui->lock13->setChecked(lock & 0x2000);
    ui->lock14->setChecked(lock & 0x4000);

    //qDebug() << data.toHex();

    ui->tlvEditor->clear();

    QTlvReader reader(data);
    reader.addReservedMemory(0, 12);    // skip uid, cc
    reader.addReservedMemory(104, 16);  // skip reserved block D, lock block E
    reader.addReservedMemory(120, 8);   // skip reserved block F

    while (!reader.atEnd()) {
        if (!reader.readNext())
            break;

        ui->tlvEditor->addTlvItem(reader.tag(), reader.data());
    }

    settings->endGroup();
}

void TagType1::on_memoryStructure_currentIndexChanged(int index)
{
    switch (index) {
    case 0: // static
        ui->hr0->setRange(0x11, 0x11);
        ui->hr0->setEnabled(false);
        break;
    case 1: // dynamic
        ui->hr0->setRange(0x10, 0x1f);
        ui->hr0->setEnabled(true);
        break;
    case 2: // proprietary
        ui->hr0->setRange(0x00, 0xff);
        ui->hr0->setEnabled(true);
        break;
    default:
        ;
    }
}

