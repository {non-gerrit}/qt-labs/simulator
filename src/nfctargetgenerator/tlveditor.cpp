/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "tlveditor.h"
#include "ui_tlveditor.h"

#include <qndefmessage.h>

#include <QtGui/QMenu>

QTM_USE_NAMESPACE

class TlvItem : public QListWidgetItem
{
public:
    TlvItem(quint8 tag, const QByteArray &value);
    ~TlvItem();

    quint8 tagType() const;

    QByteArray value() const;
    void setValue(const QByteArray &value);

    QVariant data(int role) const;

private:
    quint8 m_tagType;
    QByteArray m_value;
};

TlvItem::TlvItem(quint8 tag, const QByteArray &value)
:   m_tagType(tag), m_value(value)
{
}

TlvItem::~TlvItem()
{
}

QVariant TlvItem::data(int role) const
{
    if (role == Qt::DisplayRole) {
        switch (m_tagType) {
        case 0x00:
            return TlvEditor::tr("Null");
        case 0x01:
            return TlvEditor::tr("Locked Memory");
        case 0x02:
            return TlvEditor::tr("Reserved Memory");
        case 0x03:
            return TlvEditor::tr("NDEF Message");
        case 0xfe:
            return TlvEditor::tr("Terminator");
        }
    }

    return QListWidgetItem::data(role);
}

quint8 TlvItem::tagType() const
{
    return m_tagType;
}

QByteArray TlvItem::value() const
{
    return m_value;
}

void TlvItem::setValue(const QByteArray &value)
{
    m_value = value;
}

TlvEditor::TlvEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TlvEditor)
{
    ui->setupUi(this);

    addMenu = new QMenu(this);
    addMenu->addAction(tr("NDEF Message"), this, SLOT(addNdefMessage()));
    addMenu->addAction(tr("NULL"), this, SLOT(addNull()));
    addMenu->addAction(tr("Terminator"), this, SLOT(addTerminator()));
    addMenu->addAction(tr("Reserved Memory"), this, SLOT(addReservedMemory()));
    addMenu->addAction(tr("Locked Memory"), this, SLOT(addLockedMemory()));

    ui->add->setMenu(addMenu);

    on_tlvList_currentRowChanged(ui->tlvList->currentRow());

    connect(ui->ndefEditor, SIGNAL(editingFinished()), this, SLOT(ndefMessageSaved()));

    connect(ui->pageAddress, SIGNAL(valueChanged(int)), this, SLOT(updateReservedMemory()));
    connect(ui->pageSize, SIGNAL(currentIndexChanged(int)), this, SLOT(updateReservedMemory()));
    connect(ui->byteOffset, SIGNAL(valueChanged(int)), this, SLOT(updateReservedMemory()));
    connect(ui->size, SIGNAL(valueChanged(int)), this, SLOT(updateReservedMemory()));

    connect(ui->lockPageAddress, SIGNAL(valueChanged(int)), this, SLOT(updateLockedMemory()));
    connect(ui->lockPageSize, SIGNAL(currentIndexChanged(int)), this, SLOT(updateLockedMemory()));
    connect(ui->lockByteOffset, SIGNAL(valueChanged(int)), this, SLOT(updateLockedMemory()));
    connect(ui->lockSize, SIGNAL(valueChanged(int)), this, SLOT(updateLockedMemory()));
    connect(ui->lockBytesPerBit, SIGNAL(currentIndexChanged(int)),
            this, SLOT(updateLockedMemory()));
}

TlvEditor::~TlvEditor()
{
    delete ui;
}

void TlvEditor::clear()
{
    while (ui->tlvList->count())
        delete ui->tlvList->takeItem(0);
}

void TlvEditor::addTlvItem(quint8 tag, const QByteArray &value)
{
    ui->tlvList->addItem(new TlvItem(tag, value));
}

QPair<quint8, QByteArray> TlvEditor::tlvItemAt(int i) const
{
    TlvItem *item = static_cast<TlvItem *>(ui->tlvList->item(i));
    if (!item)
        return qMakePair(quint8(0), QByteArray());

    return qMakePair(item->tagType(), item->value());
}

int TlvEditor::tlvItemCount() const
{
    return ui->tlvList->count();
}

void TlvEditor::addNdefMessage()
{
    TlvItem *item = new TlvItem(0x03, QByteArray());

    ui->tlvList->addItem(item);
}

void TlvEditor::addNull()
{
    TlvItem *item = new TlvItem(0x00, QByteArray());

    ui->tlvList->addItem(item);
}

void TlvEditor::addTerminator()
{
    TlvItem *item = new TlvItem(0xfe, QByteArray());

    ui->tlvList->addItem(item);
}

void TlvEditor::addReservedMemory()
{
    TlvItem *item = new TlvItem(0x02, QByteArray(3, '\0'));

    ui->tlvList->addItem(item);
}

void TlvEditor::addLockedMemory()
{
    TlvItem *item = new TlvItem(0x01, QByteArray(3, '\0'));

    ui->tlvList->addItem(item);
}

void TlvEditor::on_tlvList_currentRowChanged(int currentRow)
{
    ui->remove->setEnabled(currentRow != -1);

    TlvItem *item = static_cast<TlvItem *>(ui->tlvList->item(currentRow));
    if (!item) {
        ui->tlvEditor->setCurrentIndex(0);
        return;
    }

    switch (item->tagType()) {
    case 0x01:
        ui->tlvEditor->setCurrentIndex(3);
        setLockedMemory(item->value());
        break;
    case 0x02:
        ui->tlvEditor->setCurrentIndex(2);
        setReservedMemory(item->value());
        break;
    case 0x03:
        ui->tlvEditor->setCurrentIndex(1);
        ui->ndefEditor->setNdefMessage(QNdefMessage::fromByteArray(item->value()));
        break;
    default:
        ui->tlvEditor->setCurrentIndex(0);
    }
}

void TlvEditor::ndefMessageSaved()
{
    TlvItem *item = static_cast<TlvItem *>(ui->tlvList->currentItem());
    if (!item) {
        ui->tlvEditor->setCurrentIndex(0);
        return;
    }

    //qDebug() << "Saving ndef message tlv";

    QNdefMessage message = ui->ndefEditor->ndefMessage();
    item->setValue(message.toByteArray());
}

void TlvEditor::on_remove_clicked()
{
    delete ui->tlvList->takeItem(ui->tlvList->currentRow());
}

void TlvEditor::setReservedMemory(const QByteArray &value)
{
    quint8 position = value.at(0);
    int pageAddress = position >> 4;
    int byteOffset = position & 0x0f;

    int size = quint8(value.at(1));
    if (size == 0)
        size = 256;

    quint8 pageControl = value.at(2);
    int bytesPerPageN = pageControl & 0x0f;

    int byteAddress = bytesPerPageN ? pageAddress * (1 << bytesPerPageN) + byteOffset : 0;

    ui->pageAddress->setValue(pageAddress);
    ui->pageSize->setCurrentIndex(bytesPerPageN - 1);
    ui->byteOffset->setValue(byteOffset);
    ui->size->setValue(size);

    ui->byteAddress->setText(QString::number(byteAddress));
}

void TlvEditor::updateReservedMemory()
{
    int pageAddress = ui->pageAddress->value();
    int bytesPerPageN = ui->pageSize->currentIndex() + 1;
    int byteOffset = ui->byteOffset->value();
    int size = ui->size->value();

    int byteAddress = pageAddress * (1 << bytesPerPageN) + byteOffset;

    ui->byteAddress->setText(QString::number(byteAddress));

    TlvItem *item = static_cast<TlvItem *>(ui->tlvList->currentItem());
    if (!item) {
        ui->tlvEditor->setCurrentIndex(0);
        return;
    }

    QByteArray value;
    value.append((quint8(pageAddress) << 4) | (quint8(byteOffset) & 0x0f));
    value.append(quint8(size == 256 ? 0 : size));
    value.append(quint8(bytesPerPageN));

    item->setValue(value);
}

void TlvEditor::setLockedMemory(const QByteArray &value)
{
    quint8 position = value.at(0);
    int pageAddress = position >> 4;
    int byteOffset = position & 0x0f;

    int size = quint8(value.at(1));
    if (size == 0)
        size = 256;

    quint8 pageControl = value.at(2);
    int bytesPerPageN = pageControl & 0x0f;
    int bytesPerBitN = pageControl >> 4;

    int byteAddress = bytesPerPageN ? pageAddress * (1 << bytesPerPageN) + byteOffset : 0;

    ui->lockPageAddress->setValue(pageAddress);
    ui->lockPageSize->setCurrentIndex(bytesPerPageN - 1);
    ui->lockByteOffset->setValue(byteOffset);
    ui->lockSize->setValue(size);
    ui->lockBytesPerBit->setCurrentIndex(bytesPerBitN - 1);

    ui->byteAddress->setText(QString::number(byteAddress));
}

void TlvEditor::updateLockedMemory()
{
    int pageAddress = ui->lockPageAddress->value();
    int bytesPerPageN = ui->lockPageSize->currentIndex() + 1;
    int byteOffset = ui->lockByteOffset->value();
    int size = ui->lockSize->value();
    int bytesPerBitN = ui->lockBytesPerBit->currentIndex() + 1;

    int byteAddress = pageAddress * (1 << bytesPerPageN) + byteOffset;

    ui->lockByteAddress->setText(QString::number(byteAddress));

    TlvItem *item = static_cast<TlvItem *>(ui->tlvList->currentItem());
    if (!item) {
        ui->tlvEditor->setCurrentIndex(0);
        return;
    }

    QByteArray value;
    value.append((quint8(pageAddress) << 4) | (quint8(byteOffset) & 0x0f));
    value.append(quint8(size == 256 ? 0 : size));
    value.append(quint8(bytesPerBitN) << 4 | quint8(bytesPerPageN));

    item->setValue(value);
}

