/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef TLVEDITOR_H
#define TLVEDITOR_H

#include <QtGui/QWidget>

namespace Ui {
    class TlvEditor;
}

class QMenu;

class TlvEditor : public QWidget
{
    Q_OBJECT

public:
    explicit TlvEditor(QWidget *parent = 0);
    ~TlvEditor();

    void clear();
    void addTlvItem(quint8 tag, const QByteArray &value);
    QPair<quint8, QByteArray> tlvItemAt(int i) const;
    int tlvItemCount() const;

private slots:
    void on_remove_clicked();
    void on_tlvList_currentRowChanged(int currentRow);
    void addNdefMessage();
    void addNull();
    void addTerminator();
    void addReservedMemory();
    void addLockedMemory();

    void setReservedMemory(const QByteArray &value);
    void updateReservedMemory();

    void setLockedMemory(const QByteArray &value);
    void updateLockedMemory();

    void ndefMessageSaved();

private:
    Ui::TlvEditor *ui;
    QMenu *addMenu;
};

#endif // TLVEDITOR_H
