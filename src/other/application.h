/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef APPLICATION_H
#define APPLICATION_H

#include "qsimulatordata_p.h"
#include <QtGui/QMenuBar>
#include <QtCore/QObject>
#include <QtCore/QRect>
#include <QtCore/QHash>

class ApplicationManager;
class PhononManager;
class WidgetManager;
class DisplayWidget;
class QString;
class QLocalSocket;

class Application : public QObject
{
    Q_OBJECT
public:
    ~Application();
    inline QString name() const { return m_name; }
    inline int id() const { return m_id; }
    qint64 processId() const { return m_processId; }
    VersionStruct clientVersion() const { return m_clientVersion; }
    void setClientVersion(VersionStruct v) { m_clientVersion = v; }

    void setPhononManager(PhononManager *manager);
    void setWidgetManager(WidgetManager *wm);
    DisplayWidget* display() const;
    QLocalSocket *socket() { return sendSocket; }

    QString symbianSoftKeyText(int buttonNr) const;

signals:
    void widgetUpdate();
    void widgetDestroyed();

public slots:
    void readData();
    void kill();
    void triggerSymbianSoftKeyAction(int buttonNr);

private slots:
    // called remotely
    void disconnect();
    QtSimulatorPrivate::DisplayInfo displayInfo();
    QString systemFontDirectory();
    QtSimulatorPrivate::NewWindowInfo createWidget(int parentId, QRect geometry, QString title);

    void registerMenuBar(QtSimulatorPrivate::RemotePointer remoteMenuBar, int widgetId);
    void unregisterMenuBar(QtSimulatorPrivate::RemotePointer remoteMenuBar, int widgetId);
    void reparentMenuBar(QtSimulatorPrivate::RemotePointer remoteMenuBar, int oldWidgetId, int newWidgetId);
    void menuBarAddAction(QtSimulatorPrivate::RemotePointer remoteMenuBar, QVariantList actionData, QtSimulatorPrivate::RemotePointer beforeAction);
    void menuBarSyncAction(QVariantList actionData);
    void menuBarRemoveAction(QtSimulatorPrivate::RemotePointer remoteMenuBar, QtSimulatorPrivate::RemotePointer action);

    void setSymbianSoftKeys(QtSimulatorPrivate::RemoteQAction positive, QtSimulatorPrivate::RemoteQAction negative);

private:
    QMenu* provideMenu(QtSimulatorPrivate::RemotePointer remoteMenu);
private slots:
    void destroyMenu(QtSimulatorPrivate::RemotePointer remoteMenu);
    void menuAddAction(QtSimulatorPrivate::RemotePointer menu, QVariantList actionData, QtSimulatorPrivate::RemotePointer beforeAction);
    void menuSyncAction(QVariantList actionData);
    void menuRemoveAction(QtSimulatorPrivate::RemotePointer menu, QtSimulatorPrivate::RemotePointer action);
    void onMenuActionTriggered();

private:
    friend class ApplicationManager;
    explicit Application(const QString &name,
                         int id,
                         qint64 pid,
                         QLocalSocket *s,
                         ApplicationManager* m);

    struct SimulatorActionData
    {
        QAction* action;
        QtSimulatorPrivate::RemotePointer remotePointer;
    };

    SimulatorActionData variantListToSimulatorActionData(QVariantList& list, bool createAction = true);
    void updateActionFromVariantList(QAction* action, const QVariantList& list);

    QString m_name;
    int m_id;    // TODO: can we just use the process id?
    VersionStruct m_clientVersion;
    qint64 m_processId;
    QLocalSocket *receiveSocket, *sendSocket;
    QByteArray readBuffer;
    ApplicationManager *applicationManager;
    PhononManager* mPhononManager;
    WidgetManager *mWidgetManager;
    QHash<QtSimulatorPrivate::RemotePointer, QAction*> mActionHash;
    QHash<QtSimulatorPrivate::RemotePointer, QMenuBar*> mMenuBarHash;
    QHash<QtSimulatorPrivate::RemotePointer, QMenu*> mMenuHash;
    QtSimulatorPrivate::RemoteQAction mSymbianPositiveSoftKey;
    QtSimulatorPrivate::RemoteQAction mSymbianNegativeSoftKey;

    QTimer *killTimer;
};

#endif // APPLICATION_H
