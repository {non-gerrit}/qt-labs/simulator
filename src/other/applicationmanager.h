/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef APPLICATIONMANAGER_H
#define APPLICATIONMANAGER_H

#include "qsimulatordata_p.h"
#include <QtCore/QObject>
#include <QtCore/QSize>

class Application;
class ApplicationTableWidget;
class PhononManager;
class WidgetManager;
class QLocalServer;
class QMenuBar;
class ApplicationUi;
struct DeviceData;

class ApplicationManager : public QObject
{
    Q_OBJECT
public:
    ApplicationManager(const VersionStruct &simulatorVersion, QObject* parent = 0);
    ~ApplicationManager();

    Application* applicationForId(int);
    void setPhononManager(PhononManager *manager);
    void setWidgetManager(WidgetManager *widMan);
    void setApplicationUi(ApplicationUi *applicationUi);
    inline QWidget* logWidget() { return mConfigWidget; }

    QString fontDirectory();
    const QtSimulatorPrivate::DisplayInfo &displayInfo();

    bool hasApplications() const;
    bool simulatorAlreadyStarted() const;
    bool initializeFontDirectory();

signals:
    void applicationRegistered(Application*);
    void applicationUnRegistered(int id);
    void applicationTimedOut(int id);
    void firstAppRegistered();
    void lastAppUnregistered();
    void softkeyTextSizeChanged(double newSize);

public slots:
    void handleConnection();
    void registerApplication(Application*);
    void unregisterApplication(int id);
    void killApplication(int id);
    void killCurrentApplication();
    void killAllApplications();
    void updateDisplayInformation(const QSize &resolution, const DeviceData &device);
    void updateMobilityVersion(int appId, VersionStruct version);
    void recommendRestartIfAppRunning();

protected slots:
    void watchDogTimer();

private:
    void watchDogBark(Application* deadApp);

private:
    VersionStruct mVersion;
    QLocalServer *mServer;
    PhononManager *mPhononManager;
    WidgetManager *mWidgetManager;
    QList<Application*> mApps;
    ApplicationTableWidget *mTableWidget;
    QWidget *mConfigWidget;
    QString mFontDirectory;
    ApplicationUi *mApplicationUi;

    bool mSimulatorStarted;

    QtSimulatorPrivate::DisplayInfo mDisplayInfo;
};

#endif // APPLICATIONMANAGER_H
