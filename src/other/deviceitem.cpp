/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "deviceitem.h"
#include "displaywidget.h"

#include <qmath.h>
#include <QtCore/QState>
#include <QtCore/QSignalTransition>
#include <QtCore/QPropertyAnimation>
#include <QtGui/QGraphicsSceneMouseEvent>
#include <QtGui/QGraphicsScene>
#include <QtGui/QCursor>
#include <QtGui/QPainter>

DeviceItem::DeviceItem(QGraphicsItem *parent)
    : QGraphicsObject(parent)
    , mDeviceOrientation(topUp)
    , mScreenOrientation(topUp)
    , mFixedOrientation(Qt::WA_AutoOrientation)
    , mDisplay(0)
    , mMockup(0)
    , mMaemoNavigationButton(0)
    , mCurrentMaemoNavigationMode(maemoClose)
    , mMenu(0)
{
    qRegisterMetaType<DeviceData>("DeviceData");

    mMockup = new QGraphicsPixmapItem(QPixmap(), this);
    mMockup->setFlag(QGraphicsItem::ItemIsFocusable);

    mDisplay = new DisplayWidget(mMockup);
    mMockup->setFocusProxy(mDisplay);

    mMenu = new MenuPixmapItem(QPixmap(), mDisplay);
    mMenu->setFlag(QGraphicsItem::ItemIsFocusable);
    mMenu->setFocusProxy(mDisplay);
    mMenu->setZValue(SIMULATOR_MENU_Z);

    mMaemoNavigationButton = new PixmapButton(mMenu);
    mMaemoNavigationButton ->setZValue(SIMULATOR_MENU_Z);
    connect(mMaemoNavigationButton, SIGNAL(pressed()), this, SIGNAL(closeWindowPressed()));

    const int rotationDuration = 250;

    mDefaultAnimation = new QPropertyAnimation(this, "rotation", this);
    mDefaultAnimation->setDuration(rotationDuration);
    connect(mDefaultAnimation, SIGNAL(finished()), this, SLOT(newViewSize()));
    connect(mDefaultAnimation, SIGNAL(finished()), this, SLOT(updateScreenOrientation()));
}

DeviceItem::~DeviceItem()
{
    mMockup->setFocusProxy(0);
    mMenu->setFocusProxy(0);
}

DisplayWidget *DeviceItem::display()
{
    return mDisplay;
}

Orientation DeviceItem::deviceOrientation() const
{
    return mDeviceOrientation;
}

Orientation DeviceItem::screenOrientation() const
{
    return mScreenOrientation;
}

QList<Orientation> DeviceItem::supportedScreenOrientations() const
{
    return mDeviceData.menus.keys();
}

QRect DeviceItem::availableGeometry() const
{
    return mAvailableGeometry;
}

QRectF DeviceItem::boundingRect() const
{
    return QRectF(QPointF(-mSize.width()/2, -mSize.height()/2), mSize);
}

void DeviceItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void DeviceItem::changeDevice(const DeviceData &data)
{
    mDeviceData = data;

    // if the the device mockup was not yet read, do so now
    if (!mDeviceData.mockup) {
        mDeviceData.mockup = QSharedPointer<QPixmap>(new QPixmap(mDeviceData.mockupPath));
        if (!mDeviceData.mockup || mDeviceData.mockup->isNull())
            qFatal("Could not read mockup image %s", qPrintable(mDeviceData.mockupPath));
    }

    // if the the menu mockups were not yet read, do so now
    foreach(const Orientation &orientation, mDeviceData.menus.keys())
    {
        MenuData menuData = mDeviceData.menus[orientation];
        if (!menuData.pixmapPath.isNull()
            && !menuData.pixmapPath.isEmpty()
            && !menuData.pixmap) {
                menuData.pixmap = QSharedPointer<QPixmap>(new QPixmap(menuData.pixmapPath));
                if (!menuData.pixmap || menuData.pixmap->isNull())
                    qFatal("Could not read menu image %s", qPrintable(menuData.pixmapPath));
                mDeviceData.menus[orientation].pixmap = menuData.pixmap;
        }
    }

    // maemo navigation button setup
    if (!mDeviceData.maemoNavigationBackButton.isNull()
        && !mDeviceData.maemoNavigationBackButton.isEmpty()
        && !mDeviceData.maemoBackPixmap) {
            mDeviceData.maemoBackPixmap = QSharedPointer<QPixmap>(new QPixmap(mDeviceData.maemoNavigationBackButton));
            if (!mDeviceData.maemoBackPixmap || mDeviceData.maemoBackPixmap->isNull())
                qFatal("Could not read maemo back button image %s", qPrintable(mDeviceData.maemoNavigationBackButton));
    }
    if (!mDeviceData.maemoNavigationCloseButton.isNull()
        && !mDeviceData.maemoNavigationCloseButton.isEmpty()
        && !mDeviceData.maemoClosePixmap) {
            mDeviceData.maemoClosePixmap = QSharedPointer<QPixmap>(new QPixmap(mDeviceData.maemoNavigationCloseButton));
            if (!mDeviceData.maemoClosePixmap || mDeviceData.maemoClosePixmap->isNull())
                qFatal("Could not read maemo close button image %s", qPrintable(mDeviceData.maemoNavigationCloseButton));
    }
    setMaemoNavigationMode(mCurrentMaemoNavigationMode);

    // self size
    prepareGeometryChange();
    mSize = mDeviceData.mockup->size();

    newWindowSize();
    newViewSize();

    // mockup setup
    mMockup->setPixmap(*mDeviceData.mockup);
    mMockup->setPos(-mSize.width()/2, -mSize.height()/2);
    mMockup->setTransformOriginPoint(mSize.width()/2, mSize.height()/2);

    // button setup
    qDeleteAll(mButtons);
    mButtons.clear();
    foreach (const Button &buttonmDeviceData, mDeviceData.buttons) {
        KeyButton *button = new KeyButton(buttonmDeviceData, mMockup);
        connect(button, SIGNAL(pressed(Qt::Key, QString)), this, SIGNAL(buttonPressed(Qt::Key, QString)));
        connect(button, SIGNAL(released(Qt::Key)), this, SIGNAL(buttonReleased(Qt::Key)));
        mButtons.append(button);
    }

    updateScreenOrientation();
}

void DeviceItem::changeOrientation(Orientation newOrientation, bool rotateScreen)
{
    bool animatedRotate = needsAnimation(mDeviceOrientation, newOrientation);
    mDeviceOrientation = newOrientation;
    if (mFixedOrientation == Qt::WA_AutoOrientation && rotateScreen) {
        mScreenOrientation = newOrientation;
        if (!animatedRotate)
            updateScreenOrientation();
    }

    if (animatedRotate)
        initiateRotate();

    emit orientationChanged(newOrientation);
}

bool DeviceItem::needsAnimation(Orientation oldValue, Orientation newValue)
{
    if (oldValue == newValue)
        return false;
    return true;
}

void DeviceItem::setInitialRotation(Orientation device, Orientation screen)
{
    mDeviceOrientation = device;
    mScreenOrientation = screen;
    switch (device) {
        case topUp:
            setRotation(0);
            break;
        case topDown:
            setRotation(180);
            break;
        case leftUp:
            setRotation(90);
            break;
        case rightUp:
            setRotation(-90);
            break;
    }
    newViewSize();
    updateScreenOrientation();
    emit orientationChanged(device);
}

void DeviceItem::updateScreenOrientation()
{
    QSize newResolution = mDeviceData.resolution;

    if (mFixedOrientation == Qt::WA_AutoOrientation) {
        // switch to a valid screen orientation if invalid
        if (!(mScreenOrientation & mDeviceData.supportedOrientations) && !mDeviceData.menus.isEmpty())
            mScreenOrientation = mDeviceData.menus.begin().key();
    } else if (mFixedOrientation == Qt::WA_LockLandscapeOrientation) {
        mScreenOrientation = mDeviceData.landscapeOrientation;
    } else if (mFixedOrientation == Qt::WA_LockPortraitOrientation) {
        mScreenOrientation = mDeviceData.portraitOrientation;
    }

    switch (mScreenOrientation) {
    case topUp:
        mDisplay->setRotation(0);
        mDisplay->setSize(mDeviceData.resolution);
        mDisplay->setX(mDeviceData.offset.x());
        mDisplay->setY(mDeviceData.offset.y());
        break;
    case rightUp:
        mDisplay->setRotation(90);
        newResolution.transpose();
        mDisplay->setSize(newResolution);
        mDisplay->setX(mDeviceData.offset.x() + mDeviceData.resolution.width());
        mDisplay->setY(mDeviceData.offset.y());
        break;
    case topDown:
        mDisplay->setRotation(180);
        mDisplay->setSize(mDeviceData.resolution);
        mDisplay->setX(mDeviceData.offset.x() + mDeviceData.resolution.width());
        mDisplay->setY(mDeviceData.offset.y() + mDeviceData.resolution.height());
        break;
    case leftUp:
        mDisplay->setRotation(270);
        newResolution.transpose();
        mDisplay->setSize(newResolution);
        mDisplay->setX(mDeviceData.offset.x());
        mDisplay->setY(mDeviceData.offset.y() + mDeviceData.resolution.height());
        break;
    }

    updateMenuPositionAndSize();
    mDeviceData.availableGeometry = mAvailableGeometry;

    emit deviceChanged(newResolution, mDeviceData);
    emit deviceChanged(mDeviceData.mockup->width() < mDeviceData.mockup->height());
}

void DeviceItem::initiateRotate()
{
    mDefaultAnimation->stop();

    // set the view to the maximum size
    int rotationSide = rotationSideLength() * scale();
    setPos(rotationSide/2, rotationSide/2);
    emit viewSizeRequired(QSize(rotationSide, rotationSide));

    switch (mDeviceOrientation) {
        case topUp:
            mDefaultAnimation->setEndValue(0);
            break;
        case topDown:
            mDefaultAnimation->setEndValue(180);
            break;
        case leftUp:
            mDefaultAnimation->setEndValue(90);
            break;
        case rightUp:
            mDefaultAnimation->setEndValue(-90);
            break;
    }
    mDefaultAnimation->start();
}

void DeviceItem::changeScaleFactor(qreal newScaleFactor)
{
    setScale(newScaleFactor);

    newWindowSize();
    newViewSize();
}

void DeviceItem::setSymbianSoftKeyText(int buttonNumber, const QString &text)
{
    foreach (SymbianSoftKeyButton *b, mSymbianSoftKeyButtons) {
        if (b->buttonData().buttonNumber == buttonNumber)
            b->setText(text);
    }
}

void DeviceItem::setSymbianSoftKeyTextSize(double size)
{
    foreach (SymbianSoftKeyButton *b, mSymbianSoftKeyButtons) {
        b->setTextSize(size);
    }
}

void DeviceItem::setMaemoNavigationMode(MaemoNavigationMode mode)
{
    if (mode == maemoClose && mDeviceData.maemoClosePixmap)
        mMaemoNavigationButton->setPixmap(*mDeviceData.maemoClosePixmap);
    else if (mode == maemoBack && mDeviceData.maemoBackPixmap)
        mMaemoNavigationButton->setPixmap(*mDeviceData.maemoBackPixmap);
    else
        mMaemoNavigationButton->setPixmap(QPixmap());
    mCurrentMaemoNavigationMode = mode;
}

void DeviceItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    mDragOffset = event->scenePos().toPoint();
}

void DeviceItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
#ifndef Q_WS_X11
    emit drag(event->screenPos() - mDragOffset);
#else
    ungrabMouse();
    emit drag(event->screenPos());
#endif
}

void DeviceItem::newViewSize()
{
    QSizeF windowSize = mSize * scale();
    if (mDeviceOrientation == leftUp || mDeviceOrientation == rightUp)
        windowSize.transpose();
    setPos(windowSize.width()/2, windowSize.height()/2);
    emit viewSizeRequired(windowSize.toSize());
}

void DeviceItem::newWindowSize()
{
    int rotationSide = rotationSideLength() * scale();
    emit sizeChanged(QSize(rotationSide, rotationSide));
}

int DeviceItem::rotationSideLength()
{
#ifdef Q_OS_WIN
    return sqrt(pow(mSize.width(), 2) + pow(mSize.height(), 2));
#else
    return std::max(mSize.width(), mSize.height());
#endif
}

void DeviceItem::updateMenuPositionAndSize()
{
    QRect displayRect;
    MenuData menuData;
    switch (mScreenOrientation) {
        case topUp:
        case topDown:
            displayRect.setRect(0, 0, mDeviceData.resolution.width(), mDeviceData.resolution.height());
            mMaemoNavigationButton->setPos(mDeviceData.maemoNavigationButtonLandscape);
            break;
        case leftUp:
        case rightUp:
            displayRect.setRect(0, 0, mDeviceData.resolution.height(), mDeviceData.resolution.width());
            mMaemoNavigationButton->setPos(mDeviceData.maemoNavigationButtonPortrait);
            break;
    }
    menuData = mDeviceData.menus.value(mScreenOrientation);
    if (menuData.pixmap) {
        mMenu->setPixmap(*menuData.pixmap);
        mAvailableGeometry = menuData.availableGeometry;
    } else {
        mMenu->setPixmap(QPixmap());
        mAvailableGeometry = displayRect;
    }

    QHash<int, QString> oldSoftKeyTexts;
    foreach (SymbianSoftKeyButton *button, mSymbianSoftKeyButtons)
        oldSoftKeyTexts.insert(button->buttonData().buttonNumber, button->text());
    qDeleteAll(mSymbianSoftKeyButtons);
    mSymbianSoftKeyButtons.clear();
    foreach (const SymbianSoftKeyButtonData &data, mDeviceData.symbianSoftKeys) {
        if (data.orientation == mScreenOrientation) {
            SymbianSoftKeyButton *button = new SymbianSoftKeyButton(data, mMenu);
            connect(button, SIGNAL(clicked(int)), this, SIGNAL(symbianSoftKeyClicked(int)));
            button->setText(oldSoftKeyTexts.value(data.buttonNumber));
            mSymbianSoftKeyButtons.append(button);
        }
    }

    emit offsetChanged(mAvailableGeometry.topLeft());
}

DeviceButton::DeviceButton(QGraphicsItem *parent)
    : QGraphicsObject(parent)
{
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocusProxy(parent);
}

DeviceButton::~DeviceButton()
{
    setFocusProxy(0);
}

QRectF DeviceButton::boundingRect() const
{
    return QRectF(QPointF(), mSize);
}

void DeviceButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void DeviceButton::setSize(const QSizeF &size)
{
    mSize = size;
}

QSizeF DeviceButton::size() const
{
    return mSize;
}

void DeviceButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    emit pressed();
}

void DeviceButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    emit released();
}

void PixmapButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    emit pressed();
}

void PixmapButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    emit released();
}

KeyButton::KeyButton(const Button &button, QGraphicsItem *parent)
    : QGraphicsObject(parent)
    , mButton(button)
    , mMouseOver(false)
{
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocusProxy(parent);
    setPos(button.area.topLeft());
    setCursor(Qt::PointingHandCursor);
    setAcceptHoverEvents(true);
}

KeyButton::~KeyButton()
{
    setFocusProxy(0);
}

QRectF KeyButton::boundingRect() const
{
    return QRectF(QPointF(), mButton.area.size());
}

void KeyButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if (mMouseOver) {
        painter->setRenderHint(QPainter::Antialiasing);
        painter->setBrush(QColor(255, 255, 0, 50));
        painter->drawRoundedRect(boundingRect().adjusted(.5, .5, -1, -1), 5, 5);
    }
}

void KeyButton::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    mMouseOver = true;
    update();
}

void KeyButton::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    mMouseOver = false;
    update();
}

void KeyButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    emit pressed(mButton.key, mButton.text);
}

void KeyButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    emit released(mButton.key);
}

SymbianSoftKeyButton::SymbianSoftKeyButton(const SymbianSoftKeyButtonData &button, QGraphicsItem *parent)
    : QGraphicsObject(parent)
    , mButton(button)
    , mTextSize(12)
    , mMouseOver(false)
{
    setFlag(QGraphicsItem::ItemIsFocusable);
    setFocusProxy(parent);
    setPos(button.area.topLeft());
    setCursor(Qt::PointingHandCursor);
    setAcceptHoverEvents(true);
}

SymbianSoftKeyButton::~SymbianSoftKeyButton()
{
    setFocusProxy(0);
}

QRectF SymbianSoftKeyButton::boundingRect() const
{
    return QRectF(QPointF(), mButton.area.size());
}

void SymbianSoftKeyButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->setPen(Qt::white);
    QFont f = painter->font();
    f.setPointSizeF(mTextSize);
    painter->setFont(f);
    painter->drawText(boundingRect(), Qt::AlignHCenter | Qt::AlignVCenter, mText);
    if (mMouseOver) {
        painter->setPen(Qt::NoPen);
        painter->setBrush(QColor(255, 255, 0, 50));
        painter->drawRoundedRect(boundingRect(), 5, 5);
    }
}

SymbianSoftKeyButtonData SymbianSoftKeyButton::buttonData() const
{
    return mButton;
}

void SymbianSoftKeyButton::setText(const QString &text)
{
    mText = text;
    update();
}

QString SymbianSoftKeyButton::text() const
{
    return mText;
}

void SymbianSoftKeyButton::setTextSize(double size)
{
    mTextSize = size;
    update();
}

void SymbianSoftKeyButton::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    mMouseOver = true;
    update();
}

void SymbianSoftKeyButton::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    mMouseOver = false;
    update();
}

void SymbianSoftKeyButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
}

void SymbianSoftKeyButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    emit clicked(mButton.buttonNumber);
}

void DeviceItem::updateOrientation(Qt::WidgetAttribute orientation)
{
    mFixedOrientation = orientation;
    updateScreenOrientation();
    emit orientationLocked(orientation != Qt::WA_AutoOrientation);
}
