/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef DEVICEITEM_H
#define DEVICEITEM_H

#include <QtGui/QGraphicsObject>
#include <QtCore/QStateMachine>
#include <QtCore/QHash>

#define SIMULATOR_MENU_Z 10000

struct Button
{
    QRectF area;
    QString text;
    Qt::Key key;
};

enum Orientation {
    topUp       = 0x0000001,
    topDown     = 0x0000002,
    leftUp      = 0x0000004,
    rightUp     = 0x0000008
};
Q_DECLARE_FLAGS(SupportedOrientations, Orientation)

struct MenuData {
    QString pixmapPath;
    mutable QSharedPointer<QPixmap> pixmap;
    QRect availableGeometry;
};

struct SymbianSoftKeyButtonData {
    enum ButtonType {
        PositiveButton = 0,
        NegativeButton = 1,
        MaxButtons = 5
    };
    Orientation orientation;
    QRectF area;
    int buttonNumber;
};

enum MaemoNavigationMode {
    maemoBack,
    maemoClose
};

struct DeviceData
{
    DeviceData()
        : diagonalInInch(0.)
        , defaultFontSize(12)
        , forceDpi(-1)
        , landscapeOrientation(topUp)
        , portraitOrientation(topUp)
    {}

    QString name;
    QSize resolution;
    QRect availableGeometry;
    qreal diagonalInInch;
    QString mockupPath;
    mutable QSharedPointer<QPixmap> mockup;
    QPoint offset;
    int defaultFontSize;
    int forceDpi;
    QString maemoNavigationBackButton;
    mutable QSharedPointer<QPixmap> maemoBackPixmap;
    QString maemoNavigationCloseButton;
    mutable QSharedPointer<QPixmap> maemoClosePixmap;
    QPointF maemoNavigationButtonPortrait;
    QPointF maemoNavigationButtonLandscape;
    QList<Button> buttons;
    QString style;
    QString styleTheme;
    SupportedOrientations supportedOrientations;
    QHash<Orientation, MenuData> menus;
    QList<SymbianSoftKeyButtonData> symbianSoftKeys;
    Orientation landscapeOrientation;
    Orientation portraitOrientation;
};

class DisplayWidget;
class QState;
class QPropertyAnimation;

class MenuPixmapItem: public QGraphicsPixmapItem
{
public:
    MenuPixmapItem(const QPixmap &pixmap, QGraphicsItem *parent = 0)
        : QGraphicsPixmapItem(pixmap, parent)
    {}
    enum {MenuType = UserType + 2};
    int type() const {return MenuType;}
};

class DeviceButton: public QGraphicsObject
{
    Q_OBJECT
public:
    explicit DeviceButton(QGraphicsItem *parent = 0);
    virtual ~DeviceButton();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    void setSize(const QSizeF &size);
    QSizeF size() const;

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    QSizeF mSize;

signals:
    void pressed() const;
    void released() const;
};

class PixmapButton: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    explicit PixmapButton(QGraphicsItem *parent = 0) : QGraphicsPixmapItem(parent) {};
    virtual ~PixmapButton() {};

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

signals:
    void pressed() const;
    void released() const;

};

class KeyButton: public QGraphicsObject
{
    Q_OBJECT
public:
    explicit KeyButton(const Button &button, QGraphicsItem *parent = 0);
    virtual ~KeyButton();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

protected:
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent  *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent  *event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    Button mButton;
    bool mMouseOver;

signals:
    void pressed(Qt::Key, QString) const;
    void released(Qt::Key) const;
};

class SymbianSoftKeyButton: public QGraphicsObject
{
    Q_OBJECT
public:
    explicit SymbianSoftKeyButton(const SymbianSoftKeyButtonData &button, QGraphicsItem *parent = 0);
    virtual ~SymbianSoftKeyButton();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    SymbianSoftKeyButtonData buttonData() const;

    void setText(const QString &text);
    QString text() const;

    void setTextSize(double size);

protected:
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent  *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent  *event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

signals:
    void clicked(int buttonNr);

private:
    SymbianSoftKeyButtonData mButton;
    QString mText;
    double mTextSize;
    bool mMouseOver;
};

class DeviceItem: public QGraphicsObject
{
    Q_OBJECT
public:
    DeviceItem(QGraphicsItem *parent = 0);
    ~DeviceItem();

    DisplayWidget *display();

    Orientation deviceOrientation() const;
    Orientation screenOrientation() const;
    QList<Orientation> supportedScreenOrientations() const;
    QRect availableGeometry() const;

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

public slots:
    void changeDevice(const DeviceData &data);
    void changeOrientation(Orientation newOrientation, bool rotateScreen);
    void updateOrientation(Qt::WidgetAttribute orientation);
    void setInitialRotation(Orientation device, Orientation screen);
    void changeScaleFactor(qreal newScaleFactor);
    void setSymbianSoftKeyText(int buttonNumber, const QString &text);
    void setSymbianSoftKeyTextSize(double);
    void setMaemoNavigationMode(MaemoNavigationMode mode);

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

private:
    void newWindowSize();
    int rotationSideLength();
    void updateMenuPositionAndSize();
    bool needsAnimation(Orientation oldValue, Orientation newValue);
    void applyNewOrientation(Orientation orientation);

    Orientation mDeviceOrientation;
    Orientation mScreenOrientation;
    Qt::WidgetAttribute mFixedOrientation;

    DeviceData mDeviceData;

    QSizeF mSize;
    QRect mAvailableGeometry;
    DisplayWidget *mDisplay;
    QGraphicsPixmapItem *mMockup;

    QPropertyAnimation *mDefaultAnimation;
    QPoint mDragOffset;

    QList<KeyButton*> mButtons;
    PixmapButton *mMaemoNavigationButton;
    MaemoNavigationMode mCurrentMaemoNavigationMode;
    QList<SymbianSoftKeyButton *> mSymbianSoftKeyButtons;
    MenuPixmapItem *mMenu;

private slots:
    void initiateRotate();
    void newViewSize();
    void updateScreenOrientation();

signals:
    void sizeChanged(const QSize &size);
    void drag(const QPoint &to); // to is the new top-left point of the view in screen coordinates
    void viewSizeRequired(const QSize &size);

    void deviceChanged(const QSize &resolution, const DeviceData &device);
    void deviceChanged(bool mainOrientationPortrait);
    void offsetChanged(const QPoint &newOffset);

    void buttonPressed(Qt::Key key, QString text);
    void buttonReleased(Qt::Key key);
    void closeWindowPressed();
    void symbianSoftKeyClicked(int buttonNr);

    void orientationChanged(Orientation to);
    void orientationLocked(bool locked);

    friend class InputScriptInterface;
};

#endif //DEVICEITEM_H
