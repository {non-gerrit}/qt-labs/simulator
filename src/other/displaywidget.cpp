/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "displaywidget.h"
#include "widgetmanager.h"
#include "widget.h"

#include <QtGui/QKeyEvent>
#include <QtGui/QPainter>

DisplayWidget::DisplayWidget(QGraphicsItem *parent)
: QGraphicsItem(parent)
{
    setFlags(QGraphicsItem::ItemClipsChildrenToShape | QGraphicsItem::ItemIsFocusable);
}

DisplayWidget::~DisplayWidget()
{
    setFocusProxy(0);
}

int DisplayWidget::width() const
{
    return mSize.width();
}

int DisplayWidget::height() const
{
    return mSize.height();
}

QSize DisplayWidget::size() const
{
    return mSize;
}

void DisplayWidget::setWidth(quint32 w)
{
    mSize.setWidth(w);
}

void DisplayWidget::setHeight(quint32 h)
{
    mSize.setHeight(h);
}

void DisplayWidget::setSize(const QSize &s)
{
    if (mSize != s)
    {
        mSize = s;
    }
}

QRectF DisplayWidget::boundingRect() const
{
    return QRectF(0, 0, width(), height());
}

void DisplayWidget::paint(QPainter * painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setBrush(QColor(0, 0, 33));
    painter->drawRect(boundingRect().toRect());
}
