/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "inputfilter.h"
#include "mouseindicator.h"
#include "qsimulatordata_p.h"
#include "widget.h"
#include "displaywidget.h"
#include "application.h"

#include <QtGui/QGraphicsSceneEvent>

InputFilter::InputFilter(QGraphicsItem *parent)
    : QGraphicsItem(parent)
{
}

InputFilter::~InputFilter()
{
    removeValuePoints();
}

QRectF InputFilter::boundingRect() const
{
    return QRectF();
}

void InputFilter::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter)
    Q_UNUSED(option)
    Q_UNUSED(widget)
}

QtSimulatorPrivate::TouchEventData InputFilter::createTouchEventFromMouseEvent(Widget *widget,
                                                                  QGraphicsSceneMouseEvent *mouseEvent)
{
    DisplayWidget *display = widget->owner->display();
    QtSimulatorPrivate::TouchEventData touchEvent;
    switch (mouseEvent->type()) {
    case QEvent::GraphicsSceneMousePress:
        touchEvent.type = static_cast<int>(QEvent::TouchBegin);
        touchEvent.touchPointStates = Qt::TouchPointPressed;
        break;
    case QEvent::GraphicsSceneMouseMove:
        touchEvent.type = static_cast<int>(QEvent::TouchUpdate);
        touchEvent.touchPointStates = Qt::TouchPointMoved;
        break;
    case QEvent::GraphicsSceneMouseRelease:
        touchEvent.type = static_cast<int>(QEvent::TouchEnd);
        touchEvent.touchPointStates = Qt::TouchPointReleased;
        break;
    default:
        break;
    }
    touchEvent.deviceType = QTouchEvent::TouchScreen;
    touchEvent.modifiers = Qt::NoModifier;
    QList<QtSimulatorPrivate::TouchPointData> touchPointList;
    int id = 0;
    foreach (const MouseIndicator *valuePoint, mValuePoints) {
        QtSimulatorPrivate::TouchPointData touchPoint;
        QSizeF size = valuePoint->rect().size();
        touchPoint.id = ++id;
        touchPoint.rect = QRectF(valuePoint->center(), size);
        touchPoint.sceneRect = QRectF(widget->mapToScene(valuePoint->center()), size);
        touchPoint.screenRect = QRectF(widget->mapToItem(display, valuePoint->center()), size);
        touchPoint.normalizedPos = QPointF(touchPoint.screenRect.center().x() / display->width(),
                                           touchPoint.screenRect.center().y() / display->height());
        touchPoint.startPos = valuePoint->startPos();
        touchPoint.startScenePos = widget->mapToScene(valuePoint->startPos());
        touchPoint.startScreenPos = widget->mapToItem(display, valuePoint->startPos());
        touchPoint.startNormalizedPos = QPointF(touchPoint.startScreenPos.x() / display->width(),
                                                touchPoint.startScreenPos.y() / display->height());
        touchPoint.lastPos = valuePoint->lastCenter();
        touchPoint.lastScenePos = widget->mapToScene(valuePoint->lastCenter());
        touchPoint.lastScreenPos = widget->mapToItem(display, valuePoint->lastCenter());
        touchPoint.lastNormalizedPos = QPointF(touchPoint.lastScreenPos.x() / display->width(),
                                               touchPoint.lastScreenPos.y() / display->height());
        touchPoint.pressure = .75;
        touchPoint.state = static_cast<Qt::TouchPointStates>(touchEvent.touchPointStates);
        touchPointList.append(touchPoint);
    }
    touchEvent.touchPoints = touchPointList;
    return touchEvent;
}

void InputFilter::removeValuePoints()
{
    qDeleteAll(mValuePoints);
    mValuePoints.clear();
}

QPointF InputFilter::constrainPointToRectangle(const QPointF &point, const QRectF &rectangle)
{
    QPointF returnValue = point;
    returnValue.setX(qMax(returnValue.x(), (qreal)rectangle.x()));
    returnValue.setX(qMin(returnValue.x(), (qreal)rectangle.width()));
    returnValue.setY(qMax(returnValue.y(), (qreal)rectangle.y()));
    returnValue.setY(qMin(returnValue.y(), (qreal)rectangle.height()));
    return returnValue;
}
