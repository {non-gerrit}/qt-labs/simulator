/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "mouseindicator.h"

#include <QtGui/QBrush>
#include <QtGui/QCursor>

MouseIndicator::MouseIndicator(QGraphicsItem *parent)
    : QGraphicsEllipseItem(parent)
{
}

MouseIndicator::MouseIndicator(qreal x, qreal y, QGraphicsItem *parent)
    : QGraphicsEllipseItem(0, 0, 15, 15, parent)
{
    setCenter(QPointF(x, y));
    setBrush(Qt::red);
    setCursor(Qt::OpenHandCursor);
}

MouseIndicator::MouseIndicator(QPointF center, QGraphicsItem *parent)
    : QGraphicsEllipseItem(0, 0, 15, 15, parent)
{
    setCenter(center);
    setBrush(Qt::red);
    setCursor(Qt::OpenHandCursor);
}

MouseIndicator::~MouseIndicator()
{
}

QPointF MouseIndicator::center() const
{
    return pos() + QPointF(rect().width(), rect().height()) / 2;
}

void MouseIndicator::setCenter(const QPointF &newCenter)
{
    mLastCenter = center();
    setPos(newCenter - QPointF(rect().width(), rect().height()) / 2);
}

QPointF MouseIndicator::startPos() const
{
    return mStartPosition;
}

void MouseIndicator::setStartPos(const QPointF &startPos)
{
    mLastCenter = startPos;
    mStartPosition = startPos;
}


