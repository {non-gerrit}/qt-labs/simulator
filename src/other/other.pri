INCLUDEPATH += src/other \
    $$QT_NOKIA_SDK_PATH/src/gui/kernel \
    $$QT_NOKIA_SDK_PATH/src/plugins/phonon/simulator/shared
DEPENDPATH += src/other
SOURCES += application.cpp \
    applicationmanager.cpp \
    configurationreader.cpp \
    deviceitem.cpp \
    displaywidget.cpp \
    phononmanager.cpp \
    widgetmanager.cpp \
    widget.cpp \
    mouseindicator.cpp \
    inputfilter.cpp \
    pinchmodefilter.cpp \
    panmodefilter.cpp \
    swipemodefilter.cpp

HEADERS += application.h \
    applicationmanager.h \
    configurationreader.h \
    deviceitem.h \
    displaywidget.h \
    phononmanager.h \
    widgetmanager.h \
    widget.h \
    mouseindicator.h \
    inputfilter.h \
    pinchmodefilter.h \
    panmodefilter.h \
    swipemodefilter.h

contains(QT_CONFIG, phonon) {
    SOURCES += \
        phononsignalbridges.cpp \
        phononvideowidget.cpp
    HEADERS += \
        phononsignalbridges.h \
        phononvideowidget.h
}

HEADERS += $$QT_NOKIA_SDK_PATH/src/plugins/phonon/simulator/shared/phononsimulatorhelpers.h
SOURCES += $$QT_NOKIA_SDK_PATH/src/plugins/phonon/simulator/shared/phononsimulatorhelpers.cpp
