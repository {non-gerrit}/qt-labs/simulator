/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "panmodefilter.h"
#include "mouseindicator.h"
#include "widget.h"
#include "application.h"
#include "widgetmanager.h"
#include "displaywidget.h"

#include <QtGui/QGraphicsSceneEvent>

PanModeFilter::PanModeFilter(QGraphicsItem *parent)
    : InputFilter(parent)
    , mState(setFirstPoint)
{
}

PanModeFilter::~PanModeFilter()
{
}

bool PanModeFilter::sceneEventFilter(QGraphicsItem *watched, QEvent *event)
{
    Widget *widget = static_cast<Widget *>(watched);
    const QRectF displayRect = widget->widgetManager()->display()->boundingRect();
    QGraphicsSceneMouseEvent *ev = 0;
    QPointF validPoint;
    QtSimulatorPrivate::TouchEventData touchEvent;
    switch (event->type()) {
    case QEvent::GraphicsSceneMousePress:
        ev = static_cast<QGraphicsSceneMouseEvent *>(event);
        switch (mState) {
        case setFirstPoint:
            mState = updateFirstPoint;
            mValuePoints.append(new MouseIndicator(ev->pos(), widget));
            mValuePoints[0]->setCursor(Qt::ClosedHandCursor);
            return true;
        case setSecondPoint:
            if (mValuePoints[0]->contains(mapToItem(mValuePoints[0], ev->pos()))) {
                mState = updateFirstPoint;
                mValuePoints[0]->setCursor(Qt::ClosedHandCursor);
            } else {
                mValuePoints.append(new MouseIndicator(ev->pos(), this));
                mValuePoints[1]->setCursor(Qt::ClosedHandCursor);
                mPanDiff = ev->pos() - mValuePoints[0]->center();
                mValuePoints[0]->setStartPos(mValuePoints[0]->center());
                mValuePoints[1]->setStartPos(mValuePoints[1]->center());
                mState = updateSecondPoint;
                touchEvent = createTouchEventFromMouseEvent(widget, ev);
                QtSimulatorPrivate::RemoteMetacall<void>::call(widget->owner->socket(),
                                                               QtSimulatorPrivate::NoSync,
                                                               "dispatchTouchEvent",
                                                               widget->widgetId,
                                                               touchEvent);
            }
            return true;
        default:
            return true;
        }
        break;
    case QEvent::GraphicsSceneMouseRelease:
        ev = static_cast<QGraphicsSceneMouseEvent *>(event);
        switch (mState) {
        case updateFirstPoint:
            mState = setSecondPoint;
            mValuePoints[0]->setCursor(Qt::OpenHandCursor);
            return true;
        case updateSecondPoint:
            touchEvent = createTouchEventFromMouseEvent(widget, ev);
            QtSimulatorPrivate::RemoteMetacall<void>::call(widget->owner->socket(),
                                                           QtSimulatorPrivate::NoSync,
                                                           "dispatchTouchEvent",
                                                           widget->widgetId,
                                                           touchEvent);
            removeValuePoints();
            mState = setFirstPoint;
            return true;
        default:
            return true;
        }
        break;
    case QEvent::GraphicsSceneMouseMove:
        ev = static_cast<QGraphicsSceneMouseEvent *>(event);
        validPoint = constrainPointToRectangle(ev->pos(), displayRect);
        switch (mState) {
        case updateFirstPoint:
            mValuePoints[0]->setCenter(validPoint);
            return true;
        case updateSecondPoint:
            mValuePoints[1]->setCenter(validPoint);
            validPoint = constrainPointToRectangle(ev->pos() - mPanDiff, displayRect);
            mValuePoints[0]->setCenter(validPoint);
            touchEvent = createTouchEventFromMouseEvent(widget, ev);
            QtSimulatorPrivate::RemoteMetacall<void>::call(widget->owner->socket(),
                                                           QtSimulatorPrivate::NoSync,
                                                           "dispatchTouchEvent",
                                                           widget->widgetId,
                                                           touchEvent);
            return true;
        default:
            return true;
        }
        break;
    default:
        break;
    }
    return false;
}
