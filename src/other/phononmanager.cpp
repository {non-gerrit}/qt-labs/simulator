/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "phononmanager.h"
#include "application.h"
#include "displaywidget.h"
#include "widgetmanager.h"
#include "widget.h"

#include <QtCore/QTimer>
#include <QtCore/QDebug>
#include <qsimulatordata_p.h>

PhononManager::PhononManager(QObject *parent)
:   QObject(parent),
    mBackend(0),
    mCurrentApplication(0)
{
    Phonon::Simulator::qt_registerPhononSimulatorTypes();
}

PhononManager::~PhononManager()
{
}

QSet<QObject*> PhononManager::createObjectSet(const QVariantList& vlist)
{
    QSet<QObject*> objectSet;
    bool ok;
    foreach (const QVariant &v, vlist) {
        int i = v.toInt(&ok);
        if (!ok)
            continue;
        objectSet.insert(reinterpret_cast<QObject*>(i));
    }
    return objectSet;
}

#ifndef QT_NO_PHONON
#include "phononsignalbridges.h"
#include "phononvideowidget.h"
#include <phonon/audiooutput.h>
#include <phonon/audiooutputinterface.h>
#include <phonon/effect.h>
#include <phonon/backendinterface.h>
#include <phonon/medianode.h>
#include <phonon/mediaobject.h>
#include <phonon/videowidget.h>
#include <phonon/private/factory_p.h>

void PhononManager::backend_init()
{
    QObject* phononBackend = Phonon::Factory::backend();
    mBackend = qobject_cast<Phonon::BackendInterface*>(phononBackend);
    BackendSignalBridge* bridge = new BackendSignalBridge(mCurrentApplication, phononBackend);
    bridge->setObjectName("backend signal bridge");
}

inline Phonon::MediaNode* castIdToMediaNode(const Phonon::Simulator::Pointer& p)
{
    QObject* qobj = reinterpret_cast<QObject*>(p.ptr);
    MediaNode* node = qobject_cast<MediaObject*>(qobj);
    if (!node)
        node = qobject_cast<AudioOutput*>(qobj);
    if (!node)
        node = qobject_cast<Effect*>(qobj);
    if (!node)
        node = qobject_cast<VideoWidget*>(qobj);
    return node;
}

void PhononManager::backend_destroyObjects(QVariantList vl)
{
    //qDebug("PhononManager::backend_destroyObjects");
    foreach (const QVariant &v, vl) {
        Simulator::Pointer p = v.value<Simulator::Pointer>();
        MediaNode* mediaNode = castIdToMediaNode(p);
        delete mediaNode;
    }
}

QStringList PhononManager::backend_availableMimeTypes()
{
    return mBackend->availableMimeTypes();
}

QStringList PhononManager::backend_objectDescriptionIndexes(int type)
{
    QStringList ret;
    foreach (int idx, mBackend->objectDescriptionIndexes(static_cast<Phonon::ObjectDescriptionType>(type)))
        ret.append(QString::number(idx));
    return ret;
}

QHash<QString, QVariant> PhononManager::backend_objectDescriptionProperties(int type, int index)
{
    QHash<QByteArray, QVariant> props;
    props = mBackend->objectDescriptionProperties(static_cast<Phonon::ObjectDescriptionType>(type), index);

    QHash<QByteArray, QVariant>::const_iterator it = props.constBegin();
    QHash<QByteArray, QVariant>::const_iterator itEnd = props.constEnd();

    QHash<QString, QVariant> ret;
    for (; it != itEnd; ++it)
        ret.insert(QString::fromLocal8Bit(it.key()), it.value());

    return ret;
}

Phonon::Simulator::Pointer PhononManager::backend_createObject(int c, const QVariantList args)
{
    Phonon::BackendInterface::Class classId = static_cast<Phonon::BackendInterface::Class>(c);

    QObject* phononObj = 0;
    PhononSignalBridge* bridge = 0;
    Phonon::Simulator::Pointer ret;
    switch (classId)
    {
    case Phonon::BackendInterface::MediaObjectClass:
        phononObj = new MediaObject(mCurrentApplication);
        bridge = new PhononMediaObjectSignalBridge(mCurrentApplication, phononObj);
        break;
    case Phonon::BackendInterface::AudioOutputClass:
        phononObj = new AudioOutput(mCurrentApplication);
        bridge = new PhononAudioOutputSignalBridge(mCurrentApplication, phononObj);
        break;
    case Phonon::BackendInterface::EffectClass:
        if (args.isEmpty()) {
            qWarning("PhononManager::backend_createObject can't create Phonon::Effect. Parameter missing.");
            return ret;
        }
        qWarning("PhononManager::backend_createObject ### Effect creation not implemented.");
        //phononObj = new Effect(m_audioEffects[ args[0].toInt() ], mCurrentApplication);
        break;
    case Phonon::BackendInterface::VideoWidgetClass:
        {
            PhononVideoWidget* simulatorVideoWidget = new PhononVideoWidget(mCurrentApplication);
            phononObj = simulatorVideoWidget->videoWidget();
            break;
        }
    default:
        return ret;
    }

    //qDebug() << "PhononManager::backend_createObject" << c << args << phononObj;
    ret.ptr = phononObj;
    return ret;
}

bool PhononManager::backend_startConnectionChange(QVariantList objects)
{
    return mBackend->startConnectionChange(createObjectSet(objects));
}

bool PhononManager::backend_endConnectionChange(QVariantList objects)
{
    return mBackend->endConnectionChange(createObjectSet(objects));
}

bool PhononManager::backend_connectNodes(Phonon::Simulator::Pointer _source, Phonon::Simulator::Pointer _sink)
{
    //qDebug("PhononManager::backend_connectNodes(%d, %d)", _source, _sink);
    MediaNode* sourceNode = castIdToMediaNode(_source);
    MediaNode* sinkNode = castIdToMediaNode(_sink);
    Q_ASSERT(sourceNode);
    Q_ASSERT(sinkNode);
    Path path = createPath(sourceNode, sinkNode);
    return path.isValid();
}

bool PhononManager::backend_disconnectNodes(Phonon::Simulator::Pointer _source, Phonon::Simulator::Pointer _sink)
{
    //qDebug("PhononManager::backend_disconnectNodes(%d, %d)", _source, _sink);
    MediaNode* sourceNode = castIdToMediaNode(_source);
    MediaNode* sinkNode = castIdToMediaNode(_sink);
    Q_ASSERT(sourceNode);
    Q_ASSERT(sinkNode);

    bool ret = false;
    foreach (Path p, sourceNode->outputPaths()) {
        if (p.sink() == sinkNode) {
            ret = p.disconnect();
            break;
        }
    }

    return ret;
}

inline Phonon::MediaObject* castIdToMediaObject(const Phonon::Simulator::Pointer& p)
{
    return qobject_cast<Phonon::MediaObject*>(reinterpret_cast<QObject*>(p.ptr));
}

int PhononManager::mediaObject_state(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->state() : Phonon::ErrorState;
}

bool PhononManager::mediaObject_hasVideo(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->hasVideo() : false;
}

bool PhononManager::mediaObject_isSeekable(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->isSeekable() : false;
}

qint64 PhononManager::mediaObject_totalTime(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->totalTime() : 0;
}

qint64 PhononManager::mediaObject_currentTime(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->currentTime() : 0;
}

int PhononManager::mediaObject_tickInterval(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->tickInterval() : 0;
}

void PhononManager::mediaObject_setTickInterval(Phonon::Simulator::Pointer id, int newTickInterval)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (mo) mo->setTickInterval(newTickInterval);
}

void PhononManager::mediaObject_pause(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (mo) mo->pause();
}

void PhononManager::mediaObject_stop(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (mo) mo->stop();
}

void PhononManager::mediaObject_play(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (mo) mo->play();
}

QString PhononManager::mediaObject_errorString(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->errorString() : QString();
}

int PhononManager::mediaObject_errorType(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->errorType() : Phonon::FatalError;
}

int PhononManager::mediaObject_prefinishMark(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->prefinishMark() : 0;
}

void PhononManager::mediaObject_setPrefinishMark(Phonon::Simulator::Pointer id, int newPrefinishMark)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (mo) mo->setPrefinishMark(newPrefinishMark);
}

int PhononManager::mediaObject_transitionTime(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->transitionTime() : 0;
}

void PhononManager::mediaObject_setTransitionTime(Phonon::Simulator::Pointer id, int t)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (mo) mo->setTransitionTime(t);
}

qint64 PhononManager::mediaObject_remainingTime(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    return mo ? mo->remainingTime() : 0;
}

QVariantList PhononManager::mediaObject_source(Phonon::Simulator::Pointer id)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (!mo)
        return QVariantList();

    return Phonon::Simulator::mediaSourceToVariantList(mo->currentSource());
}

void PhononManager::mediaObject_setSource(Phonon::Simulator::Pointer id, const QVariantList& vl)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (mo)
        mo->setCurrentSource(Phonon::Simulator::mediaSourceFromVariantList(vl));
}

void PhononManager::mediaObject_setNextSource(Phonon::Simulator::Pointer id, const QVariantList& vl)
{
    Q_UNUSED(id);
    Q_UNUSED(vl);
    // ### not sure if we must handle this...
    Q_UNUSED(id);
    Q_UNUSED(vl);

    //Phonon::MediaObject* mo = castIdToMediaObject(id);
    //if (mo)
    //    mo->setNextSource(Phonon::Simulator::mediaSourceFromVariantList(vl));
}

void PhononManager::mediaObject_seek(Phonon::Simulator::Pointer id, qlonglong t)
{
    Phonon::MediaObject* mo = castIdToMediaObject(id);
    if (mo) mo->seek(t);
}

inline Phonon::AudioOutput* castIdToAudioOutput(const Phonon::Simulator::Pointer& p)
{
    return qobject_cast<Phonon::AudioOutput*>(reinterpret_cast<QObject*>(p.ptr));
}

int PhononManager::audioOutput_outputDevice(Phonon::Simulator::Pointer id)
{
    Phonon::AudioOutput* iface = castIdToAudioOutput(id);
    return iface ? iface->outputDevice().index() : -1;
}

void PhononManager::audioOutput_setVolume(Phonon::Simulator::Pointer id, double v)
{
    Phonon::AudioOutput* iface = castIdToAudioOutput(id);
    if (iface) iface->setVolume(v);
}

bool PhononManager::audioOutput_setOutputDevice(Phonon::Simulator::Pointer id, int newDevice)
{
    Phonon::AudioOutput* iface = castIdToAudioOutput(id);
    return iface ? iface->setOutputDevice(AudioOutputDevice::fromIndex(newDevice)) : -1;
}

double PhononManager::audioOutput_volume(Phonon::Simulator::Pointer id)
{
    Phonon::AudioOutput* iface = castIdToAudioOutput(id);
    return iface ? iface->volume() : 0;
}

inline Phonon::VideoWidget* castIdToVideoWidget(const Phonon::Simulator::Pointer& p)
{
    return qobject_cast<Phonon::VideoWidget*>(reinterpret_cast<QObject*>(p.ptr));
}

void PhononManager::videoWidget_setAspectRatio(Phonon::Simulator::Pointer id, int r)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    if (vw)
        vw->setAspectRatio(static_cast<Phonon::VideoWidget::AspectRatio>(r));
}

void PhononManager::videoWidget_setScaleMode(Phonon::Simulator::Pointer id, int m)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    if (vw)
        vw->setScaleMode(static_cast<Phonon::VideoWidget::ScaleMode>(m));
}

void PhononManager::videoWidget_setBrightness(Phonon::Simulator::Pointer id, qreal b)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    if (vw)
        vw->setBrightness(b);
}

void PhononManager::videoWidget_setContrast(Phonon::Simulator::Pointer id, qreal c)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    if (vw)
        vw->setContrast(c);
}

void PhononManager::videoWidget_setHue(Phonon::Simulator::Pointer id, qreal h)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    if (vw)
        vw->setHue(h);
}

void PhononManager::videoWidget_setSaturation(Phonon::Simulator::Pointer id, qreal s)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    if (vw)
        vw->setSaturation(s);
}

int PhononManager::videoWidget_aspectRatio(Phonon::Simulator::Pointer id)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    return vw ? static_cast<int>(vw->aspectRatio()) : 0;
}

int PhononManager::videoWidget_scaleMode(Phonon::Simulator::Pointer id)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    return vw ? static_cast<int>(vw->scaleMode()) : 0;
}

qreal PhononManager::videoWidget_brightness(Phonon::Simulator::Pointer id)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    return vw ? vw->brightness() : 0;
}

qreal PhononManager::videoWidget_contrast(Phonon::Simulator::Pointer id)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    return vw ? vw->contrast() : 0;
}

qreal PhononManager::videoWidget_hue(Phonon::Simulator::Pointer id)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    return vw ? vw->hue() : 0;
}

qreal PhononManager::videoWidget_saturation(Phonon::Simulator::Pointer id)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    return vw ? vw->saturation() : 0;
}

void PhononManager::videoWidget_moveEvent(Phonon::Simulator::Pointer id, QPoint pos)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    if (vw)
        vw->move(pos);
}

void PhononManager::videoWidget_resizeEvent(Phonon::Simulator::Pointer id, QSize s)
{
    Phonon::VideoWidget* vw = castIdToVideoWidget(id);
    if (vw)
        vw->resize(s);
}

#else // QT_NO_PHONON

void PhononManager::backend_init()
{
    qWarning() << "Client tries to use phonon, but the simulator isn't built with phonon support.";
}

void PhononManager::backend_destroyObjects(QVariantList vl)
{
    Q_UNUSED(vl);
}

QStringList PhononManager::backend_availableMimeTypes()
{
    return QStringList();
}

QStringList PhononManager::backend_objectDescriptionIndexes(int)
{
    return QStringList();
}

QHash<QString, QVariant> PhononManager::backend_objectDescriptionProperties(int, int)
{
    return QHash<QString, QVariant>();
}

Phonon::Simulator::Pointer PhononManager::backend_createObject(int, const QVariantList)
{
    Phonon::Simulator::Pointer pointer;
    pointer.ptr = 0;
    return pointer;
}

bool PhononManager::backend_startConnectionChange(QVariantList)
{
    return false;
}

bool PhononManager::backend_endConnectionChange(QVariantList)
{
    return false;
}

bool PhononManager::backend_connectNodes(Phonon::Simulator::Pointer, Phonon::Simulator::Pointer)
{
    return false;
}

bool PhononManager::backend_disconnectNodes(Phonon::Simulator::Pointer, Phonon::Simulator::Pointer)
{
    return false;
}

int PhononManager::mediaObject_state(Phonon::Simulator::Pointer)
{
    return 0;
}

bool PhononManager::mediaObject_hasVideo(Phonon::Simulator::Pointer)
{
    return false;
}

bool PhononManager::mediaObject_isSeekable(Phonon::Simulator::Pointer)
{
    return false;
}

qint64 PhononManager::mediaObject_totalTime(Phonon::Simulator::Pointer)
{
    return 0;
}

qint64 PhononManager::mediaObject_currentTime(Phonon::Simulator::Pointer)
{
    return 0;
}

int PhononManager::mediaObject_tickInterval(Phonon::Simulator::Pointer)
{
    return 0;
}

void PhononManager::mediaObject_setTickInterval(Phonon::Simulator::Pointer, int)
{
}

void PhononManager::mediaObject_pause(Phonon::Simulator::Pointer)
{
}

void PhononManager::mediaObject_stop(Phonon::Simulator::Pointer)
{
}

void PhononManager::mediaObject_play(Phonon::Simulator::Pointer)
{
}

QString PhononManager::mediaObject_errorString(Phonon::Simulator::Pointer)
{
    return QString("Simulator does not support phonon");
}

int PhononManager::mediaObject_errorType(Phonon::Simulator::Pointer)
{
    return 0;
}

int PhononManager::mediaObject_prefinishMark(Phonon::Simulator::Pointer)
{
    return 0;
}

void PhononManager::mediaObject_setPrefinishMark(Phonon::Simulator::Pointer, int)
{
}

int PhononManager::mediaObject_transitionTime(Phonon::Simulator::Pointer)
{
    return 0;
}

void PhononManager::mediaObject_setTransitionTime(Phonon::Simulator::Pointer, int)
{
}

qint64 PhononManager::mediaObject_remainingTime(Phonon::Simulator::Pointer)
{
    return 0;
}

QVariantList PhononManager::mediaObject_source(Phonon::Simulator::Pointer)
{
    return QVariantList();
}

void PhononManager::mediaObject_setSource(Phonon::Simulator::Pointer, const QVariantList&)
{
}

void PhononManager::mediaObject_setNextSource(Phonon::Simulator::Pointer, const QVariantList&)
{
}

void PhononManager::mediaObject_seek(Phonon::Simulator::Pointer, qlonglong)
{
}

int PhononManager::audioOutput_outputDevice(Phonon::Simulator::Pointer)
{
    return 0;
}

void PhononManager::audioOutput_setVolume(Phonon::Simulator::Pointer, double)
{
}

bool PhononManager::audioOutput_setOutputDevice(Phonon::Simulator::Pointer, int)
{
    return false;
}

double PhononManager::audioOutput_volume(Phonon::Simulator::Pointer)
{
    return 0;
}

void PhononManager::videoWidget_setAspectRatio(Phonon::Simulator::Pointer id, int r)
{
    Q_UNUSED(id);
    Q_UNUSED(r);
}

void PhononManager::videoWidget_setScaleMode(Phonon::Simulator::Pointer id, int m)
{
    Q_UNUSED(id);
    Q_UNUSED(m);
}

void PhononManager::videoWidget_setBrightness(Phonon::Simulator::Pointer id, qreal b)
{
    Q_UNUSED(id);
    Q_UNUSED(b);
}

void PhononManager::videoWidget_setContrast(Phonon::Simulator::Pointer id, qreal c)
{
    Q_UNUSED(id);
    Q_UNUSED(c);
}

void PhononManager::videoWidget_setHue(Phonon::Simulator::Pointer id, qreal h)
{
    Q_UNUSED(id);
    Q_UNUSED(h);
}

void PhononManager::videoWidget_setSaturation(Phonon::Simulator::Pointer id, qreal s)
{
    Q_UNUSED(id);
    Q_UNUSED(s);
}

int PhononManager::videoWidget_aspectRatio(Phonon::Simulator::Pointer id)
{
    Q_UNUSED(id);
    return 0;
}

int PhononManager::videoWidget_scaleMode(Phonon::Simulator::Pointer id)
{
    Q_UNUSED(id);
    return 0;
}

qreal PhononManager::videoWidget_brightness(Phonon::Simulator::Pointer id)
{
    Q_UNUSED(id);
    return 0.0f;
}

qreal PhononManager::videoWidget_contrast(Phonon::Simulator::Pointer id)
{
    Q_UNUSED(id);
    return 0.0f;
}

qreal PhononManager::videoWidget_hue(Phonon::Simulator::Pointer id)
{
    Q_UNUSED(id);
    return 0.0f;
}

qreal PhononManager::videoWidget_saturation(Phonon::Simulator::Pointer id)
{
    Q_UNUSED(id);
    return 0.0f;
}

void PhononManager::videoWidget_moveEvent(Phonon::Simulator::Pointer id, QPoint pos)
{
    Q_UNUSED(id);
    Q_UNUSED(pos);
}

void PhononManager::videoWidget_resizeEvent(Phonon::Simulator::Pointer id, QSize s)
{
    Q_UNUSED(id);
    Q_UNUSED(s);
}

#endif // else QT_NO_PHONON
