/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef __PHONONMANAGER_H__
#define __PHONONMANAGER_H__

#include <phononsimulatorhelpers.h>

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtCore/QHash>
#include <QtCore/QStringList>
#include <QtCore/QSize>
#include <QtCore/QPoint>

class Application;

namespace Phonon {
    class BackendInterface;
}

class PhononManager : public QObject
{
    Q_OBJECT
public:
    PhononManager(QObject *parent = 0);
    ~PhononManager();

    void setCurrentApplication(Application* app) { mCurrentApplication = app; }

private slots:
    void backend_init();
    void backend_destroyObjects(QVariantList);
    QStringList backend_availableMimeTypes();
    QStringList backend_objectDescriptionIndexes(int type);
    QHash<QString, QVariant> backend_objectDescriptionProperties(int type, int index);
    Phonon::Simulator::Pointer backend_createObject(int c, const QVariantList args);
    bool backend_startConnectionChange(QVariantList objects);
    bool backend_endConnectionChange(QVariantList objects);
    bool backend_connectNodes(Phonon::Simulator::Pointer _source, Phonon::Simulator::Pointer _sink);
    bool backend_disconnectNodes(Phonon::Simulator::Pointer _source, Phonon::Simulator::Pointer _sink);

    int mediaObject_state(Phonon::Simulator::Pointer id);
    bool mediaObject_hasVideo(Phonon::Simulator::Pointer id);
    bool mediaObject_isSeekable(Phonon::Simulator::Pointer id);
    qint64 mediaObject_totalTime(Phonon::Simulator::Pointer id);
    qint64 mediaObject_currentTime(Phonon::Simulator::Pointer id);
    int mediaObject_tickInterval(Phonon::Simulator::Pointer id);
    void mediaObject_setTickInterval(Phonon::Simulator::Pointer id, int newTickInterval);
    void mediaObject_pause(Phonon::Simulator::Pointer id);
    void mediaObject_stop(Phonon::Simulator::Pointer id);
    void mediaObject_play(Phonon::Simulator::Pointer id);
    QString mediaObject_errorString(Phonon::Simulator::Pointer id);
    int mediaObject_errorType(Phonon::Simulator::Pointer id);
    int mediaObject_prefinishMark(Phonon::Simulator::Pointer id);
    void mediaObject_setPrefinishMark(Phonon::Simulator::Pointer id, int newPrefinishMark);
    int mediaObject_transitionTime(Phonon::Simulator::Pointer id);
    void mediaObject_setTransitionTime(Phonon::Simulator::Pointer id, int t);
    qint64 mediaObject_remainingTime(Phonon::Simulator::Pointer id);
    QVariantList mediaObject_source(Phonon::Simulator::Pointer id);
    void mediaObject_setSource(Phonon::Simulator::Pointer id, const QVariantList&);
    void mediaObject_setNextSource(Phonon::Simulator::Pointer id, const QVariantList&);
    void mediaObject_seek(Phonon::Simulator::Pointer id, qlonglong t);

    int audioOutput_outputDevice(Phonon::Simulator::Pointer id);
    void audioOutput_setVolume(Phonon::Simulator::Pointer id, double v);
    bool audioOutput_setOutputDevice(Phonon::Simulator::Pointer id, int newDevice);
    double audioOutput_volume(Phonon::Simulator::Pointer id);

    void videoWidget_setAspectRatio(Phonon::Simulator::Pointer id, int r);
    void videoWidget_setScaleMode(Phonon::Simulator::Pointer id, int m);
    void videoWidget_setBrightness(Phonon::Simulator::Pointer id, qreal b);
    void videoWidget_setContrast(Phonon::Simulator::Pointer id, qreal c);
    void videoWidget_setHue(Phonon::Simulator::Pointer id, qreal h);
    void videoWidget_setSaturation(Phonon::Simulator::Pointer id, qreal s);
    int videoWidget_aspectRatio(Phonon::Simulator::Pointer id);
    int videoWidget_scaleMode(Phonon::Simulator::Pointer id);
    qreal videoWidget_brightness(Phonon::Simulator::Pointer id);
    qreal videoWidget_contrast(Phonon::Simulator::Pointer id);
    qreal videoWidget_hue(Phonon::Simulator::Pointer id);
    qreal videoWidget_saturation(Phonon::Simulator::Pointer id);

    void videoWidget_moveEvent(Phonon::Simulator::Pointer id, QPoint pos);
    void videoWidget_resizeEvent(Phonon::Simulator::Pointer id, QSize s);

private:
    QSet<QObject*> createObjectSet(const QVariantList& vlist);

private:
    Phonon::BackendInterface*   mBackend;
    Application*                mCurrentApplication;
};

#endif // __PHONONMANAGER_H__
