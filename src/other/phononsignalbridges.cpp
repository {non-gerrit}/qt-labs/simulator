/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "phononsignalbridges.h"
#include "application.h"

#include <qsimulatordata_p.h>
#include <phononsimulatorhelpers.h>
#include <QtCore/QStringList>

BackendSignalBridge::BackendSignalBridge(Application* app, QObject* sender)
:   PhononSignalBridge(app, sender)
{
    connect(sender, SIGNAL(objectDescriptionChanged(ObjectDescriptionType)), SLOT(on_backend_ObjectDescriptionChanged(ObjectDescriptionType)));
}

void BackendSignalBridge::on_backend_ObjectDescriptionChanged(ObjectDescriptionType odt)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "backend_objectDescriptionChanged", remoteId(), static_cast<int>(odt));
}

PhononMediaObjectSignalBridge::PhononMediaObjectSignalBridge(Application* app, QObject* sender)
:   PhononSignalBridge(app, sender)
{
    connect(sender, SIGNAL(aboutToFinish()), SLOT(aboutToFinish()));
    connect(sender, SIGNAL(bufferStatus(int)), SLOT(bufferStatus(int)));
    connect(sender, SIGNAL(currentSourceChanged(const Phonon::MediaSource&)), SLOT(currentSourceChanged(const Phonon::MediaSource&)));
    connect(sender, SIGNAL(finished()), SLOT(finished()));
    connect(sender, SIGNAL(hasVideoChanged(bool)), SLOT(hasVideoChanged(bool)));
    connect(sender, SIGNAL(metaDataChanged()), SLOT(metaDataChanged()));
    connect(sender, SIGNAL(prefinishMarkReached(qint32)), SLOT(prefinishMarkReached(qint32)));
    connect(sender, SIGNAL(seekableChanged(bool)), SLOT(seekableChanged(bool)));
    connect(sender, SIGNAL(stateChanged(Phonon::State, Phonon::State)), SLOT(stateChanged(Phonon::State, Phonon::State)));
    connect(sender, SIGNAL(tick(qint64)), SLOT(tick(qint64)));
    connect(sender, SIGNAL(totalTimeChanged(qint64)), SLOT(totalTimeChanged(qint64)));
}

void PhononMediaObjectSignalBridge::aboutToFinish()
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_aboutToFinish", remoteId());
}

void PhononMediaObjectSignalBridge::bufferStatus(int percentFilled)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_bufferStatus", remoteId(), percentFilled);
}

void PhononMediaObjectSignalBridge::currentSourceChanged(const MediaSource& newSource)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_currentSourceChanged", remoteId(), Phonon::Simulator::mediaSourceToVariantList(newSource));
}

void PhononMediaObjectSignalBridge::finished()
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_finished", remoteId());
}

void PhononMediaObjectSignalBridge::hasVideoChanged(bool hasVideo)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_hasVideoChanged", remoteId(), hasVideo);
}

void PhononMediaObjectSignalBridge::metaDataChanged()
{
    QStringList payload;
    QMultiMap<QString, QString> metaData = mediaObject()->metaData();
    QMultiMap<QString, QString>::const_iterator it = metaData.begin();
    for (; it != metaData.end(); ++it)
        payload << it.key() << it.value();

    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_metaDataChanged", remoteId(), payload);
}

void PhononMediaObjectSignalBridge::prefinishMarkReached(qint32 msecToEnd)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_prefinishMarkReached", remoteId(), msecToEnd);
}

void PhononMediaObjectSignalBridge::seekableChanged(bool isSeekable)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_seekableChanged", remoteId(), isSeekable);
}

void PhononMediaObjectSignalBridge::stateChanged(Phonon::State newstate, Phonon::State oldstate)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_stateChanged", remoteId(), (int)newstate, (int)oldstate);
}

void PhononMediaObjectSignalBridge::tick(qint64 time)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_tick", remoteId(), time);
}

void PhononMediaObjectSignalBridge::totalTimeChanged(qint64 newTotalTime)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "mediaObject_totalTimeChanged", remoteId(), newTotalTime);
}

PhononAudioOutputSignalBridge::PhononAudioOutputSignalBridge(Application* app, QObject* sender)
:   PhononSignalBridge(app, sender)
{
    connect(sender, SIGNAL(mutedChanged(bool)), SLOT(mutedChanged(bool)));
    connect(sender, SIGNAL(outputDeviceChanged(const Phonon::AudioOutputDevice&)), SLOT(outputDeviceChanged(const Phonon::AudioOutputDevice&)));
    connect(sender, SIGNAL(volumeChanged(qreal)), SLOT(volumeChanged(qreal)));
}

void PhononAudioOutputSignalBridge::mutedChanged(bool muted)
{
    Q_UNUSED(muted)
    // We probably don't have to handle this. We already get volumeChanged signals.

    //QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
    //    "audioOutput_mutedChanged", remoteId(), muted);
}

void PhononAudioOutputSignalBridge::outputDeviceChanged(const Phonon::AudioOutputDevice &newAudioOutputDevice)
{
    Q_UNUSED(newAudioOutputDevice)
    /// There's no matching signal in the private AudioOutput class. :-/

    //QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
    //    "audioOutput_outputDeviceChanged", remoteId(), newAudioOutputDevice.index());
}

void PhononAudioOutputSignalBridge::volumeChanged(qreal newVolume)
{
    QtSimulatorPrivate::RemoteMetacall<void>::call(mApplication->socket(), QtSimulatorPrivate::NoSync,
        "audioOutput_volumeChanged", remoteId(), newVolume);
}
