/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef __PHONONSIGNALBRIDGES_H__
#define __PHONONSIGNALBRIDGES_H__

#include <phononsimulatorhelpers.h>
#include <phonon/mediaobject.h>
#include <phonon/audiooutput.h>

using namespace Phonon;

class Application;

class PhononSignalBridge : public QObject
{
    Q_OBJECT
public:
    PhononSignalBridge(Application* app, QObject* sender)
        : QObject(sender), mApplication(app)
    {
    }

    inline Phonon::Simulator::Pointer remoteId()
    {
        Phonon::Simulator::Pointer p;
        p.ptr = parent();
        return p;
    }

protected:
    Application* mApplication;
};

class BackendSignalBridge : public PhononSignalBridge
{
    Q_OBJECT
public:
    BackendSignalBridge(Application* app, QObject* sender);

protected slots:
    void on_backend_ObjectDescriptionChanged(ObjectDescriptionType);
};

class PhononMediaObjectSignalBridge : public PhononSignalBridge
{
    Q_OBJECT
public:
    PhononMediaObjectSignalBridge(Application* app, QObject* sender);

    Phonon::MediaObject* mediaObject()
    {
        return qobject_cast<Phonon::MediaObject*>(parent());
    }

private slots:
    void aboutToFinish();
    void bufferStatus(int percentFilled);
    void currentSourceChanged(const Phonon::MediaSource& newSource);
    void finished();
    void hasVideoChanged(bool hasVideo);
    void metaDataChanged();
    void prefinishMarkReached(qint32 msecToEnd);
    void seekableChanged(bool isSeekable);
    void stateChanged(Phonon::State newstate, Phonon::State oldstate);
    void tick(qint64 time);
    void totalTimeChanged(qint64 newTotalTime);
};

class PhononAudioOutputSignalBridge : public PhononSignalBridge
{
    Q_OBJECT
public:
    PhononAudioOutputSignalBridge(Application* app, QObject* sender);

    Phonon::AudioOutput* audioOutput()
    {
        return qobject_cast<Phonon::AudioOutput*>(parent());
    }

private slots:
    void mutedChanged(bool muted);
    void outputDeviceChanged(const Phonon::AudioOutputDevice &newAudioOutputDevice);
    void volumeChanged(qreal newVolume);
};

#endif // __PHONONSIGNALBRIDGES_H__
