/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "pinchmodefilter.h"
#include "mouseindicator.h"
#include "widget.h"
#include "application.h"
#include "widgetmanager.h"
#include "displaywidget.h"

#include <QtGui/QGraphicsSceneEvent>

PinchModeFilter::PinchModeFilter(QGraphicsItem *parent)
    : InputFilter(parent)
    , mState(setReferencePoint)
{
    mReferencePoint = new MouseIndicator(0, 0, this);
    mReferencePoint->setVisible(false);
    mReferencePoint->setBrush(Qt::gray);
}

PinchModeFilter::~PinchModeFilter()
{
}

bool PinchModeFilter::sceneEventFilter(QGraphicsItem *watched, QEvent *event)
{
    Widget *widget = static_cast<Widget *>(watched);
    const QRectF displayRect = widget->widgetManager()->display()->boundingRect();
    QGraphicsSceneMouseEvent *ev = 0;
    QPointF validPoint;
    QtSimulatorPrivate::TouchEventData touchEvent;
    QPointF diff;
    switch (event->type()) {
    case QEvent::GraphicsSceneMousePress:
        ev = static_cast<QGraphicsSceneMouseEvent *>(event);
        switch (mState) {
        case setReferencePoint:
            mState = updateReferencePoint;
            mReferencePoint->setCursor(Qt::ClosedHandCursor);
            mReferencePoint->setCenter(ev->pos());
            mReferencePoint->setVisible(true);
            return true;
        case setTouchPoints:
            if (mReferencePoint->contains(mapToItem(mReferencePoint, ev->pos()))) {
                mReferencePoint->setCursor(Qt::ClosedHandCursor);
                mState = updateReferencePoint;
            } else {
                validPoint = constrainPointToRectangle(ev->pos(), displayRect);
                mValuePoints.append(new MouseIndicator(validPoint, widget));
                mValuePoints[0]->setStartPos(mValuePoints[0]->center());
                mValuePoints[0]->setCursor(Qt::ClosedHandCursor);
                QPointF diff = ev->pos() - mReferencePoint->center();
                validPoint = constrainPointToRectangle(mReferencePoint->center() - diff, displayRect);
                mValuePoints.append(new MouseIndicator(validPoint, widget));
                mValuePoints[1]->setStartPos(mValuePoints[1]->center());
                mState = updateTouchPoints;
                touchEvent = createTouchEventFromMouseEvent(widget, ev);
                QtSimulatorPrivate::RemoteMetacall<void>::call(widget->owner->socket(),
                                                               QtSimulatorPrivate::NoSync,
                                                               "dispatchTouchEvent",
                                                               widget->widgetId,
                                                               touchEvent);
            }
            return true;
        default:
            return true;
        }
        break;
    case QEvent::GraphicsSceneMouseRelease:
        ev = static_cast<QGraphicsSceneMouseEvent *>(event);
        switch (mState) {
        case updateReferencePoint:
            mReferencePoint->setCursor(Qt::OpenHandCursor);
            mState = setTouchPoints;
            return true;
        case updateTouchPoints:
            touchEvent = createTouchEventFromMouseEvent(widget, ev);
            QtSimulatorPrivate::RemoteMetacall<void>::call(widget->owner->socket(),
                                                           QtSimulatorPrivate::NoSync,
                                                           "dispatchTouchEvent",
                                                           widget->widgetId,
                                                           touchEvent);

            removeValuePoints();
            mReferencePoint->setVisible(false);
            mState = setReferencePoint;
            return true;
        default:
            return true;
        }
        break;
    case QEvent::GraphicsSceneMouseMove:
        ev = static_cast<QGraphicsSceneMouseEvent *>(event);
        validPoint = constrainPointToRectangle(ev->pos(), displayRect);
        switch (mState) {
        case updateReferencePoint:
            mReferencePoint->setCenter(validPoint);
            return true;
        case updateTouchPoints:
            mValuePoints[0]->setCenter(validPoint);
            diff = ev->pos() - mReferencePoint->center();
            validPoint = constrainPointToRectangle(mReferencePoint->center() - diff, displayRect);
            mValuePoints[1]->setCenter(validPoint);
            touchEvent = createTouchEventFromMouseEvent(widget, ev);
            QtSimulatorPrivate::RemoteMetacall<void>::call(widget->owner->socket(),
                                                           QtSimulatorPrivate::NoSync,
                                                           "dispatchTouchEvent",
                                                           widget->widgetId,
                                                           touchEvent);
            return true;
        default:
            return true;
        }
        break;
    default:
        break;
    }
    return false;
}
