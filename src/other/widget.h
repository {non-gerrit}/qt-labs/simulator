/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef WIDGET_H
#define WIDGET_H

#include "multipointtouchui.h"

#include <QtGui/QGraphicsItem>
#include <QtGui/QtEvents>

class Application;
class QSharedMemory;
class QString;
class QMenuBar;
class MouseIndicator;
class WidgetManager;
namespace QtSimulatorPrivate {
struct TouchEventData;
struct TouchPointData;
}

class Widget : public QGraphicsItem
{
public:
    Widget(QRect geometry, QImage::Format f, const QString &t, Application* who,
           int id, const QString &sharedMemoryName, QGraphicsItem *parent=0);
    ~Widget();

    virtual QRectF boundingRect() const;
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);

    static const int WidgetType = UserType+1;
    virtual int type() const { return WidgetType; }

    bool createMemory();
    QImage::Format format;
    int widgetId;
    QString title;
    Application* owner;

    QRect geometry() const;
    void setGeometry(QRect g);

    void updateOffset(const QPoint &newOffset);
    void setFullscreen(bool full);
    bool isFullScreen() const;

    bool isWidgetVisible() const;
    void setWidgetVisible(bool visible);

    QMenuBar* menuBar() const { return mMenuBar; }
    void setMenuBar(QMenuBar* menuBar) { mMenuBar = menuBar; }
    bool maemo5Stacked() { return mMaemo5Stacked; }
    void setMaemo5Stacked(bool stacked) { mMaemo5Stacked = stacked; }
    Qt::WidgetAttribute orientation() { return mOrientation; }
    void setOrientation(Qt::WidgetAttribute orientation) { mOrientation = orientation; }
    void setMouseInputMode(MultiPointTouchUi::InputMode newMode);
    void setWidgetManager(WidgetManager *manager) { mManager = manager; }
    WidgetManager *widgetManager() const { return mManager; }

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* ev);
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* ev);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* ev);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* ev);
    virtual void hoverMoveEvent(QGraphicsSceneHoverEvent* ev);
    virtual void keyPressEvent(QKeyEvent* ev);
    virtual void keyReleaseEvent(QKeyEvent* ev);
    virtual void event(QEvent* e);
    void handleMouseEvent(QEvent::Type type, QGraphicsSceneMouseEvent* ev);
    void handleKeyEvent(QKeyEvent* ev);

private:
    friend class WidgetManager;
    WidgetManager *mManager;
    friend class DisplayWidget;
    QSharedMemory *memory;

    int width;
    int height;
    QPoint offset;
    bool fullscreen;
    QMenuBar* mMenuBar;
    bool wantsUpdate;
    bool memoryFilled;
    bool mMaemo5Stacked;
    Qt::WidgetAttribute mOrientation;
    // can't use QGraphicsItem::isVisible because we need to support visible children of
    // invisible parents
    bool mWidgetVisible;
    QString mSharedMemoryName;

    //Multipoint-touch
    MultiPointTouchUi::InputMode mMouseInputMode;

    QGraphicsItem *mFilterItem;
};

#endif // WIDGET_H
