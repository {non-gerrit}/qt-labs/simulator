/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef WIDGETMANAGER_H
#define WIDGETMANAGER_H

#include "deviceitem.h"
#include "qsimulatordata_p.h"
#include "multipointtouchui.h"

#include <QtGui/QImage>
#include <QtCore/QObject>

class Application;
class DisplayWidget;
class Widget;
class QPoint;
class QRect;
class QTableWidget;
class QWidget;
class QMenuBar;
class WidgetManager;

class TouchScriptInterface : public QObject {
    Q_OBJECT
public:
    TouchScriptInterface(WidgetManager *wm);
    virtual ~TouchScriptInterface();

    Q_INVOKABLE void beginTouch(int id, int x, int y);
    Q_INVOKABLE void updateTouch(int id, int x, int y);
    Q_INVOKABLE void endTouch(int id, int x, int y);

private:
    QtSimulatorPrivate::TouchPointData updateTouchPoint(const QtSimulatorPrivate::TouchPointData &touchPoint,
                                                        QEvent::Type touchType, const QPointF &center,
                                                        const QSizeF &size);
    void fireTouchEvent(int id, int x, int y, QEvent::Type type);

    WidgetManager *mWidgetManager;
    QtSimulatorPrivate::TouchEventData data;
};

class WidgetManager : public QObject
{
    Q_OBJECT
public:
    explicit WidgetManager(DisplayWidget *dw, QObject *parent = 0);
    ~WidgetManager();

    Widget* widgetForId(int) const;
    Widget* focusWidget() const;
    Widget* activeWidget() const;
    Widget* topWidget() const;

    DisplayWidget* display() const;
    QImage::Format imageFormat() const;
    inline QWidget* logWidget() { return configWidget; }

    QtSimulatorPrivate::NewWindowInfo create(
        int parentWidgetId, QRect geometry, const QString &title, Application *app);
    void runCurrentGestureScript();

public slots:
    int widgetAt(const QPoint &position) const;
    void showWidget(int);
    void paintWidget(int id);
    void setWidgetGeometry(int id, const QRect &geometry);
    void moveWidget(int id, const QPoint &to);
    void hideWidget(int id);
    void destroyWidget(int id);
    void raiseAboveMenus(int id);
    void dropBelowMenus(int id);
    void stackWidget(int id, int pos); // -1 front, -2 back
    void setHighestZ(Widget *w);
    void setLowestZ(Widget *w);
    void setZBefore(Widget *w, Widget *other);
    void setWidgetWindowTitle(int id, const QString &title);
    void setFocusWidget(int id);
    void setWidgetOpacity(int id, double opacity);
    void setWidgetParent(int id, int newParentId);
    void setMaemo5StackedWindowFlag(int id, bool stacked);
    void setOrientationAttribute(int id, int orientation, bool on);
    void setAcceptsTouchEvents(int id, bool accept);

    void onApplicationUnregistered(int appId);

    void sendKeyPress(Qt::Key key, const QString &text = "");
    void sendKeyRelease(Qt::Key key);
    void closeCurrentWindow();
    void triggerSymbianSoftKeyAction(int buttonNr);

    void changeOffset(const QPoint &offset);

    void updateSymbianSoftKeys();
    void setMouseInputMode(MultiPointTouchUi::InputMode newMode);
    void setCurrentGesturePath(const QString &path);

    TouchScriptInterface *scriptInterface() const { return mScriptInterface; }

private slots:
    void handleUpdateRequests();
    void updateMaemoNavigate(Widget *topWidget);
    void maybeFullscreen(Widget *topWidget);
    void handleTopOrientation(Widget *topWidget);

signals:
    void topWidgetChanged(Widget *);
    void topOrientationChanged(Qt::WidgetAttribute orientation);
    void symbianSoftKeyTextChanged(int buttonNr, QString text);
    void maemoNavigationChanged(MaemoNavigationMode mode);
    void gestureScriptRequested(const QString &filePath);

private:
    void updateLogItem(int id);
    void removeWidgetFromTable(int id);
    void updateWidgetOffset(const QPoint &newOffset);
    void updateMaemoNavigate();
    QString freeSharedMemoryName();

    QList<Widget*> widgets;
    QTableWidget *tableWidget;
    QWidget *configWidget;
    DisplayWidget *displayWidget;
    Widget *mFocusWidget;
    QPoint mOffset;
    int mSharedMemoryIndex;

    QImage::Format mImageFormat;
    bool mSymbianSoftKeysSupported;
    MultiPointTouchUi::InputMode mCurrentMouseInputMode;

    friend class TouchScriptInterface;
    TouchScriptInterface *mScriptInterface;
    QString mCurrentGesturePath;
};

#endif
