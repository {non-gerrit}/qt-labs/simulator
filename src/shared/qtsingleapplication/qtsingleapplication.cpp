/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "qtsingleapplication.h"
#include "qtlocalpeer.h"

#ifdef Q_OS_WIN
#include <Windows.h>
#endif

#include <QtGui/QWidget>
#include <QtGui/QFileOpenEvent>

#if defined(Q_WS_X11)
#include <private/qt_x11_p.h>
#include <QtGui/QX11Info>
#endif

namespace SharedTools {

void QtSingleApplication::sysInit(const QString &appId)
{
    actWin = 0;
    peer = new QtLocalPeer(this, appId);
    connect(peer, SIGNAL(messageReceived(const QString&)), SIGNAL(messageReceived(const QString&)));
}


QtSingleApplication::QtSingleApplication(int &argc, char **argv, bool GUIenabled)
    : QApplication(argc, argv, GUIenabled)
{
    sysInit();
}


QtSingleApplication::QtSingleApplication(const QString &appId, int &argc, char **argv)
    : QApplication(argc, argv)
{
    sysInit(appId);
}


QtSingleApplication::QtSingleApplication(int &argc, char **argv, Type type)
    : QApplication(argc, argv, type)
{
    sysInit();
}


#if defined(Q_WS_X11)
QtSingleApplication::QtSingleApplication(Display* dpy, Qt::HANDLE visual, Qt::HANDLE colormap)
    : QApplication(dpy, visual, colormap)
{
    sysInit();
}

QtSingleApplication::QtSingleApplication(Display *dpy, int &argc, char **argv, Qt::HANDLE visual, Qt::HANDLE cmap)
    : QApplication(dpy, argc, argv, visual, cmap)
{
    sysInit();
}

QtSingleApplication::QtSingleApplication(Display* dpy, const QString &appId,
    int argc, char **argv, Qt::HANDLE visual, Qt::HANDLE colormap)
    : QApplication(dpy, argc, argv, visual, colormap)
{
    sysInit(appId);
}
#endif

bool QtSingleApplication::event(QEvent *event)
{
    if (event->type() == QEvent::FileOpen) {
        QFileOpenEvent *foe = static_cast<QFileOpenEvent*>(event);
        emit fileOpenRequest(foe->file());
        return true;
    }
    return QApplication::event(event);
}

bool QtSingleApplication::isRunning()
{
    return peer->isClient();
}


bool QtSingleApplication::sendMessage(const QString &message, int timeout)
{
    return peer->sendMessage(message, timeout);
}


QString QtSingleApplication::id() const
{
    return peer->applicationId();
}


void QtSingleApplication::setActivationWindow(QWidget *aw, bool activateOnMessage)
{
    actWin = aw;
    if (activateOnMessage)
        connect(peer, SIGNAL(messageReceived(QString)), this, SLOT(activateWindow()));
    else
        disconnect(peer, SIGNAL(messageReceived(QString)), this, SLOT(activateWindow()));
}


QWidget* QtSingleApplication::activationWindow() const
{
    return actWin;
}

void QtSingleApplication::activateWindow()
{
    if (actWin) {
        actWin->setWindowState(actWin->windowState() & ~Qt::WindowMinimized);
        actWin->raise();
        actWin->activateWindow();
#ifdef Q_OS_WIN
        WId winId = actWin->winId();
        DWORD foregroundPId = GetWindowThreadProcessId(GetForegroundWindow(), NULL);
        DWORD myPId = GetWindowThreadProcessId(winId, NULL);
        if (foregroundPId != myPId) {
            AttachThreadInput(foregroundPId, myPId, true);
            BringWindowToTop(winId);
            AttachThreadInput(foregroundPId, myPId, false);
        }
#elif defined(Q_WS_X11)
        XEvent xev;
        xev.xclient.type = ClientMessage;
        xev.xclient.message_type = ATOM(_NET_ACTIVE_WINDOW);
        xev.xclient.display = X11->display;
        xev.xclient.window = actWin->winId();
        xev.xclient.format = 32;
        xev.xclient.data.l[0] = 2; // from pager/user
        xev.xclient.data.l[1] = X11->time;
        xev.xclient.data.l[2] = 0; // currently active window
        xev.xclient.data.l[3] = 0; // unused
        xev.xclient.data.l[4] = 0; // unused
        XSendEvent(X11->display, QX11Info::appRootWindow(actWin->x11Info().screen()), False,
                   SubstructureRedirectMask | SubstructureNotifyMask, &xev);
#endif
    }
}

} // namespace SharedTools
