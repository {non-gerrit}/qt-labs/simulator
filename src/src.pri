include(other/other.pri)
include(mobility/mobility.pri)
include(ui/ui.pri)
include(modeltest/modeltest.pri)
include(shared/qtlockedfile/qtlockedfile.pri)
include(shared/qtsingleapplication/qtsingleapplication.pri)
include(nfctargetgenerator/nfctargetgenerator.pri)

DEFINES += SIMULATOR_APPLICATION

# Input
SOURCES += src/main.cpp
HEADERS +=

# Shared communication implementation
SOURCES += $$QT_NOKIA_SDK_PATH/src/gui/kernel/qsimulatordata.cpp
HEADERS += $$QT_NOKIA_SDK_PATH/src/gui/kernel/qsimulatordata_p.h
OTHER_FILES += \
    $$QT_NOKIA_SDK_PATH/src/gui/kernel/qsimulatorconnection.cpp \
    $$QT_NOKIA_SDK_PATH/src/gui/kernel/qsimulatorconnection_p.h

# Mobility
INCLUDEPATH += \
    $$QT_MOBILITY_SOURCE_PATH/src \
    $$QT_MOBILITY_SOURCE_PATH/src/global \
    $$QT_MOBILITY_SOURCE_PATH/src/contacts \
    $$QT_MOBILITY_SOURCE_PATH/src/contacts/engines \
    $$QT_MOBILITY_SOURCE_PATH/src/contacts/filters \
    $$QT_MOBILITY_SOURCE_PATH/src/contacts/details \
    $$QT_MOBILITY_SOURCE_PATH/src/contacts/requests \
    $$QT_MOBILITY_SOURCE_PATH/src/multimedia \
    $$QT_MOBILITY_SOURCE_PATH/src/organizer \
    $$QT_MOBILITY_SOURCE_PATH/src/feedback \
    $$QT_MOBILITY_SOURCE_PATH/src/connectivity/nfc \
    $$QT_MOBILITY_SOURCE_PATH/plugins/sensors/simulator \
    $$QT_MOBILITY_SOURCE_PATH/plugins/organizer/simulator \
    $$QT_MOBILITY_SOURCE_PATH/plugins/feedback/simulator

SOURCES += $$QT_MOBILITY_SOURCE_PATH/src/systeminfo/qsysteminfodata_simulator.cpp \
           $$QT_MOBILITY_SOURCE_PATH/src/location/qlocationdata_simulator.cpp \
           $$QT_MOBILITY_SOURCE_PATH/src/contacts/engines/qcontactmemorybackenddata_simulator.cpp \
           $$QT_MOBILITY_SOURCE_PATH/plugins/sensors/simulator/qsensordata_simulator.cpp \
           $$QT_MOBILITY_SOURCE_PATH/plugins/organizer/simulator/qorganizerdata_simulator.cpp \
           $$QT_MOBILITY_SOURCE_PATH/plugins/feedback/simulator/qfeedbackdata_simulator.cpp \
           $$QT_MOBILITY_SOURCE_PATH/src/gallery/simulator/docgallerysimulatordata.cpp \
           $$QT_MOBILITY_SOURCE_PATH/plugins/multimedia/simulator/qsimulatormultimediadata.cpp \
           $$QT_MOBILITY_SOURCE_PATH/src/connectivity/nfc/targetemulator.cpp
HEADERS += $$QT_MOBILITY_SOURCE_PATH/src/systeminfo/qsysteminfodata_simulator_p.h \
           $$QT_MOBILITY_SOURCE_PATH/src/location/qlocationdata_simulator_p.h \
           $$QT_MOBILITY_SOURCE_PATH/src/contacts/engines/qcontactmemorybackenddata_simulator_p.h \
           $$QT_MOBILITY_SOURCE_PATH/plugins/sensors/simulator/qsensordata_simulator_p.h \
           $$QT_MOBILITY_SOURCE_PATH/plugins/organizer/simulator/qorganizerdata_simulator_p.h \
           $$QT_MOBILITY_SOURCE_PATH/plugins/feedback/simulator/qfeedbackdata_simulator_p.h \
           $$QT_MOBILITY_SOURCE_PATH/src/gallery/simulator/docgallerysimulatordata.h \
           $$QT_MOBILITY_SOURCE_PATH/plugins/multimedia/simulator/qsimulatormultimediadata_p.h \
           $$QT_MOBILITY_SOURCE_PATH/src/connectivity/nfc/targetemulator_p.h

OTHER_FILES += \
    $$QT_MOBILITY_SOURCE_PATH/src/mobilitysimulator/mobilityconnection.cpp \
    $$QT_MOBILITY_SOURCE_PATH/src/mobilitysimulator/mobilityconnection_p.h \
    $$QT_MOBILITY_SOURCE_PATH/src/contacts/engines/qcontactsimulatorbackend_p.h \
    $$QT_MOBILITY_SOURCE_PATH/src/contacts/engines/qcontactsimulatorbackend.cpp


INCLUDEPATH *= $$SIMULATOR_DEPENDENCY_PATH/include
!mac {
    LIBS *= -L$$SIMULATOR_DEPENDENCY_PATH/lib
} else {
    QMAKE_LFLAGS *= -F$$SIMULATOR_DEPENDENCY_PATH/lib
}
unix {
    LIBS *= -Wl,-rpath,$$SIMULATOR_DEPENDENCY_PATH/lib
}

unix:!mac: LIBS += -lX11

# link dependencies
INCLUDEPATH *= $$QMF_INCLUDEDIR $$QMF_INCLUDEDIR/support
!mac {
    qtAddLibrary(qmfclient)
    qtAddLibrary(remotecontrolwidget)
} else {
    LIBS += -framework qmfclient
    LIBS += -framework remotecontrolwidget
}
