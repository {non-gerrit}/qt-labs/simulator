/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef ACCELEROMETERCONTROL_H
#define ACCELEROMETERCONTROL_H

#include <QtGui/QQuaternion>
#include <QtGui/QVector3D>
#include <QtOpenGL/QGLWidget>

class AccelerometerControl : public QGLWidget
{
    Q_OBJECT
public:
    explicit AccelerometerControl(QWidget *parent = 0);

    void setDeviceOrientation(bool portrait);

    QVector3D value() const;
    QVector3D magneticField() const;
    double azimuth() const;

signals:
    void valueChanged(const QVector3D &value);
    void magneticFieldChanged(const QVector3D &value);
    void azimuthChanged(double azimuth);

public slots:
    void setValue(const QVector3D &value);

protected:
    virtual void initializeGL();
    virtual void resizeGL(int w, int h);
    virtual void paintGL();

    virtual void mouseMoveEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent *);

private:
    void drawMobile();
    void drawGround();
    void updateAzimuth();

private:
    QPoint mOldMousePosition;
    QQuaternion mRotation;
    qreal mGravity;
    bool mDefaultPortrait;
    QVector3D mNorthVector;
    double mMagneticFieldStrength;
    double mAzimuth;

    GLuint mFrontTexture;
    GLuint mBackTexture;
    GLuint mLeftTexture;
    GLuint mRightTexture;
    GLuint mTopTexture;
    GLuint mBottomTexture;
    GLuint mNorthTexture;

    GLuint mFrontPortraitTexture;
    GLuint mBackPortraitTexture;
    GLuint mLeftPortraitTexture;
    GLuint mRightPortraitTexture;
    GLuint mTopPortraitTexture;
    GLuint mBottomPortraitTexture;
    GLuint mFrontLandscapeTexture;
    GLuint mBackLandscapeTexture;
    GLuint mLeftLandscapeTexture;
    GLuint mRightLandscapeTexture;
    GLuint mTopLandscapeTexture;
    GLuint mBottomLandscapeTexture;
};

#endif // ACCELEROMETERCONTROL_H
