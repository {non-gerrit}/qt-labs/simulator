/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "applicationui.h"

#include <remotecontrolwidget/optionsitem.h>
#include <QtGui/QLineEdit>
#include <QtGui/QHBoxLayout>
#include <QtGui/QPushButton>
#include <QtGui/QMenuBar>
#include <QtGui/QComboBox>
#include <QtCore/QSettings>
#include "widget.h"

ApplicationUi::ApplicationUi(QWidget *parent) :
    ToolBoxPage(parent)
{
    QStringList tags;
    QList<OptionsItem *> optionsList;
    OptionsItem *item;

    mWidgetTitle = new QLineEdit();
    mWidgetTitle->setReadOnly(true);
    item = new OptionsItem(tr("Topmost widget's title"), mWidgetTitle);
    item->setTags(QStringList() << "topmost" << "wigdet" << "title");
    optionsList << item;

    QWidget *widget = new QWidget;
    mMenuLayout = new QHBoxLayout();
    widget->setLayout(mMenuLayout);
    item = new OptionsItem(tr("Application's menubar"), widget);
    item->setTags(QStringList() << "menubar");
    optionsList << item;

    QPushButton *exitButton = new QPushButton(tr("Quit"));
    item = new OptionsItem(tr("Quit current application"), exitButton);
    item->setTags(QStringList() << "quit");
    optionsList << item;
    connect(exitButton, SIGNAL(clicked()), SIGNAL(exitButtonClicked()));

    mComNokiaExtrasChooser = new QComboBox;
    mComNokiaExtrasChooser->addItem(tr("Symbian"), QVariant(QtSimulatorPrivate::SymbianComNokiaExtras));
    mComNokiaExtrasChooser->addItem(tr("Harmattan"), QVariant(QtSimulatorPrivate::HarmattanComNokiaExtras));
    connect(mComNokiaExtrasChooser, SIGNAL(currentIndexChanged(int)), SIGNAL(comNokiaExtrasChanged()));
    item = new OptionsItem(tr("Choose com.nokia.extras platform"), mComNokiaExtrasChooser);
    item->setTags(QStringList() << "extras");
    optionsList << item;

    setTitle(tr("Application"));
    setOptions(optionsList);
}

void ApplicationUi::updateMenuBarWidget(Widget *menuBarWidget)
{
    QMenuBar *menuBar = 0;
    if (menuBarWidget) {
        menuBar = menuBarWidget->menuBar();
        mWidgetTitle->setText(menuBarWidget->title);
    } else {
        mWidgetTitle->setText(QString());
    }

    if (mMenuBar == menuBar)
        return;

    if (mMenuBar) {
        mMenuBar->hide();
        mMenuLayout->removeWidget(mMenuBar);
    }

    mMenuBar = menuBar;
    if (!menuBar)
        return;

    mMenuLayout->insertWidget(0, mMenuBar);
    mMenuBar->show();
}

QIcon ApplicationUi::icon() const
{
    return QIcon(":/ui/icons/application.png");
}

QtSimulatorPrivate::ComNokiaExtrasPlatform ApplicationUi::comNokiaExtrasPlatform() const
{
    return static_cast<QtSimulatorPrivate::ComNokiaExtrasPlatform>(
                mComNokiaExtrasChooser->itemData(mComNokiaExtrasChooser->currentIndex()).toInt());
}

void ApplicationUi::writeSettings(QSettings &settings) const
{
    ToolBoxPage::writeSettings(settings);

    settings.beginGroup("ApplicationUiWidget");
    settings.setValue("comNokiaExtrasChooser", mComNokiaExtrasChooser->currentIndex());
    settings.endGroup();
}

void ApplicationUi::readSettings(QSettings &settings)
{
    ToolBoxPage::readSettings(settings);

    settings.beginGroup("ApplicationUiWidget");
    if (settings.contains("comNokiaExtrasChooser")) {
        int index = settings.value("comNokiaExtrasChooser").toInt();
        if (index >= 0 && index <= 1)
            mComNokiaExtrasChooser->setCurrentIndex(index);
    }
    settings.endGroup();
}
