/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "cameraui.h"
#include "configurationwidget.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtCore/QMetaEnum>
#include <QtGui/QComboBox>
#include <QtGui/QLineEdit>
#include <QtGui/QSlider>
#include <QtGui/QDoubleValidator>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QBoxLayout>
#include <QtGui/QDateTimeEdit>
#include <QtGui/QGroupBox>
#include <QtGui/QFormLayout>
#include <QtGui/QInputDialog>
#include <QtGui/QFileDialog>
#include <qmath.h>

CameraUi::CameraUi(QWidget *parent)
    : ToolBoxPage(parent)
{
    qRegisterMetaType<CameraUi::CameraData>("CameraUi::CameraData");
    qRegisterMetaType<CameraUi::CameraData::CameraDetails>("CameraUi::CameraData::CameraDetails");
    mScriptInterface = new CameraScriptInterface(this);

    QStringList tags;
    QList<OptionsItem *> optionsList;

    mCameras = new QComboBox();
    mRemoveCameraButton = new QPushButton(tr("Remove"));
    QPushButton *addCameraButton = new QPushButton(tr("Add"));
    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->addWidget(mRemoveCameraButton);
    hLayout->addWidget(addCameraButton);
    connect(mCameras, SIGNAL(activated(int)), SLOT(showCameraInfo()));
    connect(addCameraButton, SIGNAL(clicked()), SLOT(addCameraClicked()));
    connect(mRemoveCameraButton, SIGNAL(clicked()), SLOT(removeCameraClicked()));
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->addWidget(mCameras);
    vLayout->addLayout(hLayout);

    QFormLayout *cameraLayout = new QFormLayout();
    mDescriptionEdit = new QLineEdit();
    connect(mDescriptionEdit, SIGNAL(editingFinished()), SLOT(editCameraInfo()));
    cameraLayout->addRow(tr("Description"), mDescriptionEdit);
    mImageEdit = new QLineEdit();
    connect(mImageEdit, SIGNAL(editingFinished()), SLOT(editCameraInfo()));
    mImageBrowseButton = new QPushButton(tr("Browse"));
    connect(mImageBrowseButton, SIGNAL(clicked()), SLOT(getImagePath()));
    hLayout = new QHBoxLayout();
    hLayout->addWidget(mImageEdit);
    hLayout->addWidget(mImageBrowseButton);
    cameraLayout->addRow(tr("Image"), hLayout);
    QGroupBox *cameraPropertyGroupBox = new QGroupBox(tr("Camera Properties"));
    cameraPropertyGroupBox->setLayout(cameraLayout);
    vLayout->addWidget(cameraPropertyGroupBox);
    QWidget *cameras = new QWidget();
    cameras->setLayout(vLayout);
    OptionsItem *item = new OptionsItem("", cameras, true);
    QStringList cameraTags = tags;
    cameraTags << tr("description");
    item->setTags(cameraTags);
    optionsList << item;

    setTitle(tr("Camera"));
    setOptions(optionsList);

    // when the data changes, update the display
    connect(this, SIGNAL(cameraDataChanged(CameraUi::CameraData)),
            SLOT(showCameraInfo()));
    connect(this, SIGNAL(cameraAdded(QString, CameraUi::CameraData::CameraDetails)),
            SLOT(showCameraInfo()));
    connect(this, SIGNAL(cameraRemoved(QString)),
            SLOT(showCameraInfo()));
    connect(this, SIGNAL(cameraChanged(QString, CameraUi::CameraData::CameraDetails)),
            SLOT(showCameraInfo()));
}

CameraUi::~CameraUi()
{
}

CameraScriptInterface *CameraUi::scriptInterface() const
{
    return mScriptInterface;
}

CameraUi::CameraData CameraUi::cameraData() const
{
    return mData;
}

void CameraUi::setCameraData(const CameraUi::CameraData &data)
{
    setDisplayedCameraData(data);
    emit cameraDataChanged(data);
}

void CameraUi::setDisplayedCameraData(const CameraUi::CameraData &data)
{
    mData = data;
    updateCameraList();
}

void CameraUi::getImagePath()
{
    mImageEdit->setText(QFileDialog::getOpenFileName(this, tr("Choose Image"),
                                                 ".", tr("Images (*.png *.xpm *.jpg)")));
    editCameraInfo();
}

void CameraUi::emitCameraDataChange() const
{
    emit cameraDataChanged(cameraData());
}

void CameraUi::showCameraInfo()
{
    bool editingEnabled = mCameras->count() != 0;
    // workaround for calling this from another thread (scripts):
    // always perform the setEnabled calls in the gui thread -
    // setEnabled calls sendEvent!
    QMetaObject::invokeMethod(this, "enableCameraControls", Qt::QueuedConnection,
                              Q_ARG(bool, editingEnabled));

    if (!editingEnabled)
        return;

    const QString curCamera = mCameras->currentText();
    mDescriptionEdit->setText(mScriptInterface->description(curCamera));
    mImageEdit->setText(mScriptInterface->imagePath(curCamera));
}

void CameraUi::editCameraInfo()
{
    if (mCameras->count() == 0)
        return;

    const QString curCamera = mCameras->currentText();
    CameraData::CameraDetails &details = mData.cameras[curCamera];
    details.description = mDescriptionEdit->text();
    details.imagePath = mImageEdit->text();

    emit cameraChanged(curCamera, details);
}

void CameraUi::updateCameraList(const QString &cameraToShow)
{
    QString showCamera = cameraToShow;
    if (showCamera.isNull())
        showCamera = mCameras->currentText();

    mCameras->clear();
    mCameras->addItems(mData.cameras.keys());

    if (mData.cameras.contains(showCamera))
        mCameras->setCurrentIndex(mData.cameras.keys().indexOf(showCamera));

    showCameraInfo();
}

void CameraUi::addDisplayedCamera(const QString &name)
{
    CameraData::CameraDetails cameraDetails;
    mData.cameras.insert(name, cameraDetails);
    updateCameraList(name);
}

void CameraUi::addCameraClicked()
{
    const QString newName = QInputDialog::getText(0,
            "Add New Camera", "Enter the new camera name:");
    if (newName.isEmpty())
        return;

    addDisplayedCamera(newName);
    CameraUi::CameraData::CameraDetails details;
    emit cameraAdded(newName, details);
}

void CameraUi::removeDisplayedCamera(const QString &name)
{
    mData.cameras.remove(name);
    updateCameraList();
}

void CameraUi::removeCameraClicked()
{
    if (mCameras->count() == 0)
        return;

    emit cameraRemoved(mCameras->currentText());
    removeDisplayedCamera(mCameras->currentText());
}

void CameraUi::enableCameraControls(bool enabled)
{
    mDescriptionEdit->setEnabled(enabled);
    mImageEdit->setEnabled(enabled);
    mRemoveCameraButton->setEnabled(enabled);
    mImageBrowseButton->setEnabled(enabled);
}

QIcon CameraUi::icon() const
{
    return QIcon(":/ui/icons/camera.png");
}

/*!
    \class CameraScriptInterface
    \brief Exposed as camera.
*/

CameraScriptInterface::CameraScriptInterface(CameraUi *ui)
    : QObject(ui)
    , ui(ui)
{
}

CameraScriptInterface::~CameraScriptInterface()
{
}

QStringList CameraScriptInterface::availableCameras() const
{
    return ui->mData.cameras.keys();
}

bool CameraScriptInterface::addCamera(const QString &name, const QString &description, const QString &imagePath)
{
    if (ui->mData.cameras.contains(name))
        return false;

    ui->addDisplayedCamera(name);
    CameraUi::CameraData::CameraDetails &details = ui->mData.cameras[name];
    details.description = description;
    details.imagePath = imagePath;
    emit ui->cameraAdded(name, details);
    return true;
}

bool CameraScriptInterface::removeCamera(const QString &name)
{
    if (!ui->mData.cameras.contains(name))
        return false;

    ui->removeDisplayedCamera(name);
    emit ui->cameraRemoved(name);
    return true;
}

bool CameraScriptInterface::setDescription(const QString &camera, const QString &description)
{
    if (!ui->mData.cameras.contains(camera))
        return false;

    if (ui->mData.cameras[camera].description != description)
    {
        ui->mData.cameras[camera].description = description;
        emit ui->cameraChanged(camera, ui->mData.cameras[camera]);
    }
    return true;
}

QString CameraScriptInterface::description(const QString &camera) const
{
    if (!ui->mData.cameras.contains(camera))
        return QString();

    return ui->mData.cameras[camera].description;
}

bool CameraScriptInterface::setImagePath(const QString &camera, const QString &imagePath)
{
    if (!ui->mData.cameras.contains(camera))
        return false;

    if (ui->mData.cameras[camera].imagePath != imagePath)
    {
        ui->mData.cameras[camera].imagePath = imagePath;
        emit ui->cameraChanged(camera, ui->mData.cameras[camera]);
    }
    return true;
}

QString CameraScriptInterface::imagePath(const QString &camera) const
{
    if (!ui->mData.cameras.contains(camera))
        return QString();

    return ui->mData.cameras[camera].imagePath;
}
