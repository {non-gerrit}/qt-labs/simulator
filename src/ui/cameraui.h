/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef CAMERAUI_H
#define CAMERAUI_H

#include <QtCore/QObject>
#include <QtCore/QHash>

#include <remotecontrolwidget/toolbox.h>

class QComboBox;
class QLineEdit;
class QPushButton;
class QRadioButton;
class CameraScriptInterface;

class CameraUi;

class CameraScriptInterface : public QObject
{
    Q_OBJECT
public:
    CameraScriptInterface(CameraUi *ui);
    virtual ~CameraScriptInterface();

//    Q_PROPERTY(QString description READ description WRITE setDescription)
    Q_INVOKABLE QStringList availableCameras() const;

public slots:
    bool addCamera(const QString &name, const QString &description = "", const QString &imagePath = "");
    bool removeCamera(const QString &name);

    bool setDescription(const QString &camera, const QString &description);
    QString description(const QString &camera) const;

    bool setImagePath(const QString &camera, const QString &imagePath);
    QString imagePath(const QString &camera) const;

private:
    CameraUi *ui;
};

class CameraUi : public ToolBoxPage
{
    Q_OBJECT
public:
    struct CameraData {
        struct CameraDetails {
            QString description;
            QString imagePath;
        };
        QHash<QString, CameraDetails> cameras;
    };

    explicit CameraUi(QWidget *parent = 0);
    virtual ~CameraUi();

    CameraScriptInterface *scriptInterface() const;
    QIcon icon() const;

    CameraUi::CameraData cameraData() const;

public slots:
    void setCameraData(const CameraUi::CameraData &data);
    void setDisplayedCameraData(const CameraUi::CameraData &data);

signals:
    void cameraDataChanged(const CameraUi::CameraData &data) const;
    void cameraChanged(const QString &name, const CameraUi::CameraData::CameraDetails &details) const;
    void cameraAdded(const QString &name, const CameraUi::CameraData::CameraDetails &details) const;
    void cameraRemoved(const QString &name) const;

private slots:
    void emitCameraDataChange() const;
    void showCameraInfo();
    void editCameraInfo();
    void updateCameraList(const QString &cameraToShow = QString());

    void getImagePath();

    void addDisplayedCamera(const QString &name);
    void removeDisplayedCamera(const QString &name);

    void addCameraClicked();
    void removeCameraClicked();

private:
    Q_INVOKABLE void enableCameraControls(bool enabled);

    friend class CameraScriptInterface;
    CameraScriptInterface *mScriptInterface;

    QComboBox *mCameras;
    QLineEdit *mDescriptionEdit;
    QLineEdit *mImageEdit;
    QPushButton *mRemoveCameraButton;
    QPushButton *mImageBrowseButton;

    CameraData mData;
};

#endif // CAMERAUI_H
