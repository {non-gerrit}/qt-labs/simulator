/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "contactsui.h"

#include "contacts.h"
#include "configurationwidget.h"

#include <remotecontrolwidget/optionsitem.h>

#include <contacts/qcontactmanager.h>
#include <contacts/qcontact.h>
#include <contacts/details/qcontactname.h>

#include <QtCore/QList>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>
#include <QtGui/QHBoxLayout>

ContactsUi::ContactsUi(Contacts *contacts, QWidget *parent)
    : ToolBoxPage(parent)
    , mContacts(contacts)
    , mContactList(0)
{
    QList<OptionsItem *> optionsList;

    mContactList = new QTableWidget();
    mContactList->setColumnCount(1);
    mContactList->setColumnWidth(0, mContactList->width() - mContactList->frameWidth());
    mContactList->setHorizontalHeaderLabels(QStringList(tr("Name")));
    mContactList->horizontalHeaderItem(0)->setTextAlignment(Qt::AlignLeft);
    mContactList->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    mContactList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    OptionsItem *item = new OptionsItem("", mContactList, true);
    optionsList << item;

    QWidget *exportImportButtons = new QWidget;
    QHBoxLayout *layout = new QHBoxLayout;
    QPushButton *contactsExportButton = new QPushButton(tr("Export"));
    connect(contactsExportButton, SIGNAL(clicked()), this, SLOT(exportContacts()));
    layout->addWidget(contactsExportButton);
    QPushButton *contactsImportButton = new QPushButton(tr("Import"));
    connect(contactsImportButton, SIGNAL(clicked()), this, SLOT(importContacts()));
    layout->addWidget(contactsImportButton);
    exportImportButtons->setLayout(layout);
    item = new OptionsItem("", exportImportButtons, true);
    optionsList << item;

    connect(mContacts, SIGNAL(contactsAdded(QList<QContactLocalId>)), this, SLOT(updateContactList()));
    connect(mContacts, SIGNAL(contactsRemoved(QList<QContactLocalId>)), this, SLOT(updateContactList()));
    connect(mContacts, SIGNAL(contactsChanged(QList<QContactLocalId>)), this, SLOT(updateContactList()));
    connect(mContacts, SIGNAL(contactsDataChanged(const QContactManager &)), this, SLOT(updateContactList()));

    setTitle(tr("Contacts"));
    setOptions(optionsList);
}

void ContactsUi::importContacts()
{
    QString importFile = QFileDialog::getOpenFileName(0, tr("Select vCard File to Import"), ".", "*.vcf");
    if (importFile.isNull())
        return;

    if (!mContacts->importFromVCardFile(importFile)) {
        QMessageBox::information(0, tr("Import Failed"),
                tr("Sorry, unable to import \"%1\".").arg(importFile));
    }

    updateContactList();
}

void ContactsUi::exportContacts()
{
    QString exportFile = QFileDialog::getSaveFileName(0, tr("Select vCard File to Export to"), ".", "*.vcf");
    if (exportFile.isNull())
        return;

    if (!mContacts->exportToVCardFile(exportFile)) {
        QMessageBox::information(0, tr("Export Failed"),
                tr("Sorry, unable to export to \"%1\".").arg(exportFile));
    }
}

void ContactsUi::updateContactList() {
    QList<QContactLocalId> contactIds = mContacts->manager()->contactIds();
    mContactList->setRowCount(contactIds.size());

    int row = 0;
    foreach (const QContactLocalId &id, contactIds) {
        QString name = mContacts->manager()->contact(id).displayLabel();
        mContactList->setItem(row++, 0, new QTableWidgetItem(name));
    }
}

QIcon ContactsUi::icon() const
{
    return QIcon(":/ui/icons/contacts.png");
}

