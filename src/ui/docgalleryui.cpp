/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "docgalleryui.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtGui/QTreeWidget>
#include <QtGui/QPushButton>
#include <QtGui/QMessageBox>
#include <QtCore/QDirIterator>
#include <QtGui/QApplication>
#include <QtGui/QDesktopServices>
#include <QtCore/QUrl>
#include <QtCore/QtConcurrentRun>

DocGalleryUi::DocGalleryUi(QWidget *parent) :
    ToolBoxPage(parent)
{
    QStringList tags;
    QList<OptionsItem *> optionsList;

    mDocGalleryTreeWidget = new QTreeWidget();
    QStringList items;
    items << "FileName" << "" << "" << "" << "";
    mDocGalleryTreeWidget->setHeaderLabels(items);
    mDocGalleryTreeWidget->setColumnCount(5);
    mDocGalleryTreeWidget->setColumnWidth(1, 50);
    mDocGalleryTreeWidget->setColumnWidth(2, 50);
    mDocGalleryTreeWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(mDocGalleryTreeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(openItem(QTreeWidgetItem*, int)));

    mArtistHead = new QTreeWidgetItem(mDocGalleryTreeWidget);
    mArtistHead->setText(0, QLatin1String("Artists"));
    mImageHead = new QTreeWidgetItem(mDocGalleryTreeWidget);
    mImageHead->setText(0, QLatin1String("Images"));

    addStandardItems();

    OptionsItem *item  = new OptionsItem("", mDocGalleryTreeWidget, true);
    tags << QLatin1String("Artist") << QLatin1String("Document") << QLatin1String("Video") << QLatin1String("Images");
    item->setTags(tags);
    optionsList << item;

    setTitle(tr("Document Gallery"));
    setOptions(optionsList);
}

void DocGalleryUi::addStandardItems()
{
    mData.images.clear();
    mData.artists.clear();
    // Add some images
    QString modelPath = qApp->applicationDirPath() + "/models";

    connect(&mImageReaderWatcher, SIGNAL(finished()), this, SLOT(finishReadingImages()));
    mImageReaderWatcher.setFuture(QtConcurrent::run(this, &DocGalleryUi::readImages, modelPath));
}

void DocGalleryUi::openItem(QTreeWidgetItem *widgetItem, int)
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(widgetItem->text(0)));
}

QtMobility::DocGallerySimulatorData DocGalleryUi::data() const
{
    return mData;
}

QList<QtMobility::DocGalleryImageItem> DocGalleryUi::readImages(const QString &directory)
{
    QList<QtMobility::DocGalleryImageItem> results;
    QDirIterator it(directory, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        QString item = it.next();
        QImage image(item);
        if (image.isNull())
            continue;

        QtMobility::DocGalleryImageItem imageItem;
        imageItem.fileName = item;
        imageItem.width = image.width();
        imageItem.height = image.height();
        imageItem.title = QLatin1String("Some model image");
        imageItem.tags = QLatin1String("Simulator Model");
        results.append(imageItem);
    }
    return results;
}

void DocGalleryUi::finishReadingImages()
{
    QList<QtMobility::DocGalleryImageItem> imageList = mImageReaderWatcher.future().result();
    foreach (const QtMobility::DocGalleryImageItem &item, imageList) {
        QLocale locale;
        QTreeWidgetItem* treeItem = new QTreeWidgetItem(mImageHead);
        treeItem->setText(0, item.fileName);
        treeItem->setText(1, locale.toString(item.width));
        treeItem->setText(2, locale.toString(item.height));
        treeItem->setText(3, item.title);
        treeItem->setText(4, item.tags);
        mData.images.append(item);
    }
    emit docGalleryDataChanged(mData);
}

QIcon DocGalleryUi::icon() const
{
    return QIcon(":/ui/icons/docgallery.png");
}

#include "moc_docgalleryui.cpp"
