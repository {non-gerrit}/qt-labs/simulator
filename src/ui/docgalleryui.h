/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef DOCGALLERYUI_H
#define DOCGALLERYUI_H

#include <remotecontrolwidget/toolbox.h>
#include <gallery/simulator/docgallerysimulatordata.h>
#include <QtCore/QFutureWatcher>

#include <QtGui/QTreeWidget>

class DocGalleryUi : public ToolBoxPage
{
    Q_OBJECT
public:
    explicit DocGalleryUi(QWidget *parent = 0);
    QtMobility::DocGallerySimulatorData data() const;
    QIcon icon() const;

signals:
    void docGalleryDataChanged(const QtMobility::DocGallerySimulatorData &mData);

public slots:
    void openItem(QTreeWidgetItem*, int);

private slots:
    void finishReadingImages();

private:
    void addStandardItems();
    QList<QtMobility::DocGalleryImageItem> readImages(const QString &directory);

    QtMobility::DocGallerySimulatorData mData;
    QTreeWidget* mDocGalleryTreeWidget;
    QTreeWidgetItem* mArtistHead;
    QTreeWidgetItem* mImageHead;
    QFutureWatcher< QList<QtMobility::DocGalleryImageItem> > mImageReaderWatcher;
};

#endif // DOCGALLERYUI_H
