/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "feedbackui.h"
#include "feedback.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtGui/QComboBox>
#include <QtGui/QBoxLayout>
#include <QtGui/QFormLayout>
#include <QtGui/QPushButton>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QLabel>
#include <QtGui/QInputDialog>

FeedbackUi::FeedbackUi(Feedback *feedback, QWidget *parent)
    : ToolBoxPage(parent)
    , mFeedback(feedback)
{
    QStringList tags;
    QList<OptionsItem *> optionsList;

    mActuators = new QComboBox;
    mAddActuatorButton = new QPushButton(tr("Add"));
    mRemoveActuatorButton = new QPushButton(tr("Remove"));
    QHBoxLayout *hLayout = new QHBoxLayout;
    hLayout->addWidget(mRemoveActuatorButton);
    hLayout->addWidget(mAddActuatorButton);
    QVBoxLayout *vLayout = new QVBoxLayout;
    vLayout->addWidget(mActuators);
    vLayout->addLayout(hLayout);

    QFormLayout *actuatorInfoLayout = new QFormLayout;
    mActuatorEnabled = new QCheckBox;
    actuatorInfoLayout->addRow(tr("Enabled"), mActuatorEnabled);
    mActuatorDefault = new QCheckBox;
    actuatorInfoLayout->addRow(tr("Default"), mActuatorDefault);
    mActuatorState = new QComboBox;
    actuatorInfoLayout->addRow(tr("State"), mActuatorState);

    QFormLayout *actuatorEffectLayout = new QFormLayout;
    mEffectName = new QLabel;
    actuatorEffectLayout->addRow(tr("Name"), mEffectName);
    mEffectState = new QComboBox;
    actuatorEffectLayout->addRow(tr("State"), mEffectState);
    mEffectDuration = new QLabel;
    actuatorEffectLayout->addRow(tr("Duration"), mEffectDuration);
    QGroupBox *actuatorEffectGroup = new QGroupBox(tr("Current Effect"));
    actuatorEffectGroup->setLayout(actuatorEffectLayout);

    QVBoxLayout *actuatorLayout = new QVBoxLayout;
    actuatorLayout->addLayout(actuatorInfoLayout);
    actuatorLayout->addWidget(actuatorEffectGroup);

    QGroupBox *actuatorPropertyGroupBox = new QGroupBox(tr("Actuator Properties"));
    actuatorPropertyGroupBox->setLayout(actuatorLayout);
    vLayout->addWidget(actuatorPropertyGroupBox);

    QWidget *feedbackWidget = new QWidget;
    feedbackWidget->setLayout(vLayout);

    connect(mActuators, SIGNAL(activated(int)), SLOT(showActuatorInfo()));
    connect(mFeedback, SIGNAL(actuatorChanged(QtMobility::ActuatorData)), SLOT(showActuatorInfo()));
    connect(mFeedback, SIGNAL(effectStateChanged(int,QtMobility::QFeedbackEffect::State)), SLOT(showActuatorInfo()));
    connect(mFeedback, SIGNAL(defaultActuatorIdChanged(int)), SLOT(showActuatorInfo()));
    connect(mFeedback, SIGNAL(actuatorAdded(QtMobility::ActuatorData)), SLOT(addActuator(QtMobility::ActuatorData)));
    connect(mFeedback, SIGNAL(actuatorRemoved(int)), SLOT(removeActuator(int)));
    connect(mActuatorDefault, SIGNAL(toggled(bool)), SLOT(changeActuatorData()));
    connect(mActuatorEnabled, SIGNAL(toggled(bool)), SLOT(changeActuatorData()));
    connect(mActuatorState, SIGNAL(currentIndexChanged(int)), SLOT(changeActuatorData()));
    connect(mEffectState, SIGNAL(currentIndexChanged(int)), SLOT(changeEffectState()));
    connect(mAddActuatorButton, SIGNAL(clicked()), SLOT(newActuatorClicked()));
    connect(mRemoveActuatorButton, SIGNAL(clicked()), SLOT(removeActuatorClicked()));

    OptionsItem *item = new OptionsItem("", feedbackWidget, true);
    item->setTags(tags);
    optionsList << item;

    initializeOptions();

    setOptions(optionsList);
    setTitle(tr("Feedback"));
}

void FeedbackUi::showActuatorInfo()
{
    if (mActuators->count() == 0) {
        setEnabled(false);
        return;
    }

    int actuatorId = mActuators->itemData(mActuators->currentIndex()).toInt();
    const QtMobility::ActuatorData &actuator = mFeedback->actuator(actuatorId);
    if (actuator.id != actuatorId) {
        setEnabled(false);
        return;
    } else {
        setEnabled(true);
    }

    mActuatorDefault->setChecked(actuatorId == mFeedback->defaultActuatorId());
    mActuatorDefault->setEnabled(!mActuatorDefault->isChecked());
    mActuatorEnabled->setChecked(actuator.enabled);
    mActuatorState->setCurrentIndex(static_cast<int>(actuator.state));

    const Feedback::EffectData &effect = mFeedback->effectForActuator(actuatorId);
    if (effect.actuatorId != actuatorId) {
        mEffectName->setText("-");
        mEffectState->setEnabled(false);
        mEffectState->setCurrentIndex(static_cast<int>(QtMobility::QFeedbackEffect::Stopped));
        mEffectDuration->setText(tr("not available"));
    } else {
        mEffectName->setText(effect.info);
        mEffectState->setEnabled(true);
        mEffectState->setCurrentIndex(static_cast<int>(effect.state));
        if (effect.duration == QtMobility::QFeedbackEffect::Infinite)
            mEffectDuration->setText(tr("infinite"));
        else
            mEffectDuration->setText(QLocale().toString(effect.duration));
    }
}

void FeedbackUi::addActuator(const QtMobility::ActuatorData &data)
{
    mActuators->addItem(data.name, data.id);
    showActuatorInfo();
}

void FeedbackUi::removeActuator(int actuatorId)
{
    for (int i = mActuators->count()-1; i >= 0; --i) {
        const int itemActuatorId = mActuators->itemData(i).toInt();
        if (itemActuatorId == actuatorId)
            mActuators->removeItem(i);
    }

    showActuatorInfo();
}

void FeedbackUi::newActuatorClicked()
{
    const QString newName = QInputDialog::getText(0,
            tr("Add New Actuator"), tr("Enter a name for the new actuator:"));
    if (newName.isEmpty())
        return;

    QtMobility::ActuatorData data;
    data.enabled = true;
    data.name = newName;
    data.state = QtMobility::QFeedbackActuator::Ready;
    mFeedback->addActuator(data);
}

void FeedbackUi::removeActuatorClicked()
{
    if (mActuators->count() == 0)
        return;

    const int actuatorId = mActuators->itemData(mActuators->currentIndex()).toInt();
    mFeedback->removeActuator(actuatorId);
}

void FeedbackUi::changeActuatorData()
{
    if (mActuators->count() == 0)
        return;

    const int actuatorId = mActuators->itemData(mActuators->currentIndex()).toInt();
    const QtMobility::ActuatorData &actuator = mFeedback->actuator(actuatorId);
    if (actuator.id != actuatorId)
        return;

    if (mActuatorDefault->isChecked()) {
        mFeedback->setDefaultActuatorId(actuatorId);
        mActuatorDefault->setEnabled(false);
    }

    QtMobility::ActuatorData newData = actuator;
    newData.enabled = mActuatorEnabled->isChecked();
    newData.state = static_cast<QtMobility::QFeedbackActuator::State>(mActuatorState->currentIndex());
    mFeedback->setActuatorData(newData);
}

void FeedbackUi::changeEffectState()
{
    if (mActuators->count() == 0)
        return;

    const int actuatorId = mActuators->itemData(mActuators->currentIndex()).toInt();
    mFeedback->setEffectState(actuatorId, static_cast<QtMobility::QFeedbackEffect::State>(mEffectState->currentIndex()));
}

void FeedbackUi::initializeOptions()
{
    // must match enums
    mActuatorState->addItem(tr("Busy"));
    mActuatorState->addItem(tr("Ready"));
    mActuatorState->addItem(tr("Unknown"));

    mEffectState->addItem(tr("Stopped"));
    mEffectState->addItem(tr("Paused"));
    mEffectState->addItem(tr("Running"));
    mEffectState->addItem(tr("Loading"));
}

void FeedbackUi::setEnabled(bool enabled)
{
    mActuators->setEnabled(enabled);
    mRemoveActuatorButton->setEnabled(enabled);
    mActuatorDefault->setEnabled(enabled);
    mActuatorEnabled->setEnabled(enabled);
    mActuatorState->setEnabled(enabled);
    mEffectState->setEnabled(enabled);
}

QIcon FeedbackUi::icon() const
{
    return QIcon(":/ui/icons/feedback.png");
}
