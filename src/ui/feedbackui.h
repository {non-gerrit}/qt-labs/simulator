/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef FEEDBACKUI_H
#define FEEDBACKUI_H

#include "feedback.h"

#include <remotecontrolwidget/toolbox.h>

QT_FORWARD_DECLARE_CLASS(QComboBox)
QT_FORWARD_DECLARE_CLASS(QCheckBox)
QT_FORWARD_DECLARE_CLASS(QLabel)
QT_FORWARD_DECLARE_CLASS(QPushButton)

class FeedbackUi : public ToolBoxPage
{
    Q_OBJECT
public:
    explicit FeedbackUi(Feedback *feedback, QWidget *parent = 0);
    QIcon icon() const;

signals:

private slots:
    void showActuatorInfo();
    void newActuatorClicked();
    void removeActuatorClicked();
    void changeActuatorData();
    void changeEffectState();

    // changes from Feedback
    void addActuator(const QtMobility::ActuatorData &data); // from Feedback
    void removeActuator(int actuatorId);

private:
    void initializeOptions();
    void setEnabled(bool enabled);

    QComboBox *mActuators;
    QPushButton *mAddActuatorButton;
    QPushButton *mRemoveActuatorButton;
    QCheckBox *mActuatorDefault;
    QCheckBox *mActuatorEnabled;
    QComboBox *mActuatorState;
    QLabel *mEffectName;
    QLabel *mEffectDuration;
    QComboBox *mEffectState;
    Feedback *mFeedback;
};

#endif // FEEDBACKUI_H
