/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "inputscriptinterface.h"
#include "mainwindow.h"
#include "displaywidget.h"

#include <QtCore/QMetaEnum>
#include <QtGui/QApplication>
#include <QtGui/QGraphicsView>

InputScriptInterface::InputScriptInterface(MainWindow *ui, QWidget *viewport)
    : QObject(ui)
    , mUi(ui)
    , mViewport(viewport)
    , mPressedButtons(NoButton)
{
    int enumIndex = metaObject()->indexOfEnumerator("MouseButton");
    QMetaEnum metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));
}

InputScriptInterface::~InputScriptInterface()
{
}

void InputScriptInterface::mousePress(MouseButton button, qint32 x, qint32 y)
{
    mousePress(button, x, y, false);
}

void InputScriptInterface::mousePress(MouseButton button, qint32 x, qint32 y, bool doubleClick)
{
    mPressedButtons |= static_cast<Qt::MouseButton>(button);
    QPoint pos = mapPositionToDevice(QPoint(x, y));
    QEvent::Type type;
    if (doubleClick)
        type = QEvent::MouseButtonDblClick;
    else
        type = QEvent::MouseButtonPress;
    QMouseEvent *event = new QMouseEvent(type, pos,
                                         static_cast<Qt::MouseButton>(button),
                                         static_cast<Qt::MouseButtons>(mPressedButtons),
                                         Qt::NoModifier);
    qApp->postEvent(mViewport, event);
}

void InputScriptInterface::mouseMove(qint32 x, qint32 y)
{
    QPoint pos = mapPositionToDevice(QPoint(x, y));
    QMouseEvent *event = new QMouseEvent(QEvent::MouseMove, pos,
                                         Qt::NoButton,
                                         static_cast<Qt::MouseButtons>(mPressedButtons),
                                         Qt::NoModifier);
    qApp->postEvent(mViewport, event);
}

void InputScriptInterface::mouseRelease(MouseButton button, qint32 x, qint32 y)
{
    mPressedButtons &= ~static_cast<Qt::MouseButton>(button);
    QPoint pos = mapPositionToDevice(QPoint(x, y));
    QMouseEvent *event = new QMouseEvent(QEvent::MouseButtonRelease, pos,
                                         static_cast<Qt::MouseButton>(button),
                                         static_cast<Qt::MouseButtons>(mPressedButtons),
                                         Qt::NoModifier);
    qApp->postEvent(mViewport, event);
}

void InputScriptInterface::click(qint32 x, qint32 y)
{
    mousePress(LeftButton, x, y);
    mouseRelease(LeftButton, x, y);
}

void InputScriptInterface::doubleClick(qint32 x, qint32 y)
{
    click(x, y);
    mousePress(LeftButton, x, y, true);
    mouseRelease(LeftButton, x, y);
}

void InputScriptInterface::rightClick(qint32 x, qint32 y)
{
    mousePress(RightButton, x, y);
    mouseRelease(RightButton, x, y);
}


QPoint InputScriptInterface::mapPositionToDevice(const QPoint &position) const
{
    DeviceData d = mUi->deviceItem()->mDeviceData;
    QPoint returnedPosition;

    switch (mUi->deviceItem()->screenOrientation()) {
    case topUp:
        switch (mUi->deviceItem()->deviceOrientation()) {
        case topUp:
            returnedPosition = position + d.offset + d.availableGeometry.topLeft();
            break;
        case topDown:
            returnedPosition.setX(d.mockup->width() - d.offset.x() - d.availableGeometry.x() - position.x());
            returnedPosition.setY(d.mockup->height() - d.offset.y() - d.availableGeometry.y() - position.y());
            break;
        case leftUp:
            returnedPosition.setX(d.mockup->height() - d.offset.y() - d.availableGeometry.y() - position.y());
            returnedPosition.setY(d.offset.x() + d.availableGeometry.x() + position.x());
            break;
        case rightUp:
            returnedPosition.setX(d.offset.y() + d.availableGeometry.y() + position.y());
            returnedPosition.setY(d.mockup->width() - d.offset.x() - d.availableGeometry.x() - position.x());
            break;
        }
        break;
    case topDown:
        switch (mUi->deviceItem()->deviceOrientation()) {
        case topUp:
            returnedPosition.setX(d.offset.x() + d.resolution.width() - d.availableGeometry.x() - position.x());
            returnedPosition.setY(d.mockup->height() - d.offset.y() -  d.availableGeometry.y() - position.y());
            break;
        case topDown:
            returnedPosition.setX(d.mockup->width() - d.offset.x() - d.resolution.width() + d.availableGeometry.x() + position.x());
            returnedPosition.setY(d.mockup->height() - d.offset.y() - d.resolution.height() + d.availableGeometry.y() + position.y());
            break;
        case leftUp:
            returnedPosition.setX(d.mockup->height() - d.offset.y() - d.resolution.height() +  d.availableGeometry.y() + position.y());
            returnedPosition.setY(d.offset.x() +d.resolution.width() - d.availableGeometry.x() - position.x());
            break;
        case rightUp:
            returnedPosition.setX(d.offset.y() + d.resolution.height() - d.availableGeometry.y() - position.y());
            returnedPosition.setY(d.mockup->width() - d.offset.x() - d.resolution.width() + d.availableGeometry.x() + position.x());
            break;
        }
        break;
    case leftUp:
        switch (mUi->deviceItem()->deviceOrientation()) {
        case topUp:
            returnedPosition.setX(d.offset.x() + d.availableGeometry.y() + position.y());
            returnedPosition.setY(d.offset.y() + d.resolution.height() - d.availableGeometry.x() - position.x());
            break;
        case topDown:
            returnedPosition.setX(d.mockup->width() - d.offset.x() - d.availableGeometry.y() - position.y());
            returnedPosition.setY(d.mockup->height() - d.offset.y() - d.resolution.height() + d.availableGeometry.x() + position.x());
            break;
        case leftUp:
            returnedPosition.setX(d.mockup->height() - d.offset.y() - d.resolution.height() + d.availableGeometry.x() + position.x());
            returnedPosition.setY(d.offset.x() + d.availableGeometry.y() + position.y());
            break;
        case rightUp:
            returnedPosition.setX(d.offset.y() + d.resolution.height() - d.availableGeometry.x() - position.x());
            returnedPosition.setY(d.mockup->width() - d.offset.x() - d.availableGeometry.y() - position.y());
            break;
        }
        break;
    case rightUp:
        switch (mUi->deviceItem()->deviceOrientation()) {
        case topUp:
            returnedPosition.setX(d.offset.x() + d.resolution.width() - d.availableGeometry.y() - position.y());
            returnedPosition.setY(d.offset.y() + d.availableGeometry.x() + position.x());
            break;
        case topDown:
            returnedPosition.setX(d.mockup->width() - d.offset.x() - d.resolution.width() + d.availableGeometry.y() + position.y());
            returnedPosition.setY(d.mockup->height() - d.offset.y() - d.availableGeometry.x() - position.x());
            break;
        case leftUp:
            returnedPosition.setX(d.mockup->height() - d.offset.y() - d.availableGeometry.x() - position.x());
            returnedPosition.setY(d.offset.x() +d.resolution.width() - d.availableGeometry.y() - position.y());
            break;
        case rightUp:
            returnedPosition.setX(d.offset.y() + d.availableGeometry.left() + position.x());
            returnedPosition.setY(d.mockup->width() - d.offset.x() - d.resolution.width() + d.availableGeometry.y() + position.y());
            break;
        }
        break;
    }
    return returnedPosition;
}

void InputScriptInterface::keyPress(QChar c)
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyPress, c.unicode(), Qt::NoModifier, QString(c));
    qApp->postEvent(mViewport, event);
}

void InputScriptInterface::keyRelease(QChar c)
{
    QKeyEvent *event = new QKeyEvent(QEvent::KeyRelease, c.unicode(), Qt::NoModifier, QString(c));
    qApp->postEvent(mViewport, event);
}

void InputScriptInterface::type(QChar c)
{
    keyPress(c);
    keyRelease(c);
}

void InputScriptInterface::type(const QString &string)
{
    foreach (const QChar &c, string) {
        type(c);
    }
}

int InputScriptInterface::mouseX() const
{
    QPointF pos = mView->mapFromGlobal(QCursor::pos());
    pos = mDisplay->mapFromScene(pos);
    return pos.x() - mUi->deviceItem()->mDeviceData.availableGeometry.x();
}

int InputScriptInterface::mouseY() const
{
    QPointF pos = mView->mapFromGlobal(QCursor::pos());
    pos = mDisplay->mapFromScene(pos);
    return pos.y() - mUi->deviceItem()->mDeviceData.availableGeometry.y();
}

void InputScriptInterface::setView(QGraphicsView *view)
{
    mView = view;
}

void InputScriptInterface::setDisplay(DisplayWidget *display)
{
    mDisplay = display;
}
