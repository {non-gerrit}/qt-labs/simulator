/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef INPUTSCRIPTINTERFACE_H
#define INPUTSCRIPTINTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QPoint>

class MainWindow;
class QGraphicsView;
class DisplayWidget;

class InputScriptInterface : public QObject
{
    Q_OBJECT
public:
    // depends on order of items
    // has to match Qt::MouseButton
    enum MouseButton {
        NoButton         = 0x00000000,
        LeftButton       = 0x00000001,
        RightButton      = 0x00000002,
        MidButton        = 0x00000004, // ### Qt 5: remove me
        MiddleButton     = MidButton,
        XButton1         = 0x00000008,
        XButton2         = 0x00000010,
        MouseButtonMask  = 0x000000ff
    };
    Q_ENUMS(MouseButton);
    Q_DECLARE_FLAGS(MouseButtons, MouseButton)

    InputScriptInterface(MainWindow *mUi, QWidget *viewport);
    virtual ~InputScriptInterface();

    void setView(QGraphicsView *view);
    void setDisplay(DisplayWidget *display);

    Q_INVOKABLE void mousePress(MouseButton button, qint32 x, qint32 y);
    Q_INVOKABLE void mouseMove(qint32 x, qint32 y);
    Q_INVOKABLE void mouseRelease(MouseButton button, qint32 x, qint32 y);

    Q_INVOKABLE int mouseX() const;
    Q_INVOKABLE int mouseY() const;

    Q_INVOKABLE void click(qint32 x, qint32 y);
    Q_INVOKABLE void doubleClick(qint32 x, qint32 y);
    Q_INVOKABLE void rightClick(qint32 x, qint32 y);

    Q_INVOKABLE void keyPress(QChar c);
    Q_INVOKABLE void keyRelease(QChar c);
    Q_INVOKABLE void type(const QString &string);

private:
    void type(QChar c);
    void mousePress(MouseButton button, qint32 x, qint32 y, bool doubleClick);
    QPoint mapPositionToDevice(const QPoint &position) const;
    QPoint mapPositionToScreen(const QPoint &position) const;

    MainWindow *mUi;
    QWidget *mViewport;
    Qt::MouseButtons mPressedButtons;
    QGraphicsView *mView;
    DisplayWidget *mDisplay;
};

#endif // INPUTSCRIPTINTERFACE_H
