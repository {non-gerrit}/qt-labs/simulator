/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "mainwindow.h"
#include "configurationwidget.h"
#include "displaywidget.h"
#include "applicationmanager.h"
#include "phononmanager.h"
#include "widgetmanager.h"
#include "mobilitymanager.h"
#include "mobilitydata.h"
#include "qtsingleapplication.h"

#include "viewui.h"
#include "applicationui.h"
#include "contactsui.h"
#include "systeminfogenericui.h"
#include "systeminfonetworkui.h"
#include "systeminfostorageui.h"
#include "messagingui.h"
#include "sensorsui.h"
#include "organizerui.h"
#include "feedbackui.h"
#include "docgalleryui.h"
#include "cameraui.h"
#include "nfcui.h"
#include "inputscriptinterface.h"

#include <remotecontrolwidget/locationui.h>
#include <remotecontrolwidget/scriptui.h>
#include <remotecontrolwidget/scriptadapter.h>
#include <remotecontrolwidget/batterybutton.h>
#include <remotecontrolwidget/powerbutton.h>
#include <remotecontrolwidget/signalstrengthbutton.h>
#include <remotecontrolwidget/networkmodebutton.h>
#include <remotecontrolwidget/favoritescriptbutton.h>
#include <remotecontrolwidget/plugininterface.h>

#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtCore/QSettings>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtGui/QKeyEvent>
#include <QtGui/QLayout>
#include <QtGui/QGraphicsView>
#include <QtGui/QPixmap>
#include <QtGui/QPushButton>
#include <QtGui/QApplication>
#include <QtGui/QDesktopWidget>
#include <QtGui/QMessageBox>
#include <QtGui/QDesktopServices>
#include <QtCore/QPluginLoader>

#if defined(Q_WS_X11)
#include <private/qt_x11_p.h>
#include <QtGui/QX11Info>
#endif

// VERSION UPDATE
const VersionStruct simulatorVersion(1, 2, 0, 1);

// Increment this value if the scripts that come bundled with the Simulator
// have changed and the user should be asked whether he wants to copy them
// to his user script directory.
const int scriptVersion = 1;

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , view(0)
    , config(0)
    , applicationManager(0)
    , mDeviceItem(0)
    , debugWindow(0)
    , startupError(false)
{
    setWindowIcon(QIcon(":/ui/icons/hicolor/256x256/apps/Nokia-Simulator.png"));

    applicationManager = new ApplicationManager(simulatorVersion, this);
    if (applicationManager->simulatorAlreadyStarted()
        || !applicationManager->initializeFontDirectory()) {
        QTimer::singleShot(0, this, SLOT(close()));
        startupError = true;
        return;
    }

    MobilityData *mobility = new MobilityData(this);
    mobilityServer = new MobilityServer(simulatorVersion, mobility, this);

    setWindowTitle(tr("Qt Simulator"));
#ifdef Q_OS_MAC
    // Sorry, widgets with translucent background are broken on MacOS X.
    // Wait until QTBUG-6831 is fixed.
#else
    setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::FramelessWindowHint);
    // set a mask, because kwin's compositor won't draw shadows under the window then
    setMask(QRect(0,0,4096,4096));
#endif
#ifdef Q_OS_WIN
    // Enable closing the simulator from the taskbar
    setWindowFlags(windowFlags() | Qt::WindowSystemMenuHint);
#endif

    scene = new QGraphicsScene(this);

    mDeviceItem = new DeviceItem;

    scene->addItem(mDeviceItem);

    view = new QGraphicsView(scene);
    view->setOptimizationFlag(QGraphicsView::DontSavePainterState);
    view->setRenderHint(QPainter::SmoothPixmapTransform);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    // otherwise the graphics view won't be transparent
    view->viewport()->setAutoFillBackground(false);

    PhononManager* phononManager = new PhononManager(this);
    phononManager->setObjectName("PhononManager");
    applicationManager->setPhononManager(phononManager);

    WidgetManager *widgetManager = new WidgetManager(mDeviceItem->display(), this);
    applicationManager->setWidgetManager(widgetManager);

    connect(applicationManager, SIGNAL(applicationRegistered(Application*)), this, SLOT(activateWindow()));
    connect(applicationManager, SIGNAL(applicationUnRegistered(int)), widgetManager, SLOT(onApplicationUnregistered(int)));
    connect(applicationManager, SIGNAL(softkeyTextSizeChanged(double)), mDeviceItem, SLOT(setSymbianSoftKeyTextSize(double)));
    connect(widgetManager, SIGNAL(symbianSoftKeyTextChanged(int,QString)), mDeviceItem, SLOT(setSymbianSoftKeyText(int,QString)));
    connect(widgetManager, SIGNAL(maemoNavigationChanged(MaemoNavigationMode)), mDeviceItem, SLOT(setMaemoNavigationMode(MaemoNavigationMode)));
    connect(widgetManager, SIGNAL(topOrientationChanged(Qt::WidgetAttribute)), mDeviceItem, SLOT(updateOrientation(Qt::WidgetAttribute)));
    connect(mDeviceItem, SIGNAL(deviceChanged(QSize,DeviceData)), applicationManager, SLOT(updateDisplayInformation(QSize,DeviceData)));
    connect(mDeviceItem, SIGNAL(drag(QPoint)), this, SLOT(dragFromView(QPoint)));
    connect(mDeviceItem, SIGNAL(viewSizeRequired(QSize)), this, SLOT(setViewSize(QSize)));
    connect(mDeviceItem, SIGNAL(buttonPressed(Qt::Key, QString)), widgetManager, SLOT(sendKeyPress(Qt::Key, QString)));
    connect(mDeviceItem, SIGNAL(buttonReleased(Qt::Key)), widgetManager, SLOT(sendKeyRelease(Qt::Key)));
    connect(mDeviceItem, SIGNAL(closeWindowPressed()), widgetManager, SLOT(closeCurrentWindow()));
    connect(mDeviceItem, SIGNAL(symbianSoftKeyClicked(int)), widgetManager, SLOT(triggerSymbianSoftKeyAction(int)));
    connect(mDeviceItem, SIGNAL(offsetChanged(const QPoint &)), widgetManager, SLOT(changeOffset(const QPoint &)));
    connect(mDeviceItem, SIGNAL(deviceChanged(QSize,DeviceData)), widgetManager, SLOT(updateSymbianSoftKeys()));
    connect(mDeviceItem, SIGNAL(orientationChanged(Orientation)), mobility, SLOT(rotateDevice(Orientation)));
    connect(mobilityServer, SIGNAL(applicationConnected(int, VersionStruct)),
            applicationManager, SLOT(updateMobilityVersion(int, VersionStruct)));

    view->setFrameStyle(QFrame::NoFrame);
    view->setParent(this);

    // Setup debug window.
    debugWindow = new QWidget(this);
    debugWindow->setWindowFlags(Qt::Window);
    debugWindow->setWindowTitle(qApp->applicationName() + " " + simulatorVersion.toString());
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(applicationManager->logWidget());
    layout->addWidget(widgetManager->logWidget());
    debugWindow->setLayout(layout);

    // Setup configuration window.
    config = new ConfigurationWidget(this);
    config->setWindowFlags(Qt::Window);
    connect(mDeviceItem, SIGNAL(sizeChanged(const QSize &)), this, SLOT(setSizeToDevice(const QSize &)));
    connect(config, SIGNAL(closeMainWindow()), this, SLOT(close()));

    scriptAdapter = new ScriptAdapter(this);

    // build ui pages
    ViewUi *viewUi = new ViewUi(mDeviceItem, this);
    if (!viewUi->initializeDeviceList()) {
        QTimer::singleShot(0, this, SLOT(close()));
        startupError = true;
        return;
    }
    config->addPage(viewUi);
    connect(mDeviceItem, SIGNAL(orientationLocked(bool)), viewUi, SLOT(setOrientationLocked(bool)));

    ApplicationUi *applicationUi = new ApplicationUi(this);
    config->addPage(applicationUi);
    connect(applicationUi, SIGNAL(exitButtonClicked()), applicationManager, SLOT(killCurrentApplication()));
    connect(widgetManager, SIGNAL(topWidgetChanged(Widget *)), applicationUi, SLOT(updateMenuBarWidget(Widget *)));
    connect(applicationUi, SIGNAL(comNokiaExtrasChanged()), applicationManager, SLOT(recommendRestartIfAppRunning()));
    applicationManager->setApplicationUi(applicationUi);

    GenericSystemInfoUi *genericSystemInfoUi = new GenericSystemInfoUi(this);
    mobility->mSystemInfoGenericUi = genericSystemInfoUi;
    config->addPage(genericSystemInfoUi);

    StorageSystemInfoUi *storageSystemInfoUi = new StorageSystemInfoUi(this);
    mobility->mSystemInfoStorageUi = storageSystemInfoUi;
    config->addPage(storageSystemInfoUi);

    NetworkSystemInfoUi *networkSystemInfoUi = new NetworkSystemInfoUi(this);
    mobility->mSystemInfoNetworkUi = networkSystemInfoUi;
    config->addPage(networkSystemInfoUi);

    LocationUi *locationUi = new LocationUi(this);
    mobility->mLocationUi = locationUi;
    config->addPage(locationUi);

    config->addPage(new ContactsUi(mobility->mContacts, this));
    config->addPage(new MessagingUi(mobility, this));

    SensorsUi *sensorsUi = new SensorsUi(this);
    mobility->mSensorsUi = sensorsUi;
    config->addPage(sensorsUi);
    connect(mDeviceItem, SIGNAL(deviceChanged(bool)), sensorsUi, SLOT(updateDeviceDefaultOrientation(bool)));

    OrganizerUi *organizerUi = new OrganizerUi(mobility->mOrganizer, this);
    config->addPage(organizerUi);

    FeedbackUi *feedbackUi = new FeedbackUi(mobility->mFeedback, this);
    config->addPage(feedbackUi);

    DocGalleryUi *docgalleryUi = new DocGalleryUi(this);
    mobility->mDocGalleryUi = docgalleryUi;
    config->addPage(docgalleryUi);

    CameraUi *cameraUi = new CameraUi(this);
    mobility->mCameraUi = cameraUi;
    config->addPage(cameraUi);

    NfcUi *nfcUi = new NfcUi(this);
    mobility->mNfcUi = nfcUi;
    config->addPage(nfcUi);

    mTouchUi = new MultiPointTouchUi(this);
    mTouchUi->setGestureScriptPath(userResourcePath() + "/scripts/gestures");
    config->addPage(mTouchUi);
    connect(mTouchUi, SIGNAL(inputModeChanged(MultiPointTouchUi::InputMode)),
            widgetManager, SLOT(setMouseInputMode(MultiPointTouchUi::InputMode)));
    connect(this, SIGNAL(mouseInputModeChanged(MultiPointTouchUi::InputMode)),
            mTouchUi, SLOT(setInputMode(MultiPointTouchUi::InputMode)));
    connect(mTouchUi, SIGNAL(customGesturePathChanged(QString)),
            widgetManager, SLOT(setCurrentGesturePath(QString)));

    ScriptUi *scriptUi = new ScriptUi(scriptAdapter, this);
    FavoriteScriptButton *scriptButton = new FavoriteScriptButton(this);
    setupScriptDirectories(scriptUi, scriptButton);
    config->addPage(scriptUi);

    NetworkModeButton *networkModeButton = new NetworkModeButton(this);
    connect(networkModeButton, SIGNAL(networkModeChanged(NetworkModeButton::NetworkMode)),
            networkSystemInfoUi, SLOT(setCurrentNetworkMode(NetworkModeButton::NetworkMode)));
    connect(networkSystemInfoUi, SIGNAL(currentNetworkModeChanged(NetworkModeButton::NetworkMode)),
            networkModeButton, SLOT(setDisplayedNetworkMode(NetworkModeButton::NetworkMode)));
    config->addMenuButton(networkModeButton);

    SignalStrengthButton *signalStrengthButton = new SignalStrengthButton(this);
    connect(signalStrengthButton, SIGNAL(signalStrengthChanged(SignalStrengthButton::SignalStrength)),
            networkSystemInfoUi, SLOT(setCurrentNetworkSignalStrength(SignalStrengthButton::SignalStrength)));
    connect(networkSystemInfoUi, SIGNAL(currentNetworkSignalStrengthChanged(SignalStrengthButton::SignalStrength)),
            signalStrengthButton, SLOT(setDisplayedSignalStrength(SignalStrengthButton::SignalStrength)));
    config->addMenuButton(signalStrengthButton);

    BatteryButton *batteryButton = new BatteryButton(this);
    connect(batteryButton, SIGNAL(batteryLevelChanged(BatteryButton::BatteryLevel)),
            mobility->mSystemInfoGenericUi, SLOT(setCurrentBatteryLevel(BatteryButton::BatteryLevel)));
    connect(mobility->mSystemInfoGenericUi, SIGNAL(currentBatteryLevelChanged(BatteryButton::BatteryLevel)),
            batteryButton, SLOT(setDisplayedBatteryLevel(BatteryButton::BatteryLevel)));
    config->addMenuButton(batteryButton);

    PowerButton *powerButton = new PowerButton(this);
    connect(powerButton, SIGNAL(powerStateChanged(PowerButton::PowerState)),
            mobility->mSystemInfoGenericUi, SLOT(setCurrentPowerState(PowerButton::PowerState)));
    connect(mobility->mSystemInfoGenericUi, SIGNAL(currentPowerStateChanged(PowerButton::PowerState)),
            powerButton, SLOT(setDisplayedState(PowerButton::PowerState)));
    config->addMenuButton(powerButton);

    // scriptButton is created above
    connect(scriptButton, SIGNAL(scriptSelected(QString)),
            scriptAdapter, SLOT(run(QString)));
    config->addMenuButton(scriptButton);

    InputScriptInterface *inputScriptInterface = new InputScriptInterface(this, view->viewport());
    inputScriptInterface->setView(view);
    inputScriptInterface->setDisplay(deviceItem()->display());

    // register script interfaces
    scriptAdapter->addScriptInterface("simulator", viewUi->scriptInterface());
    scriptAdapter->addScriptInterface("sensors", sensorsUi->scriptInterface());
    scriptAdapter->addScriptInterface("location", locationUi->scriptInterface());
    scriptAdapter->addScriptInterface("sysinfo.generic", genericSystemInfoUi->scriptInterface());
    scriptAdapter->addScriptInterface("sysinfo.network", networkSystemInfoUi->scriptInterface());
    scriptAdapter->addScriptInterface("sysinfo.storage", storageSystemInfoUi->scriptInterface());
    scriptAdapter->addScriptInterface("camera", cameraUi->scriptInterface());
    scriptAdapter->addScriptInterface("input", inputScriptInterface);
    scriptAdapter->addScriptInterface("touch", widgetManager->scriptInterface());

    connect(viewUi, SIGNAL(deviceSelectionChanged(const DeviceData &)), SLOT(callDeviceScript(const DeviceData &)));
    connect(widgetManager, SIGNAL(gestureScriptRequested(QString)), scriptAdapter, SLOT(run(QString)));

    config->setWindowFlags(Qt::Tool);

    loadPlugins();

    // read settings
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    mTouchUi->readSettings(settings);

    mobility->setInitialData();

    config->show();
    config->readSettings(settings);

    // Filter to show debug window on special key press.
    view->installEventFilter(this);
    view->setFocus();

    readSettings(settings);
    scriptAdapter->runAutostartScripts();
}

MainWindow::~MainWindow()
{
}

bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == view)
    {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
            if (keyEvent->key() == Qt::Key_F9
                && keyEvent->modifiers() & Qt::ControlModifier)
            {
                debugWindow->show();
            } else if (keyEvent->modifiers() & Qt::AltModifier) {
                if (keyEvent->key() == Qt::Key_1)
                    emit mouseInputModeChanged(MultiPointTouchUi::defaultMode);
                else if (keyEvent->key() == Qt::Key_2)
                    emit mouseInputModeChanged(MultiPointTouchUi::pinchMode);
                else if (keyEvent->key() == Qt::Key_3)
                    emit mouseInputModeChanged(MultiPointTouchUi::panMode);
                else if (keyEvent->key() == Qt::Key_4)
                    emit mouseInputModeChanged(MultiPointTouchUi::swipeMode);
                else if (keyEvent->key() == Qt::Key_5)
                    emit mouseInputModeChanged(MultiPointTouchUi::freeMode);
            }
        }
    }
    return false;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (applicationManager->hasApplications()) {
        event->ignore();
        applicationManager->killAllApplications();
        connect(applicationManager, SIGNAL(lastAppUnregistered()), this, SLOT(close()));
        return;
    }

    if (!startupError) {
        QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
        writeSettings(settings);
        config->writeSettings(settings);
        mTouchUi->writeSettings(settings);
    }
}

void MainWindow::setSizeToDevice(const QSize &size)
{
    QSize targetSize(size);

    // don't try to make windows bigger than allowed on X11
#ifdef Q_WS_X11
    QSize maxSize = QApplication::desktop()->availableGeometry(this).size();
    targetSize = targetSize.boundedTo(maxSize);
#endif
    setMaximumSize(targetSize);
    resize(targetSize);
}

void MainWindow::dragFromView(const QPoint &to)
{
#ifdef Q_WS_X11
    XEvent xev;
    xev.xclient.type = ClientMessage;
    xev.xclient.message_type = ATOM(_NET_WM_MOVERESIZE);
    xev.xclient.display = X11->display;
    xev.xclient.window = winId();
    xev.xclient.format = 32;
    xev.xclient.data.l[0] = to.x();
    xev.xclient.data.l[1] = to.y();
    // only moving (the "move via keyboard" value seems to impose less
    // movement restrictions)
    xev.xclient.data.l[2] = 10;
    xev.xclient.data.l[3] = Button1;
    xev.xclient.data.l[4] = 2; // user event
    XUngrabPointer(X11->display, X11->time);
    XSendEvent(X11->display, QX11Info::appRootWindow(x11Info().screen()), False,
               SubstructureRedirectMask | SubstructureNotifyMask, &xev);
#else
    move(to - view->pos());
#endif
}

void MainWindow::setViewSize(const QSize &size)
{
    QSize targetSize(size);

    // X11 seems to be unable to draw windows bigger than the available desktop geometry.
    // Therefore, we enable scroll bars there if the view is required to be bigger than that.
#ifdef Q_WS_X11
    QSize maxSize = QApplication::desktop()->availableGeometry(this).size();
    targetSize = targetSize.boundedTo(maxSize);
    // the first check is for whether we're rotating at the moment
    if (targetSize == this->size() || targetSize == size) {
        view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    } else {
        view->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        view->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    }

    // adjust the window mask, otherwise the window is transparent, but clickable
    setMask(QRect(QPoint(width() - targetSize.width(), height() - targetSize.height()) / 2, targetSize));
#endif

    // resize and center the view
    view->move(QPoint(width() - targetSize.width(), height() - targetSize.height()) / 2);
    view->resize(targetSize);

    view->setSceneRect(0, 0, size.width(), size.height());
    view->centerOn(size.width() / 2, size.height() / 2);
}

void MainWindow::writeSettings(QSettings &settings) const
{
    settings.beginGroup("MainWindow");
    settings.setValue("pos", pos());
    int deviceOrientation = static_cast<int>(mDeviceItem->deviceOrientation());
    settings.setValue("deviceOrientation", deviceOrientation);
    int screenOrientation = static_cast<int>(mDeviceItem->screenOrientation());
    settings.setValue("screenOrientation", screenOrientation);
    settings.endGroup();
}

void MainWindow::readSettings(QSettings &settings)
{
    settings.beginGroup("MainWindow");
    QPoint positionToSet;
    if (settings.contains("pos"))
        positionToSet = settings.value("pos").toPoint();
    else
        positionToSet = QPoint(config->geometry().right() + 50, config->geometry().top());
    const QRect geometry = QApplication::desktop()->availableGeometry(positionToSet);
    if (geometry.contains(positionToSet + QPoint(height() / 2, width() / 2)))
        move(positionToSet);
    Orientation deviceOrientation = topUp;
    Orientation screenOrientation = topUp;
    if (settings.contains("deviceOrientation")) {
        int orientationInt = settings.value("deviceOrientation").toInt();
        if (orientationInt != 0)
            deviceOrientation = static_cast<Orientation>(orientationInt);
    }
    if (settings.contains("screenOrientation")) {
        int orientationInt = settings.value("screenOrientation").toInt();
        if (orientationInt != 0)
            screenOrientation = static_cast<Orientation>(orientationInt);
    }
    mDeviceItem->setInitialRotation(deviceOrientation, screenOrientation);
    mDeviceItem->setMaemoNavigationMode(maemoClose);

    settings.endGroup();
}

static bool copyDirectoryContents(const QString& sourcePath, const QString& targetPath)
{
    if (!QFileInfo(sourcePath).isDir()) {
        qWarning() << QObject::tr("Source is not a directory: %1").arg(sourcePath);
        return false;
    }
    QFileInfo targetPathInfo(targetPath);
    if (!targetPathInfo.exists()) {
        if (!QDir().mkpath(targetPath)) {
            qWarning() << QObject::tr("Could not create target directory %1").arg(targetPath);
            return false;
        }
    } else if (!targetPathInfo.isDir()) {
        qWarning() << QObject::tr("Target is not a directory: %1").arg(targetPath);
        return false;
    }

    const QDir targetDir(targetPath);
    QDirIterator it(sourcePath, QDir::NoDotAndDotDot | QDir::AllEntries);
    while(it.hasNext()) {
        const QFileInfo i(it.next());
        if(i.isDir()) {
            if (!copyDirectoryContents(i.absoluteFilePath(), targetDir.absoluteFilePath(i.fileName())))
                return false;
        } else {
            QFile f(i.filePath());
            const QString target = targetDir.absoluteFilePath(i.fileName());
            QFile::remove(target);
            if(!f.copy(target)) {
                qWarning() << QObject::tr("Could not copy file from %1 to %2: %3").arg(f.fileName(), target, f.errorString());
                return false;
            }
        }
    }

    return true;
}

void MainWindow::setupScriptDirectories(ScriptUi *ui, FavoriteScriptButton *button)
{
    const QString resourcePath = userResourcePath();
    const QString scriptsPath = resourcePath + QLatin1String("/scripts");
    const QString builtinScripts = QCoreApplication::applicationDirPath() + QLatin1String("/scripts");

    QSettings settings;
    settings.beginGroup("Scripts");
    int installedScriptVersion = settings.value("version", 0).toInt();

    // copy initial scripts if directory does not exist yet or the script version is newer
    const QFileInfo fi(scriptsPath + QLatin1Char('/'));
    bool copy = !fi.exists();
    bool updateScriptVersion = false;
    if (!copy && installedScriptVersion < scriptVersion) {
        QMessageBox *copyQuestion = new QMessageBox;
        copyQuestion->setWindowTitle(tr("Copy new Simulator scripts?"));
        copyQuestion->setText(tr("New versions of the default scripts are available. Copy them into your user script directory?\n\n"
                                 "WARNING: If you have modified the default scripts, this will revert your changes.\n\n"
                                 "You can also update your scripts manually.\n"
                                 "Default script directory: %1\n"
                                 "User script directory: %2").arg(builtinScripts, scriptsPath));
        copyQuestion->setIcon(QMessageBox::Question);
        QPushButton *copyButton = copyQuestion->addButton(tr("Copy"), QMessageBox::AcceptRole);
        copyQuestion->addButton(tr("Ask again"), QMessageBox::DestructiveRole);
        QPushButton *dontButton = copyQuestion->addButton(tr("Do not copy"), QMessageBox::RejectRole);
        copyQuestion->exec();

        if (copyQuestion->clickedButton() == copyButton) {
            copy = updateScriptVersion = true;
        } else if (copyQuestion->clickedButton() == dontButton) {
            updateScriptVersion = true;
        }
    }
    if (copy) {
        if (!copyDirectoryContents(builtinScripts, scriptsPath)) {
            qWarning() << "Could not copy scripts to" << scriptsPath;
        }
    }
    if (updateScriptVersion) {
        settings.setValue("version", scriptVersion);
    }

    ui->setScriptDirectory(scriptsPath);
    button->setPath(scriptsPath + QLatin1String("/favorites"));
}

void MainWindow::handleMessage(const QString &message)
{
    if (message.startsWith(QLatin1String("runscript "))) {
        const QString filePath = message.mid(10);
        scriptAdapter->run(filePath);
    }
}

void MainWindow::callDeviceScript(const DeviceData &newDevice)
{
    QString deviceScript = QCoreApplication::applicationDirPath()
        + QString::fromUtf8("/scripts/devices/") + newDevice.name
        + QString::fromUtf8(".qs");

    if (!QFile::exists(deviceScript)) {
        deviceScript = deviceScript.replace(newDevice.name + QString::fromUtf8(".qs"),
            QString::fromUtf8("Default.qs"));
    }
    scriptAdapter->run(deviceScript);
}

void MainWindow::activateWindow()
{
    SharedTools::QtSingleApplication::instance()->activateWindow();
}

void MainWindow::changeEvent(QEvent* e)
{
    if (e->type() == QEvent::ActivationChange && isActiveWindow()) {
        config->raise();
        raise();
    }
}

QString MainWindow::userResourcePath() const
{
    // Create resource dir if it doesn't yet exist
    const QString urp = QDesktopServices::storageLocation(QDesktopServices::DataLocation);

    QFileInfo fi(urp + QLatin1Char('/'));
    if (!fi.exists()) {
        QDir dir;
        if (!dir.mkpath(urp))
            qWarning() << "could not create" << urp;
    }

    return urp;
}

void MainWindow::loadPlugins()
{
    // NOTE: static plugins are not supported

    QDir pluginsDir = QDir(qApp->applicationDirPath());

#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS") {
        pluginsDir.cdUp();
        pluginsDir.cdUp();
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("plugins");
    pluginsDir.cd("simulator");

    foreach (QString fileName, pluginsDir.entryList(QDir::Files)) {
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();
        if (plugin) {
            PluginInterface *pi = qobject_cast<PluginInterface *>(plugin);
            if (pi) {
                foreach (ToolBoxPage *tbp, pi->pages(this)) {
                    config->addPage(tbp);

                    QString name = tbp->scriptInterfaceName();
                    QObject *obj = tbp->scriptInterface();
                    if ( obj && !name.isEmpty() )
                      scriptAdapter->addScriptInterface(name, obj);
                }
            }
        }
        else {
            qWarning() << tr("Could not load plugin:") << pluginsDir.absoluteFilePath(fileName);
            qWarning() << loader.errorString();
        }
    }
}
