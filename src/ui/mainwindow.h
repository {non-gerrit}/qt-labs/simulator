/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "deviceitem.h"
#include <QtGui/QWidget>
#include "qsimulatordata_p.h"
#include "multipointtouchui.h"

class DeviceItem;
class ApplicationManager;
class MobilityServer;
class ConfigurationWidget;
class ScriptAdapter;
class QGraphicsView;
class QGraphicsScene;
class ScriptUi;
class FavoriteScriptButton;
class InputScriptInterface;
class QSettings;

extern const VersionStruct simulatorVersion;

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    virtual bool eventFilter(QObject *watched, QEvent *event);
    virtual void closeEvent(QCloseEvent *event);

    DeviceItem *deviceItem() const { return mDeviceItem; }
    QString userResourcePath() const;

public slots:
    void setSizeToDevice(const QSize &size);
    void dragFromView(const QPoint &to);
    void setViewSize(const QSize &size);

    // for messages from other simulator instances
    void handleMessage(const QString &message);

private slots:
    void callDeviceScript(const DeviceData &newDevice);
    void activateWindow();

signals:
    void mouseInputModeChanged(MultiPointTouchUi::InputMode);

private:
    void writeSettings(QSettings &) const;
    void readSettings(QSettings &);
    void setupScriptDirectories(ScriptUi *ui, FavoriteScriptButton *button);
    void loadPlugins();

private:
    QGraphicsView *view;
    QGraphicsScene *scene;

    ConfigurationWidget *config;
    ApplicationManager *applicationManager;
    DeviceItem *mDeviceItem;

    MobilityServer *mobilityServer;

    ScriptAdapter *scriptAdapter;

    MultiPointTouchUi *mTouchUi;

    QWidget *debugWindow;
    bool startupError;

protected:
    virtual void changeEvent (QEvent* e);
};

#endif // MAINWINDOW_H
