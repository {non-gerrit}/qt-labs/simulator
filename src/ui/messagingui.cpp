/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "messagingui.h"

#include "configurationwidget.h"
#include "mobilitydata.h"

#include <remotecontrolwidget/optionsitem.h>

#include <QtGui/QPushButton>
#include <QtGui/QFileDialog>

MessagingUi::MessagingUi(MobilityData *mobilityData, QWidget *parent)
    : ToolBoxPage(parent)
    , mMobilityData(mobilityData)
{
    QList<OptionsItem *> options;
    OptionsItem *item;

    QPushButton *incomingEmailButton = new QPushButton(tr("Trigger"));
    item = new OptionsItem(tr("Incoming email"), incomingEmailButton);
    item->addTag(tr("message"));
    connect(incomingEmailButton, SIGNAL(clicked()), mobilityData, SLOT(addNewEmail()));
    options += item;

    QPushButton *incomingSmsButton = new QPushButton(tr("Trigger"));
    item = new OptionsItem(tr("Incoming SMS"), incomingSmsButton);
    item->addTag(tr("message"));
    connect(incomingSmsButton, SIGNAL(clicked()), mobilityData, SLOT(addNewSms()));
    options += item;

    QPushButton *importMaildirButton = new QPushButton(tr("Import"));
    item = new OptionsItem(tr("Import mail directory"), importMaildirButton);
    item->addTag(tr("message"));
    connect(importMaildirButton, SIGNAL(clicked()), this, SLOT(importMaildir()));
    options += item;

    setTitle(tr("Messaging"));
    setOptions(options);
}

void MessagingUi::importMaildir()
{
    QString dir = QFileDialog::getExistingDirectory(0, tr("Select maildir Directory to Import"));
    if (dir.isEmpty())
        return;

    mMobilityData->mMessaging->importMaildir(dir);
}

QIcon MessagingUi::icon() const
{
    return QIcon(":/ui/icons/messaging.png");
}
