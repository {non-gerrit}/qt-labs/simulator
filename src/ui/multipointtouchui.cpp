/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "multipointtouchui.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtCore/QSettings>
#include <QtGui/QVBoxLayout>
#include <QtGui/QButtonGroup>
#include <QtGui/QRadioButton>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QFileDialog>

MultiPointTouchUi::MultiPointTouchUi(QWidget *parent) :
    ToolBoxPage(parent)
{
    QStringList tags;
    QList<OptionsItem *> optionsList;
    tags  << tr("multi point touch");

    mDefaultRadio = new QRadioButton(tr("Default mode") + " (Alt+1)");
    mPinchRadio = new QRadioButton(tr("Pinch mode") + " (Alt+2)");
    mPanRadio = new QRadioButton(tr("Pan mode") + " (Alt+3)");
    mSwipeRadio = new QRadioButton(tr("Swipe mode") + " (Alt+4)");
    mFreeRadio = new QRadioButton(tr("Free mode") + " (Alt+5)");
    mCustomLineEdit = new QLineEdit();
    QPushButton *loadButton = new QPushButton(tr("Browse"));
    connect(loadButton, SIGNAL(clicked()), this, SLOT(getCustomGesturePath()));
    connect (mCustomLineEdit, SIGNAL(editingFinished()), SLOT(emitCustomGesturePathChanged()));

    QButtonGroup *buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(mDefaultRadio, 0);
    buttonGroup->addButton(mPinchRadio, 1);
    buttonGroup->addButton(mPanRadio, 2);
    buttonGroup->addButton(mSwipeRadio, 3);
    buttonGroup->addButton(mFreeRadio, 4);
    mDefaultRadio->setChecked(true);

    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->addWidget(mFreeRadio);
    hLayout->addWidget(mCustomLineEdit);
    hLayout->addWidget(loadButton);

    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->addWidget(mDefaultRadio);
    vLayout->addWidget(mPinchRadio);
    vLayout->addWidget(mPanRadio);
    vLayout->addWidget(mSwipeRadio);
    vLayout->addLayout(hLayout);
    QWidget *widget = new QWidget;
    widget->setLayout(vLayout);

    connect(buttonGroup, SIGNAL(buttonClicked(int)), SLOT(emitInputModeChanged(int)));
    OptionsItem *item = new OptionsItem(tr(""), widget, true);
    optionsList << item;
    setTitle(tr("Multipoint-touch"));
    setOptions(optionsList);
}

MultiPointTouchUi::~MultiPointTouchUi()
{
}

void MultiPointTouchUi::emitInputModeChanged(int newMode)
{
    emit inputModeChanged(static_cast<InputMode>(newMode));
}

void MultiPointTouchUi::setInputMode(MultiPointTouchUi::InputMode inputMode)
{
    switch (inputMode) {
    case defaultMode:
        mDefaultRadio->setChecked(true);
        break;
    case pinchMode:
        mPinchRadio->setChecked(true);
        break;
    case panMode:
        mPanRadio->setChecked(true);
        break;
    case swipeMode:
        mSwipeRadio->setChecked(true);
        break;
    case freeMode:
        mFreeRadio->setChecked(true);
        break;
    }
    emit inputModeChanged(inputMode);
}

void MultiPointTouchUi::getCustomGesturePath()
{
    mCustomLineEdit->setText(QFileDialog::getOpenFileName(this, tr("Choose Gesture Script"),
                                                 mGestureScriptPath, tr("Qt scripts (*.js *.qs)")));
    emitCustomGesturePathChanged();
}

void MultiPointTouchUi::emitCustomGesturePathChanged()
{
    emit customGesturePathChanged(mCustomLineEdit->text());
}

void MultiPointTouchUi::setGestureScriptPath(const QString &path)
{
    mGestureScriptPath = path;
}

void MultiPointTouchUi::writeSettings(QSettings &settings) const
{
    ToolBoxPage::writeSettings(settings);
    if (!QFile::exists(mCustomLineEdit->text()))
        return;

    settings.beginGroup(title());
    settings.setValue("customGesturePath", mCustomLineEdit->text());
    settings.endGroup();
}

void MultiPointTouchUi::readSettings(QSettings &settings)
{
    ToolBoxPage::readSettings(settings);
    settings.beginGroup(title());
    if (settings.contains("customGesturePath")) {
        mCustomLineEdit->setText(settings.value("customGesturePath").toString());
        emitCustomGesturePathChanged();
    }
    settings.endGroup();
}

QIcon MultiPointTouchUi::icon() const
{
    return QIcon(":/ui/icons/multipointtouch.png");
}
