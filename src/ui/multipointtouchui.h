/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef MULTIPOINTTOUCHUI_H
#define MULTIPOINTTOUCHUI_H

#include <QtCore/QObject>

#include <remotecontrolwidget/toolbox.h>

class MultiPointTouchUi;
class QRadioButton;
class QLineEdit;

class MultiPointTouchUi : public ToolBoxPage
{
    Q_OBJECT
public:
    enum InputMode {
        defaultMode,
        pinchMode,
        panMode,
        swipeMode,
        freeMode
    };

    explicit MultiPointTouchUi(QWidget *parent = 0);
    virtual ~MultiPointTouchUi();

    void setGestureScriptPath(const QString &path);
    void writeSettings(QSettings &settings) const;
    void readSettings(QSettings &settings);
    QIcon icon() const;

signals:
    void inputModeChanged(MultiPointTouchUi::InputMode newMode);
    void customGesturePathChanged(const QString &path);

private slots:
    void emitInputModeChanged(int newMode);
    void emitCustomGesturePathChanged();
    void getCustomGesturePath();

public slots:
    void setInputMode(MultiPointTouchUi::InputMode inputMode);

private:
    InputMode mInputMode;

    QRadioButton *mDefaultRadio;
    QRadioButton *mPinchRadio;
    QRadioButton *mPanRadio;
    QRadioButton *mSwipeRadio;
    QRadioButton *mFreeRadio;

    QLineEdit *mCustomLineEdit;
    QString mGestureScriptPath;
};

#endif // MULTIPOINTTOUCHUI_H
