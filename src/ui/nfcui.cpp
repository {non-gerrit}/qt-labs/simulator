/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/


#include "nfcui.h"
#include "targetemulator_p.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtCore/QModelIndex>
#include <QtCore/QDirIterator>
#include <QtCore/QSettings>

#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QHBoxLayout>

#include <QtCore/QDebug>
#include "nfctargetgenerator.h"

class NfcTargetItem : public QListWidgetItem
{
public:
    NfcTargetItem();
    virtual ~NfcTargetItem();

    enum Roles {
        NameRole = Qt::DisplayRole,
        InProximity = Qt::UserRole
    };

    QVariant data(int role) const;
    void setData(int role, const QVariant &value);

    TagBase *tag() const;
    void setTag(TagBase *tag);

private:
    TagBase *m_tag;
};

NfcTargetItem::NfcTargetItem()
:   m_tag(0)
{
    setData(InProximity, false);
}

NfcTargetItem::~NfcTargetItem()
{
}

QVariant NfcTargetItem::data(int role) const
{
    if (role == NameRole && QListWidgetItem::data(InProximity).toBool()) {
        return NfcUi::tr("%1 (in proximity)").arg(QListWidgetItem::data(NameRole).toString());
    }

    return QListWidgetItem::data(role);
}

void NfcTargetItem::setData(int role, const QVariant &value)
{
    QListWidgetItem::setData(role, value);
}

TagBase *NfcTargetItem::tag() const
{
    return m_tag;
}

void NfcTargetItem::setTag(TagBase *tag)
{
    m_tag = tag;
}

NfcUi::NfcUi(QWidget *parent)
:   ToolBoxPage(parent)
{
    QList<OptionsItem *> optionsList;

    mTargets = new QListWidget();
    mTargets->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    connect(mTargets, SIGNAL(activated(QModelIndex)), SLOT(targetActivated(QModelIndex)));

    optionsList << new OptionsItem(tr("Targets"), mTargets);

    QPushButton *editButton = new QPushButton(tr("Start Editor"), this);
    connect(editButton, SIGNAL(clicked()), this, SLOT(startEditor()));
    OptionsItem *item = new OptionsItem("", editButton, true);
    optionsList << item;

    setTitle(tr("NFC"));
    setOptions(optionsList);

    loadTagList();
}

NfcUi::~NfcUi()
{
}

void NfcUi::loadTagList()
{
    mTargets->clear();

    QDirIterator nfcTargets("stubdata/nfctargets", QStringList(QLatin1String("*.nfc")),
                            QDir::Files);
    while (nfcTargets.hasNext()) {
        const QString targetFilename = nfcTargets.next();

        QSettings target(targetFilename, QSettings::IniFormat);

        NfcTargetItem *targetItem = new NfcTargetItem;

        target.beginGroup(QLatin1String("Target"));
        targetItem->setData(NfcTargetItem::NameRole,
                            target.value(QLatin1String("Name")).toString());


        const QString tagType = target.value(QLatin1String("Type")).toString();

        target.endGroup();

        if (tagType == QLatin1String("TagType1")) {
            NfcTagType1 *tag = new NfcTagType1;
            tag->load(&target);
            targetItem->setTag(tag);
        } else {
            qWarning("Unknown tag type %s\n", qPrintable(tagType));
        }

        mTargets->addItem(targetItem);
    }
}

QByteArray NfcUi::processCommand(const QByteArray &command)
{
    QByteArray response;
    bool collision = false;

    for (int i = 0; i < mTargets->count(); ++i) {
        NfcTargetItem *targetItem = static_cast<NfcTargetItem *>(mTargets->item(i));
        // check if target is in proximity
        if (!targetItem->data(NfcTargetItem::InProximity).toBool())
            continue;

        QByteArray targetResponse = targetItem->tag()->processCommand(command);

        // skip if target didn't respond (e.g. UID mismatch)
        if (targetResponse.isEmpty())
            continue;

        if (response.isEmpty()) {
            response = targetResponse;
        } else {
            collision = true;
            qWarning("Collision detected, multiple NFC targets responded.");
        }
    }

    if (collision)
        return QByteArray();

    return response;
}

void NfcUi::targetActivated(const QModelIndex &index)
{
    // toggle in proximity
    NfcTargetItem *targetItem = static_cast<NfcTargetItem *>(mTargets->item(index.row()));
    if (targetItem) {
        if (targetItem->data(NfcTargetItem::InProximity).toBool()) {
            targetItem->setData(NfcTargetItem::InProximity, false);
            emit targetLeavingProximity(targetItem->tag()->uid());
        } else {
            targetItem->setData(NfcTargetItem::InProximity, true);
            emit targetEnteringProximity(targetItem->tag()->uid());
        }
    }
}

void NfcUi::startEditor()
{
    NfcTargetGenerator dialog(this);
    dialog.exec();
    loadTagList();
}

QIcon NfcUi::icon() const
{
    return QIcon(":/ui/icons/nfc.png");
}
