/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "organizerui.h"
#include "organizer.h"
#include "organizertreemodel.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtCore/QList>
#include <QtGui/QPushButton>
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>
#include <QtGui/QTreeView>
#include <QtGui/QHBoxLayout>

#include <QDebug>

OrganizerUi::OrganizerUi(Organizer *organizer, QWidget *parent)
    : ToolBoxPage(parent)
    , m_organizer(organizer)
{
    QList<OptionsItem *> optionsList;

    QTreeView *organizerTreeView = new QTreeView();
    organizerTreeView->setModel(m_organizer->organizerModel());
    organizerTreeView->setHeaderHidden(true);
    organizerTreeView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    OptionsItem *item  = new OptionsItem("", organizerTreeView, true);
    optionsList << item;

    QWidget *exportImportButtons = new QWidget;
    QHBoxLayout *layout = new QHBoxLayout;
    QPushButton *organizerExportButton = new QPushButton(tr("Export"));
    connect(organizerExportButton, SIGNAL(clicked()), this, SLOT(exportOrganizer()));
    layout->addWidget(organizerExportButton);
    QPushButton *organizerImportButton = new QPushButton(tr("Import"));
    connect(organizerImportButton, SIGNAL(clicked()), this, SLOT(importOrganizer()));
    layout->addWidget(organizerImportButton);
    exportImportButtons->setLayout(layout);
    item = new OptionsItem("", exportImportButtons, true);
    optionsList << item;

    setTitle(tr("Organizer"));
    setOptions(optionsList);
}

void OrganizerUi::importOrganizer()
{
    QString importFile = QFileDialog::getOpenFileName(0, tr("Select iCalendar File to Import"), ".", "*.ics");
    if (importFile.isNull())
        return;

    if (!m_organizer->importFromVCardFile(importFile)) {
        QMessageBox::information(0, tr("Cannot Import File"),
                tr("Cannot import \"%1\".").arg(importFile));
    }
}

void OrganizerUi::exportOrganizer()
{
    QString exportFile = QFileDialog::getSaveFileName(0, tr("Export Organizer Data to iCalendar File"), ".", "*.ics");
    if (exportFile.isNull())
        return;

    if (!m_organizer->exportToVCardFile(exportFile)) {
        QMessageBox::information(0, tr("Cannot Export File"),
                tr("Cannot export data to \"%1\".").arg(exportFile));
    }
}

QIcon OrganizerUi::icon() const
{
    return QIcon(":/ui/icons/organizer.png");
}
