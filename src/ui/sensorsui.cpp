/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "sensorsui.h"
#include "configurationwidget.h"
#include "accelerometercontrol.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtCore/QMetaEnum>
#include <QtGui/QComboBox>
#include <QtGui/QLineEdit>
#include <QtGui/QSlider>
#include <QtGui/QDoubleValidator>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QBoxLayout>
#include <QtGui/QDateTimeEdit>
#include <qmath.h>

SensorDoubleEdit::SensorDoubleEdit(QWidget *parent)
    : QWidget(parent)
    , mValue(0)
    , mDecimalPlaces(0)
    , mCorrectionFactor(1)
{
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->setMargin(0);
    vLayout->setSpacing(1);
    mSlider = new QSlider();
    mSlider->setOrientation(Qt::Horizontal);
    mSlider->setTickInterval(1);
    connect(mSlider, SIGNAL(valueChanged(int)), SLOT(changeValue(int)));
    mLineEdit = new QLineEdit("0.00");
    mLineEdit->setValidator(new QDoubleValidator(this));
    connect(mLineEdit, SIGNAL(editingFinished()), SLOT(updateValueFromLineEdit()));
    vLayout->addWidget(mSlider);
    vLayout->addWidget(mLineEdit);
    setLayout(vLayout);
}

double SensorDoubleEdit::value() const
{
    return mValue;
}

void SensorDoubleEdit::setValue(double newValue, bool skipSignal)
{
    if (mValue != newValue)
    {
        mValue = newValue;
        mLineEdit->setText(QLocale().toString(newValue, 'f', mDecimalPlaces));
        int sliderPos = qRound(newValue * mCorrectionFactor);
        if (mSlider->value() != sliderPos)
            mSlider->setValue(sliderPos);
        if (!skipSignal)
            emit valueChanged(newValue);
    }
}

void SensorDoubleEdit::changeValue(int newValue)
{
    if (qAbs(mValue * mCorrectionFactor - newValue) < 1)
        return;
    double actualValue = newValue / (double)mCorrectionFactor;
    setValue(actualValue);
}

void SensorDoubleEdit::updateValueFromLineEdit()
{
    double newValue = QLocale().toDouble(mLineEdit->text());
    if (newValue >= mRange.first && newValue <= mRange.second)
        setValue(newValue);
    else
        mLineEdit->setText(QLocale().toString(mValue));
}

void SensorDoubleEdit::setRange(double min, double max)
{
    if (mRange.first != min || mRange.second != max) {
        mRange = QPair<double, double>(min, max);
        updateSlider();
    }
}

void SensorDoubleEdit::setDecimalPlaces(int places)
{
    if (mDecimalPlaces != places) {
        mDecimalPlaces = places;
        mCorrectionFactor = pow(10., places);
        updateSlider();
    }
}

void SensorDoubleEdit::updateSlider()
{
    mSlider->setRange(mRange.first * mCorrectionFactor, mRange.second * mCorrectionFactor);
    mSlider->setValue(mValue * mCorrectionFactor);
}

SensorsUi::SensorsUi(QWidget *parent)
    : ToolBoxPage(parent)
    , mAmbientLightBox(0)
    , mLightEdit(0)
    , mAccelerometerXEdit(0)
    , mAccelerometerYEdit(0)
    , mAccelerometerZEdit(0)
    , mMagnetometerXEdit(0)
    , mMagnetometerYEdit(0)
    , mMagnetometerZEdit(0)
    , mMagnetometerCalibrationLevelEdit(0)
    , mCompassCalibrationLevelEdit(0)
    , mCompassAzimuthEdit(0)
    , mProximitySensorCloseButton(0)
    , mTimeEdit(0)
{
    qRegisterMetaType<SensorsUi::SensorsData>("SensorsUi::SensorsData");

    mScriptInterface = new SensorsScriptInterface(this);

    QStringList tags;
    QList<OptionsItem *> optionsList;

    mAmbientLightBox = new QComboBox();
    connect(mAmbientLightBox, SIGNAL(activated(int)), SLOT(ambientLightChanged()));
    OptionsItem *item = new OptionsItem(tr("Ambient light"), mAmbientLightBox);
    optionsList << item;

    mLightEdit = new SensorDoubleEdit();
    mLightEdit->setRange(0, 5000);
    mLightEdit->setDecimalPlaces(0);
    item = new OptionsItem(tr("Light (LUX)"), mLightEdit);
    connect(mLightEdit, SIGNAL(valueChanged(double)), SLOT(lightChanged()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    {
        QWidget *control = new QWidget;
        QVBoxLayout *vlayout = new QVBoxLayout;

        mAccelerometerControl = new AccelerometerControl();
        mAccelerometerControl->setMinimumHeight(150);
        connect(mAccelerometerControl, SIGNAL(valueChanged(QVector3D)), this, SLOT(setAccelerometerValue(QVector3D)));
        if (mAccelerometerControl->isValid())
            vlayout->addWidget(mAccelerometerControl);

        QHBoxLayout *hlayout = new QHBoxLayout;
        vlayout->addLayout(hlayout);

        struct ButtonData {
            QString toolTip;
            const char *iconPath;
            const char *slot;
        };
        ButtonData data[] = {
            { tr("Top Up"), ":/ui/icons/topup.png", SLOT(setAccelerometerTopUp()) },
            { tr("Left Up"), ":/ui/icons/leftup.png", SLOT(setAccelerometerLeftUp()) },
            { tr("Top Down"), ":/ui/icons/topdown.png", SLOT(setAccelerometerTopDown()) },
            { tr("Right Up"), ":/ui/icons/rightup.png", SLOT(setAccelerometerRightUp()) },
            { tr("Face Up"), ":/ui/icons/faceup.png", SLOT(setAccelerometerFaceUp()) },
            { tr("Face Down"), ":/ui/icons/facedown.png", SLOT(setAccelerometerFaceDown()) }
        };
        for (int i = 0; i < 6; ++i) {
            ButtonData b = data[i];
            QPushButton *button = new QPushButton;
            button->setToolTip(b.toolTip);
            button->setIcon(QIcon(b.iconPath));
            connect(button, SIGNAL(clicked()), this, b.slot);
            hlayout->addWidget(button);
            mOrientationButtons.append(button);
        }
        hlayout->setMargin(0);
        hlayout->setSpacing(0);

        control->setLayout(vlayout);

        item = new OptionsItem(tr("Orientation"), control);
        item->setTags(tags);
        optionsList << item;
    }

    mAccelerometerXEdit = new SensorDoubleEdit();
    mAccelerometerXEdit->setRange(-50, 50);
    mAccelerometerXEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Accelerometer x"), mAccelerometerXEdit);
    connect(mAccelerometerXEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    mAccelerometerYEdit = new SensorDoubleEdit();
    mAccelerometerYEdit->setRange(-50, 50);
    mAccelerometerYEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Accelerometer y"), mAccelerometerYEdit);
    connect(mAccelerometerYEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    mAccelerometerZEdit = new SensorDoubleEdit();
    mAccelerometerZEdit->setRange(-50, 50);
    mAccelerometerZEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Accelerometer z"), mAccelerometerZEdit);
    connect(mAccelerometerZEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    connect(mAccelerometerControl, SIGNAL(magneticFieldChanged(QVector3D)), SLOT(setMagnetometerValue(QVector3D)));

    mMagnetometerXEdit = new SensorDoubleEdit();
    mMagnetometerXEdit->setRange(-30, 30);
    mMagnetometerXEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Magnetometer x"), mMagnetometerXEdit);
    connect(mMagnetometerXEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    mMagnetometerYEdit = new SensorDoubleEdit();
    mMagnetometerYEdit->setRange(-30, 30);
    mMagnetometerYEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Magnetometer y"), mMagnetometerYEdit);
    connect(mMagnetometerYEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    mMagnetometerZEdit = new SensorDoubleEdit();
    mMagnetometerZEdit->setRange(-30, 30);
    mMagnetometerZEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Magnetometer z"), mMagnetometerZEdit);
    connect(mMagnetometerZEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    mMagnetometerCalibrationLevelEdit = new SensorDoubleEdit();
    mMagnetometerCalibrationLevelEdit->setRange(0, 1);
    mMagnetometerCalibrationLevelEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Magnetometer calibration level"), mMagnetometerCalibrationLevelEdit);
    connect(mMagnetometerCalibrationLevelEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    mCompassAzimuthEdit = new SensorDoubleEdit();
    mCompassAzimuthEdit->setRange(0, 360);
    mCompassAzimuthEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Compass azimuth"), mCompassAzimuthEdit);
    connect(mCompassAzimuthEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    connect(mAccelerometerControl, SIGNAL(azimuthChanged(double)), mCompassAzimuthEdit, SLOT(setValue(double)));
    item->setTags(tags);
    optionsList << item;

    mCompassCalibrationLevelEdit = new SensorDoubleEdit();
    mCompassCalibrationLevelEdit->setRange(0, 1);
    mCompassCalibrationLevelEdit->setDecimalPlaces(2);
    item = new OptionsItem(tr("Compass calibration level"), mCompassCalibrationLevelEdit);
    connect(mCompassCalibrationLevelEdit, SIGNAL(valueChanged(double)), SLOT(emitSensorsDataChange()));
    item->setTags(tags);
    item->setAdvanced(true);
    optionsList << item;

    mProximitySensorCloseButton = new QPushButton(tr("Far"));
    mProximitySensorCloseButton->setCheckable(true);
    item = new OptionsItem(tr("Proximity distance"), mProximitySensorCloseButton);
    connect(mProximitySensorCloseButton, SIGNAL(toggled(bool)), SLOT(emitSensorsDataChange()));
    connect(mProximitySensorCloseButton, SIGNAL(toggled(bool)), SLOT(updateProximityButtonText()));
    item->setTags(tags);
    optionsList << item;

    QHBoxLayout *radioLayout = new QHBoxLayout();
    mCurrentRadio = new QRadioButton(tr("Current"));
    mOverrideRadio = new QRadioButton(tr("Override"));
    radioLayout->addWidget(mCurrentRadio);
    radioLayout->addWidget(mOverrideRadio);
    mTimeEdit = new QDateTimeEdit();
    mTimeEdit->setDisabled(true);
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->setContentsMargins(0, 0, 0, 0);
    vLayout->addLayout(radioLayout);
    vLayout->addWidget(mTimeEdit);
    QWidget *tmp = new QWidget();
    tmp->setLayout(vLayout);
    item = new OptionsItem(tr("Timestamp"), tmp);
    item->setTags(tags << tr("Current") << tr("Override"));
    item->setAdvanced(true);
    optionsList << item;
    connect(mCurrentRadio, SIGNAL(toggled(bool)), SLOT(updateTimeEditDisabled()));
    connect(mTimeEdit, SIGNAL(dateTimeChanged(const QDateTime &)), SLOT(emitSensorsDataChange()));

    initializeAmbientLightOptions();

    setTitle(tr("Sensors"));
    setOptions(optionsList);
}

SensorsUi::~SensorsUi()
{
}

SensorsScriptInterface *SensorsUi::scriptInterface() const
{
    return mScriptInterface;
}

SensorsUi::SensorsData SensorsUi::sensorsData() const
{
    SensorsUi::SensorsData data;
    data.ambientLightLevel = static_cast<SensorsScriptInterface::LightLevel>(mAmbientLightBox->currentIndex());
    data.lux = mLightEdit->value();
    data.accelerometerX = mAccelerometerXEdit->value();
    data.accelerometerY = mAccelerometerYEdit->value();
    data.accelerometerZ = mAccelerometerZEdit->value();
    data.magnetometerX = mMagnetometerXEdit->value();
    data.magnetometerY = mMagnetometerYEdit->value();
    data.magnetometerZ = mMagnetometerZEdit->value();
    data.magnetometerCalibrationLevel = mMagnetometerCalibrationLevelEdit->value();
    data.compassCalibrationLevel = mCompassCalibrationLevelEdit->value();
    data.compassAzimuth = mCompassAzimuthEdit->value();
    data.proximitySensorClose = mProximitySensorCloseButton->isChecked();
    data.useCurrentTime = mCurrentRadio->isChecked();
    data.timestamp = mTimeEdit->dateTime();
    return data;
}

void SensorsUi::setSensorsData(const SensorsUi::SensorsData &data)
{
    setDisplayedSensorsData(data);
    emit sensorsDataChanged(data);
}

void SensorsUi::setDisplayedSensorsData(const SensorsUi::SensorsData &data)
{
    mAmbientLightBox->setCurrentIndex(data.ambientLightLevel);
    mLightEdit->setValue(data.lux);
    mAccelerometerXEdit->setValue(data.accelerometerX);
    mAccelerometerYEdit->setValue(data.accelerometerY);
    mAccelerometerZEdit->setValue(data.accelerometerZ);
    mMagnetometerXEdit->setValue(data.magnetometerX);
    mMagnetometerYEdit->setValue(data.magnetometerY);
    mMagnetometerZEdit->setValue(data.magnetometerZ);
    mMagnetometerCalibrationLevelEdit->setValue(data.magnetometerCalibrationLevel);
    mCompassCalibrationLevelEdit->setValue(data.compassCalibrationLevel);
    mCompassAzimuthEdit->setValue(data.compassAzimuth);
    mProximitySensorCloseButton->setChecked(data.proximitySensorClose);
    mCurrentRadio->setChecked(data.useCurrentTime);
    mTimeEdit->setDateTime(data.timestamp);
}

void SensorsUi::emitSensorsDataChange() const
{
    mAccelerometerControl->setValue(
                QVector3D(mAccelerometerXEdit->value(),
                          mAccelerometerYEdit->value(),
                          mAccelerometerZEdit->value()));
    emit sensorsDataChanged(sensorsData());
}

void SensorsUi::setAccelerometerValue(const QVector3D &value)
{
    mAccelerometerXEdit->setValue(value.x(), true);
    mAccelerometerYEdit->setValue(value.y(), true);
    mAccelerometerZEdit->setValue(value.z(), true);
    emit sensorsDataChanged(sensorsData());
}

void SensorsUi::setMagnetometerValue(const QVector3D &value)
{
    mMagnetometerXEdit->setValue(value.x(), true);
    mMagnetometerYEdit->setValue(value.y(), true);
    mMagnetometerZEdit->setValue(value.z(), true);
    emit sensorsDataChanged(sensorsData());
}

void SensorsUi::initializeAmbientLightOptions()
{
    mAmbientLightBox->clear();
    QStringList lightOptions;

    const QMetaObject &metaObject = SensorsScriptInterface::staticMetaObject;
    int enumIndex = metaObject.indexOfEnumerator("LightLevel");
    QMetaEnum metaEnum = metaObject.enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        lightOptions.append(metaEnum.key(i));
    mAmbientLightBox->addItems(lightOptions);
}

void SensorsUi::updateProximityButtonText()
{
    mProximitySensorCloseButton->setText(mProximitySensorCloseButton->isChecked() ? tr("Near") : tr("Far"));
}

void SensorsUi::updateTimeEditDisabled()
{
    mTimeEdit->setDisabled(mCurrentRadio->isChecked());
    emitSensorsDataChange();
}

void SensorsUi::setAccelerometerTopUp()
{
    setAccelerometerValue(QVector3D(0, 9.8, 0));
    emitSensorsDataChange();
}

void SensorsUi::setAccelerometerLeftUp()
{
    setAccelerometerValue(QVector3D(-9.8, 0, 0));
    emitSensorsDataChange();
}

void SensorsUi::setAccelerometerRightUp()
{
    setAccelerometerValue(QVector3D(9.8, 0, 0));
    emitSensorsDataChange();
}

void SensorsUi::setAccelerometerTopDown()
{
    setAccelerometerValue(QVector3D(0, -9.8, 0));
    emitSensorsDataChange();
}

void SensorsUi::setAccelerometerFaceUp()
{
    setAccelerometerValue(QVector3D(0, 0, 9.8));
    emitSensorsDataChange();
}

void SensorsUi::setAccelerometerFaceDown()
{
    setAccelerometerValue(QVector3D(0, 0, -9.8));
    emitSensorsDataChange();
}

/*!
    \class SensorsScriptInterface
    \brief Exposed as sensors.
*/

SensorsScriptInterface::SensorsScriptInterface(SensorsUi *ui)
    : QObject(ui)
    , ui(ui)
{
    int enumIndex = metaObject()->indexOfEnumerator("LightLevel");
    QMetaEnum metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));
}

SensorsScriptInterface::~SensorsScriptInterface()
{
}

void SensorsScriptInterface::setAmbientLightLevel(const LightLevel &data)
{
    int index = static_cast<int>(data);
    if (ui->mAmbientLightBox->currentIndex() != index) {
        ui->mAmbientLightBox->setCurrentIndex(index);
        ui->ambientLightChanged();
    }

}

SensorsScriptInterface::LightLevel SensorsScriptInterface::ambientLightLevel() const
{
    SensorsScriptInterface::LightLevel lightLevel = static_cast<SensorsScriptInterface::LightLevel>(ui->mAmbientLightBox->currentIndex());
    return lightLevel;
}

void SensorsScriptInterface::setLux(double lux)
{
    if (ui->mLightEdit->value() != lux) {
        ui->mLightEdit->setValue(lux);
        ui->lightChanged();
    }

}

double SensorsScriptInterface::lux() const
{
    return ui->mLightEdit->value();
}

void SensorsScriptInterface::setAccelerometerX(double x)
{
    if (ui->mAccelerometerXEdit->value() != x) {
        ui->mAccelerometerXEdit->setValue(x);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::accelerometerX() const
{
    return ui->mAccelerometerXEdit->value();
}

void SensorsScriptInterface::setAccelerometerY(double Y)
{
    if (ui->mAccelerometerYEdit->value() != Y) {
        ui->mAccelerometerYEdit->setValue(Y);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::accelerometerY() const
{
    return ui->mAccelerometerYEdit->value();
}

void SensorsScriptInterface::setAccelerometerZ(double Z)
{
    if (ui->mAccelerometerZEdit->value() != Z) {
        ui->mAccelerometerZEdit->setValue(Z);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::accelerometerZ() const
{
    return ui->mAccelerometerZEdit->value();
}

void SensorsScriptInterface::setMagnetometerX(double x)
{
    if (ui->mMagnetometerXEdit->value() != x) {
        ui->mMagnetometerXEdit->setValue(x);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::magnetometerX() const
{
    return ui->mMagnetometerXEdit->value();
}

void SensorsScriptInterface::setMagnetometerY(double Y)
{
    if (ui->mMagnetometerYEdit->value() != Y) {
        ui->mMagnetometerYEdit->setValue(Y);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::magnetometerY() const
{
    return ui->mMagnetometerYEdit->value();
}

void SensorsScriptInterface::setMagnetometerZ(double Z)
{
    if (ui->mMagnetometerZEdit->value() != Z) {
        ui->mMagnetometerZEdit->setValue(Z);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::magnetometerZ() const
{
    return ui->mMagnetometerZEdit->value();
}

void SensorsScriptInterface::setMagnetometerCalibrationLevel(double l)
{
    if (ui->mMagnetometerCalibrationLevelEdit->value() != l) {
        ui->mMagnetometerCalibrationLevelEdit->setValue(l);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::magnetometerCalibrationLevel() const
{
    return ui->mMagnetometerCalibrationLevelEdit->value();
}

void SensorsScriptInterface::setCompassCalibrationLevel(double l)
{
    if (ui->mCompassCalibrationLevelEdit->value() != l) {
        ui->mCompassCalibrationLevelEdit->setValue(l);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::compassCalibrationLevel() const
{
    return ui->mCompassCalibrationLevelEdit->value();
}

void SensorsScriptInterface::setCompassAzimuth(double a)
{
    if (ui->mCompassAzimuthEdit->value() != a) {
        ui->mCompassAzimuthEdit->setValue(a);
        ui->emitSensorsDataChange();
    }
}

double SensorsScriptInterface::compassAzimuth() const
{
    return ui->mCompassAzimuthEdit->value();
}

void SensorsScriptInterface::setProximitySensorClose(bool c)
{
    if (ui->mProximitySensorCloseButton->isChecked() != c) {
        ui->mProximitySensorCloseButton->setChecked(c);
        ui->emitSensorsDataChange();
    }
}

bool SensorsScriptInterface::proximitySensorClose() const
{
    return ui->mProximitySensorCloseButton->isChecked();
}

void SensorsScriptInterface::setUseCurrentTimestamp(bool t)
{
    if (ui->mCurrentRadio->isChecked() != t) {
        ui->mCurrentRadio->setChecked(t);
        ui->emitSensorsDataChange();
    }
}

bool SensorsScriptInterface::useCurrentTimestamp() const
{
    return ui->mCurrentRadio->isChecked();
}

void SensorsScriptInterface::setTimestamp(const QDateTime &t)
{
    if (ui->mTimeEdit->dateTime() != t) {
        ui->mTimeEdit->setDateTime(t);
        ui->emitSensorsDataChange();
    }
}

QDateTime SensorsScriptInterface::timestamp() const
{
    return ui->mTimeEdit->dateTime();
}

void SensorsUi::updateDeviceDefaultOrientation(bool isPortrait)
{
    mAccelerometerControl->setDeviceOrientation(isPortrait);
    if (isPortrait) {
        mOrientationButtons.at(0)->setIcon(QIcon(":/ui/icons/topup.png"));
        mOrientationButtons.at(1)->setIcon(QIcon(":/ui/icons/leftup.png"));
        mOrientationButtons.at(2)->setIcon(QIcon(":/ui/icons/topdown.png"));
        mOrientationButtons.at(3)->setIcon(QIcon(":/ui/icons/rightup.png"));
        mOrientationButtons.at(4)->setIcon(QIcon(":/ui/icons/faceup.png"));
        mOrientationButtons.at(5)->setIcon(QIcon(":/ui/icons/facedown.png"));
    } else {
        mOrientationButtons.at(0)->setIcon(QIcon(":/ui/icons/topup_landscape.png"));
        mOrientationButtons.at(1)->setIcon(QIcon(":/ui/icons/leftup_landscape.png"));
        mOrientationButtons.at(2)->setIcon(QIcon(":/ui/icons/topdown_landscape.png"));
        mOrientationButtons.at(3)->setIcon(QIcon(":/ui/icons/rightup_landscape.png"));
        mOrientationButtons.at(4)->setIcon(QIcon(":/ui/icons/faceup_landscape.png"));
        mOrientationButtons.at(5)->setIcon(QIcon(":/ui/icons/facedown_landscape.png"));
    }
}

// Defines the min and max lux values that a given level has.
// These are used to add histeresis to the sensor.
// If the previous level is below a level, the lux must be at or above the minimum.
// If the previous level is above a level, the lux muyt be at or below the maximum.
static struct {
    int min;
    int max;
} limits[] = {
    { 0,    0    }, // Undefined (not used)
    { 0,    5    }, // Dark
    { 10,   50   }, // Twilight
    { 100,  200  }, // Light
    { 500,  2000 }, // Bright
    { 5000, 0    }  // Sunny
};

void SensorsUi::ambientLightChanged()
{
    int lightLevel = mAmbientLightBox->currentIndex();
    qreal lux = limits[lightLevel].min;
    mLightEdit->setValue(lux);
    emitSensorsDataChange();
}

void SensorsUi::lightChanged()
{
    // It's unweildly dealing with these constants so make some
    // local aliases that are shorter. This makes the code below
    // much easier to read.
    enum {
        Undefined = 0,
        Dark,
        Twilight,
        Light,
        Bright,
        Sunny
    };

    int lightLevel = mAmbientLightBox->currentIndex();
    qreal lux = mLightEdit->value();

    if (lux != 0 || lightLevel != Undefined) {
        // Check for change direction to allow for histeresis
        if      (lightLevel < Sunny    && lux >= limits[Sunny   ].min) lightLevel = Sunny;
        else if (lightLevel < Bright   && lux >= limits[Bright  ].min) lightLevel = Bright;
        else if (lightLevel < Light    && lux >= limits[Light   ].min) lightLevel = Light;
        else if (lightLevel < Twilight && lux >= limits[Twilight].min) lightLevel = Twilight;
        else if (lightLevel < Dark     && lux >= limits[Dark    ].min) lightLevel = Dark;
        else if (lightLevel > Dark     && lux <= limits[Dark    ].max) lightLevel = Dark;
        else if (lightLevel > Twilight && lux <= limits[Twilight].max) lightLevel = Twilight;
        else if (lightLevel > Light    && lux <= limits[Light   ].max) lightLevel = Light;
        else if (lightLevel > Bright   && lux <= limits[Bright  ].max) lightLevel = Bright;
    }

    if (mAmbientLightBox->currentIndex() != lightLevel) {
        mAmbientLightBox->setCurrentIndex(lightLevel);
    }

    emitSensorsDataChange();
}

QIcon SensorsUi::icon() const
{
    return QIcon(":/ui/icons/sensors.png");
}

