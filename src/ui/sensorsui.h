/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef SENSORSUI_H
#define SENSORSUI_H

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtGui/QSlider>

#include <remotecontrolwidget/toolbox.h>

class Sensors;
class QComboBox;
class QLineEdit;
class QDateTimeEdit;
class QPushButton;
class QRadioButton;
class SensorsScriptInterface;
class AccelerometerControl;
class QVector3D;

class SensorDoubleEdit: public QWidget
{
    Q_OBJECT
public:
    SensorDoubleEdit(QWidget *parent = 0);

    double value() const;

public slots:
    void setValue(double newValue, bool skipSignal = false);

public:

    void setRange(double min, double max);
    QPair<double, double> range() const;

    void setDecimalPlaces(int places);
    int decimalPlaces() const;

private slots:
    void changeValue(int newValue);
    void updateValueFromLineEdit();

signals:
    void valueChanged(double);

private:
    double mValue;
    QPair<double, double> mRange;
    int mDecimalPlaces;
    int mCorrectionFactor;
    QSlider *mSlider;
    QLineEdit *mLineEdit;

    void updateSlider();
};

class SensorsUi;

class SensorsScriptInterface : public QObject
{
    Q_OBJECT
public:
    // depends on order of items
    // has to match QAmbientLightReading::LightLevel
    enum LightLevel {
        Undefined,
        Dark,
        Twilight,
        Light,
        Bright,
        Sunny
    };
    Q_ENUMS(LightLevel);

    SensorsScriptInterface(SensorsUi *ui);
    virtual ~SensorsScriptInterface();

    Q_PROPERTY(LightLevel ambientLightLevel READ ambientLightLevel WRITE setAmbientLightLevel)
    Q_PROPERTY(double lux READ lux WRITE setLux)
    Q_PROPERTY(double accelerometerX READ accelerometerX WRITE setAccelerometerX)
    Q_PROPERTY(double accelerometerY READ accelerometerY WRITE setAccelerometerY)
    Q_PROPERTY(double accelerometerZ READ accelerometerZ WRITE setAccelerometerZ)

    Q_PROPERTY(double magnetometerX READ magnetometerX WRITE setMagnetometerX)
    Q_PROPERTY(double magnetometerY READ magnetometerY WRITE setMagnetometerY)
    Q_PROPERTY(double magnetometerZ READ magnetometerZ WRITE setMagnetometerZ)
    Q_PROPERTY(double magnetometerCalibrationLevel READ magnetometerCalibrationLevel WRITE setMagnetometerCalibrationLevel)

    Q_PROPERTY(double compassCalibrationLevel READ compassCalibrationLevel WRITE setCompassCalibrationLevel)
    Q_PROPERTY(double compassAzimuth READ compassAzimuth WRITE setCompassAzimuth)

    Q_PROPERTY(bool proximitySensorClose READ proximitySensorClose WRITE setProximitySensorClose)

    Q_PROPERTY(bool useCurrentTimestamp READ useCurrentTimestamp WRITE setUseCurrentTimestamp)
    Q_PROPERTY(QDateTime timestamp READ timestamp WRITE setTimestamp)

    void setAmbientLightLevel(const LightLevel &data);
    LightLevel ambientLightLevel() const;
    void setLux(double lux);
    double lux() const;

    void setAccelerometerX(double x);
    double accelerometerX() const;

    void setAccelerometerY(double y);
    double accelerometerY() const;

    void setAccelerometerZ(double z);
    double accelerometerZ() const;

    void setMagnetometerX(double x);
    double magnetometerX() const;

    void setMagnetometerY(double y);
    double magnetometerY() const;

    void setMagnetometerZ(double z);
    double magnetometerZ() const;

    void setMagnetometerCalibrationLevel(double l);
    double magnetometerCalibrationLevel() const;

    void setCompassCalibrationLevel(double l);
    double compassCalibrationLevel() const;

    void setCompassAzimuth(double a);
    double compassAzimuth() const;

    void setProximitySensorClose(bool c);
    bool proximitySensorClose() const;

    void setUseCurrentTimestamp(bool t);
    bool useCurrentTimestamp() const;

    void setTimestamp(const QDateTime &t);
    QDateTime timestamp() const;

private:
    SensorsUi *ui;
};

class SensorsUi : public ToolBoxPage
{
    Q_OBJECT
public:
    struct SensorsData {
        int ambientLightLevel;
        double lux;
        double accelerometerX;
        double accelerometerY;
        double accelerometerZ;
        double magnetometerX;
        double magnetometerY;
        double magnetometerZ;
        double magnetometerCalibrationLevel;
        double compassCalibrationLevel;
        double compassAzimuth;
        bool proximitySensorClose;
        bool useCurrentTime;
        QDateTime timestamp;
    };

    explicit SensorsUi(QWidget *parent = 0);
    virtual ~SensorsUi();

    SensorsScriptInterface *scriptInterface() const;

    SensorsUi::SensorsData sensorsData() const;
    QIcon icon() const;

public slots:
    void setSensorsData(const SensorsUi::SensorsData &data);
    void setDisplayedSensorsData(const SensorsUi::SensorsData &data);
    void updateDeviceDefaultOrientation(bool isPortrait);
    void ambientLightChanged();
    void lightChanged();

signals:
    void sensorsDataChanged(const SensorsUi::SensorsData &data) const;

private slots:
    void emitSensorsDataChange() const;
    void setAccelerometerValue(const QVector3D &value);
    void setMagnetometerValue(const QVector3D &value);
    void updateTimeEditDisabled();
    void updateProximityButtonText();

    void setAccelerometerTopUp();
    void setAccelerometerLeftUp();
    void setAccelerometerRightUp();
    void setAccelerometerTopDown();
    void setAccelerometerFaceUp();
    void setAccelerometerFaceDown();

private:
    void initializeAmbientLightOptions();

    friend class SensorsScriptInterface;
    SensorsScriptInterface *mScriptInterface;

    void setTimestamp(const QDateTime &t);
    QDateTime getTimestamp() const;

    QComboBox *mAmbientLightBox;
    SensorDoubleEdit *mLightEdit;

    SensorDoubleEdit *mAccelerometerXEdit;
    SensorDoubleEdit *mAccelerometerYEdit;
    SensorDoubleEdit *mAccelerometerZEdit;
    AccelerometerControl *mAccelerometerControl;

    SensorDoubleEdit *mMagnetometerXEdit;
    SensorDoubleEdit *mMagnetometerYEdit;
    SensorDoubleEdit *mMagnetometerZEdit;
    SensorDoubleEdit *mMagnetometerCalibrationLevelEdit;

    SensorDoubleEdit *mCompassCalibrationLevelEdit;
    SensorDoubleEdit *mCompassAzimuthEdit;

    QPushButton *mProximitySensorCloseButton;
    QList<QPushButton *> mOrientationButtons;

    QRadioButton *mCurrentRadio;
    QRadioButton *mOverrideRadio;
    QDateTimeEdit *mTimeEdit;
};

#endif // SENSORSUI_H
