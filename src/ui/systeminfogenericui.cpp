/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "systeminfogenericui.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtCore/QMetaEnum>
#include <QtGui/QPushButton>
#include <QtGui/QBoxLayout>
#include <QtGui/QLineEdit>
#include <QtGui/QComboBox>
#include <QtGui/QCheckBox>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>
#include <QtGui/QFormLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QInputDialog>

GenericSystemInfoUi::GenericSystemInfoUi(QWidget *parent)
    : ToolBoxPage(parent)
{
    qRegisterMetaType<GenericSystemInfoUi::GenericData>("GenericSystemInfoUi::GenericData");

    mScriptInterface = new GenericSystemInfoScriptInterface(this);

    initializeGeneric();

    // button translators
    connect(this, SIGNAL(genericDataChanged(GenericSystemInfoUi::GenericData)), SLOT(emitButtonSignalsOnChange()));
}

void GenericSystemInfoUi::initializeGeneric()
{
    QStringList tags;
    QList<OptionsItem *> optionsList;
    tags  << tr("generic");

    systemInfoBatteryLevel = new QSpinBox();
    systemInfoBatteryLevel->setRange(0, 100);
    systemInfoBatteryStatus = new QLabel();
    systemInfoBatteryStatus->setAlignment(Qt::AlignHCenter);
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->setContentsMargins(0, 0, 0, 0);
    vLayout->addWidget(systemInfoBatteryLevel);
    vLayout->addWidget(systemInfoBatteryStatus);
    QWidget *tmp = new QWidget();
    tmp->setContentsMargins(0, 0, 0, 0);
    tmp->setLayout(vLayout);
    connect(systemInfoBatteryLevel, SIGNAL(valueChanged(int)), SLOT(sysinfoUpdateBatteryStatus(int)));
    connect(systemInfoBatteryLevel, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    OptionsItem *item =  new OptionsItem(tr("Battery level"), tmp);
    optionsList << item;

    systemInfoPowerState = new QComboBox();
    connect(systemInfoPowerState, SIGNAL(activated(int)), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Power state"), systemInfoPowerState);
    optionsList << item;

    systemInfoProfile = new QComboBox();
    connect(systemInfoProfile, SIGNAL(activated(int)), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Profile"), systemInfoProfile);
    optionsList << item;

    systemInfoDeviceLocked = new QCheckBox();
    connect(systemInfoDeviceLocked, SIGNAL(clicked()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Device locked"), systemInfoDeviceLocked);
    optionsList << item;

    systemInfoLanguage = new QLineEdit();
    systemInfoLanguage->setInputMask("<AA");
    systemInfoLanguage->setValidator(new QRegExpValidator(QRegExp("\\S\\S"), systemInfoLanguage));
    connect(systemInfoLanguage, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Language"), systemInfoLanguage);
    item->setAdvanced();
    item->setTags(tags);
    optionsList << item;

    systemInfoCountryCode = new QLineEdit();
    systemInfoCountryCode->setInputMask(">AA");
    systemInfoCountryCode->setValidator(new QRegExpValidator(QRegExp("\\S\\S"), systemInfoCountryCode));
    connect(systemInfoCountryCode, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item =  new OptionsItem(tr("Country code"), systemInfoCountryCode);
    item->setAdvanced();
    optionsList << item;

    systemInfoAvailableLanguages = new QComboBox();
    QPushButton *systemInfoAddAvailableLanguage = new QPushButton(tr("Add"));
    QPushButton *systemInfoRemoveAvailableLanguage = new QPushButton(tr("Remove"));
    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->setContentsMargins(0, 0, 0, 0);
    hLayout->addWidget(systemInfoAvailableLanguages);
    hLayout->addWidget(systemInfoAddAvailableLanguage);
    hLayout->addWidget(systemInfoRemoveAvailableLanguage);
    QGroupBox *systemInfoLanguageGroupBox = new QGroupBox();
    systemInfoLanguageGroupBox->setLayout(hLayout);
    QPushButton *toggleLanguagesVisible = new QPushButton(tr("Show"));
    connect(systemInfoRemoveAvailableLanguage, SIGNAL(clicked()), SLOT(sysinfoRemoveAvailableLanguage()));
    connect(systemInfoAddAvailableLanguage, SIGNAL(clicked()), SLOT(sysinfoAddAvailableLanguage()));
    item =  new OptionsItem(tr("Available languages"), toggleLanguagesVisible);
    item->setTags(tags);
    item->setAdvanced();
    OptionsItem *advancedItem = new OptionsItem("", systemInfoLanguageGroupBox, true);
    item->setAdvancedPage(advancedItem);
    optionsList << item;

    systemInfoFeatures = new QComboBox();
    systemInfoFeatureEnabled = new QLabel(tr("available"));
    QPushButton *systemInfoToggleFeature = new QPushButton(tr("Toggle"));
    hLayout = new QHBoxLayout();
    hLayout->setContentsMargins(0, 0, 0, 0);
    hLayout->addWidget(systemInfoFeatures);
    hLayout->addWidget(systemInfoFeatureEnabled);
    hLayout->addWidget(systemInfoToggleFeature);
    QGroupBox *systemInfoFeaturesGroupBox = new QGroupBox();
    systemInfoFeaturesGroupBox->setLayout(hLayout);
    QPushButton *toggleFeaturesVisible = new QPushButton(tr("Show"));
    connect(systemInfoToggleFeature, SIGNAL(clicked()), this, SLOT(sysinfoToggleFeature()));
    connect(systemInfoFeatures, SIGNAL(activated(int)), this, SLOT(sysinfoDisplayFeature()));
    item =  new OptionsItem(tr("Features"), toggleFeaturesVisible);
    QStringList featureTags = tags;
    featureTags << tr("Bluetooth") << tr("Camera") << tr("FM Radio") << tr("Infrared");
    featureTags << tr("LED") << tr("Memory Card") << tr("USB") << tr("Vibration");
    featureTags << tr("WLAN") << tr("SIM") << tr("Location") << tr("Video out") << tr("Haptics");
    item->setTags(featureTags);
    item->setAdvanced();
    advancedItem = new OptionsItem("", systemInfoFeaturesGroupBox, true);
    item->setAdvancedPage(advancedItem);
    optionsList << item;

    systemInfoVersions = new QComboBox();
    systemInfoVersionString = new QLabel(tr("version text"));
    QPushButton *systemInfoChangeVersion = new QPushButton(tr("Change"));
    hLayout = new QHBoxLayout();
    hLayout->setContentsMargins(0, 0, 0, 0);
    hLayout->addWidget(systemInfoVersions);
    hLayout->addWidget(systemInfoVersionString);
    hLayout->addWidget(systemInfoChangeVersion);
    QGroupBox *systemInfoVersionsGroupBox = new QGroupBox();
    systemInfoVersionsGroupBox->setLayout(hLayout);
    QPushButton *toggleVersionsVisible = new QPushButton(tr("Show"));
    connect(systemInfoChangeVersion, SIGNAL(clicked()), SLOT(sysinfoChangeVersion()));
    connect(systemInfoVersions, SIGNAL(activated(int)), SLOT(sysinfoDisplayVersion()));
    item =  new OptionsItem(tr("Versions"), toggleVersionsVisible);
    QStringList versionTags = tags;
    versionTags << "os" << "qt" << "firmware";
    item->setTags(versionTags);
    item->setAdvanced();
    advancedItem = new OptionsItem("", systemInfoVersionsGroupBox, true);
    item->setAdvancedPage(advancedItem);
    optionsList << item;

    tags.removeAll(tr("generic"));
    tags << tr("device");

    systemInfoSimStatus = new QComboBox();
    connect(systemInfoSimStatus, SIGNAL(activated(int)), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("SIM status"), systemInfoSimStatus);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;

    systemInfoImei = new QLineEdit();
    systemInfoImei->setInputMask("99-999999-999999-9");
    systemInfoImei->setValidator(new QRegExpValidator(QRegExp("\\d{2}-\\d{6}-\\d{6}-\\d"), systemInfoImei));
    connect(systemInfoImei, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("IMEI"), systemInfoImei);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;

    systemInfoImsi = new QLineEdit();
    systemInfoImsi->setInputMask("999999999999990");
    systemInfoImsi->setValidator(new QRegExpValidator(QRegExp("\\d{14}[\\d\\s]?"), systemInfoImsi));
    connect(systemInfoImsi, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("IMSI"), systemInfoImsi);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;

    systemInfoInputKeys = new QCheckBox(tr("Keys"));
    systemInfoInputKeypad = new QCheckBox(tr("Keypad"));
    systemInfoInputKeyboard = new QCheckBox(tr("Keyboard"));
    systemInfoInputSingleTouch = new QCheckBox(tr("Single touch"));
    systemInfoInputMultiTouch = new QCheckBox(tr("Multipoint-touch"));
    systemInfoInputMouse = new QCheckBox(tr("Mouse"));
    QGridLayout *gridLayout = new QGridLayout();
    gridLayout->addWidget(systemInfoInputKeys, 0, 0);
    gridLayout->addWidget(systemInfoInputKeypad, 0, 1);
    gridLayout->addWidget(systemInfoInputKeyboard, 0, 2);
    gridLayout->addWidget(systemInfoInputSingleTouch, 1, 0);
    gridLayout->addWidget(systemInfoInputMultiTouch, 1, 1);
    gridLayout->addWidget(systemInfoInputMouse, 1, 2);
    QGroupBox *systemInfoInputGroupBox = new QGroupBox;
    systemInfoInputGroupBox->setLayout(gridLayout);
    QPushButton *toggleInputMethodsVisible = new QPushButton(tr("Show"));
    connect(systemInfoInputKeys, SIGNAL(clicked()), SLOT(emitGenericDataChanged()));
    connect(systemInfoInputKeypad, SIGNAL(clicked()), SLOT(emitGenericDataChanged()));
    connect(systemInfoInputKeyboard, SIGNAL(clicked()), SLOT(emitGenericDataChanged()));
    connect(systemInfoInputSingleTouch, SIGNAL(clicked()), SLOT(emitGenericDataChanged()));
    connect(systemInfoInputMultiTouch, SIGNAL(clicked()), SLOT(emitGenericDataChanged()));
    connect(systemInfoInputMouse, SIGNAL(clicked()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Input methods"), toggleInputMethodsVisible);
    QStringList inputTags = tags;
    inputTags << tr("keys") << tr("keypad") << tr("keyboard") << tr("single");
    inputTags << tr("multi") << tr("touch") << tr("mouse");
    item->setTags(inputTags);
    item->setAdvanced();
    advancedItem = new OptionsItem("", systemInfoInputGroupBox, true);
    item->setAdvancedPage(advancedItem);
    optionsList << item;

    systemInfoManufacturer = new QLineEdit();
    connect(systemInfoManufacturer, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Manufacturer"), systemInfoManufacturer);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;

    systemInfoModel = new QLineEdit();
    connect(systemInfoModel, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Model"), systemInfoModel);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;

    systemInfoProductName = new QLineEdit();
    connect(systemInfoProductName, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Product name"), systemInfoProductName);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;

    tags.removeAll(tr("device"));
    tags << tr("display");

    systemInfoColorDepth = new QSpinBox();
    systemInfoColorDepth->setRange(8, 32);
    systemInfoColorDepth->setValue(32);
    systemInfoColorDepth->setSingleStep(8);
    connect(systemInfoColorDepth, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Color depth"), systemInfoColorDepth);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;

    systemInfoBrightness = new QSpinBox();
    systemInfoBrightness->setRange(1, 100);
    connect(systemInfoBrightness, SIGNAL(editingFinished()), SLOT(emitGenericDataChanged()));
    item = new OptionsItem(tr("Brightness"), systemInfoBrightness);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;

    /* ### TODO: Implement.
    tags.removeAll(tr("display"));
    tags << tr("screen") << tr("saver") << tr("screensaver");

    QCheckBox *screenSaverInhibited = new QCheckBox();
    item = new OptionsItem(tr("Screen saver inhibited"), screenSaverInhibited);
    item->setTags(tags);
    item->setAdvanced();
    optionsList << item;
    */

    initializeGenericOptions();
    initializeDeviceOptions();

    setTitle(tr("Generic"));
    setOptions(optionsList);
}

GenericSystemInfoUi::~GenericSystemInfoUi()
{
}

void GenericSystemInfoUi::initializeGenericOptions()
{
    systemInfoVersions->clear();
    systemInfoVersions->addItem(tr("Operating System"), GenericSystemInfoScriptInterface::Os);
    systemInfoVersions->addItem(tr("Qt"), GenericSystemInfoScriptInterface::QtCore);
    systemInfoVersions->addItem(tr("Firmware"), GenericSystemInfoScriptInterface::Firmware);
    sysinfoDisplayVersion();

    systemInfoFeatures->clear();
    systemInfoFeatures->addItem(tr("Bluetooth"), GenericSystemInfoScriptInterface::BluetoothFeature);
    systemInfoFeatures->addItem(tr("Camera"), GenericSystemInfoScriptInterface::CameraFeature);
    systemInfoFeatures->addItem(tr("FM Radio"), GenericSystemInfoScriptInterface::FmradioFeature);
    systemInfoFeatures->addItem(tr("Infrared"), GenericSystemInfoScriptInterface::IrFeature);
    systemInfoFeatures->addItem(tr("LED"), GenericSystemInfoScriptInterface::LedFeature);
    systemInfoFeatures->addItem(tr("Memory Card"), GenericSystemInfoScriptInterface::MemcardFeature);
    systemInfoFeatures->addItem(tr("USB"), GenericSystemInfoScriptInterface::UsbFeature);
    systemInfoFeatures->addItem(tr("Vibration"), GenericSystemInfoScriptInterface::VibFeature);
    systemInfoFeatures->addItem(tr("WLAN"), GenericSystemInfoScriptInterface::WlanFeature);
    systemInfoFeatures->addItem(tr("SIM"), GenericSystemInfoScriptInterface::SimFeature);
    systemInfoFeatures->addItem(tr("Location"), GenericSystemInfoScriptInterface::LocationFeature);
    systemInfoFeatures->addItem(tr("Video out"), GenericSystemInfoScriptInterface::VideoOutFeature);
    systemInfoFeatures->addItem(tr("Haptics"), GenericSystemInfoScriptInterface::HapticsFeature);
    sysinfoDisplayFeature();
}

void GenericSystemInfoUi::initializeDeviceOptions()
{
    systemInfoPowerState->clear();
    QStringList powerStates;
    // depends on order of items
    powerStates.append(tr("Unknown or Error"));
    powerStates.append(tr("Battery"));
    powerStates.append(tr("Wall"));
    powerStates.append(tr("Wall and Charging"));
    systemInfoPowerState->addItems(powerStates);

    systemInfoProfile->clear();
    QStringList profiles;
    // depends on order of items
    profiles.append(tr("Unknown or Error"));
    profiles.append(tr("Silent"));
    profiles.append(tr("Normal"));
    profiles.append(tr("Loud"));
    profiles.append(tr("Vibrate"));
    profiles.append(tr("Offline"));
    profiles.append(tr("Powersave"));
    profiles.append(tr("Custom"));
    systemInfoProfile->addItems(profiles);

    systemInfoSimStatus->clear();
    QStringList simStates;
    // depends on order of items
    simStates.append(tr("Not Available"));
    simStates.append(tr("Single"));
    simStates.append(tr("Dual"));
    simStates.append(tr("Locked"));
    systemInfoSimStatus->addItems(simStates);
}

GenericSystemInfoUi::GenericData::GenericData()
    : features(13)
    , versions(4)
{
}


void GenericSystemInfoUi::sysinfoAddAvailableLanguage()
{
    QString newLang = QInputDialog::getText(0,
            "Add New Language", "Enter the new available language:");
    if (!newLang.isEmpty()) {
        systemInfoAvailableLanguages->addItem(newLang);
        emitGenericDataChanged();
    }
}

void GenericSystemInfoUi::sysinfoRemoveAvailableLanguage()
{
    systemInfoAvailableLanguages->removeItem(systemInfoAvailableLanguages->currentIndex());
    emitGenericDataChanged();
}

void GenericSystemInfoUi::sysinfoToggleFeature()
{
    GenericSystemInfoScriptInterface::Feature currentFeature
        = static_cast<GenericSystemInfoScriptInterface::Feature>(systemInfoFeatures->currentIndex());
    mFeatures[currentFeature] ^= 1;
    sysinfoDisplayFeature();
    emitGenericDataChanged();
}

void GenericSystemInfoUi::sysinfoChangeVersion()
{
    GenericSystemInfoScriptInterface::Version v = static_cast<GenericSystemInfoScriptInterface::Version>(systemInfoVersions->itemData(systemInfoVersions->currentIndex()).toInt());
    QString newVersion = QInputDialog::getText(0,
            "Change Version", "Enter the new version string:",
            QLineEdit::Normal, mVersions.value(v));
    if (!newVersion.isEmpty()) {
        mVersions[v] = newVersion;
        sysinfoDisplayVersion();
        emitGenericDataChanged();
    }
}

void GenericSystemInfoUi::sysinfoDisplayVersion()
{
    GenericSystemInfoScriptInterface::Version v = static_cast<GenericSystemInfoScriptInterface::Version>(systemInfoVersions->itemData(systemInfoVersions->currentIndex()).toInt());
    systemInfoVersionString->setText(mVersions.value(v));
}

void GenericSystemInfoUi::sysinfoDisplayFeature()
{
    GenericSystemInfoScriptInterface::Feature f = static_cast<GenericSystemInfoScriptInterface::Feature>(systemInfoFeatures->itemData(systemInfoFeatures->currentIndex()).toInt());
    if (mFeatures.value(f))
        systemInfoFeatureEnabled->setText("Available");
    else
        systemInfoFeatureEnabled->setText("Not Available");
}

void GenericSystemInfoUi::sysinfoSetAvailableLanguages(const QStringList &list)
{
    systemInfoAvailableLanguages->clear();
    systemInfoAvailableLanguages->addItems(list);
    emitGenericDataChanged();
}

void GenericSystemInfoUi::emitGenericDataChanged()
{
    emit genericDataChanged(genericData());
}

QIcon GenericSystemInfoUi::icon() const
{
    return QIcon(":/ui/icons/systeminfo.png");
}

GenericSystemInfoUi::GenericData GenericSystemInfoUi::genericData() const
{
    GenericData data;
    data.currentLanguage = systemInfoLanguage->text();
    data.availableLanguages.clear();
    for (int i = 0; i < systemInfoAvailableLanguages->count(); i++)
        data.availableLanguages.append(systemInfoAvailableLanguages->itemText(i));
    data.currentCountryCode = systemInfoCountryCode->text();
    data.displayBrightness = systemInfoBrightness->value();
    data.colorDepth = systemInfoColorDepth->value();
    data.profile = static_cast<GenericSystemInfoScriptInterface::Profile>(systemInfoProfile->currentIndex());
    data.currentPowerState = static_cast<GenericSystemInfoScriptInterface::PowerState>(systemInfoPowerState->currentIndex());
    data.simStatus = static_cast<GenericSystemInfoScriptInterface::SimStatus>(systemInfoSimStatus->currentIndex());
    GenericSystemInfoScriptInterface::InputMethodFlags iflags;
    if (systemInfoInputKeys->isChecked())
        iflags |= GenericSystemInfoScriptInterface::Keys;
    if (systemInfoInputKeypad->isChecked())
        iflags |= GenericSystemInfoScriptInterface::Keypad;
    if (systemInfoInputKeyboard->isChecked())
        iflags |= GenericSystemInfoScriptInterface::Keyboard;
    if (systemInfoInputSingleTouch->isChecked())
        iflags |= GenericSystemInfoScriptInterface::SingleTouch;
    if (systemInfoInputMultiTouch->isChecked())
        iflags |= GenericSystemInfoScriptInterface::MultiTouch;
    if (systemInfoInputMouse->isChecked())
        iflags |= GenericSystemInfoScriptInterface::Mouse;
    data.inputFlags = iflags;
    data.imei = systemInfoImei->text();
    data.imsi = systemInfoImsi->text();
    data.manufacturer = systemInfoManufacturer->text();
    data.model = systemInfoModel->text();
    data.productName = systemInfoProductName->text();
    data.deviceLocked = systemInfoDeviceLocked->isChecked();
    data.batteryLevel = systemInfoBatteryLevel->value();
    data.features = mFeatures;
    data.versions = mVersions;
    return data;
}

GenericSystemInfoScriptInterface *GenericSystemInfoUi::scriptInterface()
{
    return mScriptInterface;
}

void GenericSystemInfoUi::sysinfoUpdateBatteryStatus(int level)
{
    // batteryButton->updateIcon(level);
    if (level > 100)
        systemInfoBatteryStatus->setText("No battery level");
    else if (level > 40)
        systemInfoBatteryStatus->setText("Battery normal");
    else if  (level > 10)
        systemInfoBatteryStatus->setText("Battery low");
    else if  (level > 3)
        systemInfoBatteryStatus->setText("Battery very low");
    else if (level >= 0)
        systemInfoBatteryStatus->setText("Battery critical");
    else // negative
        systemInfoBatteryStatus->setText("No battery level");
}

void GenericSystemInfoUi::setGenericData(const GenericSystemInfoUi::GenericData &data)
{
    setDisplayedGenericData(data);
    emit genericDataChanged(data);
}

void GenericSystemInfoUi::setDisplayedGenericData(const GenericSystemInfoUi::GenericData &data)
{
    systemInfoAvailableLanguages->clear();
    foreach (const QString &language, data.availableLanguages)
        systemInfoAvailableLanguages->addItem(language);
    systemInfoBatteryLevel->setValue(data.batteryLevel);
    systemInfoBrightness->setValue(data.displayBrightness);
    systemInfoColorDepth->setValue(data.colorDepth);
    systemInfoCountryCode->setText(data.currentCountryCode);
    systemInfoDeviceLocked->setChecked(data.deviceLocked);
    mFeatures = data.features;
    mScriptInterface->setInputMethod(data.inputFlags);
    systemInfoImei->setText(data.imei);
    systemInfoImsi->setText(data.imsi);
    systemInfoManufacturer->setText(data.manufacturer);
    systemInfoModel->setText(data.model);
    systemInfoPowerState->setCurrentIndex(static_cast<int>(data.currentPowerState));
    systemInfoLanguage->setText(data.currentLanguage);
    systemInfoProductName->setText(data.productName);
    systemInfoProfile->setCurrentIndex(static_cast<int>(data.profile));
    systemInfoSimStatus->setCurrentIndex(static_cast<int>(data.simStatus));
    mVersions = data.versions;
}

void GenericSystemInfoUi::setCurrentPowerState(PowerButton::PowerState powerState)
{
    mScriptInterface->setCurrentPowerState(static_cast<GenericSystemInfoScriptInterface::PowerState>(powerState));
}

void GenericSystemInfoUi::setCurrentBatteryLevel(BatteryButton::BatteryLevel batteryLevel)
{
    switch (batteryLevel) {
        case BatteryButton::BatteryCritical:
            mScriptInterface->setBatteryLevel(3);
            break;
        case BatteryButton::BatteryVeryLow:
            mScriptInterface->setBatteryLevel(10);
            break;
        case BatteryButton::BatteryLow:
            mScriptInterface->setBatteryLevel(40);
            break;
        case BatteryButton::BatteryNormal:
            mScriptInterface->setBatteryLevel(100);
            break;
        default:
            mScriptInterface->setBatteryLevel(0);
            break;
    }
}

void GenericSystemInfoUi::emitButtonSignalsOnChange()
{
    GenericSystemInfoScriptInterface::PowerState currentState = mScriptInterface->currentPowerState();
    emit currentPowerStateChanged(static_cast<PowerButton::PowerState>(currentState));

    int currentLevel = mScriptInterface->batteryLevel();
    if (currentLevel <= 3)
        emit currentBatteryLevelChanged(BatteryButton::BatteryCritical);
    else if (currentLevel <= 10)
        emit currentBatteryLevelChanged(BatteryButton::BatteryVeryLow);
    else if (currentLevel <= 40)
        emit currentBatteryLevelChanged(BatteryButton::BatteryLow);
    else
        emit currentBatteryLevelChanged(BatteryButton::BatteryNormal);
}


/*!
    \class GenericSystemInfoScriptInterface
    \brief Exposed as sysinfo.generic.
*/

GenericSystemInfoScriptInterface::GenericSystemInfoScriptInterface(GenericSystemInfoUi *ui)
    : QObject(ui)
    , ui(ui)
{
    int enumIndex = metaObject()->indexOfEnumerator("Version");
    QMetaEnum metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));

    enumIndex = metaObject()->indexOfEnumerator("Feature");
    metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));

    enumIndex = metaObject()->indexOfEnumerator("BatteryStatus");
    metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));

    enumIndex = metaObject()->indexOfEnumerator("PowerState");
    metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));

    enumIndex = metaObject()->indexOfEnumerator("InputMethod");
    metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));

    enumIndex = metaObject()->indexOfEnumerator("Profile");
    metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));

    enumIndex = metaObject()->indexOfEnumerator("SimStatus");
    metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));
}

GenericSystemInfoScriptInterface::~GenericSystemInfoScriptInterface()
{
}

QString GenericSystemInfoScriptInterface::currentLanguage() const
{
    return ui->systemInfoLanguage->text();
}

QStringList GenericSystemInfoScriptInterface::availableLanguages() const
{
    QStringList returnList;
    for (int i = 0; i < ui->systemInfoAvailableLanguages->count(); i++)
        returnList.append(ui->systemInfoAvailableLanguages->itemText(i));
    return returnList;
}

QString GenericSystemInfoScriptInterface::currentCountryCode() const
{
    return ui->systemInfoCountryCode->text();
}

bool GenericSystemInfoScriptInterface::hasFeature(GenericSystemInfoScriptInterface::Feature f) const
{
    return (ui->mFeatures.contains(f) && ui->mFeatures.value(f));
}

QString GenericSystemInfoScriptInterface::version(GenericSystemInfoScriptInterface::Version v, const QString &param) const
{
    Q_UNUSED(param);
    return ui->mVersions[v];
}

int GenericSystemInfoScriptInterface::displayBrightness(int screen) const
{
    Q_UNUSED(screen);
    return ui->systemInfoBrightness->text().toInt();
}

int GenericSystemInfoScriptInterface::colorDepth(int screen) const
{
    Q_UNUSED(screen);
    return ui->systemInfoColorDepth->text().toInt();
}

GenericSystemInfoScriptInterface::Profile GenericSystemInfoScriptInterface::currentProfile() const
{
    return static_cast<GenericSystemInfoScriptInterface::Profile>(ui->systemInfoProfile->currentIndex());
}

GenericSystemInfoScriptInterface::PowerState GenericSystemInfoScriptInterface::currentPowerState() const
{
    return static_cast<GenericSystemInfoScriptInterface::PowerState>(ui->systemInfoPowerState->currentIndex());
}

GenericSystemInfoScriptInterface::SimStatus GenericSystemInfoScriptInterface::simStatus() const
{
    return static_cast<GenericSystemInfoScriptInterface::SimStatus>(ui->systemInfoSimStatus->currentIndex());
}

GenericSystemInfoScriptInterface::InputMethodFlags GenericSystemInfoScriptInterface::inputMethod() const
{
    GenericSystemInfoScriptInterface::InputMethodFlags flags;
    if (ui->systemInfoInputKeys->isChecked())
        flags |= GenericSystemInfoScriptInterface::Keys;
    if (ui->systemInfoInputKeypad->isChecked())
        flags |= GenericSystemInfoScriptInterface::Keypad;
    if (ui->systemInfoInputKeyboard->isChecked())
        flags |= GenericSystemInfoScriptInterface::Keyboard;
    if (ui->systemInfoInputSingleTouch->isChecked())
        flags |= GenericSystemInfoScriptInterface::SingleTouch;
    if (ui->systemInfoInputMultiTouch->isChecked())
        flags |= GenericSystemInfoScriptInterface::MultiTouch;
    if (ui->systemInfoInputMouse->isChecked())
        flags |= GenericSystemInfoScriptInterface::Mouse;
    return flags;
}

QString GenericSystemInfoScriptInterface::imei() const
{
    return ui->systemInfoImei->text();
}

QString GenericSystemInfoScriptInterface::imsi() const
{
    return ui->systemInfoImsi->text();
}

QString GenericSystemInfoScriptInterface::manufacturer() const
{
    return ui->systemInfoManufacturer->text();
}

QString GenericSystemInfoScriptInterface::model() const
{
    return ui->systemInfoModel->text();
}

QString GenericSystemInfoScriptInterface::productName() const
{
    return ui->systemInfoProductName->text();
}

int GenericSystemInfoScriptInterface::batteryLevel() const
{
    return ui->systemInfoBatteryLevel->value();
}

bool GenericSystemInfoScriptInterface::isDeviceLocked() const
{
    return (ui->systemInfoDeviceLocked->isChecked());
}

GenericSystemInfoScriptInterface::BatteryStatus GenericSystemInfoScriptInterface::batteryStatus() const
{
    if (ui->systemInfoBatteryLevel->value() < 0)
        return GenericSystemInfoScriptInterface::NoBatteryLevel;
    else if (ui->systemInfoBatteryLevel->value() <= 3)
        return GenericSystemInfoScriptInterface::BatteryCritical;
    else if (ui->systemInfoBatteryLevel->value() <= 10)
        return GenericSystemInfoScriptInterface::BatteryVeryLow;
    else if (ui->systemInfoBatteryLevel->value() <= 40)
        return GenericSystemInfoScriptInterface::BatteryLow;
    else if (ui->systemInfoBatteryLevel->value() <= 100)
        return GenericSystemInfoScriptInterface::BatteryNormal;
    else
        return GenericSystemInfoScriptInterface::NoBatteryLevel;
}

void GenericSystemInfoScriptInterface::setCurrentLanguage(const QString &v)
{
    if (ui->systemInfoLanguage->text() != v) {
        ui->systemInfoLanguage->setText(v);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setCurrentCountryCode(const QString &v)
{
    if (ui->systemInfoCountryCode->text() != v) {
        ui->systemInfoCountryCode->setText(v);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setAvailableLanguages(const QStringList &v)
{
    ui->systemInfoAvailableLanguages->clear();
    foreach(const QString &language, v)
        ui->systemInfoAvailableLanguages->addItem(language);
    ui->emitGenericDataChanged();
}

void GenericSystemInfoScriptInterface::addAvailableLanguage(const QString &v)
{
    ui->systemInfoAvailableLanguages->addItem(v);
    ui->emitGenericDataChanged();
}

bool GenericSystemInfoScriptInterface::removeAvailableLanguage(const QString &v)
{
    bool retValue = false;
    for (int i = 0; i < ui->systemInfoAvailableLanguages->count(); i++) {
        if (ui->systemInfoAvailableLanguages->itemText(i) == v) {
            ui->systemInfoAvailableLanguages->removeItem(i);
            i--;
            retValue = true;
        }
    }
    if (retValue)
        ui->emitGenericDataChanged();
    return retValue;
}

void GenericSystemInfoScriptInterface::removeAllAvailableLanguages()
{
    ui->systemInfoAvailableLanguages->clear();
    ui->emitGenericDataChanged();
}

void GenericSystemInfoScriptInterface::setFeature(GenericSystemInfoScriptInterface::Feature f, bool enabled)
{
    ui->mFeatures[f] = enabled;
    ui->sysinfoDisplayFeature();
    ui->emitGenericDataChanged();
}

void GenericSystemInfoScriptInterface::setVersion(GenericSystemInfoScriptInterface::Version v, const QString &to)
{
    ui->mVersions[v] = to;
    ui->sysinfoDisplayVersion();
    ui->emitGenericDataChanged();
}

void GenericSystemInfoScriptInterface::setDisplayBrightness(int brightness)
{
    if (ui->systemInfoBrightness->value() != brightness) {
        ui->systemInfoBrightness->setValue(brightness);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setColorDepth(int depth)
{
    if (ui->systemInfoColorDepth->value() != depth) {
        ui->systemInfoColorDepth->setValue(depth);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setCurrentProfile(GenericSystemInfoScriptInterface::Profile v)
{
    int index = static_cast<int>(v);
    if (ui->systemInfoProfile->currentIndex() != index) {
        ui->systemInfoProfile->setCurrentIndex(index);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setCurrentPowerState(GenericSystemInfoScriptInterface::PowerState v)
{
    int index = static_cast<int>(v);
    if (ui->systemInfoPowerState->currentIndex() != index) {
        ui->systemInfoPowerState->setCurrentIndex(index);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setSimStatus(GenericSystemInfoScriptInterface::SimStatus v)
{
    int index = static_cast<int>(v);
    if (ui->systemInfoSimStatus->currentIndex() != index) {
        ui->systemInfoSimStatus->setCurrentIndex(index);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setInputMethod(GenericSystemInfoScriptInterface::InputMethodFlags v)
{
    if (v & GenericSystemInfoScriptInterface::Keys)
        ui->systemInfoInputKeys->setChecked(true);
    else
        ui->systemInfoInputKeys->setChecked(false);
    if (v & GenericSystemInfoScriptInterface::Keypad)
        ui->systemInfoInputKeypad->setChecked(true);
    else
        ui->systemInfoInputKeypad->setChecked(false);
    if (v & GenericSystemInfoScriptInterface::Keyboard)
        ui->systemInfoInputKeyboard->setChecked(true);
    else
        ui->systemInfoInputKeyboard->setChecked(false);
    if (v & GenericSystemInfoScriptInterface::SingleTouch)
        ui->systemInfoInputSingleTouch->setChecked(true);
    else
        ui->systemInfoInputSingleTouch->setChecked(false);
    if (v & GenericSystemInfoScriptInterface::MultiTouch)
        ui->systemInfoInputMultiTouch->setChecked(true);
    else
        ui->systemInfoInputMultiTouch->setChecked(false);
    if (v & GenericSystemInfoScriptInterface::Mouse)
        ui->systemInfoInputMouse->setChecked(true);
    else
        ui->systemInfoInputMouse->setChecked(false);
}

void GenericSystemInfoScriptInterface::setImei(const QString &v)
{
    if (ui->systemInfoImei->text() != v) {
        ui->systemInfoImei->setText(v);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setImsi(const QString &v)
{
    if (ui->systemInfoImsi->text() != v) {
        ui->systemInfoImsi->setText(v);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setManufacturer(const QString &v)
{
    if (ui->systemInfoManufacturer->text() != v) {
        ui->systemInfoManufacturer->setText(v);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setModel(const QString &v)
{
    if (ui->systemInfoModel->text() != v) {
        ui->systemInfoModel->setText(v);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setProductName(const QString &v)
{
    if (ui->systemInfoProductName->text() != v) {
        ui->systemInfoProductName->setText(v);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setBatteryLevel(int v)
{
    if (ui->systemInfoBatteryLevel->value() != v) {
        ui->systemInfoBatteryLevel->setValue(v);
        ui->emitGenericDataChanged();
    }
}

void GenericSystemInfoScriptInterface::setDeviceLocked(bool v)
{
    if (ui->systemInfoDeviceLocked->isChecked() != v) {
        ui->systemInfoDeviceLocked->setChecked(v);
        ui->emitGenericDataChanged();
    }
}
