/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef SYSINFOGENERICUI_H
#define SYSINFOGENERICUI_H

#include <remotecontrolwidget/toolbox.h>
#include <remotecontrolwidget/batterybutton.h>
#include <remotecontrolwidget/powerbutton.h>

#include <QtCore/QObject>
#include <QtCore/QHash>

class BatteryButton;
class PowerButton;
class QLineEdit;
class QComboBox;
class QLabel;
class QSpinBox;
class QCheckBox;

class GenericSystemInfoUi;

class GenericSystemInfoScriptInterface : public QObject
{
    Q_OBJECT
public:
    //must mimic QSystem::Version
    enum Version {
        Os = 1,
        QtCore,
        Firmware
    };
    Q_ENUMS(Version)

    //must mimic QSystem::Feature
    enum Feature {
        BluetoothFeature=0,
        CameraFeature,
        FmradioFeature,
        IrFeature,
        LedFeature,
        MemcardFeature,
        UsbFeature,
        VibFeature,
        WlanFeature,
        SimFeature,
        LocationFeature,
        VideoOutFeature,
        HapticsFeature
	};
    Q_ENUMS(Feature)

    //must mimic QSystemDeviceInfo::BatteryStatus
    enum BatteryStatus {
        NoBatteryLevel = 0,
        BatteryCritical,
        BatteryVeryLow,
        BatteryLow,
        BatteryNormal
    };
    Q_ENUMS(BatteryStatus)

    //must mimic QSystemDeviceInfo::PowerState
    enum PowerState {
        UnknownPower = 0,
        BatteryPower,
        WallPower,
        WallPowerChargingBattery
    };
    Q_ENUMS(PowerState)

    //must mimic QSystemDeviceInfo::InputMethod
    enum InputMethod {
        Keys = 0x0000001,
        Keypad = 0x0000002,
        Keyboard = 0x0000004,
        SingleTouch = 0x0000008,
        MultiTouch = 0x0000010,
        Mouse = 0x0000020
    };
    Q_DECLARE_FLAGS(InputMethodFlags, InputMethod)
    Q_ENUMS(InputMethod)
    Q_FLAGS(InputMethodFlags)

    //must mimic QSystemDeviceInfo::Profile
    enum Profile {
        UnknownProfile = 0,
        SilentProfile,
        NormalProfile,
        LoudProfile,
        VibProfile,
        OfflineProfile,
        PowersaveProfile,
        CustomProfile
    };
    Q_ENUMS(Profile)

    //must mimic QSystemDeviceInfo::SimStatus
    enum SimStatus {
        SimNotAvailable = 0,
        SingleSimAvailable,
        DualSimAvailable,
        SimLocked
	};
    Q_ENUMS(SimStatus)

    explicit GenericSystemInfoScriptInterface(GenericSystemInfoUi *ui);
    ~GenericSystemInfoScriptInterface();

    QString currentLanguage() const;
    QStringList availableLanguages() const;
    QString currentCountryCode() const;
    Q_INVOKABLE bool hasFeature(GenericSystemInfoScriptInterface::Feature f) const;
    Q_INVOKABLE QString version(GenericSystemInfoScriptInterface::Version v, const QString &param = "") const;
    int displayBrightness(int screen = 0) const;
    int colorDepth(int screen = 0) const;
    GenericSystemInfoScriptInterface::Profile currentProfile() const;
    GenericSystemInfoScriptInterface::PowerState currentPowerState() const;
    GenericSystemInfoScriptInterface::SimStatus simStatus() const;
    GenericSystemInfoScriptInterface::InputMethodFlags inputMethod() const;
    QString imei() const;
    QString imsi() const;
    QString manufacturer() const;
    QString model() const;
    QString productName() const;
    int batteryLevel() const;
    bool isDeviceLocked() const;
    Q_INVOKABLE GenericSystemInfoScriptInterface::BatteryStatus batteryStatus() const;

    Q_PROPERTY(QString currentLanguage READ currentLanguage WRITE setCurrentLanguage)
    Q_PROPERTY(QStringList availableLanguages READ availableLanguages WRITE setAvailableLanguages)
    Q_PROPERTY(QString currentCountryCode READ currentCountryCode WRITE setCurrentCountryCode)
    Q_PROPERTY(int displayBrightness READ displayBrightness WRITE setDisplayBrightness)
    Q_PROPERTY(int colorDepth READ colorDepth WRITE setColorDepth)
    Q_PROPERTY(Profile currentProfile READ currentProfile WRITE setCurrentProfile)
    Q_PROPERTY(PowerState currentPowerState READ currentPowerState WRITE setCurrentPowerState)
    Q_PROPERTY(SimStatus simStatus READ simStatus WRITE setSimStatus)
    Q_PROPERTY(InputMethodFlags inputMethod READ inputMethod WRITE setInputMethod)
    Q_PROPERTY(QString imei READ imei WRITE setImei)
    Q_PROPERTY(QString imsi READ imsi WRITE setImsi)
    Q_PROPERTY(QString manufacturer READ manufacturer WRITE setManufacturer)
    Q_PROPERTY(QString model READ model WRITE setModel)
    Q_PROPERTY(QString productName READ productName WRITE setProductName)
    Q_PROPERTY(int batteryLevel READ batteryLevel WRITE setBatteryLevel)
    Q_PROPERTY(bool deviceLocked READ isDeviceLocked WRITE setDeviceLocked)

public slots:
    void setCurrentLanguage(const QString &v);
    void setCurrentCountryCode(const QString &v);
    void setAvailableLanguages(const QStringList &v);
    void addAvailableLanguage(const QString &v);
    bool removeAvailableLanguage(const QString &v);
    void removeAllAvailableLanguages();
    void setFeature(GenericSystemInfoScriptInterface::Feature f, bool enabled);
    void setVersion(GenericSystemInfoScriptInterface::Version v, const QString &to);
    void setDisplayBrightness(int brightness);
    void setColorDepth(int depth);
    void setCurrentProfile(GenericSystemInfoScriptInterface::Profile v);
    void setCurrentPowerState(GenericSystemInfoScriptInterface::PowerState v);
    void setSimStatus(GenericSystemInfoScriptInterface::SimStatus v);
    void setInputMethod(GenericSystemInfoScriptInterface::InputMethodFlags v);
    void setImei(const QString &v);
    void setImsi(const QString &v);
    void setManufacturer(const QString &v);
    void setModel(const QString &v);
    void setProductName(const QString &v);
    void setBatteryLevel(int v);
    void setDeviceLocked(bool v);

private:
    GenericSystemInfoUi *ui;
};

class GenericSystemInfoUi : public ToolBoxPage
{
    Q_OBJECT
public:
    struct GenericData {
        GenericData();

        QString currentLanguage;
        QStringList availableLanguages;
        QString currentCountryCode;
        int displayBrightness;
        int colorDepth;
        GenericSystemInfoScriptInterface::Profile profile;
        GenericSystemInfoScriptInterface::PowerState currentPowerState;
        GenericSystemInfoScriptInterface::SimStatus simStatus;
        GenericSystemInfoScriptInterface::InputMethodFlags inputFlags;
        QString imei;
        QString imsi;
        QString manufacturer;
        QString model;
        QString productName;
        int batteryLevel;
        bool deviceLocked;

        QVector<bool> features;
        QVector<QString> versions;
    };
    explicit GenericSystemInfoUi(QWidget *parent = 0);
    virtual ~GenericSystemInfoUi();

    GenericSystemInfoScriptInterface *scriptInterface();

    GenericData genericData() const;
    QIcon icon() const;

public slots:
    void setGenericData(const GenericSystemInfoUi::GenericData &data);
    void setDisplayedGenericData(const GenericSystemInfoUi::GenericData &data);

signals:
    void genericDataChanged(const GenericSystemInfoUi::GenericData &data) const;

    // since the quick access button enums differ from the enums here,
    // we need these translator slots/signals
public slots:
    void setCurrentBatteryLevel(BatteryButton::BatteryLevel);
    void setCurrentPowerState(PowerButton::PowerState);
signals:
    void currentBatteryLevelChanged(BatteryButton::BatteryLevel) const;
    void currentPowerStateChanged(PowerButton::PowerState) const;
private slots:
    void emitButtonSignalsOnChange();

private slots:
    void emitGenericDataChanged();

private slots:
    void sysinfoAddAvailableLanguage();
    void sysinfoRemoveAvailableLanguage();
    void sysinfoToggleFeature();
    void sysinfoChangeVersion();
    void sysinfoDisplayVersion();
    void sysinfoDisplayFeature();
    void sysinfoSetAvailableLanguages(const QStringList &list);

    void sysinfoUpdateBatteryStatus(int level);

private:
    void initializeGeneric();

    void initializeGenericOptions();
    void initializeDeviceOptions();


private:
    GenericData data;
    QLineEdit *systemInfoLanguage;
    QLineEdit *systemInfoCountryCode;
    QComboBox *systemInfoAvailableLanguages;
    QComboBox *systemInfoFeatures;
    QLabel *systemInfoFeatureEnabled;
    QComboBox *systemInfoVersions;
    QLabel *systemInfoVersionString;
    QSpinBox *systemInfoBatteryLevel;
    QLabel *systemInfoBatteryStatus;
    QComboBox *systemInfoPowerState;
    QComboBox *systemInfoProfile;
    QComboBox *systemInfoSimStatus;
    QLineEdit *systemInfoImei;
    QLineEdit *systemInfoImsi;
    QCheckBox *systemInfoInputKeys;
    QCheckBox *systemInfoInputKeypad;
    QCheckBox *systemInfoInputKeyboard;
    QCheckBox *systemInfoInputSingleTouch;
    QCheckBox *systemInfoInputMultiTouch;
    QCheckBox *systemInfoInputMouse;
    QCheckBox *systemInfoDeviceLocked;
    QLineEdit *systemInfoManufacturer;
    QLineEdit *systemInfoModel;
    QLineEdit *systemInfoProductName;
    QSpinBox *systemInfoColorDepth;
    QSpinBox *systemInfoBrightness;

    QVector<bool> mFeatures;
    QVector<QString> mVersions;

    GenericSystemInfoScriptInterface *mScriptInterface;
    friend class GenericSystemInfoScriptInterface;
};

#endif // SYSINFOGENERICUI_H
