/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "systeminfonetworkui.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtCore/QMetaEnum>
#include <QtGui/QPushButton>
#include <QtGui/QBoxLayout>
#include <QtGui/QLineEdit>
#include <QtGui/QComboBox>
#include <QtGui/QCheckBox>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>
#include <QtGui/QFormLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QInputDialog>

void NetworkSystemInfoUi::initializeNetwork()
{
    QStringList tags;
    QList<OptionsItem *> optionsList;

    networkCellId = new QSpinBox();
    networkCellId->setRange(0, 999999999);
    connect(networkCellId, SIGNAL(editingFinished()), this, SLOT(emitNetworkDataChange()));
    OptionsItem *item = new OptionsItem(tr("Cell ID"), networkCellId);
    item->setTags(tags);
    optionsList << item;

    networkLocationAreaCode = new QSpinBox();
    networkLocationAreaCode->setRange(0, 999999999);
    connect(networkLocationAreaCode, SIGNAL(editingFinished()), this, SLOT(emitNetworkDataChange()));
    item = new OptionsItem(tr("Location area code"), networkLocationAreaCode);
    item->setTags(tags);
    optionsList << item;

    networkMobileCountryCode = new QLineEdit();
    networkMobileCountryCode->setInputMask("999");
    networkMobileCountryCode->setValidator(new QRegExpValidator(QRegExp("\\d{3}"), networkMobileCountryCode));
    item = new OptionsItem(tr("Mobile country code"), networkMobileCountryCode);
    connect(networkMobileCountryCode, SIGNAL(editingFinished()), this, SLOT(emitNetworkDataChange()));
    item->setTags(tags);
    optionsList << item;

    networkMobileNetworkCode = new QLineEdit();
    QRegExp rx("\\d*");
    networkMobileNetworkCode->setValidator(new QRegExpValidator(rx, 0));

    connect(networkMobileNetworkCode, SIGNAL(editingFinished()), this, SLOT(emitNetworkDataChange()));
    item = new OptionsItem(tr("Mobile network code"), networkMobileNetworkCode);
    item->setTags(tags);
    optionsList << item;

    networkHomeCountryCode = new QLineEdit();
    networkHomeCountryCode->setInputMask("+990");
    networkHomeCountryCode->setValidator(new QRegExpValidator(QRegExp("\\+\\d\\d[\\d\\s]?"), networkHomeCountryCode));
    connect(networkHomeCountryCode, SIGNAL(editingFinished()), this, SLOT(emitNetworkDataChange()));
    item = new OptionsItem(tr("Home country code"), networkHomeCountryCode);
    item->setTags(tags);
    optionsList << item;

    networkHomeNetworkCode = new QLineEdit();
    networkHomeNetworkCode->setValidator(new QRegExpValidator(rx, 0));
    connect(networkHomeNetworkCode, SIGNAL(editingFinished()), this, SLOT(emitNetworkDataChange()));
    item = new OptionsItem(tr("Home network code"), networkHomeNetworkCode);
    item->setTags(tags);
    optionsList << item;

    networkCurrentMode = new QComboBox();
    connect(networkCurrentMode, SIGNAL(activated(int)), this, SLOT(emitNetworkDataChange()));
    item = new OptionsItem(tr("Currently active mode"), networkCurrentMode);
    optionsList << item;

    networkModes = new QComboBox();
    networkName = new QLineEdit();
    networkStatus = new QComboBox();
    networkMacAddress = new QLineEdit();
    networkMacAddress->setInputMask("HH:HH:HH:HH:HH:HH");
    networkMacAddress->setValidator(new QRegExpValidator(QRegExp("(\\S\\S:){5}\\S\\S"), networkMacAddress));
    networkSignalStrength = new QSpinBox();
    networkSignalStrength->setRange(-1, 100);
    networkSignalStrength->setValue(-1);
    QFormLayout *fLayout = new QFormLayout();
    fLayout->addRow(tr("Name"), networkName);
    fLayout->addRow(tr("Status"), networkStatus);
    fLayout->addRow(tr("MAC address"), networkMacAddress);
    fLayout->addRow(tr("Signal strength"), networkSignalStrength);
    QGroupBox *networkModeProperties = new QGroupBox(tr("Properties"));
    networkModeProperties->setLayout(fLayout);
    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->addWidget(new QLabel(tr("Edit properties of")));
    hLayout->addWidget(networkModes);
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->addLayout(hLayout);
    vLayout->addWidget(networkModeProperties);
    networkModeProperties = new QGroupBox();
    networkModeProperties->setLayout(vLayout);
    QPushButton *toggleNetworkSettingsVisible = new QPushButton(tr("Show"));
    connect(networkModes, SIGNAL(activated(int)), this, SLOT(showNetworkMode()));
    connect(networkName, SIGNAL(editingFinished()), this, SLOT(editNetworkMode()));
    connect(networkStatus, SIGNAL(activated(int)), this, SLOT(editNetworkMode()));
    connect(networkMacAddress, SIGNAL(editingFinished()), this, SLOT(editNetworkMode()));
    connect(networkSignalStrength, SIGNAL(editingFinished()), this, SLOT(editNetworkMode()));
    item = new OptionsItem("Network mode properties", toggleNetworkSettingsVisible);
    QStringList networkModeTags = tags;
    networkModeTags << tr("name") << tr("status") << tr("mac") << tr("address") << tr("mode") << tr("signal") << tr("strength");
    item->setTags(networkModeTags);
    OptionsItem *advancedItem = new OptionsItem("", networkModeProperties, true);
    item->setAdvancedPage(advancedItem);
    optionsList << item;

    initializeNetworkOptions();

    setTitle(tr("Network"));
    setOptions(optionsList);
}

void NetworkSystemInfoUi::initializeNetworkOptions()
{
    networkStatus->clear();
    // depends on order
    networkStatus->addItem(tr("Undefined"));
    networkStatus->addItem(tr("No Network Available"));
    networkStatus->addItem(tr("Emergency Only"));
    networkStatus->addItem(tr("Searching"));
    networkStatus->addItem(tr("Busy"));
    networkStatus->addItem(tr("Connected"));
    networkStatus->addItem(tr("Home Network"));
    networkStatus->addItem(tr("Denied"));
    networkStatus->addItem(tr("Roaming"));

    networkCurrentMode->clear();
    // depends on order
    networkCurrentMode->addItem(tr("Unknown Mode"), NetworkSystemInfoScriptInterface::UnknownMode);
    networkCurrentMode->addItem(tr("GSM Mode"), NetworkSystemInfoScriptInterface::GsmMode);
    networkCurrentMode->addItem(tr("CDMA Mode"), NetworkSystemInfoScriptInterface::CdmaMode);
    networkCurrentMode->addItem(tr("W-CDMA Mode"), NetworkSystemInfoScriptInterface::WcdmaMode);
    networkCurrentMode->addItem(tr("WLAN Mode"), NetworkSystemInfoScriptInterface::WlanMode);
    networkCurrentMode->addItem(tr("Ethernet Mode"), NetworkSystemInfoScriptInterface::EthernetMode);
    networkCurrentMode->addItem(tr("Bluetooth Mode"), NetworkSystemInfoScriptInterface::BluetoothMode);
    networkCurrentMode->addItem(tr("WiMAX Mode"), NetworkSystemInfoScriptInterface::WimaxMode);

    networkModes->clear();
    // depends on order
    networkModes->addItem(tr("Unknown Mode"), NetworkSystemInfoScriptInterface::UnknownMode);
    networkModes->addItem(tr("GSM Mode"), NetworkSystemInfoScriptInterface::GsmMode);
    networkModes->addItem(tr("CDMA Mode"), NetworkSystemInfoScriptInterface::CdmaMode);
    networkModes->addItem(tr("W-CDMA Mode"), NetworkSystemInfoScriptInterface::WcdmaMode);
    networkModes->addItem(tr("WLAN Mode"), NetworkSystemInfoScriptInterface::WlanMode);
    networkModes->addItem(tr("Ethernet Mode"), NetworkSystemInfoScriptInterface::EthernetMode);
    networkModes->addItem(tr("Bluetooth Mode"), NetworkSystemInfoScriptInterface::BluetoothMode);
    networkModes->addItem(tr("WiMAX Mode"), NetworkSystemInfoScriptInterface::WimaxMode);

    networkModeInfo.resize(8);
    showNetworkMode();
}

NetworkSystemInfoUi::NetworkData::NetworkData()
    : networkInfo(8)
{
}


NetworkSystemInfoUi::NetworkSystemInfoUi(QWidget *parent)
    : ToolBoxPage(parent)
{
    qRegisterMetaType<NetworkSystemInfoUi::NetworkData>("NetworkSystemInfoUi::NetworkData");

    mScriptInterface = new NetworkSystemInfoScriptInterface(this);
    initializeNetwork();

    // update displayed values when underlying data changes
    connect(this, SIGNAL(networkDataChanged(NetworkSystemInfoUi::NetworkData)), SLOT(showNetworkMode()));

    // button translators
    connect(this, SIGNAL(networkDataChanged(NetworkSystemInfoUi::NetworkData)), SLOT(emitButtonSignalsOnChange()));
}

NetworkSystemInfoUi::~NetworkSystemInfoUi()
{
}

NetworkSystemInfoScriptInterface *NetworkSystemInfoUi::scriptInterface()
{
    return mScriptInterface;
}

void NetworkSystemInfoUi::showNetworkMode()
{
    int index = networkModes->itemData(networkModes->currentIndex()).toInt();
    const NetworkData::ModeInfo &info = networkModeInfo.at(index);
    networkName->setText(info.name);
    networkMacAddress->setText(info.macAddress);
    networkSignalStrength->setValue(info.signalStrength);
    networkStatus->setCurrentIndex(static_cast<int>(info.status));
}

void NetworkSystemInfoUi::editNetworkMode()
{
    int index = networkModes->currentIndex();
    networkModeInfo[index].name = networkName->text();
    networkModeInfo[index].macAddress = networkMacAddress->text();
    networkModeInfo[index].signalStrength = networkSignalStrength->value();
    networkModeInfo[index].status = static_cast<NetworkSystemInfoScriptInterface::NetworkStatus>(networkStatus->currentIndex());

    emitNetworkDataChange();
}

void NetworkSystemInfoUi::emitNetworkDataChange()
{
    emit networkDataChanged(networkData());
}

NetworkSystemInfoUi::NetworkData NetworkSystemInfoUi::networkData() const
{
    NetworkData d;
    d.cellId = mScriptInterface->cellId();
    d.locationAreaCode = mScriptInterface->locationAreaCode();
    d.currentMobileCountryCode = mScriptInterface->currentMobileCountryCode();
    d.currentMobileNetworkCode = mScriptInterface->currentMobileNetworkCode();
    d.homeMobileCountryCode = mScriptInterface->homeMobileCountryCode();
    d.homeMobileNetworkCode = mScriptInterface->homeMobileNetworkCode();
    d.currentMode = mScriptInterface->currentMode();
    d.networkInfo = networkModeInfo;
    return d;
}

void NetworkSystemInfoUi::setNetworkData(const NetworkSystemInfoUi::NetworkData &data)
{
    setDisplayedNetworkData(data);
    emit networkDataChanged(data);
}

void NetworkSystemInfoUi::setDisplayedNetworkData(const NetworkSystemInfoUi::NetworkData &data)
{
    networkCellId->setValue(data.cellId);
    networkLocationAreaCode->setValue(data.locationAreaCode);
    networkMobileCountryCode->setText(data.currentMobileCountryCode);
    networkMobileNetworkCode->setText(data.currentMobileNetworkCode);
    networkHomeCountryCode->setText(data.homeMobileCountryCode);
    networkHomeNetworkCode->setText(data.homeMobileNetworkCode);
    networkCurrentMode->setCurrentIndex(static_cast<int>(data.currentMode));
    networkModeInfo = data.networkInfo;
    showNetworkMode();
}

void NetworkSystemInfoUi::emitButtonSignalsOnChange()
{
    NetworkSystemInfoScriptInterface::NetworkMode currentMode = mScriptInterface->currentMode();
    emit currentNetworkModeChanged(static_cast<NetworkModeButton::NetworkMode>(currentMode));

    int strength = mScriptInterface->networkSignalStrength(currentMode);

    SignalStrengthButton::SignalStrength s;
    if (strength == 0) {
        s = SignalStrengthButton::NoSignal;
    } else if (strength <= 20) {
        s = SignalStrengthButton::SignalVeryLow;
    } else if (strength <= 40) {
        s = SignalStrengthButton::SignalLow;
    } else {
        s = SignalStrengthButton::SignalNormal;
    }
    emit currentNetworkSignalStrengthChanged(s);
}

void NetworkSystemInfoUi::setCurrentNetworkMode(NetworkModeButton::NetworkMode m)
{
    mScriptInterface->setCurrentMode(static_cast<NetworkSystemInfoScriptInterface::NetworkMode>(m));
}

void NetworkSystemInfoUi::setCurrentNetworkSignalStrength(SignalStrengthButton::SignalStrength s)
{
    NetworkSystemInfoScriptInterface::NetworkMode mode = mScriptInterface->currentMode();

    switch (s) {
    case SignalStrengthButton::NoSignal:
        mScriptInterface->setNetworkSignalStrength(mode, 0);
        break;
    case SignalStrengthButton::SignalVeryLow:
        mScriptInterface->setNetworkSignalStrength(mode, 20);
        break;
    case SignalStrengthButton::SignalLow:
        mScriptInterface->setNetworkSignalStrength(mode, 40);
        break;
    case SignalStrengthButton::SignalNormal:
        mScriptInterface->setNetworkSignalStrength(mode, 100);
        break;
    }
}

QIcon NetworkSystemInfoUi::icon() const
{
    return QIcon(":/ui/icons/network.png");
}


/*!
    \class NetworkSystemInfoScriptInterface
    \brief Exposed as sysinfo.network.
*/

NetworkSystemInfoScriptInterface::NetworkSystemInfoScriptInterface(NetworkSystemInfoUi *ui)
    : QObject(ui)
    , ui(ui)
{
    int enumIndex = metaObject()->indexOfEnumerator("NetworkStatus");
    QMetaEnum metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));

    enumIndex = metaObject()->indexOfEnumerator("NetworkMode");
    metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));
}

NetworkSystemInfoScriptInterface::~NetworkSystemInfoScriptInterface()
{
}

int NetworkSystemInfoScriptInterface::cellId() const
{
    return ui->networkCellId->value();
}

void NetworkSystemInfoScriptInterface::setCellId(int id)
{
    if (id != cellId()) {
        ui->networkCellId->setValue(id);
        ui->emitNetworkDataChange();
    }
}

int NetworkSystemInfoScriptInterface::locationAreaCode() const
{
    return ui->networkLocationAreaCode->value();
}

void NetworkSystemInfoScriptInterface::setLocationAreaCode(int code)
{
    if (code != locationAreaCode()) {
        ui->networkLocationAreaCode->setValue(code);
        ui->emitNetworkDataChange();
    }
}

QString NetworkSystemInfoScriptInterface::currentMobileCountryCode() const
{
    return ui->networkMobileCountryCode->text();
}

QString NetworkSystemInfoScriptInterface::currentMobileNetworkCode() const
{
    return ui->networkMobileNetworkCode->text();
}

QString NetworkSystemInfoScriptInterface::homeMobileCountryCode() const
{
    return ui->networkHomeCountryCode->text();
}

QString NetworkSystemInfoScriptInterface::homeMobileNetworkCode() const
{
    return ui->networkHomeNetworkCode->text();
}

void NetworkSystemInfoScriptInterface::setCurrentMobileCountryCode(const QString &code)
{
    if (code != currentMobileCountryCode()) {
        ui->networkMobileCountryCode->setText(code);
        ui->emitNetworkDataChange();
    }
}

void NetworkSystemInfoScriptInterface::setCurrentMobileNetworkCode(const QString &code)
{
    if (code != currentMobileNetworkCode()) {
        ui->networkMobileNetworkCode->setText(code);
        ui->emitNetworkDataChange();
    }
}

void NetworkSystemInfoScriptInterface::setHomeMobileCountryCode(const QString &code)
{
    if (code != homeMobileCountryCode()) {
        ui->networkHomeCountryCode->setText(code);
        ui->emitNetworkDataChange();
    }
}

void NetworkSystemInfoScriptInterface::setHomeMobileNetworkCode(const QString &code)
{
    if (code != homeMobileNetworkCode()) {
        ui->networkHomeNetworkCode->setText(code);
        ui->emitNetworkDataChange();
    }
}

NetworkSystemInfoScriptInterface::NetworkMode NetworkSystemInfoScriptInterface::currentMode() const
{
    return static_cast<NetworkMode>(ui->networkCurrentMode->currentIndex());
}

void NetworkSystemInfoScriptInterface::setCurrentMode(NetworkSystemInfoScriptInterface::NetworkMode m)
{
    if (m != currentMode()) {
        ui->networkCurrentMode->setCurrentIndex(static_cast<int>(m));
        ui->emitNetworkDataChange();
    }
}

QString NetworkSystemInfoScriptInterface::networkName(NetworkMode m) const
{
    return ui->networkModeInfo.at(static_cast<int>(m)).name;
}

void NetworkSystemInfoScriptInterface::setNetworkName(NetworkMode m, const QString &name)
{
    if (name != networkName(m)) {
        ui->networkModeInfo[static_cast<int>(m)].name = name;
        ui->emitNetworkDataChange();
    }
}

QString NetworkSystemInfoScriptInterface::macAddress(NetworkMode m) const
{
    return ui->networkModeInfo.at(static_cast<int>(m)).macAddress;
}

void NetworkSystemInfoScriptInterface::setNetworkMacAddress(NetworkMode m, const QString &mac)
{
    if (mac != macAddress(m)) {
        ui->networkModeInfo[static_cast<int>(m)].macAddress = mac;
        ui->emitNetworkDataChange();
    }
}

qint32 NetworkSystemInfoScriptInterface::networkSignalStrength(NetworkMode m) const
{
    return ui->networkModeInfo.at(static_cast<int>(m)).signalStrength;
}

void NetworkSystemInfoScriptInterface::setNetworkSignalStrength(NetworkMode m, qint32 strength)
{
    if (strength != networkSignalStrength(m)) {
        ui->networkModeInfo[static_cast<int>(m)].signalStrength = strength;
        ui->emitNetworkDataChange();
    }
}

NetworkSystemInfoScriptInterface::NetworkStatus NetworkSystemInfoScriptInterface::networkStatus(NetworkMode m) const
{
    return ui->networkModeInfo.at(static_cast<int>(m)).status;
}

void NetworkSystemInfoScriptInterface::setNetworkStatus(NetworkMode m, NetworkSystemInfoScriptInterface::NetworkStatus status)
{
    if (status != networkStatus(m)) {
        ui->networkModeInfo[static_cast<int>(m)].status = status;
        ui->emitNetworkDataChange();
    }
}
