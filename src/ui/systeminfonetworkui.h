/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef SYSINFONETWORKUI_H
#define SYSINFONETWORKUI_H

#include <remotecontrolwidget/toolbox.h>
#include <remotecontrolwidget/networkmodebutton.h>
#include <remotecontrolwidget/signalstrengthbutton.h>

#include <QtCore/QObject>

class NetworkModeButton;
class SignalStrengthButton;
class QLineEdit;
class QComboBox;
class QLabel;
class QSpinBox;
class QCheckBox;

class NetworkSystemInfoUi;

class NetworkSystemInfoScriptInterface : public QObject
{
    Q_OBJECT
public:
    // must mimic QSystemNetworkInfo::NetworkStatus
    enum NetworkStatus {
        UndefinedStatus = 0,
        NoNetworkAvailable,
        EmergencyOnly,
        Searching,
        Busy,
        Connected,
        HomeNetwork,
        Denied,
        Roaming
    };
    Q_ENUMS(NetworkStatus)

    // must mimic QSystemNetworkInfo::NetworkMode
    enum NetworkMode {
        UnknownMode=0,
        GsmMode,
        CdmaMode,
        WcdmaMode,
        WlanMode,
        EthernetMode,
        BluetoothMode,
        WimaxMode
    };
    Q_ENUMS(NetworkMode)

    explicit NetworkSystemInfoScriptInterface(NetworkSystemInfoUi *ui);
    virtual ~NetworkSystemInfoScriptInterface();

    int cellId() const;
    int locationAreaCode() const;
    QString currentMobileCountryCode() const;
    QString currentMobileNetworkCode() const;
    QString homeMobileCountryCode() const;
    QString homeMobileNetworkCode() const;
    NetworkMode currentMode() const;

    Q_PROPERTY(int cellId READ cellId WRITE setCellId)
    Q_PROPERTY(int locationAreaCode READ locationAreaCode WRITE setLocationAreaCode)
    Q_PROPERTY(QString currentMobileCountryCode READ currentMobileCountryCode WRITE setCurrentMobileCountryCode)
    Q_PROPERTY(QString currentMobileNetworkCode READ currentMobileNetworkCode WRITE setCurrentMobileNetworkCode)
    Q_PROPERTY(QString homeMobileCountryCode READ homeMobileCountryCode WRITE setHomeMobileCountryCode)
    Q_PROPERTY(QString homeMobileNetworkCode READ homeMobileNetworkCode WRITE setHomeMobileNetworkCode)
    Q_PROPERTY(NetworkMode currentMode READ currentMode WRITE setCurrentMode)

    Q_INVOKABLE QString networkName(NetworkMode m) const;
    Q_INVOKABLE QString macAddress(NetworkMode m) const;
    Q_INVOKABLE qint32 networkSignalStrength(NetworkMode m) const;
    Q_INVOKABLE NetworkStatus networkStatus(NetworkMode m) const;

public slots:
    void setCellId(int id);
    void setLocationAreaCode(int code);
    void setCurrentMobileCountryCode(const QString &code);
    void setCurrentMobileNetworkCode(const QString &code);
    void setHomeMobileCountryCode(const QString &code);
    void setHomeMobileNetworkCode(const QString &code);
    void setCurrentMode(NetworkSystemInfoScriptInterface::NetworkMode m);

    void setNetworkName(NetworkSystemInfoScriptInterface::NetworkMode m, const QString &name);
    void setNetworkMacAddress(NetworkSystemInfoScriptInterface::NetworkMode m, const QString &mac);
    void setNetworkSignalStrength(NetworkSystemInfoScriptInterface::NetworkMode m, qint32 strength);
    void setNetworkStatus(NetworkSystemInfoScriptInterface::NetworkMode m, NetworkSystemInfoScriptInterface::NetworkStatus status);

private:
    NetworkSystemInfoUi *ui;
};

class NetworkSystemInfoUi : public ToolBoxPage
{
    Q_OBJECT
public:
    struct NetworkData
    {
        NetworkData();

        struct ModeInfo
        {
            NetworkSystemInfoScriptInterface::NetworkStatus status;
            QString name;
            QString macAddress;
            qint32 signalStrength;
            //QNetworkInterface interface;
        };

        QVector<ModeInfo> networkInfo;

        int cellId;
        int locationAreaCode;

        QString currentMobileCountryCode;
        QString currentMobileNetworkCode;
        QString homeMobileCountryCode;
        QString homeMobileNetworkCode;
        NetworkSystemInfoScriptInterface::NetworkMode currentMode;
    };

    explicit NetworkSystemInfoUi(QWidget *parent = 0);
    virtual ~NetworkSystemInfoUi();

    NetworkSystemInfoScriptInterface *scriptInterface();

    NetworkData networkData() const;
    QIcon icon() const;

public slots:
    void setNetworkData(const NetworkSystemInfoUi::NetworkData &data);
    void setDisplayedNetworkData(const NetworkSystemInfoUi::NetworkData &data);

signals:
    void networkDataChanged(const NetworkSystemInfoUi::NetworkData &data) const;


// since the quick access button enums differ from the enums here,
// we need these translator slots/signals
public slots:
    void setCurrentNetworkMode(NetworkModeButton::NetworkMode);
    void setCurrentNetworkSignalStrength(SignalStrengthButton::SignalStrength);
signals:
    void currentNetworkModeChanged(NetworkModeButton::NetworkMode) const;
    void currentNetworkSignalStrengthChanged(SignalStrengthButton::SignalStrength) const;
private slots:
    void emitButtonSignalsOnChange();


private slots:
    void emitNetworkDataChange();
    void showNetworkMode();
    void editNetworkMode();

private:
    void initializeNetwork();
    void initializeNetworkOptions();

private:
    QSpinBox *networkCellId;
    QSpinBox *networkLocationAreaCode;
    QLineEdit* networkMobileCountryCode;
    QLineEdit* networkMobileNetworkCode;
    QLineEdit* networkHomeCountryCode;
    QLineEdit* networkHomeNetworkCode;
    QComboBox *networkCurrentMode;
    QComboBox *networkModes;
    QLineEdit *networkName;
    QComboBox *networkStatus;
    QLineEdit *networkMacAddress;
    QSpinBox * networkSignalStrength;

    QVector<NetworkData::ModeInfo> networkModeInfo;

    NetworkSystemInfoScriptInterface *mScriptInterface;
    friend class NetworkSystemInfoScriptInterface;
};

#endif // SYSINFONETWORKUI_H
