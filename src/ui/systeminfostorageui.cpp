/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "systeminfostorageui.h"
#include <remotecontrolwidget/optionsitem.h>

#include <QtCore/QMetaEnum>
#include <QtGui/QPushButton>
#include <QtGui/QBoxLayout>
#include <QtGui/QLineEdit>
#include <QtGui/QComboBox>
#include <QtGui/QCheckBox>
#include <QtGui/QLabel>
#include <QtGui/QSpinBox>
#include <QtGui/QFormLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QInputDialog>

void StorageSystemInfoUi::initializeStorage()
{
    QStringList tags;
    QList<OptionsItem *> optionsList;
    tags << tr("drives") << tr("memory") << tr("storage");

    systemInfoDrives = new QComboBox();
    systemInfoChangeDriveName = new QPushButton(tr("Change Name"));
    systemInfoRemoveDrive = new QPushButton(tr("Remove"));
    QPushButton *systemInfoAddDrive = new QPushButton(tr("Add"));
    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->addWidget(systemInfoChangeDriveName);
    hLayout->addWidget(systemInfoRemoveDrive);
    hLayout->addWidget(systemInfoAddDrive);
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->addWidget(systemInfoDrives);
    vLayout->addLayout(hLayout);
    systemInfoDriveType = new QComboBox();
    systemInfoDriveTotalSpace = new QLineEdit();
    QRegExp rx("\\d*");
    systemInfoDriveTotalSpace->setValidator(new QRegExpValidator(rx, this));
    systemInfoDriveAvailableSpace = new QLineEdit();
    systemInfoDriveAvailableSpace->setValidator(new QRegExpValidator(rx, this));
    QFormLayout *driveLayout = new QFormLayout();
    driveLayout->addRow(tr("Type"), systemInfoDriveType);
    driveLayout->addRow(tr("Total space"), systemInfoDriveTotalSpace);
    driveLayout->addRow(tr("Available space"), systemInfoDriveAvailableSpace);
    QGroupBox *drivePropertyGroupBox = new QGroupBox(tr("Storage Properties"));
    drivePropertyGroupBox->setLayout(driveLayout);
    vLayout->addWidget(drivePropertyGroupBox);
    QWidget *drives = new QWidget();
    drives->setLayout(vLayout);
    connect(systemInfoDrives, SIGNAL(activated(int)), SLOT(showDriveInfo()));
    connect(systemInfoAddDrive, SIGNAL(clicked()), SLOT(addDriveClicked()));
    connect(systemInfoRemoveDrive, SIGNAL(clicked()), SLOT(removeDriveClicked()));
    connect(systemInfoChangeDriveName, SIGNAL(clicked()), SLOT(renameDriveClicked()));
    connect(systemInfoDriveType, SIGNAL(activated(int)), SLOT(editDriveInfo()));
    connect(systemInfoDriveTotalSpace, SIGNAL(editingFinished()), SLOT(editDriveInfo()));
    connect(systemInfoDriveAvailableSpace, SIGNAL(editingFinished()), SLOT(editDriveInfo()));
    OptionsItem *item = new OptionsItem("", drives, true);
    QStringList drivesTags = tags;
    drivesTags << tr("total") << tr("available") << tr("space");
    item->setTags(drivesTags);
    optionsList << item;

    initializeStorageOptions();

    setTitle(tr("Storage"));
    setOptions(optionsList);
}

void StorageSystemInfoUi::initializeStorageOptions()
{
    systemInfoDriveType->clear();
    QStringList driveTypes;
    // depends on order of items
    driveTypes.append(tr("No Drive"));
    driveTypes.append(tr("Internal Drive"));
    driveTypes.append(tr("Removable Drive"));
    driveTypes.append(tr("Remote Drive"));
    driveTypes.append(tr("CD-ROM Drive"));
    systemInfoDriveType->addItems(driveTypes);
}

StorageSystemInfoUi::StorageSystemInfoUi(QWidget *parent)
    : ToolBoxPage(parent)
{
    qRegisterMetaType<StorageSystemInfoUi::StorageData>("StorageSystemInfoUi::StorageData");

    mScriptInterface = new StorageSystemInfoScriptInterface(this);

    initializeStorage();

    // when the data changes, update the display
    connect(this, SIGNAL(storageDataChanged(StorageSystemInfoUi::StorageData)),
            SLOT(showDriveInfo()));
}

StorageSystemInfoScriptInterface *StorageSystemInfoUi::scriptInterface()
{
    return mScriptInterface;
}

StorageSystemInfoUi::StorageData StorageSystemInfoUi::storageData() const
{
    return mData;
}

void StorageSystemInfoUi::setStorageData(const StorageSystemInfoUi::StorageData &data)
{
    setDisplayedStorageData(data);
    emit storageDataChanged(data);
}

void StorageSystemInfoUi::setDisplayedStorageData(const StorageSystemInfoUi::StorageData &data)
{
    mData = data;
    updateDrivesList();
}

void StorageSystemInfoUi::emitStorageDataChange()
{
    emit storageDataChanged(storageData());
}

void StorageSystemInfoUi::showDriveInfo()
{
    bool editingEnabled = systemInfoDrives->count() != 0;

    // workaround for calling this from another thread (scripts):
    // always perform the setEnabled calls in the gui thread -
    // setEnabled calls sendEvent!
    QMetaObject::invokeMethod(this, "enableDriveControls", Qt::QueuedConnection,
                              Q_ARG(bool, editingEnabled));

    if (!editingEnabled)
        return;

    const QString curDrive = systemInfoDrives->currentText();
    systemInfoDriveType->setCurrentIndex(static_cast<int>(mScriptInterface->typeForDrive(curDrive)));
    QLocale locale;
    systemInfoDriveAvailableSpace->setText(locale.toString(mScriptInterface->availableDiskSpace(curDrive)));
    systemInfoDriveTotalSpace->setText(locale.toString(mScriptInterface->totalDiskSpace(curDrive)));
}

void StorageSystemInfoUi::editDriveInfo()
{
    if (systemInfoDrives->count() == 0)
        return;

    QLocale locale;
    const QString curDrive = systemInfoDrives->currentText();
    StorageData::DriveInfo &driveInfo = mData.drives[curDrive];
    driveInfo.type = static_cast<StorageSystemInfoScriptInterface::DriveType>(systemInfoDriveType->currentIndex());
    driveInfo.totalSpace = locale.toLongLong(systemInfoDriveTotalSpace->text());
    driveInfo.availableSpace = locale.toLongLong(systemInfoDriveAvailableSpace->text());

    emitStorageDataChange();
}

void StorageSystemInfoUi::updateDrivesList(const QString &driveToShow)
{
    QString showDrive = driveToShow;
    if (showDrive.isNull())
        showDrive = systemInfoDrives->currentText();

    systemInfoDrives->clear();
    systemInfoDrives->addItems(mData.drives.keys());

    if (mData.drives.contains(showDrive))
        systemInfoDrives->setCurrentIndex(mData.drives.keys().indexOf(showDrive));

    showDriveInfo();
}

void StorageSystemInfoUi::renameDisplayedDrive(const QString &from, const QString &to)
{
    StorageData::DriveInfo prevDriveInfo = mData.drives.value(from);
    mData.drives.remove(from);
    mData.drives.insert(to, prevDriveInfo);
    updateDrivesList(to);
}

void StorageSystemInfoUi::renameDriveClicked()
{
    if (systemInfoDrives->count() == 0)
        return;

    const QString oldName = systemInfoDrives->currentText();
    const QString newName = QInputDialog::getText(0,
            "Change Drive Name", "Enter the new drive name:",
            QLineEdit::Normal, oldName);

    if (newName.isEmpty())
        return;

    renameDisplayedDrive(oldName, newName);
    emitStorageDataChange();
}

void StorageSystemInfoUi::addDisplayedDrive(const QString &name)
{
    StorageData::DriveInfo driveInfo;
    mData.drives.insert(name, driveInfo);
    updateDrivesList(name);
}

void StorageSystemInfoUi::addDriveClicked()
{
    const QString newName = QInputDialog::getText(0,
            "Add New Drive", "Enter the new drive name:");
    if (newName.isEmpty())
        return;

    addDisplayedDrive(newName);
    emitStorageDataChange();
}

void StorageSystemInfoUi::removeDisplayedDrive(const QString &name)
{
    mData.drives.remove(name);
    updateDrivesList();
}

void StorageSystemInfoUi::removeDriveClicked()
{
    if (systemInfoDrives->count() == 0)
        return;

    removeDisplayedDrive(systemInfoDrives->currentText());
    emitStorageDataChange();
}


StorageSystemInfoUi::StorageData::DriveInfo::DriveInfo()
    : type(StorageSystemInfoScriptInterface::NoDrive)
    , totalSpace(-1)
    , availableSpace(-1)
{
}


/*!
    \class StorageSystemInfoScriptInterface
    \brief Exposed as sysinfo.storage.
*/

StorageSystemInfoScriptInterface::StorageSystemInfoScriptInterface(StorageSystemInfoUi *ui)
    : QObject(ui)
    , ui(ui)
{
    int enumIndex = metaObject()->indexOfEnumerator("DriveType");
    QMetaEnum metaEnum = metaObject()->enumerator(enumIndex);
    for (int i = 0; i < metaEnum.keyCount(); ++i)
        setProperty(metaEnum.key(i), metaEnum.value(i));
}

StorageSystemInfoScriptInterface::~StorageSystemInfoScriptInterface()
{
}

QStringList StorageSystemInfoScriptInterface::logicalDrives() const
{
    return ui->mData.drives.keys();
}

StorageSystemInfoScriptInterface::DriveType StorageSystemInfoScriptInterface::typeForDrive(const QString &name) const
{
    if (!ui->mData.drives.contains(name))
        return NoDrive;
    return ui->mData.drives.value(name).type;
}

qint64 StorageSystemInfoScriptInterface::totalDiskSpace(const QString &name) const
{
    if (!ui->mData.drives.contains(name))
        return -1;
    return ui->mData.drives.value(name).totalSpace;
}

qint64 StorageSystemInfoScriptInterface::availableDiskSpace(const QString &name) const
{
    if (!ui->mData.drives.contains(name))
        return -1;
    return ui->mData.drives.value(name).availableSpace;
}

bool StorageSystemInfoScriptInterface::addDrive(const QString &name)
{
    if (ui->mData.drives.contains(name))
        return false;

    ui->addDisplayedDrive(name);
    ui->emitStorageDataChange();
    return true;
}

bool StorageSystemInfoScriptInterface::addDrive(const QString &name,
                                                StorageSystemInfoScriptInterface::DriveType type,
                                                qint64 totalSpace, qint64 availableSpace)
{
    if (ui->mData.drives.contains(name))
        return false;

    ui->addDisplayedDrive(name);
    StorageSystemInfoUi::StorageData::DriveInfo &di = ui->mData.drives[name];
    di.type = type;
    di.availableSpace = availableSpace;
    di.totalSpace = totalSpace;
    ui->emitStorageDataChange();
    return true;
}

bool StorageSystemInfoScriptInterface::removeDrive(const QString &name)
{
    if (!ui->mData.drives.contains(name))
        return false;

    ui->removeDisplayedDrive(name);
    ui->emitStorageDataChange();
    return true;
}

bool StorageSystemInfoScriptInterface::setName(const QString &oldname, const QString &newname)
{
    if (!ui->mData.drives.contains(oldname))
        return false;

    ui->renameDisplayedDrive(oldname, newname);
    ui->emitStorageDataChange();
    return true;
}

bool StorageSystemInfoScriptInterface::setType(const QString &name, StorageSystemInfoScriptInterface::DriveType type)
{
    if (!ui->mData.drives.contains(name))
        return false;

    if (ui->mData.drives[name].type != type)
    {
        ui->mData.drives[name].type = type;
        ui->emitStorageDataChange();
    }
    return true;
}

bool StorageSystemInfoScriptInterface::setTotalSpace(const QString &name, qint64 space)
{
    if (!ui->mData.drives.contains(name))
        return false;

    if (ui->mData.drives[name].totalSpace != space)
    {
        ui->mData.drives[name].totalSpace = space;
        ui->emitStorageDataChange();
    }
    return true;
}

bool StorageSystemInfoScriptInterface::setAvailableSpace(const QString &name, qint64 space)
{
    if (!ui->mData.drives.contains(name))
        return false;

    if (ui->mData.drives[name].availableSpace != space)
    {
        ui->mData.drives[name].availableSpace = space;
        ui->emitStorageDataChange();
    }
    return true;
}

void StorageSystemInfoUi::enableDriveControls(bool enabled)
{
    systemInfoDriveType->setEnabled(enabled);
    systemInfoDriveAvailableSpace->setEnabled(enabled);
    systemInfoDriveTotalSpace->setEnabled(enabled);
    systemInfoRemoveDrive->setEnabled(enabled);
    systemInfoChangeDriveName->setEnabled(enabled);
}

QIcon StorageSystemInfoUi::icon() const
{
    return QIcon(":/ui/icons/storage.png");
}
