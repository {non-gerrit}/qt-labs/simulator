/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef SYSINFOSTORAGEUI_H
#define SYSINFOSTORAGEUI_H

#include <remotecontrolwidget/toolbox.h>

#include <QtCore/QObject>
#include <QtCore/QHash>

class QLineEdit;
class QComboBox;
class QPushButton;

class StorageSystemInfoUi;

class StorageSystemInfoScriptInterface : public QObject
{
    Q_OBJECT
public:
    // must mimic QSystemStorageInfo::DriveType
    enum DriveType {
        NoDrive = 0,
        InternalDrive,
        RemovableDrive,
        RemoteDrive,
        CdromDrive
    };
    Q_ENUMS(DriveType)

    explicit StorageSystemInfoScriptInterface(StorageSystemInfoUi *ui);
    virtual ~StorageSystemInfoScriptInterface();

    Q_INVOKABLE QStringList logicalDrives() const;
    Q_INVOKABLE DriveType typeForDrive(const QString &name) const;
    Q_INVOKABLE qint64 totalDiskSpace(const QString &name) const;
    Q_INVOKABLE qint64 availableDiskSpace(const QString &name) const;

public slots:
    bool addDrive(const QString &name);
    bool addDrive(const QString &name, StorageSystemInfoScriptInterface::DriveType type,
                  qint64 totalSpace, qint64 availableSpace);
    bool removeDrive(const QString &name);
    bool setName(const QString &oldname, const QString &newname);
    bool setType(const QString &name, StorageSystemInfoScriptInterface::DriveType type);
    bool setTotalSpace(const QString &name, qint64 space);
    bool setAvailableSpace(const QString &name, qint64 space);

private:
    StorageSystemInfoUi *ui;
};

class StorageSystemInfoUi : public ToolBoxPage
{
    Q_OBJECT
public:
    struct StorageData {
        struct DriveInfo
        {
            DriveInfo();

            StorageSystemInfoScriptInterface::DriveType type;
            qint64 totalSpace;
            qint64 availableSpace;
        };

        QHash<QString, DriveInfo> drives;
    };

    explicit StorageSystemInfoUi(QWidget *parent = 0);

    StorageSystemInfoScriptInterface *scriptInterface();

    QIcon icon() const;
    StorageData storageData() const;

public slots:
    void setStorageData(const StorageSystemInfoUi::StorageData &data);
    void setDisplayedStorageData(const StorageSystemInfoUi::StorageData &data);

signals:
    void storageDataChanged(const StorageSystemInfoUi::StorageData &data) const;

private slots:
    void emitStorageDataChange();
    void showDriveInfo();
    void editDriveInfo();
    void updateDrivesList(const QString &driveToShow = QString());

    void addDisplayedDrive(const QString &name);
    void removeDisplayedDrive(const QString &name);
    void renameDisplayedDrive(const QString &from, const QString &to);

    void renameDriveClicked();
    void addDriveClicked();
    void removeDriveClicked();

private:
    void initializeStorage();
    void initializeStorageOptions();
    Q_INVOKABLE void enableDriveControls(bool enabled);

private:
    QComboBox *systemInfoDrives;
    QComboBox *systemInfoDriveType;
    QLineEdit *systemInfoDriveTotalSpace;
    QLineEdit *systemInfoDriveAvailableSpace;
    QPushButton *systemInfoRemoveDrive;
    QPushButton *systemInfoChangeDriveName;

    StorageData mData;

    StorageSystemInfoScriptInterface *mScriptInterface;
    friend class StorageSystemInfoScriptInterface;
};

#endif // SYSINFOSTORAGEUI_H
