INCLUDEPATH += src/ui
DEPENDPATH += src/ui
HEADERS += applicationtablewidget.h \
    mainwindow.h \
    configurationwidget.h \
    contactsui.h \
    systeminfogenericui.h \
    systeminfonetworkui.h \
    systeminfostorageui.h \
    messagingui.h \
    sensorsui.h \
    viewconfiguration.h \
    accelerometercontrol.h \
    organizerui.h \
    feedbackui.h \
    docgalleryui.h \
    cameraui.h \
    nfcui.h \
    inputscriptinterface.h \
    applicationui.h \
    viewui.h \
    multipointtouchui.h

SOURCES += applicationtablewidget.cpp \
    mainwindow.cpp \
    configurationwidget.cpp \
    contactsui.cpp \
    systeminfogenericui.cpp \
    systeminfonetworkui.cpp \
    systeminfostorageui.cpp \
    messagingui.cpp \
    sensorsui.cpp \
    viewconfiguration.cpp \
    accelerometercontrol.cpp \
    organizerui.cpp \
    feedbackui.cpp \
    docgalleryui.cpp \
    cameraui.cpp \
    nfcui.cpp \
    inputscriptinterface.cpp \
    applicationui.cpp \
    viewui.cpp \
    multipointtouchui.cpp

FORMS += inspector.ui \
    viewconfiguration.ui

RESOURCES += \
    src/ui/ui.qrc
