/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#include "viewconfiguration.h"

#include <QtGui/QGraphicsLineItem>
#include <QtGui/QDesktopWidget>
#include <QtGui/QPainter>
#include <qmath.h>

ViewConfiguration::ViewConfiguration(qreal correctionFactor, QWidget *parent)
    : QDialog(parent)
    , mCurrentCorrectionFactor(correctionFactor)
    , mCurrentUnit(Centrimetre)
{
    ui.setupUi(this);

    mRulerWidget = new RulerWidget(correctionFactor, this);
    ui.scrollArea->setWidget(mRulerWidget);

    qreal widthInInch = qApp->desktop()->width() / qApp->desktop()->logicalDpiX();
    qreal heightInInch = qApp->desktop()->height() / qApp->desktop()->logicalDpiY();
    mDiagonalInInch = sqrt(pow(widthInInch, 2) + pow(heightInInch, 2));
    ui.diagonalInInch->setText(QLocale().toString(mDiagonalInInch));

    connect(ui.horizontalSlider, SIGNAL(valueChanged(int)), SLOT(updateCorrectionFromSlider()));
    connect(ui.cmRadioButton, SIGNAL(toggled(bool)), SLOT(updateUnit()));
    connect(ui.inchRadioButton, SIGNAL(toggled(bool)), SLOT(updateUnit()));
    connect(ui.exitButton, SIGNAL(clicked()), SLOT(close()));
    connect(ui.diagonalInInch, SIGNAL(editingFinished()), SLOT(updateCorrectionFromMonitorValues()));
    connect(ui.correctionFactorEdit, SIGNAL(editingFinished()), SLOT(updateCorrectionFromLineEdit()));
    connect(this, SIGNAL(correctionFactorChanged(qreal)), SLOT(updateValues(qreal)));
    updateValues(mCurrentCorrectionFactor);
}

ViewConfiguration::~ViewConfiguration()
{

}

void ViewConfiguration::updateValues(qreal factor)
{
    ui.horizontalSlider->blockSignals(true);
    ui.horizontalSlider->setValue(factor * 100);
    ui.horizontalSlider->blockSignals(false);

    updateLine();
    QLocale locale;
    ui.correctionFactorEdit->setText(locale.toString(factor));
    ui.diagonalInInch->setText(locale.toString(mDiagonalInInch / factor));
}
void ViewConfiguration::updateLine()
{
    qreal lineWidth;
    if (mCurrentUnit == Centrimetre)
        lineWidth = qApp->desktop()->logicalDpiX() / 2.54 * 10 * mCurrentCorrectionFactor;
    else
        lineWidth = qApp->desktop()->logicalDpiX() * 4 * mCurrentCorrectionFactor;
    ui.scrollArea->setFixedWidth(lineWidth);
    mRulerWidget->updateCorrectionFactor(ui.horizontalSlider->value() / 100.);
}

void ViewConfiguration::updateUnit()
{
    if (ui.cmRadioButton->isChecked())
        mCurrentUnit = Centrimetre;
    else
        mCurrentUnit = Inch;
    mRulerWidget->updateCurrentUnit(mCurrentUnit);
    updateLine();
}

void ViewConfiguration::updateCorrectionFromSlider()
{
    if (mCurrentCorrectionFactor != ui.horizontalSlider->value() / 100.) {
        mCurrentCorrectionFactor = ui.horizontalSlider->value() / 100.;
        emit correctionFactorChanged(mCurrentCorrectionFactor);
    }
}

void ViewConfiguration::updateCorrectionFromMonitorValues()
{
    mCurrentCorrectionFactor = mDiagonalInInch / QLocale().toDouble(ui.diagonalInInch->text());
    if (mCurrentCorrectionFactor > 2)
        mCurrentCorrectionFactor = 2;
    else if (mCurrentCorrectionFactor < 0.5)
        mCurrentCorrectionFactor = 0.5;
    emit correctionFactorChanged(mCurrentCorrectionFactor);
}

void ViewConfiguration::updateCorrectionFromLineEdit()
{
    qreal newValue = QLocale().toDouble(ui.correctionFactorEdit->text());
    if (newValue < 0.5)
        newValue = 0.5;
    else if (newValue > 2)
        newValue = 2;
    if (mCurrentCorrectionFactor != newValue) {
        mCurrentCorrectionFactor = newValue;
        emit correctionFactorChanged(mCurrentCorrectionFactor);
    }
}

RulerWidget::RulerWidget(qreal correctionFactor, QWidget *parent)
    : QWidget(parent)
    , mCorrectionFactor(correctionFactor)
    , mCurrentUnit(Centrimetre)
{
    setFixedHeight(5);
}

void RulerWidget::updateCorrectionFactor(qreal newFactor)
{
    mCorrectionFactor = newFactor;
    repaint();
}

void RulerWidget::updateCurrentUnit(Unit newUnit)
{
    mCurrentUnit = newUnit;
    repaint();
}

void RulerWidget::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e);
    QPalette p = QApplication::palette();
    QPainter painter(this);
    painter.setPen(Qt::NoPen);
    qreal oneStep = qApp->desktop()->logicalDpiX() * mCorrectionFactor;;
    if (mCurrentUnit == Centrimetre)
        oneStep /= 2.54;
    qreal drawnWidth = 0;
    bool highlight = false;
    painter.setBrush(p.color(QPalette::Shadow));
    QRectF currentRect = rect();
    currentRect.setWidth(oneStep);
    while ((drawnWidth + oneStep)<= rect().width()) {
        painter.drawRect(currentRect);
        currentRect.translate(oneStep, 0);
        drawnWidth += oneStep;
        if (!highlight)
            painter.setBrush(p.color(QPalette::Mid));
        else
            painter.setBrush(p.color(QPalette::Shadow));
        highlight ^= 1;
    }
    if (drawnWidth < rect().width()) {
        currentRect.setWidth(rect().width() - drawnWidth);
        painter.drawRect(currentRect);
    }
}
