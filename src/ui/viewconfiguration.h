/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef VIEWCONFIGURATION_H
#define VIEWCONFIGURATION_H

#include <QDialog>
#include "ui_viewconfiguration.h"

class QGraphicsLineItem;

enum Unit {
    Centrimetre,
    Inch
};

class RulerWidget: public QWidget
{
    Q_OBJECT
public:

    explicit RulerWidget(qreal correctionFactor = 1, QWidget *parent = 0);
    void updateCorrectionFactor(qreal newFactor);
    void updateCurrentUnit(Unit newUnit);

protected:
    virtual void paintEvent(QPaintEvent *e);

private:
    qreal mCorrectionFactor;
    Unit mCurrentUnit;
};

class ViewConfiguration : public QDialog
{
    Q_OBJECT

public:
    explicit ViewConfiguration(qreal correctionFactor = 1, QWidget *parent = 0);
    ~ViewConfiguration();

signals:
    void correctionFactorChanged(qreal factor);

private slots:
    void updateValues(qreal factor);
    void updateLine();
    void updateUnit();

    void updateCorrectionFromSlider();
    void updateCorrectionFromMonitorValues();
    void updateCorrectionFromLineEdit();

private:
    Ui::ViewConfigurationClass ui;

    qreal mCurrentCorrectionFactor;
    Unit mCurrentUnit;
    qreal mDiagonalInInch;
    RulerWidget *mRulerWidget;
};

#endif // VIEWCONFIGURATION_H
