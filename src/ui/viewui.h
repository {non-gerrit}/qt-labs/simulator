/**************************************************************************
**
** This file is part of Qt Simulator
**
** Copyright (c) 2011 Nokia Corporation and/or its subsidiary(-ies).
**
** Contact: Nokia Corporation (info@qt.nokia.com)
**
**
** GNU Lesser General Public License Usage
**
** This file may be used under the terms of the GNU Lesser General Public
** License version 2.1 as published by the Free Software Foundation and
** appearing in the file LICENSE.LGPL included in the packaging of this file.
** Please review the following information to ensure the GNU Lesser General
** Public License version 2.1 requirements will be met:
** http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights. These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** Other Usage
**
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and Nokia.
**
** If you have questions regarding the use of this file, please contact
** Nokia at info@qt.nokia.com.
**
**************************************************************************/

#ifndef VIEWUI_H
#define VIEWUI_H

#include "deviceitem.h"
#include <remotecontrolwidget/toolbox.h>

class Ui_Inspector;
class QButtonGroup;
class ViewConfiguration;
class ViewScriptInterface;

class ViewUi : public ToolBoxPage
{
    Q_OBJECT
public:
    explicit ViewUi(const DeviceItem *deviceItem, QWidget *parent = 0);

    QString currentDeviceName() const;
    int currentDeviceIndex() const;
    void initializeSelection();
    bool initializeDeviceList();
    QObject * scriptInterface() const;
    void writeSettings(QSettings &) const;
    void readSettings(QSettings &);
    QIcon icon() const;

signals:
    void deviceSelectionChanged(const DeviceData &data);
    void orientationChangeRequested(Orientation newOrientation, bool rotateScreen);
    void scaleFactorChanged(qreal newScaleFactor);
    void rotateRequested();

public slots:
    void changeDeviceSelection(int newIndex);
    void updateOrientationButtonsState(Orientation orientation);
    void updateOrientationsButtonsIcons(bool standardOrientationPortrait);
    void setOrientationLocked(bool locked);

private slots:
    void updateOrientationButtons(const DeviceData &data);
    void rotateScreenToggled();
    void changeScaleFactor(int sliderPosition);
    void changeCorrectionFactor(qreal);
    void changeOrientation(int orientation);
    void showViewConfiguration();

private:
    qreal mCorrectionFactor;
    ViewConfiguration *mViewConfiguration;
    QSize mLogicalDpi;
    Ui_Inspector *ui_inspector;
    QButtonGroup *mOrientationButtons;
    ViewScriptInterface *mScriptInterface;
    QList<DeviceData> deviceList;
    const DeviceItem *mDeviceItem;
    bool mOrientationLocked;

    friend class ViewScriptInterface;
};

class ViewScriptInterface : public QObject
{
    Q_OBJECT
public:
    ViewScriptInterface(ViewUi *ui);
    virtual ~ViewScriptInterface();

    // must mimic ::Orientation
    enum Orientation {
        topUp       = 0x0000001,
        topDown     = 0x0000002,
        leftUp      = 0x0000004,
        rightUp     = 0x0000008,
    };
    Q_ENUMS(Orientation);

    Q_PROPERTY(int zoom READ zoom WRITE setZoom)

    int zoom() const;
    void setZoom(int z);

    Q_INVOKABLE int currentDeviceIndex() const;
    Q_INVOKABLE QString currentDeviceName() const;
    Q_INVOKABLE int deviceCount() const;
    Q_INVOKABLE void setDevice(const QString &dev);
    Q_INVOKABLE void setDevice(int index);

    Q_INVOKABLE QString deviceOrientation() const;
    Q_INVOKABLE QString screenOrientation() const;
    Q_INVOKABLE QStringList supportedScreenOrientations() const;
    Q_INVOKABLE bool setDeviceOrientation(QString orientation, bool rotateScreen);
    Q_INVOKABLE int availableGeometryX() const;
    Q_INVOKABLE int availableGeometryY() const;
    Q_INVOKABLE int availableGeometryWidth() const;
    Q_INVOKABLE int availableGeometryHeight() const;

private:
    ViewUi *ui;
};

#endif // VIEWUI_H
